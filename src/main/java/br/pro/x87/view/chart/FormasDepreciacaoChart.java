/**
 * 
 */
package br.pro.x87.view.chart;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Paint;
import java.awt.Toolkit;

import javax.swing.JFrame;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.TextAnchor;

import br.pro.x87.controller.DepreciacaoModel;
import br.pro.x87.depreciacao.DeclinioEmDobro;
import br.pro.x87.depreciacao.Depreciacao;
import br.pro.x87.depreciacao.DigitosPeriodicos;
import br.pro.x87.depreciacao.PorcentagensConstantes;
import br.pro.x87.depreciacao.Proporcional;
import br.pro.x87.depreciacao.QuotasConstantes;
import br.pro.x87.model.Maquina;

/**
 * @author jrlamb
 *
 */
public class FormasDepreciacaoChart extends JFrame {

	private Maquina m;
	private Depreciacao depre;	

	public Depreciacao getDepre() {
		return depre;
	}

	public void setDepre(Depreciacao depre) {
		this.depre = depre;
	}

	public Maquina getM() {
		return m;
	}

	public void setM(Maquina m) {
		this.m = m;
	}

	/**
	 * Create the frame.
	 */
	public FormasDepreciacaoChart(Maquina m) {
		super("Formas de depreciação");
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 800, 400);
		
		getContentPane().setLayout(new BorderLayout(0, 0));
		setM(m);
				
        final CategoryDataset dataset = createDataset();
        final JFreeChart chart = createChart(dataset);
        final ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
        setContentPane(chartPanel);
	}

	/* (non-Javadoc)
	 * @see br.pro.x87.view.maquina.ChartAncestor#createDataset()
	 */
	
	public CategoryDataset createDataset() {
		Depreciacao d;

		float declinioEmDobro, 
				digitosPeriodicos, 
				porcentagensConstantes, 
				proporcional, 
				quotasConstantes;
		
		setDepre(new DeclinioEmDobro(getM()));		
		DepreciacaoModel model = new DepreciacaoModel(getDepre());
		declinioEmDobro = getDepre().depreciacaoHoraria();

		setDepre(new DigitosPeriodicos(getM()));
		model = new DepreciacaoModel(getDepre());
		digitosPeriodicos = getDepre().depreciacaoHoraria();
		
		setDepre(new PorcentagensConstantes(getM()));
		model = new DepreciacaoModel(getDepre());
		porcentagensConstantes = getDepre().depreciacaoHoraria();
		
		setDepre(new Proporcional(getM()));
		model = new DepreciacaoModel(getDepre());
		proporcional = getDepre().depreciacaoHoraria();
		
		setDepre(new QuotasConstantes(getM()));
		model = new DepreciacaoModel(getDepre());
		quotasConstantes = getDepre().depreciacaoHoraria();
		
		/*DecimalFormat df = new DecimalFormat("#0.00");	
		df.setRoundingMode(RoundingMode.UP);			

		tfDepreciacaoAno.setText(df.format(depreciacaoAno));		
		tfDepreciacaoHora.setText(df.format(depreciacaoHora));*/
		
		final DefaultCategoryDataset dataset = new DefaultCategoryDataset();

	    dataset.addValue(declinioEmDobro, "Declínio em dobro", "Declínio em dobro");
	    dataset.addValue(digitosPeriodicos, "Soma dos dígitos periódicos", "Dígitos periódicos");
	    dataset.addValue(porcentagensConstantes, "Porcentagens constantes", "Porcentagens constantes");
	    dataset.addValue(proporcional, "Proporcional às horas trabalhadas", "Proporcional às horas");
	    dataset.addValue(quotasConstantes, "Quotas constantes", "Quotas constantes");
	    
		return dataset;		
	}
	
	@SuppressWarnings("deprecation")
	private JFreeChart createChart(final CategoryDataset dataset) {

        final JFreeChart chart = ChartFactory.createBarChart(
            "Formas de depreciação (R$/h)",       // chart title
            "Método",               // domain axis label
            "Valor depreciado (R$/h)",                  // range axis label
            dataset,                  // data
            PlotOrientation.VERTICAL, // the plot orientation
            true,                    // include legend
            true,
            false
        );

        chart.setBackgroundPaint(Color.lightGray);

        // get a reference to the plot for further customisation...
        final CategoryPlot plot = chart.getCategoryPlot();
        plot.setNoDataMessage("NO DATA!");

        final CategoryItemRenderer renderer = new CustomRenderer(
            new Paint[] {Color.red, Color.blue, Color.green,
                Color.yellow, Color.orange, Color.cyan,
                Color.magenta, Color.blue}
        );
//        renderer.setLabelGenerator(new StandardCategoryLabelGenerator());
        renderer.setItemLabelsVisible(true);
        final ItemLabelPosition p = new ItemLabelPosition(
            ItemLabelAnchor.CENTER, TextAnchor.CENTER, TextAnchor.CENTER, 45.0
        );
        renderer.setPositiveItemLabelPosition(p);
        plot.setRenderer(renderer);

        // change the margin at the top of the range axis...
        final ValueAxis rangeAxis = plot.getRangeAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        rangeAxis.setLowerMargin(0.15);
        rangeAxis.setUpperMargin(0.15);

        return chart;
    }
	
	class CustomRenderer extends BarRenderer {
		
		private static final long serialVersionUID = 1L;
		
		private Paint[] colors;

        public CustomRenderer(final Paint[] colors) {
            this.colors = colors;
        }

        public Paint getItemPaint(final int row, final int column) {
            return this.colors[column % this.colors.length];
        }
    }

	
	public void centraliza(){
		Dimension T = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension J = getSize();
		if (J.height > T.height) setSize(J.width,T.height);
		if (J.width > T.width) setSize(T.width,J.height);
		setLocation((T.width - J.width)/2,(T.height - J.height)/2);		
	}

}
