/**
 * 
 */
package br.pro.x87.view.chart;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;

import br.pro.x87.controller.CustosFixosModel;
import br.pro.x87.controller.CustosVariaveisModel;
import br.pro.x87.despesas.CampoDeCusto;

/**
 * @author jrlamb
 *
 */
public class ComponentesCustoHorarioChart extends JFrame {

	private static final long serialVersionUID = 1L;
	
	private JPanel contentPane;
	private CustosFixosModel cf;
	private CustosVariaveisModel cv;
	
	private static ArrayList<CampoDeCusto> listCv;
	private static ArrayList<CampoDeCusto> listCf;	
	
	public ComponentesCustoHorarioChart(CustosFixosModel cf, CustosVariaveisModel cv) {
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 600, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		this.cv = cv;
		listCv = cv.getList();
		
		this.cf = cf;
		listCf = cf.getList();
		
		JPanel p = createDemoPanel();
		getContentPane().add(p);
	}

	private static PieDataset createDataset() {
		DefaultPieDataset dataset = new DefaultPieDataset();

		for (CampoDeCusto c : listCf) {
			dataset.setValue(c.getItem(), c.getValor());
		}
		
		for (CampoDeCusto c : listCv) {
			dataset.setValue(c.getItem(), c.getValor());
		}
		
		return dataset;        
	}
	
	 /**
     * Creates a chart.
     * 
     * @param dataset  the dataset.
     * 
     * @return A chart.
     */
    private static JFreeChart createChart(PieDataset dataset) {
        
        JFreeChart chart = ChartFactory.createPieChart(
            "Componentes do custo horário (R$/h)",  // chart title
            dataset,             // data
            true,               // include legend
            true,
            false
        );

        PiePlot plot = (PiePlot) chart.getPlot();
        plot.setLabelFont(new Font("SansSerif", Font.PLAIN, 12));
        plot.setNoDataMessage("No data available");
        plot.setCircular(false);
        plot.setLabelGap(0.02);
        return chart;
        
    }
    
    /**
     * Creates a panel for the demo (used by SuperDemo.java).
     * 
     * @return A panel.
     */
    public static JPanel createDemoPanel() {
        JFreeChart chart = createChart(createDataset());
        return new ChartPanel(chart);
    }
    
	public void centraliza(){
		Dimension T = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension J = getSize();
		if (J.height > T.height) setSize(J.width,T.height);
		if (J.width > T.width) setSize(T.width,J.height);
		setLocation((T.width - J.width)/2,(T.height - J.height)/2);		
	}


}
