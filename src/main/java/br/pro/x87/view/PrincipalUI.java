/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pro.x87.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.plaf.metal.DefaultMetalTheme;
import javax.swing.plaf.metal.MetalLookAndFeel;
import javax.swing.plaf.metal.OceanTheme;

import br.pro.x87.dao.DAO;

/**
 *
 * @author JulianoRodrigo
 */
public class PrincipalUI extends JFrame {

	VinculoUI vinculoUI;
	AreaUI areaUI;
	BairroUI bairroUI;
	CidadeUI cidadeUI;
	CotacaoUI cotacaoUI;
	EmpresaUI empresaUI;
	EnderecoUI enderecoUI;
	EstimativaUI estimativaUI;
	FuncaoUI funcaoUI;
	VinculoFuncaoUI vinculoFuncaoUI;
	VinculoSalarioUI vinculoSalarioUI;
	FuncionarioUI funcionarioUI;
	ManutencaoUI manutencaoUI;
	MarcaUI marcaUI;
	MaquinaUI maquinaUI;
	OperacaoUI operacaoUI;
	ProdutoUI produtoUI;
	TipoServicoUI tipoServicoUI;
	TipoOperacaoUI tipoOperacaoUI;

	private DAO dao;

	private JMenu menuArquivo;
	private JMenu menuCadastro;
	private JMenu menuCustos;
	private JMenu menuEndereco;	
	private JMenu menuEmpresa;
	private JMenu menuFuncionario;
	private JMenu menuRegistro;
	private JMenu menuAjuda;

	private JMenuBar barraMenu;
	private JMenuItem miCrudArea;
	private JMenuItem miCrudBairro;
	private JMenuItem miCrudCidade;
	private JMenuItem miCrudCotacao;
	private JMenuItem miCrudLogradouro;

	private JMenuItem miCrudEmpresa;
	private JMenuItem miCrudFuncao;
	private JMenuItem miCrudFuncaoFuncionario;
	private JMenuItem miCrudVinculoSalario;
	private JMenuItem miCrudFuncionario;
	private JMenuItem miCrudAdmissao;
	private JMenuItem miCrudMarca;
	private JMenuItem miCrudMaquina;
	private JMenuItem miCrudProduto;
	private JMenuItem miCrudTipoServico;
	private JMenuItem miCrudTipoOperacao;
	private JMenuItem miSair;
	private JMenuItem miSobre;

	private JMenuItem miRegManutencao;
	private JMenuItem miRegOperacao;

	private JMenuItem miEstimativa;

	// Specify the look and feel to use by defining the LOOKANDFEEL constant
	// Valid values are: null (use the default), "Metal", "System", "Motif",
	// and "GTK"
	final static String LOOKANDFEEL = "System";

	// If you choose the Metal L&F, you can also choose a theme.
	// Specify the theme to use by defining the THEME constant
	// Valid values are: "DefaultMetal", "Ocean",  and "Test"
	final static String THEME = "Ocean";
	
	

	public PrincipalUI() {
		initLookAndFeel();
		JFrame.setDefaultLookAndFeelDecorated(true);
		
		getContentPane().setBackground(Color.WHITE);
		setBackground(Color.WHITE);
		setForeground(Color.WHITE);
		getContentPane().setForeground(Color.WHITE);



		URL url = this.getClass().getResource("/br/pro/x87/images/iconeSistema.png");
		Image imagemTitulo = Toolkit.getDefaultToolkit().getImage(url);
		this.setIconImage(imagemTitulo);



		barraMenu = new javax.swing.JMenuBar();	


		/**
		 * Itens de menu
		 */

		menuArquivo = new javax.swing.JMenu();
		menuArquivo.setText("Arquivo");

		menuCadastro = new javax.swing.JMenu();
		menuCadastro.setText("Cadastro");

		menuCustos = new javax.swing.JMenu();
		menuCustos.setText("Custos");

		menuEmpresa = new javax.swing.JMenu();
		menuEmpresa.setText("Empresa");

		menuEndereco = new javax.swing.JMenu();
		menuEndereco.setText("Endereco");

		menuFuncionario = new javax.swing.JMenu();
		menuFuncionario.setText("Funcionário");

		menuRegistro = new javax.swing.JMenu();
		menuRegistro.setText("Registro");
		
		menuAjuda = new javax.swing.JMenu();
		menuAjuda.setText("Ajuda");

		barraMenu.add(menuArquivo);
		barraMenu.add(menuCadastro);
		barraMenu.add(menuCustos);		
		barraMenu.add(menuRegistro);
		barraMenu.add(menuEndereco);
		barraMenu.add(menuEmpresa);
		barraMenu.add(menuFuncionario);
		barraMenu.add(menuAjuda);

		/**
		 * Botão sair
		 */

		miSair = new javax.swing.JMenuItem();
		miSair.setText("Sair");
		miSair.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				//dao.disconnect();
				dispose();
			}
		});

		menuArquivo.add(miSair);

		setJMenuBar(barraMenu);


		/**
		 * Menu empresa
		 */

		miCrudArea = new javax.swing.JMenuItem();
		miCrudArea.setText("Area");
		miCrudArea.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {				
				areaUI = new AreaUI();  
				areaUI.setVisible(true);
				areaUI.centraliza();
			}
		});

		miCrudEmpresa = new javax.swing.JMenuItem();
		miCrudEmpresa.setText("Empresa");
		miCrudEmpresa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				empresaUI = new EmpresaUI();        
				empresaUI.setVisible(true);
				empresaUI.centraliza();
			}
		});
		menuEmpresa.add(miCrudEmpresa);		
		menuEmpresa.add(miCrudArea);


		/**
		 * Menu cadastro
		 */

		miCrudMarca = new javax.swing.JMenuItem();
		miCrudMarca.setText("Marca");
		miCrudMarca.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				marcaUI = new MarcaUI();        
				marcaUI.setVisible(true);
				marcaUI.centraliza();
			}
		});		


		miCrudMaquina = new javax.swing.JMenuItem();
		miCrudMaquina.setText("Máquinas");
		miCrudMaquina.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				maquinaUI = new MaquinaUI();        
				maquinaUI.setVisible(true);
				maquinaUI.centraliza();
			}
		});	

		miCrudCotacao = new javax.swing.JMenuItem();
		miCrudCotacao.setText("Cotação de peças e/ou produtos");
		miCrudCotacao.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				cotacaoUI = new CotacaoUI();        
				cotacaoUI.setVisible(true);
				cotacaoUI.centraliza();
			}
		});

		miCrudFuncao = new JMenuItem();
		miCrudFuncao.setText("Função");
		miCrudFuncao.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				funcaoUI = new FuncaoUI();        
				funcaoUI.setVisible(true);
				funcaoUI.centraliza();
			}
		});


		miCrudProduto = new JMenuItem();
		miCrudProduto.setText("Produto");
		miCrudProduto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				produtoUI = new ProdutoUI();        
				produtoUI.setVisible(true);
				produtoUI.centraliza();
			}
		});

		miCrudTipoServico = new JMenuItem();
		miCrudTipoServico.setText("Tipo de serviço");
		miCrudTipoServico.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				tipoServicoUI = new TipoServicoUI();        
				tipoServicoUI.setVisible(true);
				tipoServicoUI.centraliza();
			}
		});

		miCrudTipoOperacao = new JMenuItem();
		miCrudTipoOperacao.setText("Tipo de Operação");
		miCrudTipoOperacao.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				tipoOperacaoUI = new TipoOperacaoUI();        
				tipoOperacaoUI.setVisible(true);
				tipoOperacaoUI.centraliza();
			}
		});

		menuCadastro.add(miCrudCotacao);
		menuCadastro.add(miCrudFuncao);			
		menuCadastro.add(miCrudMarca);
		menuCadastro.add(miCrudMaquina);
		menuCadastro.add(miCrudProduto);
		menuCadastro.add(miCrudTipoOperacao);
		menuCadastro.add(miCrudTipoServico);

		/**
		 * Menu Funcionario
		 */

		miCrudFuncionario = new JMenuItem();
		miCrudFuncionario.setText("Cadastrar pessoa");
		miCrudFuncionario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				funcionarioUI = new FuncionarioUI();        
				funcionarioUI.setVisible(true);
				funcionarioUI.centraliza();
			}
		});

		miCrudAdmissao = new JMenuItem();
		miCrudAdmissao.setText("Realizar admissão");
		miCrudAdmissao.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				vinculoUI = new VinculoUI();        
				vinculoUI.setVisible(true);
				vinculoUI.centraliza();
			}
		});

		miCrudFuncaoFuncionario = new JMenuItem();
		miCrudFuncaoFuncionario.setText("Atribuir funções");
		miCrudFuncaoFuncionario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				vinculoFuncaoUI = new VinculoFuncaoUI();        
				vinculoFuncaoUI.setVisible(true);
				vinculoFuncaoUI.centraliza();
			}
		});


		miCrudVinculoSalario = new JMenuItem();
		miCrudVinculoSalario.setText("Reajustar salário");
		miCrudVinculoSalario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				vinculoSalarioUI = new VinculoSalarioUI();        
				vinculoSalarioUI.setVisible(true);
				vinculoSalarioUI.centraliza();
			}
		});


		menuFuncionario.add(miCrudFuncionario);
		menuFuncionario.add(miCrudAdmissao);
		menuFuncionario.add(miCrudFuncaoFuncionario);
		menuFuncionario.add(miCrudVinculoSalario);


		/**
		 * Menu endereco
		 */

		miCrudBairro = new javax.swing.JMenuItem();
		miCrudBairro.setText("Bairro");
		miCrudBairro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				bairroUI = new BairroUI();        
				bairroUI.setVisible(true);
				bairroUI.centraliza();
			}
		});


		miCrudCidade = new javax.swing.JMenuItem();
		miCrudCidade.setText("Cidade");
		miCrudCidade.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				cidadeUI = new CidadeUI();        
				cidadeUI.setVisible(true);
				cidadeUI.centraliza();
			}
		});		

		miCrudLogradouro = new javax.swing.JMenuItem();
		miCrudLogradouro.setText("Logradouro");
		miCrudLogradouro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				enderecoUI = new EnderecoUI();        
				enderecoUI.setVisible(true);
				enderecoUI.centraliza();
			}
		});

		menuEndereco.add(miCrudCidade);		
		menuEndereco.add(miCrudBairro);
		menuEndereco.add(miCrudLogradouro);

		/*
		 * Menu registro
		 */

		miRegManutencao = new javax.swing.JMenuItem();
		miRegManutencao.setText("Manutenção");
		miRegManutencao.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				manutencaoUI = new ManutencaoUI();        
				manutencaoUI.setVisible(true);
				manutencaoUI.centraliza();
			}
		});	

		miRegOperacao= new javax.swing.JMenuItem();
		miRegOperacao.setText("Operação");
		miRegOperacao.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				operacaoUI = new OperacaoUI();        
				operacaoUI.setVisible(true);
				operacaoUI.centraliza();
			}
		});

		menuRegistro.add(miRegManutencao);
		menuRegistro.add(miRegOperacao);


		/*
		 * Menu custos
		 */

		miEstimativa = new javax.swing.JMenuItem();
		miEstimativa.setText("Estimativa");
		miEstimativa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				estimativaUI = new EstimativaUI();        
				estimativaUI.setVisible(true);
				estimativaUI.centraliza();
			}
		});

		menuCustos.add(miEstimativa);
		
		/*
		 * Menu ajuda
		 */
		
		miSobre = new javax.swing.JMenuItem();
		miSobre.setText("Sobre");
		miSobre.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				JOptionPane.showMessageDialog(null, 
						  "Este software é um protótipo em fase de avaliação e validação, podendo apresentar resultados inesperados."
						+ "\nO relato das experiências obtidas pode contribuir para a melhoria deste software!"
						+ "\n\n<html> <b>:: Criação e desenvolvimento :: </b></html> "
						+ "\nLiane Piacentini / lianepia@hotmail.com"
						+ "\nEduardo Godoy de Souza / godoy@unioeste.br"
						+ "\n\n<html> <b>:: Atualização ::</b></html> "
						+ "\nJuliano Rodrigo Lamb / lamb@utfpr.edu.br"
						+ "\nMárcio Furlan Maggi / marcio.maggi@unioeste.br"						
						,"Sobre", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		
		menuAjuda.add(miSobre);

		setTitle("MaqControl | Software para estimativa do custo operacional agrícola");
		setSize(600,400);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		ImageIcon icon = new ImageIcon(PrincipalUI.class.getResource("/br/pro/x87/images/figurasistema.png"));
		JLabel label = new JLabel(new ImageIcon(PrincipalUI.class.getResource("/br/pro/x87/images/figurasistema.png")));
		label.setBackground(Color.WHITE);
		label.setForeground(Color.WHITE);	

		getContentPane().add(label, BorderLayout.CENTER);

		dao = new DAO<>();
		dao.connect();
	}

	public void centraliza(){
		Dimension T = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension J = getSize();
		if (J.height > T.height) setSize(J.width,T.height);
		if (J.width > T.width) setSize(T.width,J.height);
		setLocation((T.width - J.width)/2,(T.height - J.height)/2);		
	}

	private static void initLookAndFeel() {
		String lookAndFeel = null;

		if (LOOKANDFEEL != null) {
			if (LOOKANDFEEL.equals("Metal")) {
				lookAndFeel = UIManager.getCrossPlatformLookAndFeelClassName();
				//  an alternative way to set the Metal L&F is to replace the 
				// previous line with:
				// lookAndFeel = "javax.swing.plaf.metal.MetalLookAndFeel";

			}

			else if (LOOKANDFEEL.equals("System")) {
				lookAndFeel = UIManager.getSystemLookAndFeelClassName();
			} 

			else if (LOOKANDFEEL.equals("Motif")) {
				lookAndFeel = "com.sun.java.swing.plaf.motif.MotifLookAndFeel";
			} 

			else if (LOOKANDFEEL.equals("GTK")) { 
				lookAndFeel = "com.sun.java.swing.plaf.gtk.GTKLookAndFeel";
			} 

			else {
				System.err.println("Unexpected value of LOOKANDFEEL specified: "
						+ LOOKANDFEEL);
				lookAndFeel = UIManager.getCrossPlatformLookAndFeelClassName();
			}

			try {


				UIManager.setLookAndFeel(lookAndFeel);

				// If L&F = "Metal", set the theme

				if (LOOKANDFEEL.equals("Metal")) {
					if (THEME.equals("DefaultMetal"))
						MetalLookAndFeel.setCurrentTheme(new DefaultMetalTheme());
					else if (THEME.equals("Ocean"))
						MetalLookAndFeel.setCurrentTheme(new OceanTheme());


					UIManager.setLookAndFeel(new MetalLookAndFeel()); 
				}	




			} 

			catch (ClassNotFoundException e) {
				System.err.println("Couldn't find class for specified look and feel:"
						+ lookAndFeel);
				System.err.println("Did you include the L&F library in the class path?");
				System.err.println("Using the default look and feel.");
			} 

			catch (UnsupportedLookAndFeelException e) {
				System.err.println("Can't use the specified look and feel ("
						+ lookAndFeel
						+ ") on this platform.");
				System.err.println("Using the default look and feel.");
			} 

			catch (Exception e) {
				System.err.println("Couldn't get specified look and feel ("
						+ lookAndFeel
						+ "), for some reason.");
				System.err.println("Using the default look and feel.");
				e.printStackTrace();
			}
		}
	}
}

