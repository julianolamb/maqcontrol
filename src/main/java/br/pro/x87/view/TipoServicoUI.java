package br.pro.x87.view;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import br.pro.x87.enums.TServico;
import br.pro.x87.model.TipoServico;
import br.pro.x87.view.command.ExcluirCommand;
import br.pro.x87.view.command.SalvarCommand;

public class TipoServicoUI extends AncestorUI {

	private static final long serialVersionUID = 1L;
	
	private TipoServico tipoServico;
	private TServico tServico;	

	private JComboBox<String> comboTServico;
	private JTextField tfDescricao;
	private JLabel jlServico;
	private JLabel jlTipo;	
	
	private boolean update;

	public TipoServico getServico() {
		return tipoServico;
	}

	public void setServico(TipoServico servico) {
		this.tipoServico = servico;
	}

	public TServico gettServico() {
		return tServico;
	}

	public void settServico(TServico tServico) {
		this.tServico = tServico;
	}

	public boolean isUpdate() {
		return update;
	}

	public void setUpdate(boolean update) {
		this.update = update;
	}

	public TipoServicoUI() {
		popularCombo();
	}

	@Override
	public void monteInterface(String s) {
		super.monteInterface("MaqControl | Cadastro dos tipos de serviços");	

		JPanel panel = new JPanel();
		jlServico = new JLabel(" Serviço ");
		jlTipo = new JLabel(" Tipo ");
		tfDescricao = new JTextField();
		comboTServico = new JComboBox<String>();
		
		panel.setBounds(0, 0, 495, 40);		
		panel.setLayout(new GridLayout(0, 2, 10, 0));		
		panel.add(jlServico);		
		panel.add(jlTipo);		
		panel.add(tfDescricao);		
		panel.add(comboTServico);

		P2.setBounds(0, 75, 495, 150);
		pBotoes.setBounds(0, 45, 495, 23);
		
		StatusBar.setForeground(new Color(153, 153, 153));
		StatusBar.setText("***MaqControl*** Cadastro de serviços");
		getContentPane().setLayout(null);
		getContentPane().add(panel);
		setResizable(false);		
		        
		setSize(500,255);		
	}


	private void popularCombo() {
		for (TServico o : TServico.values()) {
			comboTServico.addItem(o.getNome());
		}
	}

	@Override
	public void habilitarCampos() {		
		tfDescricao.setEnabled(true);	
		comboTServico.setEnabled(true);	
	}

	@Override
	public void desabiltarCampos() {		
		comboTServico.setSelectedIndex(-1);
		
		tfDescricao.setEnabled(false);
		comboTServico.setEnabled(false);
	}

	@Override
	public void definirFoco() {
		tfDescricao.requestFocus();
	}

	@Override
	public void executarCommandExcluir() {
		new ExcluirCommand().execute(tipoServico);	
	}

	@Override
	public void atualizarGrade() {
		atualizarGrade("TipoServico", TipoServico.class);
	}

	@Override
	public void carregarValoresAlterar() {
		getServico().setDescricao(tfDescricao.getText());
		getServico().setTipoServico((String) comboTServico.getSelectedItem());		
	}

	@Override
	public void carregarValoresSalvar() {
		setServico(new TipoServico(tfDescricao.getText(), (String) comboTServico.getSelectedItem()));		
	}

	@Override
	public void carregarValoresObjetos() {
		//nothing to do here
	}

	@Override
	public void executarCommandSalvar() {
		new SalvarCommand().execute(tipoServico);
	}

	@Override
	public void carregarValores() {
		setServico((TipoServico) lista.get(grade.getSelectedRow()));
		tfDescricao.setText(getServico().getDescricao());
		comboTServico.setSelectedItem(getServico().getTipoServico());		
	}
	
	@Override
	public void adicionaListenerCampoVazio() {
		tfDescricao.addKeyListener(createKeyListenerCampoVazio(tfDescricao));		
	}
	
	@Override
	public boolean validarFlutuantes() {
		return true;
	}
	
	@Override
	public boolean validarCombos() {

		if (!validaEsteCombo(comboTServico, "TIPO DE SERVIÇO"))
			return false; 
		
		return true;
	}

	@Override
	public void addDocument() {}
}
