package br.pro.x87.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import br.pro.x87.model.Empresa;
import br.pro.x87.model.Funcionario;
import br.pro.x87.model.Vinculo;
import br.pro.x87.view.command.ExcluirCommand;
import br.pro.x87.view.command.SalvarCommand;
import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.UtilDateModel;

public class VinculoUI extends AncestorUI {

	private static final long serialVersionUID = 1L;

	private Funcionario funcionario;	
	private Empresa empresa;
	private Vinculo vinculo;

	private List<Funcionario> listFuncionario;
	private List<Empresa> listEmpresa;

	private JComboBox<Object> comboEmpresa;
	private JComboBox<Object> comboFuncionario;
	private JCheckBox chInformarDemissao;
	private JRadioButton rdbEfetiva;
	private JRadioButton rdbTemporaria; 
	private JLabel lblDemissao;
	private JPanel panel1;
	private JPanel panel2;

	private UtilDateModel modelAdmissao;
	private JDatePanelImpl datePanelAdmissao;
	private JDatePickerImpl dpDataAdmissao;

	private UtilDateModel modelDemissao;
	private JDatePanelImpl datePanelDemissao;
	private JDatePickerImpl dpDataDemissao;

	private String status;
	private Date dataAdmissao;
	private Date dataDemissao;

	public Vinculo getVinculo() {
		return vinculo;
	}

	public void setVinculo(Vinculo vinculo) {
		this.vinculo = vinculo;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public void popularEmpresa(){		
		comboEmpresa.removeAllItems();
		listEmpresa = getDao().get("Empresa");

		for (Object o : listEmpresa) {	
			Empresa e = (Empresa) o;
			comboEmpresa.addItem(e.getNomeFantasia());
		}
	}

	public void popularFuncionario(){		
		comboFuncionario.removeAllItems();
		listFuncionario = getDao().get("Funcionario");

		for (Object o : listFuncionario) {	
			Funcionario f = (Funcionario) o;
			comboFuncionario.addItem(f.getNome());
		}
	}

	@Override
	public void monteInterface(String s) {		
		super.monteInterface("MaqControl | Admissão de funcionário");		

		modelAdmissao = new UtilDateModel();
		datePanelAdmissao = new JDatePanelImpl(modelAdmissao);
		dpDataAdmissao = new JDatePickerImpl(datePanelAdmissao);

		modelDemissao = new UtilDateModel();
		datePanelDemissao = new JDatePanelImpl(modelDemissao);
		dpDataDemissao = new JDatePickerImpl(datePanelDemissao);

		panel2 = new JPanel();
		JLabel lblAdmisso = new JLabel("Admissão");
		lblDemissao = new JLabel("Demissão");
		chInformarDemissao = new JCheckBox("Informar demissão desta empresa");

		ButtonGroup group = new ButtonGroup();		
		rdbEfetiva = new JRadioButton("Efetiva");
		rdbTemporaria = new JRadioButton("Temporária");

		group.add(rdbEfetiva);
		group.add(rdbTemporaria);

		dpDataAdmissao.getJFormattedTextField().setPreferredSize(new Dimension(178, 18));
		dpDataAdmissao.setPreferredSize(new Dimension(202, 18));
		dpDataAdmissao.getJFormattedTextField().setBackground(Color.WHITE);		

		dpDataDemissao.setPreferredSize(new Dimension(202, 18));
		dpDataDemissao.getJFormattedTextField().setPreferredSize(new Dimension(178, 18));
		dpDataDemissao.getJFormattedTextField().setBackground(Color.WHITE);

		panel2.setBounds(0, 45, 545, 90);
		panel2.setLayout(new GridLayout(4, 2, 10, 0));		
		panel2.add(lblAdmisso);
		panel2.add(lblDemissao);
		panel2.add(dpDataAdmissao);
		panel2.add(dpDataDemissao);
		panel2.add(rdbEfetiva);
		panel2.add(chInformarDemissao);
		panel2.add(rdbTemporaria);	

		panel1 = new JPanel();
		JLabel lblFuncionrio = new JLabel("Funcionário");
		JLabel lblEmpresa = new JLabel("Empresa");		
		comboFuncionario = new JComboBox<>();
		comboEmpresa = new JComboBox<>();

		panel1.setBounds(0, 0, 545, 40);		
		panel1.setLayout(new GridLayout(0, 2, 10, 0));		
		panel1.add(lblFuncionrio);		
		panel1.add(lblEmpresa);		
		panel1.add(comboFuncionario);		
		panel1.add(comboEmpresa);

		pBotoes.setSize(545, 23);
		pBotoes.setLocation(0, 140);

		P2.setBounds(0, 170, 545, 150);

		getContentPane().setLayout(null);
		getContentPane().add(panel2);	
		getContentPane().add(panel1);

		setResizable(false);	
		setSize(550, 350);
	}

	public VinculoUI() {		
		chInformarDemissao.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				if (chInformarDemissao.isSelected()){
					lblDemissao.setEnabled(true);
					dpDataDemissao.setEnabled(true);
				} else{
					lblDemissao.setEnabled(false);
					dpDataDemissao.setEnabled(false);
				}
			}
		});

		popularEmpresa();
		popularFuncionario();
	}

	@Override
	public void executarCommandExcluir() {
		new ExcluirCommand().execute(vinculo);
	}

	@Override
	public void executarCommandSalvar() {
		new SalvarCommand().execute(vinculo);
	}

	@Override
	public void atualizarGrade() {
		atualizarGrade("Vinculo", Vinculo.class);
	}

	@Override
	public void carregarValores() {		
		setVinculo((Vinculo) lista.get(grade.getSelectedRow()));		

		comboFuncionario.setSelectedItem(getVinculo().getFuncionario().getNome());
		comboEmpresa.setSelectedItem(getVinculo().getEmpresa().getNomeFantasia());

		SimpleDateFormat anoFormato = new SimpleDateFormat("yyyy");
		SimpleDateFormat mesFormato = new SimpleDateFormat("MM");
		SimpleDateFormat diaFormato = new SimpleDateFormat("dd");	

		int d = Integer.parseInt(diaFormato.format((Date) getVinculo().getAdmissao()));
		int m = Integer.parseInt(mesFormato.format((Date) getVinculo().getAdmissao()));
		int a = Integer.parseInt(anoFormato.format((Date) getVinculo().getAdmissao()));

		modelAdmissao.setDate(a, m, d);
		modelAdmissao.setSelected(true);

		try {
			d = Integer.parseInt(diaFormato.format((Date) getVinculo().getDemissao()));
			m = Integer.parseInt(mesFormato.format((Date) getVinculo().getDemissao()));
			a = Integer.parseInt(anoFormato.format((Date) getVinculo().getDemissao()));

			modelDemissao.setDate(a, m, d);
			modelDemissao.setSelected(true);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public void definirFoco() {
		comboFuncionario.requestFocus();
	}

	@Override
	public void habilitarCampos() {
		comboFuncionario.setEnabled(true);
		comboFuncionario.setSelectedIndex(-1);
		comboEmpresa.setEnabled(true);
		comboEmpresa.setSelectedIndex(-1);
		dpDataAdmissao.setEnabled(true);
		rdbEfetiva.setEnabled(true);
		rdbTemporaria.setEnabled(true);
		chInformarDemissao.setEnabled(true);
	}

	@Override
	public void desabiltarCampos() {
		comboFuncionario.setEnabled(false);
		comboEmpresa.setEnabled(false);
		dpDataAdmissao.setEnabled(false);
		rdbEfetiva.setEnabled(false);
		rdbTemporaria.setEnabled(false);
		chInformarDemissao.setSelected(false);
		chInformarDemissao.setEnabled(false);
	}

	@Override
	public void carregarValoresAlterar() {
		getVinculo().setAdmissao(dataAdmissao);
		getVinculo().setEmpresa(getEmpresa());
		getVinculo().setFuncionario(getFuncionario());
		getVinculo().setStatus(status);
		if (chInformarDemissao.isSelected()){
			getVinculo().setDemissao(dataDemissao);
		}
	}

	@Override
	public void carregarValoresSalvar() {
		if (chInformarDemissao.isSelected()){
			setVinculo(new Vinculo( getFuncionario(), getEmpresa(), dataAdmissao, dataDemissao, status));
		} else{
			setVinculo(new Vinculo(getFuncionario(), getEmpresa(), dataAdmissao, status));
		}
	}

	@Override
	public void carregarValoresObjetos() {
		setEmpresa(listEmpresa.get(comboEmpresa.getSelectedIndex()));
		setFuncionario(listFuncionario.get(comboFuncionario.getSelectedIndex()));

		dataAdmissao = (Date) dpDataAdmissao.getModel().getValue();

		if (chInformarDemissao.isSelected()){
			dataDemissao = (Date) dpDataDemissao.getModel().getValue();
		}

		if (rdbEfetiva.isSelected()){
			status = "Efetiva";
		} else {
			status = "Temporária";
		}
	}

	@Override
	public boolean templateValidar() {
		if (!validaEsteCombo(comboEmpresa, "EMPRESA"))
			return false;
		
		if (!validaEsteCombo(comboFuncionario, "FUNCIONÁRIO"))			
			return false;
		
		Date dataAdmissao = null;		
		try {
			dataAdmissao = (Date) dpDataAdmissao.getModel().getValue();								
		} catch (Exception e) {
			return false;
		}			

		if (dataAdmissao == null)
			return false;		

		if (chInformarDemissao.isSelected()){
			Date inicio = (Date) dpDataAdmissao.getModel().getValue();
			Date termino = (Date) dpDataDemissao.getModel().getValue();
			if (termino.before(inicio)){			
				mensagemErro("ERRO: A data de demissão é anterior à data de admissão!");
				dpDataAdmissao.requestFocus();					
				return false;
			}		
		}

		if (!rdbEfetiva.isSelected() && !rdbTemporaria.isSelected())
			return false;

		return true;
	}

	public void adicionaListenerCampoVazio() { }

	@Override
	public boolean validarFlutuantes() {
		return true;
	}

	@Override
	public boolean validarCombos() {
		if (!validaEsteCombo(comboEmpresa, "EMPRESA"))
			return false;

		if (!validaEsteCombo(comboFuncionario, "FUNCIONÁRIO"))
			return false;	

		return true;
	}
	
	@Override
	public void addDocument() {}
}
