/**
 * 
 */
package br.pro.x87.view.management;

import java.awt.Component;
import java.awt.Container;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;

/**
 * @author jrlamb
 *
 */
public class ManageTextArea implements Management{


	private static ManageTextArea instance;

	private ManageTextArea() {
		// TODO Auto-generated constructor stub
	}

	public static ManageTextArea getInstance(){
		if (instance == null){
			instance = new ManageTextArea();
		}
		return instance;
	}

	@Override
	public void disable(Container c) {
		Component [] p = c.getComponents();
		for(Component pp : p){
			if(pp instanceof JPanel)
				disableInsidePanel((JPanel) pp);
			if(pp instanceof JTabbedPane)
				disableInsideTabbedPane((JTabbedPane) pp);
		}				
	}

	@Override
	public void disableInsidePanel(JPanel p) {
		Component [] c = p.getComponents();
		for(Component cc: c){
			if(cc instanceof JTextArea){				
				((JTextArea) cc).setEnabled(false);
			} else if (cc instanceof JPanel){
				disableInsidePanel((JPanel) cc);
			}			
		}		
	}

	/* (non-Javadoc)
	 * @see br.pro.x87.view.management.Management#disableInsideTabbedPane(javax.swing.JTabbedPane)
	 */
	@Override
	public void disableInsideTabbedPane(JTabbedPane p) {
		Component [] c = p.getComponents();
		for(Component cc: c){
			if(cc instanceof JTextArea){				
				((JTextArea) cc).setEnabled(false);
			} else if (cc instanceof JPanel){
				disableInsidePanel((JPanel) cc);
			}			
		}
	}

	@Override
	public void enable(Container c) {
		Component [] p = c.getComponents();
		for(Component pp : p){
			if(pp instanceof JPanel)
				enableInsidePanel((JPanel) pp);
			if(pp instanceof JTabbedPane)
				enableInsideTabbedPane((JTabbedPane) pp);
		}
	}

	@Override
	public void enableInsidePanel(JPanel p) {
		Component [] c = p.getComponents();
		for(Component cc: c){
			if(cc instanceof JTextArea){				
				((JTextArea) cc).setEnabled(true);
			} else if (cc instanceof JPanel){
				enableInsidePanel((JPanel) cc);
			}			
		}
	}

	@Override
	public void enableInsideTabbedPane(JTabbedPane p) {
		Component [] c = p.getComponents();
		for(Component cc: c){
			if(cc instanceof JTextArea){				
				((JTextArea) cc).setEnabled(true);
			} else if (cc instanceof JPanel){
				enableInsidePanel((JPanel) cc);
			}			
		}
	}

}
