package br.pro.x87.view.management;

import java.awt.Container;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

//Esta interface serve para habilitar ou desabilitar componentes

public interface Management {
	public void disable(Container c);
	public void disableInsidePanel(JPanel p);
	public void disableInsideTabbedPane(JTabbedPane p);
	public void enable(Container c);
	public void enableInsidePanel(JPanel p);
	public void enableInsideTabbedPane(JTabbedPane p);
}
