package br.pro.x87.view.management;

import java.awt.Component;
import java.awt.Container;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;

public class ManageTextField implements Management {

	private static ManageTextField instance;

	private ManageTextField() {
		// TODO Auto-generated constructor stub
	}

	public static ManageTextField getInstance(){
		if (instance == null){
			instance = new ManageTextField();
		}
		return instance;
	}

	@Override
	public void disable(Container c) {
		Component [] p = c.getComponents();
		for(Component pp : p){
			if(pp instanceof JPanel)
				disableInsidePanel((JPanel) pp);
			if(pp instanceof JTabbedPane)
				disableInsideTabbedPane((JTabbedPane) pp);
		}		
	}

	@Override
	public void disableInsidePanel(JPanel p) {
		Component [] c = p.getComponents();
		for(Component cc: c){
			if(cc instanceof JTextField){				
				((JTextField) cc).setEnabled(false);
			} else if (cc instanceof JPanel){
				disableInsidePanel((JPanel) cc);
			}			
		}		
	}

	@Override
	public void disableInsideTabbedPane(JTabbedPane p) {
		Component [] c = p.getComponents();
		for(Component cc: c){
			if(cc instanceof JTextField){				
				((JTextField) cc).setEnabled(false);
			} else if (cc instanceof JPanel){
				disableInsidePanel((JPanel) cc);
			}			
		}		
	}		

	@Override
	public void enable(Container c) {
		Component [] p = c.getComponents();
		for(Component pp : p){
			if(pp instanceof JPanel)
				enableInsidePanel((JPanel) pp);
			if(pp instanceof JTabbedPane)
				enableInsideTabbedPane((JTabbedPane) pp);
		}
	}

	@Override
	public void enableInsidePanel(JPanel p) {
		Component [] c = p.getComponents();
		for(Component cc: c){
			if(cc instanceof JTextField){				
				((JTextField) cc).setEnabled(true);
			} else if (cc instanceof JPanel){
				enableInsidePanel((JPanel) cc);
			}			
		}
	}

	@Override
	public void enableInsideTabbedPane(JTabbedPane p) {
		Component [] c = p.getComponents();
		for(Component cc: c){
			if(cc instanceof JTextField){				
				((JTextField) cc).setEnabled(true);
			} else if (cc instanceof JPanel){
				enableInsidePanel((JPanel) cc);
			}			
		}	

	}
}
