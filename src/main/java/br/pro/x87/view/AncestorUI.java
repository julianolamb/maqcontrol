/**
 * 
 */
package br.pro.x87.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.table.JTableHeader;

import br.pro.x87.controller.GenericTableModel;
import br.pro.x87.dao.DAO;
import br.pro.x87.view.clear.Clear;
import br.pro.x87.view.clear.ClearTextArea;
import br.pro.x87.view.clear.ClearTextField;
import br.pro.x87.view.clear.SetComboBox;
import br.pro.x87.view.management.ManageComboBox;
import br.pro.x87.view.management.ManageTextArea;
import br.pro.x87.view.management.ManageTextField;
import br.pro.x87.view.management.Management;
import br.pro.x87.view.state.AtualizarUI;
import br.pro.x87.view.state.ExcluirUI;
import br.pro.x87.view.state.NovoUI;
import br.pro.x87.view.state.SalvarUI;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sourceforge.jdatepicker.impl.UtilDateModel;

/**
 * @author jrlamb
 *
 */
public abstract class AncestorUI extends JFrame {

	private boolean flag;	

	public JPanel pBotoes, P2;
	public JButton bNovo, bSalvar, bListagem, bExcluir, bFechar;

	public JTable grade;
	public JScrollPane scrollPane;

	private Management d;
	private Clear c;

	public JLabel StatusBar;
	private boolean update;
	private DAO dao = new DAO();	
	private GenericTableModel table;

	protected List<?> lista;	

	public abstract void executarCommandExcluir();
	public abstract void executarCommandSalvar();
	public abstract void atualizarGrade();
	public abstract void carregarValores();
	public abstract void definirFoco();

	public abstract void habilitarCampos();
	public abstract void desabiltarCampos();
	public abstract void carregarValoresAlterar();
	public abstract void carregarValoresSalvar();
	public abstract void carregarValoresObjetos();		

	private DecimalFormat df = new DecimalFormat("0.00");	

	public DecimalFormat getDf() {
		return df;
	}
	public void setDf(DecimalFormat df) {
		this.df = df;
	}
	public GenericTableModel getTable() {
		return table;
	}

	public void setTable(GenericTableModel table) {
		this.table = table;
	}

	public DAO getDao() {
		return dao;
	}

	public void setDao(DAO dao) {
		this.dao = dao;
	}

	public boolean isUpdate() {
		return update;
	}

	public void setUpdate(boolean update) {
		this.update = update;
	}

	public AncestorUI(){
		monteInterface("***MaqControl*** Janela default");
		new NovoUI().gerenciar(bNovo, bSalvar, bListagem, bExcluir, bFechar);
		centraliza();

		adicionaListenerCampoVazio();
		addDocument();
	}

	public void monteInterface(String s)
	{
		setTitle(s);
		setResizable(false);
		setSize(500,300);

		pBotoes = new JPanel();
		grade = new JTable();
		grade.setBackground(Color.WHITE);
			
		JTableHeader header = grade.getTableHeader();
		header.setBackground(pBotoes.getBackground());
		header.setForeground(Color.BLACK);
		header.setOpaque(false);
		header.setDefaultRenderer(new HeaderRenderer(grade));

		StatusBar = new JLabel();
		scrollPane = new JScrollPane(grade);
		P2 = new JPanel();

		grade.setFocusable(false);
		grade.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		grade.setCursor(new Cursor(Cursor.HAND_CURSOR));
		grade.setToolTipText("Clique aqui para carregar as informaçoes");

		grade.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				templateClicarGrid();	
			}
		});

		bNovo = new JButton("Novo");
		bNovo.addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent m) {
				StatusBar.setText("Clique aqui para cadastrar um NOVO registro ");
			}
			public void mouseExited(MouseEvent m) {
				StatusBar.setText("***MaqControl*** Software para estimativa do custo operacional agrícola");
			}
		});
		bNovo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {				
				templateNovo();
			}
		});

		bSalvar = new JButton("Salvar");
		bSalvar.addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent m) {
				StatusBar.setText("Clique aqui para SALVAR ou ALTERAR as informações digitadas");
			}
			public void mouseExited(MouseEvent m) {
				StatusBar.setText("***MaqControl*** Software para estimativa do custo operacional agrícola");
			}
		});
		bSalvar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {				
				templateSalvar();
			}
		});

		bListagem = new JButton("Listagem");
		bListagem.addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent m) {
				StatusBar.setText("Clique aqui para visualizar uma listagem completa das informações");
			}
			public void mouseExited(MouseEvent m) {
				StatusBar.setText("***MaqControl*** Software para estimativa do custo operacional agrícola");			
			}
		});
		bListagem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {				

			}
		});

		bExcluir = new JButton("Excluir");
		bExcluir.addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent m) {
				StatusBar.setText("Clique aqui para EXCLUIR o registro selecionado");
			}
			public void mouseExited(MouseEvent m) {
				StatusBar.setText("***MaqControl*** Software para estimativa do custo operacional agrícola");			
			}
		});
		bExcluir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				templateExcluir();
			}
		});

		bFechar = new JButton("Fechar");
		bFechar.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseEntered(MouseEvent m) {
				StatusBar.setText("Clique aqui para retornar a janela principal");
			}
			public void mouseExited(MouseEvent m) {
				StatusBar.setText("***MaqControl*** Software para estimativa do custo operacional agrícola");			
			}
		});  
		bFechar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});

		P2.setLayout(new BorderLayout());
		P2.add(new JLabel("Registros já cadastrados", JLabel.CENTER),BorderLayout.NORTH);
		P2.add(scrollPane,BorderLayout.CENTER);
		P2.add(StatusBar, BorderLayout.SOUTH);

		pBotoes.setLayout(new GridLayout(1,6));
		pBotoes.add(bNovo);
		pBotoes.add(bSalvar);
		pBotoes.add(bListagem);
		pBotoes.add(bExcluir);
		pBotoes.add(bFechar);	


		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(pBotoes,BorderLayout.NORTH);
		getContentPane().add(P2,BorderLayout.SOUTH);

		atualizarGrade();

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		df.setRoundingMode(RoundingMode.UP);
	}

	public void atualizarGrade(String s, Class<?> c){
		lista = getDao().get(s);
		setTable(new GenericTableModel(lista, c));
		grade.setModel(getTable());
		grade.repaint();
		clearTextField();
		setComboBox();
	}

	public final void templateNovo(){
		setUpdate(false);
		limparHabilitarCampos();
		definirFoco();
		controlarBotoesInterface();		
	}

	public final void templateSalvar(){
		if (!templateValidar()) {
			return;
		}

		carregarValoresObjetos();				

		if (!isUpdate()) {
			carregarValoresSalvar();					
		} else {
			if (JOptionPane.showConfirmDialog(null,
					"Tem certeza que deseja ALTERAR esse registro?",
					"Confirmar alteração",
					JOptionPane.YES_NO_OPTION) == 1){
				return;
			}
			carregarValoresAlterar();
		}	
		executarCommandSalvar();
		atualizarGrade();

		desabiltarCampos();
		limparCampos();
		new SalvarUI().gerenciar(bNovo, bSalvar, bListagem, bExcluir, bFechar);         
	}

	public void limparCampos(){		
		clearTextField();
		setComboBox();
		clearTextArea();
	}

	public final void templateExcluir(){
		if (JOptionPane.showConfirmDialog(null,
				"Tem certeza que deseja EXCLUIR esse registro?",
				"Confirmar exclusão",
				JOptionPane.YES_NO_OPTION) == 1)
			return;

		executarCommandExcluir();
		cleaAndDisableJTextField();
		atualizarGrade();
		desabiltarCampos();
		new ExcluirUI().gerenciar(bNovo, bSalvar, bListagem, bExcluir, bFechar); 			
	}

	public final void templateClicarGrid(){
		setUpdate(true);	
		habilitarCampos();

		carregarValores();								
		new AtualizarUI().gerenciar(bNovo, bSalvar, bListagem, bExcluir, bFechar);
	}

	public void controlarBotoesInterface(){
		new NovoUI().gerenciar(bNovo, bSalvar, bListagem, bExcluir, bFechar);
	}

	public Management getD() {
		return d;
	}
	public void setD(Management d) {
		this.d = d;
	}
	public Clear getC() {
		return c;
	}
	public void setC(Clear c) {
		this.c = c;
	}
	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;		
	}

	public void limparHabilitarCampos(){			
		enableTextField();
		clearTextField();

		enableComboBox();
		setComboBox();

		enableTextArea();
		clearTextArea();
	}

	public void centraliza(){
		Dimension T = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension J = getSize();
		if (J.height > T.height) setSize(J.width,T.height);
		if (J.width > T.width) setSize(T.width,J.height);
		setLocation((T.width - J.width)/2,(T.height - J.height)/2);		
	}

	public void clearTextArea(){
		setC(ClearTextArea.getInstance());
		c.clear(getContentPane());
	}

	public void disableTextArea(){
		setD(ManageTextArea.getInstance());		
		d.disable(getContentPane());
	}

	public void clearTextField(){
		setC(ClearTextField.getInstance());		
		c.clear(getContentPane());
	}

	public void disableTextField(){
		setD(ManageTextField.getInstance());		
		d.disable(getContentPane());
	}

	public void enableTextField(){
		setD(ManageTextField.getInstance());
		d.enable(getContentPane());
	}

	public void enableTextArea(){
		setD(ManageTextArea.getInstance());
		d.enable(getContentPane());
	}

	public void enableComboBox(){
		setD(ManageComboBox.getInstance());
		d.enable(getContentPane());
	}

	public void disableComboBox(){
		setD(ManageComboBox.getInstance());
		d.disable(getContentPane());
	}

	public void cleaAndDisableJTextField(){
		clearTextField();
		disableTextField();
	}

	public void setComboBox(){		
		setC(SetComboBox.getInstance());		
		c.clear(getContentPane());
	}

	public KeyAdapter createKeyListenerCampoVazio(final Component c){		
		KeyAdapter k = new KeyAdapter() {
			public void keyPressed(KeyEvent arg0) {
				if (c instanceof JTextField){
					if (((JTextField) c).getText().isEmpty() || !((JTextField) c).getText().equals("")) {
						((JTextField) c).setBackground(Color.WHITE);
					}
				}				
			}		
		};
		return k;
	}

	public boolean templateValidar(){
		if (!changeEmptyJTextFieldColor()){			
			JOptionPane.showMessageDialog(null, "Por favor prencha todos os campos!", "Erro", JOptionPane.ERROR_MESSAGE);
			return false;
		}

		if (!validarFlutuantes())
			return false;	

		if (!validarCombos())
			return false;	

		return true;
	}

	public boolean changeEmptyJTextFieldColor(){
		boolean f = true;
		flag = true;
		Component [] p = getContentPane().getComponents();
		for(Component pp : p){
			if(pp instanceof JPanel){
				changeEmptyJTextFieldColorInsidePanel((JPanel) pp);
				if (!flag)	
					f = false;				 				
			}	
		}
		return f;
	}

	public void changeEmptyJTextFieldColorInsidePanel(JPanel p){
		Component [] c = p.getComponents();		
		for(Component cc: c){
			if(cc instanceof JTextField){		
				if (((JTextField) cc).getText().isEmpty() ||((JTextField) cc).getText().equals("")){
					((JTextField) cc).setBackground(Color.RED);
					flag = false;
				}
			} else if (cc instanceof JPanel){
				changeEmptyJTextFieldColorInsidePanel((JPanel) cc);
			}			
		}		
	}

	public static float converte(String arg) throws ParseException{
		/*obtem um NumberFormat para o Locale default (BR)*/
		NumberFormat nf = NumberFormat.getNumberInstance(new Locale("pt", "BR"));

		/*converte um número com vírgulas ex: 2,56 para float*/
		float number = nf.parse(arg).floatValue();	

		return number;
	}

	public void setPanelTitleAndBorder(JPanel p, String s){
		p.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder(s),
				BorderFactory.createEmptyBorder(5,5,5,5)));
		p.updateUI();
	}	

	public void mensagemErro(String s){
		JOptionPane.showMessageDialog(null, s, "Erro", JOptionPane.ERROR_MESSAGE);
	}

	public boolean validaEsteFloat(JTextField t, String s){		
		try {
			converte(t.getText());
		} catch (Exception ex) {
			mensagemErro("O número informado para o campo \'" + s + "\' não corresponde a um número real válido!");
			t.setBackground(Color.RED);
			t.requestFocus();			
			return false;
		}			
		return true;
	}

	public boolean validaEsteCombo(JComboBox<?> combo, String s){		
		if (combo.getSelectedIndex() == -1){
			mensagemErro("Você deve selecionar um item na caixa " + s);
			combo.requestFocus();
			return false;
		}

		return true;
	}

	public void carregarDataDateTimePicker(Date data, UtilDateModel model){
		SimpleDateFormat anoFormato = new SimpleDateFormat("yyyy");
		SimpleDateFormat mesFormato = new SimpleDateFormat("MM");
		SimpleDateFormat diaFormato = new SimpleDateFormat("dd");	

		int d = Integer.parseInt(diaFormato.format(data));
		int m = Integer.parseInt(mesFormato.format(data)) -1;
		int a = Integer.parseInt(anoFormato.format(data));

		model.setDate(a, m, d);
		model.setSelected(true);		
	}

	public void templateImprimir(List lista, String file){
		String path = System.getProperty("user.dir");
		String temp = System.getProperty("java.io.tmpdir");

		//String pathToReportPackage = "\\src\\main\\java\\br\\pro\\x87\\reports\\src\\";
		String pathToReportPackage = "\\reports\\";
		

		try {					
			JasperReport report = JasperCompileManager.compileReport(path + pathToReportPackage + file);						
			JasperPrint print = JasperFillManager.fillReport(report, null, new JRBeanCollectionDataSource(lista));

			JasperExportManager.exportReportToPdfFile(print, temp + "MaqControl.pdf");

			File f = new File(temp + "MaqControl.pdf");
			Desktop.getDesktop().open(f);	
		} catch (JRException jre) {
			mensagemErro("Ocorreu um erro inesperado, contate o administrador! "
					+ "\n\nErro ocorrido: \n" + jre.getMessage());			
			return;				
		} catch (IOException ex) {
			mensagemErro("Nenhum leitor associado! "
					+ "\n\nErro ocorrido: \n" + ex.getMessage());			
			return;	
	    } catch (Exception e){
			mensagemErro("Não foi possível gerar o relatório. Possíveis razões:"
					+ "\n- o leitor de PDF está em execução; "
					+ "\n- algum relatório antigo está carregado."
					+ "\n\n Salve os trabalhos, feche o leitor de PDF e clique novamente em LISTAGEM para gerar o relatório.");
			e.printStackTrace();
			return;						
		}

		JOptionPane.showMessageDialog(null, "O relatório foi gerado com sucesso! "
				+ "Lembre de salvar ou imprimir o seu trabalho", "Relatório gerado com sucesso", 
				JOptionPane.INFORMATION_MESSAGE);
	}

	public abstract void adicionaListenerCampoVazio();
	public abstract boolean validarFlutuantes();
	public abstract boolean validarCombos();
	public abstract void addDocument();
}
