/**
 * 
 */
package br.pro.x87.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import br.pro.x87.controller.OperacaoMaquinaModel;
import br.pro.x87.dao.OperacaoDAO;
import br.pro.x87.model.Area;
import br.pro.x87.model.Empresa;
import br.pro.x87.model.Funcionario;
import br.pro.x87.model.Maquina;
import br.pro.x87.model.Operacao;
import br.pro.x87.model.TipoOperacao;
import br.pro.x87.model.Vinculo;
import br.pro.x87.model.VinculoFuncao;
import br.pro.x87.reports.RelatorioCustoReal;
import br.pro.x87.reports.RelatorioOperacao;
import br.pro.x87.validators.FloatFilter;
import br.pro.x87.view.command.ExcluirCommand;
import br.pro.x87.view.command.SalvarCommand;
import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.UtilDateModel;

/**
 * @author Juliano Rodrigo Lamb
 *
 */
public class OperacaoUI extends AncestorUI {

	private static final long serialVersionUID = 1L;

	private OperacaoDAO<Object> opDAO = new  OperacaoDAO<>();

	private Operacao operacao;
	private OperacaoMaquinaModel omm;

	private Maquina maquina;
	private List<Maquina> listaDeMaquinaComboBox;
	private List<Maquina> listaConjuntoMecanizado;
	private List<Empresa> listEmpresa;
	private List<VinculoFuncao> listVinculoFuncao;
	private VinculoFuncao vinculoFuncao;

	private Area area;
	private List<Area> listArea;

	private Funcionario funcionario;
	private List<Vinculo> listFuncionario;

	private TipoOperacao tipoOperacao;
	private List<TipoOperacao> listTipoOperacao;

	private UtilDateModel modelData;
	private JDatePanelImpl datePanelData;
	private JDatePickerImpl dpData;

	private JPanel pInicio;	
	private JTextField tfGraxa;
	private JTextField tfCombustivel;
	private JTextField tfArea;
	private JTextField tfDuracao;
	private JTextField tfHorimetro;
	private JTextField tfTipoMaquina;
	private JTextField tfVelocidade;
	private JTextField tflarguraOperacional;
	private JTextField tfEficiencia;
	private JTextField tfCapacidadeCampo;	
	private JComboBox<Object> comboFuncionario;	
	private JComboBox<Object> comboArea;
	private JComboBox<Object> comboOperacao;
	private JComboBox<Object> comboMaquina;
	private JComboBox<Object> comboEmpresa;
	private JComboBox<Object> comboFuncao;

	private JTextArea taObservacao;
	private JLabel lblHormetro;
	private JLabel lblCombustvelrl;
	private JLabel lblResponsvelFuncionrio;
	private JLabel lblDuraoh;
	private JLabel lblGraxarkg;
	private JLabel lblLocal;
	private JLabel lblOperao;
	private JLabel lblreaha;
	private JPanel pMeio;
	private JPanel pObservacao;	
	private JLabel lblNewLabel_2;
	private JTable gradeConjuntoMecanizado;
	private JButton btnRemover;
	private JButton btnAdicionar;	
	private JLabel lblFuno;	

	public VinculoFuncao getVinculoFuncao() {
		return vinculoFuncao;
	}

	public void setVinculoFuncao(VinculoFuncao vinculoFuncao) {
		this.vinculoFuncao = vinculoFuncao;
	}

	public OperacaoDAO<Object> getOpDAO() {
		return opDAO;
	}

	public void setOpDAO(OperacaoDAO<Object> opDAO) {
		this.opDAO = opDAO;
	}

	public List<Maquina> getListaConjuntoMecanizado() {
		return listaConjuntoMecanizado;
	}

	public void setListaConjuntoMecanizado(List<Maquina> listConjuntoMecanizado) {
		this.listaConjuntoMecanizado = listConjuntoMecanizado;
	}

	public Maquina getMaquina() {
		return maquina;
	}

	public void setMaquina(Maquina m) {
		this.maquina = m;
	}

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public TipoOperacao getTipoOperacao() {
		return tipoOperacao;
	}

	public void setTipoOperacao(TipoOperacao tipoOperacao) {
		this.tipoOperacao = tipoOperacao;
	}

	public Operacao getOperacao() {
		return operacao;
	}

	public void setOperacao(Operacao operacao) {
		this.operacao = operacao;
	}

	public OperacaoUI() {
		bListagem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (getOperacao() == null){
					mensagemErro("Não foi possível efetuar a impressão. Por favor selecione um registro na grade abaixo para prosseguir.");
					return;
				}					

				Object[] options = { "Individual", "Listagem", "Custo real" };
				int resposta = JOptionPane.showOptionDialog(null, "Qual relatório você deseja imprimir?"
						+ "\n\na. Individual: exibe um resumo da operação;"
						+ "\nb. Listagem: exibe uma lista das operações realizadas;"
						+ "\nc. Custo real: exibe o custo real da operação selecionada. "
						+ "Você deverá informar a quantidade de graxa utilizada, bem como o intervalor entre as lubrificações."
						+ "\n\nEscolha:", "Escolha",
						JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, 
						options, options[0]);

				switch (resposta) {
				case 0:
					RelatorioOperacao ro = new RelatorioOperacao(getOperacao());
					ro.imprimir(ro);					
					break;
				case 1:
					List<Operacao> lista = opDAO.imprimirOperacoes();				
					templateImprimir(lista, "operacao.jrxml");
					break;
				case 2:		
					float tempoLubrificacao = 0;
					float quantidadeGraxa = 0;

					while (true){
						String st = JOptionPane.showInputDialog(null, "Informe o tempo médio entre as lubrificações: (h)", 
								"Tempo entre lubrificações", JOptionPane.QUESTION_MESSAGE);
						try {
							tempoLubrificacao = converte(st);
							break;
						} catch (Exception e2) {
							mensagemErro("Por favor informe um número válido!");
						}
					}

					while (true){
						String st = JOptionPane.showInputDialog(null, "Informe a quantidade aproximada de graxa utilizada: (kg)", 
								"Quantidade de graxa", JOptionPane.QUESTION_MESSAGE);
						try {
							quantidadeGraxa = converte(st);
							break;
						} catch (Exception e2) {
							mensagemErro("Por favor informe um número válido!");
						}
					}

					RelatorioCustoReal rcr = new RelatorioCustoReal(getOperacao(), tempoLubrificacao, quantidadeGraxa);
					rcr.imprimir(rcr);
					break;
				default:
					break;
				}
			}
		});
		grade.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				super.mouseClicked(e);
				comboMaquina.setEnabled(true);
				popularMaquina();
				comboMaquina.setSelectedIndex(-1);
				tfTipoMaquina.setText("");
				btnAdicionar.setEnabled(false);
			}
		});

		comboMaquina.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if (comboMaquina.getSelectedIndex() != -1){
					btnAdicionar.setEnabled(true);
					tfTipoMaquina.setText(listaDeMaquinaComboBox.get(comboMaquina.getSelectedIndex()).getTipoMaquina());
				}
			}
		});

		gradeConjuntoMecanizado.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				setMaquina((Maquina) omm.getMaquina(gradeConjuntoMecanizado.getSelectedRow()));

				if (getMaquina() != null){					
					comboMaquina.setSelectedItem(getMaquina().getDescricao());				
					btnRemover.setEnabled(true);					
				}
			}
		});

		btnRemover.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {			
				removerMaquinaConjuntoMecanizado();
			}
		});

		btnAdicionar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				adicionarMaquinaConjuntoMecanizado();				
			}
		});

		comboEmpresa.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent arg0) {
				popularArea();
				popularFuncionario();
			}
		});

		comboFuncionario.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent arg0) {				
				popularFuncao();
			}
		});

		popularEmpresa();		
		popularTipoOperacao();
	}	

	@Override
	public void monteInterface(String s) {
		super.monteInterface("MaqControl | Registro de operações efetuadas");

		pInicio = new JPanel();
		JLabel lblData = new JLabel("Data");
		lblLocal = new JLabel("Local");
		comboArea = new JComboBox<>();

		modelData = new UtilDateModel();
		datePanelData = new JDatePanelImpl(modelData);	
		dpData = new JDatePickerImpl(datePanelData);	
		dpData.getJFormattedTextField().setPreferredSize(new Dimension(178, 20));
		dpData.setPreferredSize(new Dimension(202, 20));

		pInicio.setBounds(0, 0, 845, 40);
		pInicio.setLayout(new GridLayout(2, 4, 5, 0));

		dpData.getJFormattedTextField().setBackground(Color.WHITE);

		pInicio.add(lblData);
		lblOperao = new JLabel("Operação");
		pInicio.add(lblOperao);

		JLabel lblNewLabel_5 = new JLabel("Empresa\r\n");
		pInicio.add(lblNewLabel_5);
		pInicio.add(lblLocal);
		pInicio.add(dpData);	
		comboOperacao = new JComboBox<>();
		pInicio.add(comboOperacao);

		comboEmpresa = new JComboBox();
		pInicio.add(comboEmpresa);
		pInicio.add(comboArea);

		pMeio = new JPanel();
		lblHormetro = new JLabel("Horímetro (h)");
		lblreaha = new JLabel("Área (ha)");
		lblCombustvelrl = new JLabel("Combustível (R$/l)");
		lblDuraoh = new JLabel("Duração (h)");
		lblGraxarkg = new JLabel("Graxa (R$/kg)");
		JLabel lblVelocidadekmh = new JLabel("Velocidade (km/h)");
		tfHorimetro = new JTextField();
		tfArea = new JTextField();
		tfCombustivel = new JTextField();
		tfDuracao = new JTextField();
		tfGraxa = new JTextField();
		tfVelocidade = new JTextField();
		pObservacao = new JPanel();

		pMeio.setBounds(0, 95, 845, 45);		
		pMeio.setLayout(new GridLayout(2, 5, 5, 0));

		tfHorimetro.setColumns(10);		
		tfArea.setColumns(10);		
		tfCombustivel.setColumns(10);	
		tfDuracao.setColumns(10);		
		tfGraxa.setColumns(10);
		tfVelocidade.setColumns(10);	

		pMeio.add(lblHormetro);		
		pMeio.add(lblreaha);		
		pMeio.add(lblCombustvelrl);		
		pMeio.add(lblDuraoh);		
		pMeio.add(lblGraxarkg);		
		pMeio.add(lblVelocidadekmh);		
		pMeio.add(tfHorimetro);
		pMeio.add(tfArea);
		pMeio.add(tfCombustivel);
		pMeio.add(tfDuracao);
		pMeio.add(tfGraxa);		
		pMeio.add(tfVelocidade);			

		pObservacao.setBounds(0, 45, 845, 45);
		GridBagLayout gbl_pObservacao = new GridBagLayout();
		gbl_pObservacao.columnWidths = new int[]{120, 120, 600, 0};
		gbl_pObservacao.rowHeights = new int[]{22, 21, 0};
		gbl_pObservacao.columnWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_pObservacao.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		pObservacao.setLayout(gbl_pObservacao);
		lblResponsvelFuncionrio = new JLabel("Resp. / Funcionário");
		GridBagConstraints gbc_lblResponsvelFuncionrio = new GridBagConstraints();
		gbc_lblResponsvelFuncionrio.anchor = GridBagConstraints.SOUTH;
		gbc_lblResponsvelFuncionrio.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblResponsvelFuncionrio.insets = new Insets(0, 0, 5, 5);
		gbc_lblResponsvelFuncionrio.gridx = 0;
		gbc_lblResponsvelFuncionrio.gridy = 0;
		pObservacao.add(lblResponsvelFuncionrio, gbc_lblResponsvelFuncionrio);

		lblFuno = new JLabel("Função");
		GridBagConstraints gbc_lblFuno = new GridBagConstraints();
		gbc_lblFuno.anchor = GridBagConstraints.SOUTH;
		gbc_lblFuno.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblFuno.insets = new Insets(0, 0, 5, 5);
		gbc_lblFuno.gridx = 1;
		gbc_lblFuno.gridy = 0;
		pObservacao.add(lblFuno, gbc_lblFuno);
		JLabel lblNewLabel = new JLabel("Observação");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.anchor = GridBagConstraints.SOUTH;
		gbc_lblNewLabel.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel.gridx = 2;
		gbc_lblNewLabel.gridy = 0;
		pObservacao.add(lblNewLabel, gbc_lblNewLabel);

		JPanel pManageConjuntoMaquina = new JPanel();
		JPanel pConjuntoMecanizado = new JPanel();
		JLabel lblMaquinaImplemento = new JLabel("Máquina / Implemento");
		comboMaquina = new JComboBox<>();
		JLabel lblNewLabel_1 = new JLabel("Tipo");
		lblNewLabel_2 = new JLabel("\r\n");
		JLabel lblNewLabel_3 = new JLabel("");
		tfTipoMaquina = new JTextField();
		btnAdicionar = new JButton("Adicionar");		
		btnRemover = new JButton("Remover");

		pManageConjuntoMaquina.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder(" Gerenciar conjunto mecanizado "),
				BorderFactory.createEmptyBorder(5,5,5,5)));
		pManageConjuntoMaquina.setBounds(0, 300, 845, 205);		
		pManageConjuntoMaquina.setLayout(null);				

		tfTipoMaquina.setBackground(SystemColor.info);
		tfTipoMaquina.setEditable(false);
		tfTipoMaquina.setColumns(10);

		btnAdicionar.setEnabled(false);
		btnRemover.setEnabled(false);

		comboMaquina.setEnabled(false);

		pConjuntoMecanizado.setBounds(5, 15, 835, 40);		
		pConjuntoMecanizado.setLayout(new GridLayout(2, 4, 5, 0));		
		pConjuntoMecanizado.add(lblMaquinaImplemento);		
		pConjuntoMecanizado.add(lblNewLabel_1);		
		pConjuntoMecanizado.add(lblNewLabel_2);		
		pConjuntoMecanizado.add(lblNewLabel_3);
		pConjuntoMecanizado.add(comboMaquina);
		pConjuntoMecanizado.add(tfTipoMaquina);	
		pConjuntoMecanizado.add(btnAdicionar);
		pConjuntoMecanizado.add(btnRemover);		

		JPanel pParametrosOperacionais = new JPanel();
		pParametrosOperacionais.setBounds(5, 160, 835, 40);
		pManageConjuntoMaquina.add(pParametrosOperacionais);
		JLabel lblNewLabel_2_1 = new JLabel("Largura Operacional (m)");
		JLabel lblNewLabel_31 = new JLabel("Eficiência (%)");
		JLabel lblNewLabel_4 = new JLabel("Capacidade de campo (CCO)");
		tflarguraOperacional = new JTextField();
		tfEficiencia = new JTextField();
		tfCapacidadeCampo = new JTextField();

		tflarguraOperacional.setEditable(false);
		tflarguraOperacional.setBackground(SystemColor.info);		
		tflarguraOperacional.setColumns(10);

		tfEficiencia.setEditable(false);
		tfEficiencia.setBackground(SystemColor.info);		
		tfEficiencia.setColumns(10);

		tfCapacidadeCampo.setEditable(false);
		tfCapacidadeCampo.setBackground(SystemColor.info);
		tfCapacidadeCampo.setColumns(10);
		pParametrosOperacionais.setLayout(new GridLayout(0, 3, 5, 0));

		pParametrosOperacionais.add(lblNewLabel_2_1);		
		pParametrosOperacionais.add(lblNewLabel_31);
		pParametrosOperacionais.add(lblNewLabel_4);	
		pParametrosOperacionais.add(tflarguraOperacional);
		pParametrosOperacionais.add(tfEficiencia);
		pParametrosOperacionais.add(tfCapacidadeCampo);

		pManageConjuntoMaquina.add(pConjuntoMecanizado);

		P2.setBounds(0, 175, 845, 115);
		pBotoes.setBounds(0, 145, 845, 23);

		getContentPane().setLayout(null);
		getContentPane().add(pInicio);
		getContentPane().add(pMeio);
		getContentPane().add(pObservacao);
		comboFuncionario = new JComboBox<>();
		GridBagConstraints gbc_comboFuncionario = new GridBagConstraints();
		gbc_comboFuncionario.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboFuncionario.insets = new Insets(0, 0, 0, 5);
		gbc_comboFuncionario.gridx = 0;
		gbc_comboFuncionario.gridy = 1;
		pObservacao.add(comboFuncionario, gbc_comboFuncionario);

		comboFuncao = new JComboBox();
		GridBagConstraints gbc_comboFuncao = new GridBagConstraints();
		gbc_comboFuncao.insets = new Insets(0, 0, 0, 5);
		gbc_comboFuncao.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboFuncao.gridx = 1;
		gbc_comboFuncao.gridy = 1;
		pObservacao.add(comboFuncao, gbc_comboFuncao);
		taObservacao = new JTextArea();
		GridBagConstraints gbc_taObservacao = new GridBagConstraints();
		gbc_taObservacao.fill = GridBagConstraints.BOTH;
		gbc_taObservacao.gridx = 2;
		gbc_taObservacao.gridy = 1;
		pObservacao.add(taObservacao, gbc_taObservacao);		
		getContentPane().add(pManageConjuntoMaquina);

		JPanel panel_3 = new JPanel();
		panel_3.setBounds(5, 60, 835, 94);
		pManageConjuntoMaquina.add(panel_3);
		gradeConjuntoMecanizado = new JTable();
		JScrollPane scrollPane = new JScrollPane(gradeConjuntoMecanizado);

		gradeConjuntoMecanizado.setCursor(new Cursor(Cursor.HAND_CURSOR));
		panel_3.setLayout(new BorderLayout(0, 0));

		panel_3.add(scrollPane);

		setResizable(false);		
		setBounds(100, 100, 850, 531);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}

	@Override
	public boolean templateValidar() {	
		Date data = null;		 

		try {
			data = (Date) dpData.getModel().getValue();				
		} catch (Exception e) {
			// TODO: handle exception
		}	

		if ((data == null)){		
			mensagemErro("A data de realização da operação deve ser informada!");
			return false;
		}		

		if (!validarFlutuantes())
			return false;	

		if (!validarCombos())
			return false;	

		return true;	
	}

	@Override
	public void executarCommandExcluir() {
		new ExcluirCommand().execute(getOperacao());		
	}

	@Override
	public void executarCommandSalvar() {
		new SalvarCommand().execute(getOperacao());	
	}

	@Override
	public void atualizarGrade() {
		atualizarGrade("Operacao", Operacao.class);		
	}

	@Override
	public void carregarValores() {
		setOperacao((Operacao) lista.get(grade.getSelectedRow()));		

		taObservacao.setText(getOperacao().getObservao());

		popularEmpresa();

		comboEmpresa.setSelectedItem(getOperacao().getVinculoFuncao().getVinculo().getEmpresa().getNomeFantasia());
		popularArea();
		popularFuncionario();

		comboFuncionario.setSelectedItem(getOperacao().getVinculoFuncao().getVinculo().getFuncionario().getNome());
		popularFuncao();

		comboArea.setSelectedItem(getOperacao().getArea());
		comboFuncao.setSelectedItem(getOperacao().getVinculoFuncao().getFuncao().getDescricao());

		comboOperacao.setSelectedItem(getOperacao().getTipoOperacao().getDescricao());

		tfArea.setText(getOperacao().getTamanhoAreaFormatado());
		tfCombustivel.setText(getOperacao().getCombustivelFormatado());
		tfDuracao.setText(getOperacao().getDuracaoFormatado());
		tfGraxa.setText(getOperacao().getGraxaFormatado());
		tfHorimetro.setText(getOperacao().getHorimetroFormatado());
		tfVelocidade.setText(getOperacao().getVelocidadeFormatado());		

		carregarDataDateTimePicker(getOperacao().getData(), modelData);		

		setListaConjuntoMecanizado(getOpDAO().getConjuntoMecanizado(getOperacao()));
		loadParametrosOperacao();
	}

	@Override
	public void definirFoco() {
		dpData.requestFocus();
	}

	@Override
	public void habilitarCampos() {
		dpData.setEnabled(true);
		comboOperacao.setEnabled(true);
		comboFuncionario.setEnabled(true);
		comboOperacao.setEnabled(true);
		tfArea.setEnabled(true);
		tfCombustivel.setEnabled(true);
		tfDuracao.setEnabled(true);
		tfGraxa.setEnabled(true);
		tfHorimetro.setEnabled(true);
		tfVelocidade.setEnabled(true);
		taObservacao.setEnabled(true);
	}

	@Override
	public void desabiltarCampos() {
		dpData.setEnabled(false);
		comboOperacao.setEnabled(false);
		comboFuncionario.setEnabled(false);
		comboOperacao.setEnabled(false);
		tfArea.setEnabled(false);
		tfCombustivel.setEnabled(false);
		tfDuracao.setEnabled(false);
		tfGraxa.setEnabled(false);
		tfHorimetro.setEnabled(false);
		tfVelocidade.setEnabled(false);
		taObservacao.setEnabled(false);
	}

	@Override
	public void carregarValoresAlterar() {
		getOperacao().setArea(getArea());		
		getOperacao().setVinculoFuncao(getVinculoFuncao());
		getOperacao().setTipoOperacao(tipoOperacao);		
		getOperacao().setData((Date) dpData.getModel().getValue()); 
		getOperacao().setObservao(taObservacao.getText());

		try {
			getOperacao().setTamanhoArea(converte(tfArea.getText()));
			getOperacao().setDuracao(converte(tfDuracao.getText())); 
			getOperacao().setHorimetro(converte(tfHorimetro.getText())); 
			getOperacao().setCombustivel(converte(tfCombustivel.getText())); 
			getOperacao().setGraxa(converte(tfGraxa.getText()));
			getOperacao().setVelocidade(converte(tfVelocidade.getText()));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}

	@Override
	public void carregarValoresSalvar() {
		try {
			setOperacao(new Operacao(getArea(), getTipoOperacao(), 
					converte(tfArea.getText()), (Date) dpData.getModel().getValue(), taObservacao.getText(), 
					converte(tfDuracao.getText()), converte(tfHorimetro.getText()), 
					converte(tfCombustivel.getText()), converte(tfGraxa.getText()), converte(tfVelocidade.getText()), 
					getVinculoFuncao()));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}

	@Override
	public void carregarValoresObjetos() {
		setVinculoFuncao(listVinculoFuncao.get(comboFuncao.getSelectedIndex()));
		setArea((Area) listArea.get(comboArea.getSelectedIndex()));
		setFuncionario(listFuncionario.get(comboFuncionario.getSelectedIndex()).getFuncionario());
		setTipoOperacao((TipoOperacao) listTipoOperacao.get(comboOperacao.getSelectedIndex()));	
	}

	private void popularArea(){
		comboArea.removeAllItems();

		listArea = getDao().filter("Area", "empresa.nomeFantasia", (String) comboEmpresa.getSelectedItem());

		for (Object o : listArea) {
			Area a = (Area) o;
			comboArea.addItem(a.getDescricao());
		}
	}

	public void popularFuncionario(){		
		comboFuncionario.removeAllItems();
		listFuncionario = getDao().filter("Vinculo", "empresa.nomeFantasia", (String) comboEmpresa.getSelectedItem());

		for (Object o : listFuncionario) {	
			Vinculo v = (Vinculo) o;			
			comboFuncionario.addItem(v.getFuncionario().getNome());
		}
	}

	public void popularFuncao(){
		comboFuncao.removeAllItems();
		listVinculoFuncao = getDao().filter("VinculoFuncao", "vinculo.funcionario.nome", (String) comboFuncionario.getSelectedItem());

		for (Object o : listVinculoFuncao) {	
			VinculoFuncao vf = (VinculoFuncao) o;			
			comboFuncao.addItem(vf.getFuncao().getDescricao());
		}
	}

	private void popularTipoOperacao(){
		comboOperacao.removeAllItems();
		listTipoOperacao = getDao().get("TipoOperacao");

		for (Object o : listTipoOperacao) {
			TipoOperacao t = (TipoOperacao) o;
			comboOperacao.addItem(t.getDescricao());
		}
	}

	public void popularMaquina(){		
		comboMaquina.removeAllItems();
		listaDeMaquinaComboBox = getDao().get("Maquina");

		for (Object o : listaDeMaquinaComboBox) {	
			Maquina m = (Maquina) o;
			comboMaquina.addItem(m.getDescricao());
		}
	}

	public void popularEmpresa(){		
		comboEmpresa.removeAllItems();
		listEmpresa = getDao().get("Empresa");

		for (Object o : listEmpresa) {	
			Empresa e = (Empresa) o;
			comboEmpresa.addItem(e.getNomeFantasia());
		}
	}

	public void adicionarMaquinaConjuntoMecanizado(){
		if (!validaEsteCombo(comboMaquina, "MÁQUINA/IMPLEMENTO"))
			return;			

		setMaquina(listaDeMaquinaComboBox.get(comboMaquina.getSelectedIndex()));	

		if (!getOpDAO().adicionarConjuntoMecanizado(getMaquina(), getOperacao())){
			mensagemErro("A máquina selecionada já foi adicionada ao conjunto mecanizado!");			
			return;
		}							

		setListaConjuntoMecanizado(getOpDAO().getConjuntoMecanizado(getOperacao()));
		loadParametrosOperacao();

		gradeConjuntoMecanizado.repaint();
	}

	public void removerMaquinaConjuntoMecanizado(){

		if (gradeConjuntoMecanizado.getSelectedRow() == -1){
			mensagemErro("Selecione uma máquina para exclusão! A operação não foi completada");
			return;
		}

		setMaquina(omm.getMaquina(gradeConjuntoMecanizado.getSelectedRow()));	

		getOpDAO().removerConjuntoMecanizado(getMaquina(), getOperacao());

		setListaConjuntoMecanizado(getOpDAO().getConjuntoMecanizado(getOperacao()));
		loadParametrosOperacao();

		gradeConjuntoMecanizado.repaint();
	}

	public void loadParametrosOperacao(){
		if (getOperacao() != null){			
			omm = new OperacaoMaquinaModel(getOpDAO().getConjuntoMecanizado(getOperacao()));
			gradeConjuntoMecanizado.setModel(omm);		
			gradeConjuntoMecanizado.repaint();		
		}

		tflarguraOperacional.setText(getDf().format(getOpDAO().getLarguraOperacional(getOperacao())));
		tfEficiencia.setText(getDf().format(getOpDAO().getEficiencia(getOperacao())));
		tfCapacidadeCampo.setText(getDf().format(getOpDAO().getCCO(getOperacao())));
	}

	@Override
	public void adicionaListenerCampoVazio() {
		tfHorimetro.addKeyListener(createKeyListenerCampoVazio(tfHorimetro));
		tfArea.addKeyListener(createKeyListenerCampoVazio(tfArea));
		tfCombustivel.addKeyListener(createKeyListenerCampoVazio(tfCombustivel));
		tfDuracao.addKeyListener(createKeyListenerCampoVazio(tfDuracao));
		tfGraxa.addKeyListener(createKeyListenerCampoVazio(tfGraxa));
		tfVelocidade.addKeyListener(createKeyListenerCampoVazio(tfVelocidade));
		taObservacao.addKeyListener(createKeyListenerCampoVazio(taObservacao));
	}

	@Override
	public boolean validarFlutuantes() {
		if (!validaEsteFloat(tfHorimetro, "HORÍMETRO"))
			return false;

		if (!validaEsteFloat(tfArea, "ÁREA"))
			return false;

		if (!validaEsteFloat(tfCombustivel, "COMBUSTÍVEL"))
			return false;

		if (!validaEsteFloat(tfDuracao, "DURAÇÃO"))
			return false;

		if (!validaEsteFloat(tfGraxa, "GRAXA"))
			return false;

		if (!validaEsteFloat(tfVelocidade, "VELOCIDADE"))
			return false;
		return true;
	}

	@Override
	public boolean validarCombos() {		
		if (!validaEsteCombo(comboArea, "ÁREA"))
			return false;

		if (!validaEsteCombo(comboOperacao, "OPERAÇÃO"))
			return false;

		if (!validaEsteCombo(comboFuncionario, "FUNCIONÁRIO"))
			return false;

		return true;
	}

	@Override
	public void addDocument() {
		tfHorimetro.setDocument(new FloatFilter(FloatFilter.FLOAT));	
		tfArea.setDocument(new FloatFilter(FloatFilter.FLOAT));
		tfCombustivel.setDocument(new FloatFilter(FloatFilter.FLOAT));
		tfDuracao.setDocument(new FloatFilter(FloatFilter.FLOAT));
		tfGraxa.setDocument(new FloatFilter(FloatFilter.FLOAT));
		tfVelocidade.setDocument(new FloatFilter(FloatFilter.FLOAT));	
	}
}
