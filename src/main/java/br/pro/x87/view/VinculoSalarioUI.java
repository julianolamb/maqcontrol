/**
 * 
 */
package br.pro.x87.view;

import java.awt.GridLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import br.pro.x87.model.Cidade;
import br.pro.x87.model.CotacaoProduto;
import br.pro.x87.model.Funcionario;
import br.pro.x87.model.Vinculo;
import br.pro.x87.model.VinculoFuncao;
import br.pro.x87.model.VinculoSalario;
import br.pro.x87.validators.FloatFilter;
import br.pro.x87.view.command.ExcluirCommand;
import br.pro.x87.view.command.SalvarCommand;
import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.UtilDateModel;
import java.awt.Dimension;
import java.awt.Color;

/**
 * @author jrlamb
 *
 */
public class VinculoSalarioUI extends AncestorUI {

	private static final long serialVersionUID = 1L;

	private VinculoSalario vinculoSalario;
	private VinculoFuncao vinculoFuncao;
	private Funcionario funcionario;
	private Vinculo vinculo;
	private Date data;

	private List<Funcionario> listFuncionario;
	private List<Vinculo> listVinculo;
	private List<VinculoFuncao> listVinculoFuncao;

	private UtilDateModel modelData;
	private JDatePanelImpl datePanel;
	private JDatePickerImpl dpData;
	private JTextField tfSalario;	
	private JComboBox<Object> comboFuncionario; 
	private JComboBox<Object> comboVinculo;
	private JComboBox<Object> comboFuncao;	

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public VinculoFuncao getVinculoFuncao() {
		return vinculoFuncao;
	}

	public void setVinculoFuncao(VinculoFuncao vinculoFuncao) {
		this.vinculoFuncao = vinculoFuncao;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public Vinculo getVinculo() {
		return vinculo;
	}

	public void setVinculo(Vinculo vinculo) {
		this.vinculo = vinculo;
	}

	public VinculoSalario getVinculoSalario() {
		return vinculoSalario;
	}

	public void setVinculoSalario(VinculoSalario vinculoSalario) {
		this.vinculoSalario = vinculoSalario;
	}

	public VinculoSalarioUI() {		
		comboFuncionario.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent arg0) {
				popularVinculo();
			}
		});
		
		comboVinculo.addItemListener(new ItemListener() {			
			@Override
			public void itemStateChanged(ItemEvent e) {
				popularFuncao();				
			}
		});
		
		popularFuncionario();
	}	

	@Override
	public void monteInterface(String s) {
		super.monteInterface("MaqControl | Reajuste de salário de um funcionário");		

		modelData = new UtilDateModel();		
		datePanel = new JDatePanelImpl(modelData);
		JPanel panel = new JPanel();
		JLabel lblNewLabel = new JLabel("Funcionário");		
		JLabel lblNewLabel_2 = new JLabel("Vínculo");
		JLabel lblNewLabel_1 = new JLabel("Função");
		comboFuncionario = new JComboBox<>();
		comboVinculo = new JComboBox<>();
		comboFuncao = new JComboBox<>();
		JPanel panel_1 = new JPanel();
		JLabel lblNewLabel_3 = new JLabel("Data de alteração");
		JLabel lblNewLabel_4 = new JLabel("Novo salário (R$)");
		dpData = new JDatePickerImpl(datePanel);
		tfSalario = new JTextField();
		tfSalario.setColumns(10);
		
		panel.setBounds(0, 0, 695, 40);		
		panel.setLayout(new GridLayout(2, 0, 10, 0));
		
		panel.add(lblNewLabel);		
		panel.add(lblNewLabel_2);		
		panel.add(lblNewLabel_1);		
		panel.add(comboFuncionario);		
		panel.add(comboVinculo);		
		panel.add(comboFuncao);

		getContentPane().setLayout(null);
		getContentPane().add(panel);
		getContentPane().add(panel_1);
		
		dpData.getJFormattedTextField().setBackground(Color.WHITE);
		dpData.setForeground(Color.BLACK);
		dpData.setBackground(Color.WHITE);
		dpData.getJFormattedTextField().setPreferredSize(new Dimension(178, 20));
		dpData.setPreferredSize(new Dimension(202, 20));
		
		panel_1.setBounds(0, 40, 695, 40);		
		panel_1.setLayout(new GridLayout(2, 0, 10, 0));
		
		panel_1.add(lblNewLabel_3);		
		panel_1.add(lblNewLabel_4);		
		panel_1.add(dpData);		
		panel_1.add(tfSalario);
				
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 700, 294);
		P2.setBounds(0, 115, 695, 150);		
		pBotoes.setBounds(0, 85, 695, 23);		
	}

	public void popularFuncionario(){		
		comboFuncionario.removeAllItems();
		listFuncionario = getDao().get("Funcionario");

		for (Object o : listFuncionario) {	
			Funcionario f = (Funcionario) o;
			comboFuncionario.addItem(f.getNome());
		}
	}
	
	public void popularVinculo(){
		if (comboFuncionario.getSelectedIndex() == -1)
			return;		
		
		comboVinculo.removeAllItems();
		listVinculo = getDao().filter("Vinculo", "funcionario.nome", (String) comboFuncionario.getSelectedItem());

		for (Object o : listVinculo) {	
			Vinculo c = (Vinculo) o;
			comboVinculo.addItem(c.getEmpresa().getNomeFantasia());
		}
	}
	
	public void popularFuncao(){
		if (comboVinculo.getSelectedIndex() == -1)
			return;
				
		comboFuncao.removeAllItems();
		listVinculoFuncao = getDao().filter("VinculoFuncao", "vinculo.empresa.nomeFantasia", (String) comboVinculo.getSelectedItem());

		for (Object o : listVinculoFuncao) {	
			VinculoFuncao c = (VinculoFuncao) o;
			comboFuncao.addItem(c.getDescricao());
		}
	}

	
	@Override
	public void executarCommandExcluir() {
		new ExcluirCommand().execute(getVinculoSalario());
	}

	@Override
	public void executarCommandSalvar() {
		new SalvarCommand().execute(getVinculoSalario());		
	}

	@Override
	public void atualizarGrade() {
		atualizarGrade("VinculoSalario", VinculoSalario.class);		
	}

	/* (non-Javadoc)
	 * @see br.pro.x87.view.AncestorUI#carregarValores()
	 */
	@Override
	public void carregarValores() {
		setVinculoSalario((VinculoSalario) lista.get(grade.getSelectedRow()));
				
		carregarDataDateTimePicker(getVinculoSalario().getDataAlteracao(), modelData);
		tfSalario.setText(getDf().format(getVinculoSalario().getNovoSalario()));

		popularFuncionario();
		comboFuncionario.setSelectedItem(getVinculoSalario().getNomeFuncionario());
		
		popularVinculo();
		comboVinculo.setSelectedItem(getVinculoSalario().getNomeEmpresa());
		
		popularFuncao();
		comboFuncao.setSelectedItem(getVinculoSalario().getNomeFuncao());
	}

	@Override
	public void definirFoco() {
		comboFuncionario.requestFocus();
	}

	@Override
	public void habilitarCampos() {
		comboFuncao.setEnabled(true);
		comboFuncionario.setEnabled(true);
		comboVinculo.setEnabled(true);
		dpData.setEnabled(true);
		tfSalario.setEnabled(true);
	}

	@Override
	public void desabiltarCampos() {
		comboFuncao.setSelectedIndex(-1);
		comboFuncionario.setSelectedIndex(-1);
		comboVinculo.setSelectedIndex(-1);
		
		comboFuncao.setEnabled(false);
		comboFuncionario.setEnabled(false);
		comboVinculo.setEnabled(false);
		dpData.setEnabled(false);
		tfSalario.setEnabled(false);
	}

	@Override
	public void carregarValoresAlterar() {
		getVinculoSalario().setDataAlteracao(getData());
		getVinculoSalario().setVinculoFuncao(getVinculoFuncao());

		try {
			getVinculoSalario().setNovoSalario(converte(tfSalario.getText()));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}

	@Override
	public void carregarValoresSalvar() {
		try {
			setVinculoSalario(new VinculoSalario(getVinculoFuncao(), getData(), converte(tfSalario.getText())));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void carregarValoresObjetos() {
		setFuncionario(listFuncionario.get(comboFuncionario.getSelectedIndex()));
		setVinculo(listVinculo.get(comboVinculo.getSelectedIndex()));
		setVinculoFuncao(listVinculoFuncao.get(comboFuncao.getSelectedIndex()));

		setData((Date) dpData.getModel().getValue());
	}

	@Override
	public void adicionaListenerCampoVazio() {
		tfSalario.addKeyListener(createKeyListenerCampoVazio(tfSalario));		
	}

	@Override
	public boolean validarFlutuantes() {
		if (!validaEsteFloat(tfSalario, "SALÁRIO"))		
			return false;
		
		return true;
	}

	@Override
	public boolean validarCombos() {

		if (!validaEsteCombo(comboFuncao, "FUNÇÃO"))
			return false;
		
		if (!validaEsteCombo(comboFuncionario, "FUNCIONÁRIO"))
			return false;
		
		if (!validaEsteCombo(comboVinculo, "VÍNCULO"))
			return false;
		
		return true;
	}

	@Override
	public void addDocument() {
		tfSalario.setDocument(new FloatFilter(FloatFilter.FLOAT));
	}
}
