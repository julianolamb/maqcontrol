/**
 * 
 */
package br.pro.x87.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.GridLayout;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import br.pro.x87.controller.ServicosModel;
import br.pro.x87.model.Funcionario;
import br.pro.x87.model.Manutencao;
import br.pro.x87.model.Maquina;
import br.pro.x87.model.Operacao;
import br.pro.x87.model.Produto;
import br.pro.x87.model.Servicos;
import br.pro.x87.model.TipoServico;
import br.pro.x87.validators.FloatFilter;
import br.pro.x87.view.command.ExcluirCommand;
import br.pro.x87.view.command.SalvarCommand;
import br.pro.x87.view.state.AtualizarUI;
import br.pro.x87.view.state.ExcluirUI;
import br.pro.x87.view.state.NovoUI;
import br.pro.x87.view.state.SalvarUI;
import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.UtilDateModel;

/**
 * @author JULIANO RODRIGO LAMB
 * @since 24 fev. 2018
 */
public class ManutencaoUI extends AncestorUI {

	private static final long serialVersionUID = 1L;

	private Manutencao manutencao;
	private Servicos servicos;
	private ServicosModel sm;

	private List<Funcionario> listFuncionario;
	private List<Maquina> listMaquina;
	private List<Operacao> listOperacao;
	private List<Produto> listProduto;
	private List<TipoServico> listTipoServico;

	private Funcionario funcionario;
	private Maquina maquina;	
	private Operacao operacao;		
	private Produto produto;	
	private TipoServico tipoServico;	

	private UtilDateModel modelInicio;
	private JDatePanelImpl datePanelInicio;
	private JDatePickerImpl dpDataInicio;
	private Date dataInicio;

	private UtilDateModel modelTermino;
	private JDatePanelImpl datePanelTermino;
	private JDatePickerImpl dpDataTermino;
	private Date dataTermino;

	private JPanel pInformacoesGerais;
	private JLabel lblOperaoRelacionada;
	private JComboBox<Object> comboFuncionario;
	private JComboBox<Object> comboMaquina;
	private JComboBox<Object> comboOperacao;
	private JComboBox<Object> comboProduto;
	private JComboBox<Object> comboTipoServico;

	private JRadioButton rbCorretiva;
	private JRadioButton rbRotina;
	private JTextField tfHorimetro;	
	private JPanel panel1;
	private JPanel panel2;
	private JTextField tfValorUnitario;
	private JTextField tfTotal;
	private JTextField tfQuantidade;
	private JTable gradeServico;
	private JTextArea taObservacaoManutencao;	
	private JTextArea taObservacaoServico;

	private JButton btnNovo;
	private JButton btnSalvar;
	private JButton btnExcluir;
	private JButton btnListagem;
	private JButton btnFechar = new JButton();

	private boolean updateServicos = false;

	public boolean isUpdateServicos() {
		return updateServicos;
	}

	public void setUpdateServicos(boolean updateServicos) {
		this.updateServicos = updateServicos;
	}

	public ServicosModel getSm() {
		return sm;
	}

	public void setSm(ServicosModel sm) {
		this.sm = sm;
	}

	public Servicos getServicos() {
		return servicos;
	}

	public void setServicosSalvar(Servicos servicos) {
		this.servicos = servicos;
	}

	public void setServicosAlterar(Servicos servicos) {
		this.servicos = servicos;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public TipoServico getTipoServico() {
		return tipoServico;
	}

	public void setTipoServico(TipoServico tipoServico) {
		this.tipoServico = tipoServico;
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Date getDataTermino() {
		return dataTermino;
	}

	public void setDataTermino(Date dataTermino) {
		this.dataTermino = dataTermino;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public Maquina getMaquina() {
		return maquina;
	}

	public void setMaquina(Maquina maquina) {
		this.maquina = maquina;
	}

	public Operacao getOperacao() {
		return operacao;
	}

	public void setOperacao(Operacao operacao) {
		this.operacao = operacao;
	}

	public Manutencao getManutencao() {
		return manutencao;
	}

	public void setManutencao(Manutencao manutencao) {
		this.manutencao = manutencao;
	}

	public ManutencaoUI() {
		setResizable(false);
		grade.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				super.mouseClicked(e);
				clicarGridManutencao();
			}
		});

		tfQuantidade.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void removeUpdate(DocumentEvent e) {
				calcularTotalServicos();				
			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				calcularTotalServicos();
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				calcularTotalServicos();				
			}
		});

		tfValorUnitario.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void removeUpdate(DocumentEvent e) {
				calcularTotalServicos();				
			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				calcularTotalServicos();
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				calcularTotalServicos();				
			}
		});

		btnNovo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				novoServicos();
			}
		});

		btnSalvar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				salvarServicos();
			}
		});

		btnSalvar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {

			}
		});

		btnExcluir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				excluirServicos();
			}
		});

		gradeServico.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				clicarGridServicos();
			}
		});

		popularMaquina();
		popularFuncionario();
	}	

	@Override
	public void monteInterface(String s) {	
		super.monteInterface("MaqControl | Registro de manutenção");

		pInformacoesGerais = new JPanel();		
		panel1 = new JPanel();
		tfHorimetro = new JTextField();
		JLabel lblNewLabel = new JLabel("Data de início");		
		JLabel lblFim = new JLabel("Data final");
		JLabel lblHorasHormetro = new JLabel("Horas horímetro / Odômetro");
		lblOperaoRelacionada = new JLabel("Operação relacionada");
		lblOperaoRelacionada.setEnabled(false);
		comboOperacao = new JComboBox<>();

		modelInicio = new UtilDateModel();
		datePanelInicio = new JDatePanelImpl(modelInicio);
		dpDataInicio = new JDatePickerImpl(datePanelInicio);		
		modelTermino = new UtilDateModel();
		datePanelTermino = new JDatePanelImpl(modelTermino);				
		dpDataTermino = new JDatePickerImpl(datePanelTermino);

		pInformacoesGerais.setBounds(0, 0, 794, 140);	
		pInformacoesGerais.setLayout(null);		
		setPanelTitleAndBorder(pInformacoesGerais, " Informações gerais ");		

		tfHorimetro.setColumns(10);

		panel1.setBounds(0, 0, 795, 40);
		panel1.setLayout(new GridLayout(2, 4, 10, 0));			

		dpDataInicio.getJFormattedTextField().setBackground(Color.WHITE);		
		dpDataTermino.getJFormattedTextField().setBackground(Color.WHITE);

		comboOperacao.setEnabled(false);

		panel1.add(lblNewLabel);		
		panel1.add(lblFim);				
		panel1.add(lblHorasHormetro);		
		panel1.add(lblOperaoRelacionada);		
		panel1.add(dpDataInicio);
		panel1.add(dpDataTermino);		
		panel1.add(tfHorimetro);
		panel1.add(comboOperacao);				

		panel2 = new JPanel();
		JLabel lblNewLabel_2 = new JLabel("Responsável/Funcionário");
		JLabel lblNewLabel_1 = new JLabel("Máquina/implemento");
		comboFuncionario = new JComboBox<>();
		comboMaquina = new JComboBox<>();

		panel2.setBounds(0, 41, 795, 40);		
		panel2.setLayout(new GridLayout(2, 3, 10, 0));

		panel2.add(lblNewLabel_2);
		panel2.add(lblNewLabel_1);		
		panel2.add(comboFuncionario);		
		panel2.add(comboMaquina);

		JPanel panel3 = new JPanel();
		JPanel pJustificativa =  new JPanel();
		ButtonGroup group = new ButtonGroup();
		RadioButtonHandler handler = new RadioButtonHandler();
		rbRotina = new JRadioButton("Rotina");
		rbCorretiva = new JRadioButton("Corretiva");

		panel3.setBounds(0, 85, 795, 50);		
		panel3.setLayout(new GridLayout(0, 2, 0, 0));		

		pJustificativa.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder(" Justificativa da manutenção "),
				BorderFactory.createEmptyBorder(5,5,5,5)));
		pJustificativa.setLayout(new GridLayout(1, 2, 0, 0));

		panel3.add(pJustificativa);

		rbRotina.setSelected(true);
		rbRotina.addItemListener(handler);
		rbCorretiva.addItemListener(handler);
		group.add(rbRotina);						
		group.add(rbCorretiva);

		pJustificativa.add(rbCorretiva);		
		pJustificativa.add(rbRotina);

		pInformacoesGerais.add(panel1);
		pInformacoesGerais.add(panel2);
		pInformacoesGerais.add(panel3);
		getContentPane().add(pInformacoesGerais);

		JPanel pObservacao = new JPanel();
		panel3.add(pObservacao);
		pObservacao.setLayout(new GridLayout(2, 1, 0, 0));

		JLabel lblObservao = new JLabel("Observação");
		taObservacaoManutencao = new JTextArea();
		taObservacaoManutencao.setLineWrap(true);

		pObservacao.add(lblObservao);		
		pObservacao.add(taObservacaoManutencao);

		JLabel lblTituloServico = new JLabel("Tarefas executadas na manutenção selecionada");
		lblTituloServico.setHorizontalAlignment(SwingConstants.CENTER);
		lblTituloServico.setOpaque(true);
		lblTituloServico.setBackground(SystemColor.info);
		lblTituloServico.setBounds(0, 270, 795, 14);
		getContentPane().add(lblTituloServico);

		JPanel panel4 = new JPanel();
		panel4.setBounds(0, 288, 795, 40);
		getContentPane().add(panel4);
		panel4.setLayout(new GridLayout(0, 5, 10, 0));

		JLabel lblNewLabel_4 = new JLabel("Serviço");
		JLabel lblProduto = new JLabel("Peça/Produto");
		JLabel lblValorUnitario = new JLabel("Valor Unitário (R$)");
		JLabel lblQuantidade = new JLabel("Quantidade");
		JLabel lblNewLabel_5 = new JLabel("Total");
		comboTipoServico = new JComboBox<>();
		comboProduto = new JComboBox<>();
		tfValorUnitario = new JTextField();

		tfValorUnitario.setColumns(10);

		tfQuantidade = new JTextField();		
		tfQuantidade.setColumns(10);
		tfTotal = new JTextField();
		tfTotal.setEnabled(false);
		tfTotal.setBackground(SystemColor.info);
		tfTotal.setEditable(false);		
		tfTotal.setColumns(10);

		panel4.add(lblNewLabel_4);		
		panel4.add(lblProduto);		
		panel4.add(lblValorUnitario);		
		panel4.add(lblQuantidade);		
		panel4.add(lblNewLabel_5);		
		panel4.add(comboTipoServico);		
		panel4.add(comboProduto);
		panel4.add(tfValorUnitario);
		panel4.add(tfQuantidade);
		panel4.add(tfTotal);	

		JPanel panel5 = new JPanel();
		JLabel lblObservao_1 = new JLabel("Observação");
		taObservacaoServico = new JTextArea();
		JPanel pBotoesServico = new JPanel();
		btnNovo = new JButton("Novo");
		btnSalvar = new JButton("Salvar");		
		btnSalvar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnListagem = new JButton("Listagem");
		btnExcluir = new JButton("Excluir");
		JPanel pGradeServico = new JPanel();
		gradeServico = new JTable();
		JScrollPane scrollPaneServico = new JScrollPane(gradeServico);

		panel5.setBounds(0, 328, 794, 40);		
		panel5.setLayout(new GridLayout(2, 0, 0, 0));
		panel5.add(lblObservao_1);
		panel5.add(taObservacaoServico);

		pBotoesServico.setBounds(3, 379, 163, 131);
		pBotoesServico.setLayout(new GridLayout(4, 1, 1, 2));					

		btnNovo.setEnabled(false);		
		btnSalvar.setEnabled(false);		
		btnListagem.setEnabled(false);
		btnExcluir.setEnabled(false);

		pBotoesServico.add(btnNovo);		
		pBotoesServico.add(btnSalvar);		
		pBotoesServico.add(btnListagem);		
		pBotoesServico.add(btnExcluir);

		pGradeServico.setBounds(170, 379, 625, 131);
		pGradeServico.setLayout(new BorderLayout(0, 0));
		gradeServico.setCursor(new Cursor(Cursor.HAND_CURSOR));		

		pGradeServico.add(scrollPaneServico);

		P2.setBounds(0, 167, 794, 100);
		pBotoes.setBounds(0, 140, 794, 23);

		getContentPane().setLayout(null);
		getContentPane().add(panel5);
		getContentPane().add(pBotoesServico);
		getContentPane().add(pGradeServico);

		setSize(800,545);		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}

	@Override
	public boolean templateValidar() {		
		if (!validarPasso1())
			return false;

		return true;	
	}

	@Override
	public void executarCommandExcluir() {
		new ExcluirCommand().execute(getManutencao());
	}

	@Override
	public void executarCommandSalvar() {
		new SalvarCommand().execute(getManutencao());
	}

	@Override
	public void atualizarGrade() {
		atualizarGrade("Manutencao", Manutencao.class);
	}

	@Override
	public void carregarValores() {
		setManutencao((Manutencao) lista.get(grade.getSelectedRow()));			

		carregarDataDateTimePicker(getManutencao().getInicio(), modelInicio);
		carregarDataDateTimePicker(getManutencao().getFim(), modelTermino);
		
		tfHorimetro.setText(getManutencao().getHoraMaquinaFormatado());

		if (getManutencao().getOperacao() != null){
			comboOperacao.setSelectedItem(getManutencao().getOperacao().getDescricaoOperacao());
			rbCorretiva.setSelected(true);
		} else {			
			rbRotina.setSelected(true);
		}

		comboFuncionario.setSelectedItem(getManutencao().getFuncionario().getNome());
		comboMaquina.setSelectedItem(getManutencao().getMaquina().getDescricao());
		taObservacaoManutencao.setText(getManutencao().getObservacao());	}

	@Override
	public void definirFoco() {
		dpDataInicio.requestFocus();
	}

	@Override
	public void habilitarCampos() {		
		dpDataInicio.setEnabled(true);
		dpDataTermino.setEnabled(true);
		tfHorimetro.setEnabled(true);
		comboMaquina.setEnabled(true);
		comboFuncionario.setEnabled(true);
		rbCorretiva.setEnabled(true);
		rbRotina.setEnabled(true);
		taObservacaoManutencao.setEnabled(true);
	}

	@Override
	public void desabiltarCampos() {
		dpDataInicio.setEnabled(false);
		dpDataTermino.setEnabled(false);
		tfHorimetro.setEnabled(false);
		comboMaquina.setEnabled(false);
		comboFuncionario.setEnabled(false);
		rbCorretiva.setEnabled(false);
		rbRotina.setEnabled(false);
		taObservacaoManutencao.setEnabled(false);
	}

	@Override
	public void carregarValoresAlterar() {
		getManutencao().setMaquina(getMaquina());
		getManutencao().setFuncionario(getFuncionario());
		getManutencao().setInicio(getDataInicio());
		getManutencao().setFim(getDataTermino());
		try {
			getManutencao().setHoraMaquina(converte(tfHorimetro.getText()));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		getManutencao().setObservacao(taObservacaoManutencao.getText());

		if (rbCorretiva.isSelected() == true){
			getManutencao().setOperacao(getOperacao());
		} else {
			getManutencao().setOperacao(null);
		}
	}

	@Override
	public void carregarValoresSalvar() {
		if (rbRotina.isSelected() == true){
			// Manutenção de rotina não está ligada a uma operação
			try {
				setManutencao(new Manutencao(getMaquina(), getFuncionario(), getDataInicio(), 
						getDataTermino(), converte(tfHorimetro.getText()), taObservacaoManutencao.getText()));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {			
			// Manutenção corretiva. Está associada a uma operação sim.		
			try {
				setManutencao(new Manutencao(getOperacao(), getMaquina(), getFuncionario(), getDataInicio(), getDataTermino(), 
						converte(tfHorimetro.getText()), taObservacaoManutencao.getText()));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public void carregarValoresObjetos() {
		if (comboFuncionario.getSelectedIndex() != -1)
			setFuncionario(listFuncionario.get(comboFuncionario.getSelectedIndex()));		

		if (comboMaquina.getSelectedIndex() != -1)
			setMaquina(listMaquina.get(comboMaquina.getSelectedIndex()));		

		if (comboOperacao.getSelectedIndex() != -1)
			setOperacao(listOperacao.get(comboOperacao.getSelectedIndex()));

		setDataInicio((Date) dpDataInicio.getModel().getValue());
		setDataTermino((Date) dpDataTermino.getModel().getValue());
	}

	public void popularFuncionario(){		
		comboFuncionario.removeAllItems();
		listFuncionario = getDao().get("Funcionario");

		for (Object o : listFuncionario) {	
			Funcionario f = (Funcionario) o;
			comboFuncionario.addItem(f.getNome());
		}
	}

	public void popularMaquina(){		
		comboMaquina.removeAllItems();
		listMaquina = getDao().get("Maquina");

		for (Object o : listMaquina) {	
			Maquina m = (Maquina) o;
			comboMaquina.addItem(m.getDescricao());
		}
	}

	public void popularOperacao(){		
		comboOperacao.removeAllItems();
		listOperacao = getDao().get("Operacao");

		for (Object o : listOperacao) {	
			Operacao op = (Operacao) o;
			comboOperacao.addItem(op.getDescricaoOperacao());
		}
	}

	public void popularServico(){		
		comboTipoServico.removeAllItems();
		listTipoServico = getDao().get("TipoServico");

		for (Object o : listTipoServico) {	
			TipoServico s = (TipoServico) o;
			comboTipoServico.addItem(s.getDescricao());
		}
	}

	public void popularProduto(){		
		comboProduto.removeAllItems();
		listProduto = getDao().get("Produto");

		for (Object o : listProduto) {	
			Produto p = (Produto) o;
			comboProduto.addItem(p.getDescricao());
		}
	}

	public void clicarGridManutencao(){
		popularServico();
		popularProduto();
		atualizarGradeServicos();
		new NovoUI().gerenciar(btnNovo, btnSalvar, btnListagem, btnExcluir, btnFechar);
	}

	//Servico.class
	public void clicarGridServicos(){
		setUpdateServicos(true);
		habilitarCamposServicos();

		carregarValoresServicos();
		new AtualizarUI().gerenciar(btnNovo, btnSalvar, btnListagem, btnExcluir, btnFechar);
	}

	//Servico.class
	public void novoServicos(){
		setUpdateServicos(false);
		limparCamposServicos();
		habilitarCamposServicos();
		comboTipoServico.requestFocus();		
		new NovoUI().gerenciar(btnNovo, btnSalvar, btnListagem, btnExcluir, btnFechar);
	}

	//Servico.class
	public void salvarServicos(){
		if (!validarPasso2()) {
			return;
		}

		carregarValoresObjetosServicos();				

		if (!isUpdateServicos()) {
			carregarValoresSalvarServicos();					
		} else {
			if (JOptionPane.showConfirmDialog(null,
					"Tem certeza que deseja ALTERAR esse registro?",
					"Confirmar alteração",
					JOptionPane.YES_NO_OPTION) == 1){
				return;
			}
			carregarValoresAlterarServicos();
		}	
		new SalvarCommand().execute(getServicos());
		atualizarGradeServicos();

		desabilitarCamposServicos();

		new SalvarUI().gerenciar(btnNovo, btnSalvar, btnListagem, btnExcluir, btnFechar);
	}

	//Servico.class
	public void excluirServicos(){
		new ExcluirCommand().execute(getServicos());		
		desabilitarCamposServicos();
		limparCamposServicos();
		atualizarGradeServicos();
		new ExcluirUI().gerenciar(btnNovo, btnSalvar, btnListagem, btnExcluir, btnFechar);

	}

	//Servico.class
	public void carregarValoresObjetosServicos(){
		setTipoServico(listTipoServico.get(comboTipoServico.getSelectedIndex()));
		setProduto(listProduto.get(comboProduto.getSelectedIndex()));
	}

	//Servico.class
	public void carregarValoresSalvarServicos(){
		try {
			setServicosSalvar(new Servicos(getManutencao(), getTipoServico(), getProduto(), converte(tfQuantidade.getText()), 
					converte(tfValorUnitario.getText()), taObservacaoServico.getText()));
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//Servico.class	
	public void carregarValoresAlterarServicos(){
		getServicos().setTipoServico(listTipoServico.get(comboTipoServico.getSelectedIndex()));
		getServicos().setProduto(listProduto.get(comboProduto.getSelectedIndex()));		
		getServicos().setObservacao(taObservacaoServico.getText());

		try {
			getServicos().setQuantidade(converte(tfQuantidade.getText()));
			getServicos().setCusto(converte(tfValorUnitario.getText()));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}

	//Servico.class
	public void atualizarGradeServicos(){
		if (getManutencao() != null){
			sm = new ServicosModel(getDao().getListaServicos(getManutencao()));
			gradeServico.setModel(sm);
			gradeServico.repaint();
		}
	}

	//Servico.class
	public void limparCamposServicos(){
		comboTipoServico.setSelectedIndex(-1);
		comboProduto.setSelectedIndex(-1);
		tfQuantidade.setText("");
		tfValorUnitario.setText("");
		tfTotal.setText("");
	}

	//Servico.class
	public void desabilitarCamposServicos(){
		comboTipoServico.setEnabled(false);
		comboProduto.setEnabled(false);
		tfValorUnitario.setEnabled(false);
		tfQuantidade.setEnabled(false);
		taObservacaoServico.setEnabled(false);
	}

	//Servico.class
	public void habilitarCamposServicos(){
		comboTipoServico.setEnabled(true);
		comboProduto.setEnabled(true);
		tfValorUnitario.setEnabled(true);
		tfQuantidade.setEnabled(true);
		taObservacaoServico.setEnabled(true);
	}

	//Servico.class
	public void carregarValoresServicos() {
		setServicosSalvar((Servicos) getDao().getListaServicos(getManutencao()).get(gradeServico.getSelectedRow()));

		tfValorUnitario.setText(getServicos().getCustoFormatado());
		tfQuantidade.setText(getServicos().getQuantidadeFormatado());
		comboTipoServico.setSelectedItem(getServicos().getTipoServico().getDescricao());
		comboProduto.setSelectedItem(getServicos().getProduto().getDescricao());
	}

	//Servico.class
	public void calcularTotalServicos(){	
		float quantidade = 0;
		float valor = 0;

		try {
			quantidade = converte(tfQuantidade.getText());
			valor = converte(tfValorUnitario.getText());
			tfTotal.setText(getDf().format(quantidade*valor));
		} catch (Exception e) {
			tfTotal.setText("");
		}
	}

	@Override
	public void adicionaListenerCampoVazio() {
		tfHorimetro.addKeyListener(createKeyListenerCampoVazio(tfHorimetro));
		tfQuantidade.addKeyListener(createKeyListenerCampoVazio(tfQuantidade));
		tfValorUnitario.addKeyListener(createKeyListenerCampoVazio(tfValorUnitario));		
	}

	@SuppressWarnings("unused")
	public boolean validarPasso1(){
		Date inicio = null; 
		Date termino = null; 

		try {
			inicio = (Date) dpDataInicio.getModel().getValue();
			termino = (Date) dpDataTermino.getModel().getValue();	
		} catch (Exception e) {
			// TODO: handle exception
		}	

		if ((inicio == null) || (termino == null)){		
			mensagemErro("A data de início e fim da manutenção devem ser informadas!");
			return false;
		}

		if (termino.before(inicio)){			
			JOptionPane.showMessageDialog(null, "Atenção: a data de término da manutenção é inferior a data de início ", "Erro", JOptionPane.ERROR_MESSAGE);
			dpDataInicio.requestFocus();					
			return false;
		}		

		if (!validaEsteFloat(tfHorimetro, "HORAS"))
			return false;

		if (!validaEsteCombo(comboFuncionario, "FUNCIONÁRIO"))
			return false;

		if (!validaEsteCombo(comboMaquina, "MÁQUINA"))
			return false;		

		return true;
	}

	public boolean validarPasso2(){		
		if (!validaEsteCombo(comboTipoServico, "TIPO DE SERVIÇO"))
			return false;

		if (!validaEsteCombo(comboProduto, "PRODUTO"))
			return false;

		if (!validaEsteFloat(tfValorUnitario, "VALOR UNITÁRIO"))
			return false;

		if (!validaEsteFloat(tfQuantidade, "QUANTIDADE"))
			return false;		

		return true;
	}

	@Override
	public boolean validarFlutuantes() {return true;}
		
	@Override
	public void addDocument() {	}

	@Override
	public boolean validarCombos() {return true;}

	private class RadioButtonHandler implements ItemListener{
		@Override
		public void itemStateChanged(ItemEvent event) {			
			if (rbCorretiva.isSelected() == true){
				lblOperaoRelacionada.setEnabled(true);
				comboOperacao.setEnabled(true);	
				popularOperacao();
			} else {
				lblOperaoRelacionada.setEnabled(false);
				comboOperacao.setEnabled(false);
			}			
		}
	}	
}
