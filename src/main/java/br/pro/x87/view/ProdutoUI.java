package br.pro.x87.view;

import java.awt.Color;
import java.awt.GridLayout;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.jfree.ui.IntegerDocument;

import br.pro.x87.enums.TMaquina;
import br.pro.x87.enums.TProduto;
import br.pro.x87.model.Marca;
import br.pro.x87.model.Produto;
import br.pro.x87.view.command.ExcluirCommand;
import br.pro.x87.view.command.SalvarCommand;

public class ProdutoUI extends AncestorUI {

	private static final long serialVersionUID = 1L;

	private Produto produto;
	private Marca m;
	protected List<Marca> listaMarca;

	private JLabel jlTipoProduto;
	private JLabel jlTipoMaquina;
	private JLabel jlMarca;
	private JLabel jlDescricao;
	private JLabel jlTroca;
	private JComboBox<Object> comboTipoProduto;	
	private JComboBox<Object> comboTipoMaquina;	
	private JComboBox<Object> comboMarca;
	private JTextField tfDescricao;	
	private JTextField tfTroca;	
	private JPanel panel;
	private JPanel panel_1;


	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public void popularTipoMaquina(){		
		for (TMaquina o : TMaquina.values()) {
			comboTipoMaquina.addItem(o.getNome());
		}		
	}

	public void popularTipoProduto(){		
		for (TProduto o : TProduto.values()) {
			comboTipoProduto.addItem(o.getNome());
		}		
	}

	public void popularMarca(){
		listaMarca = getDao().get("Marca");

		for (Object o : listaMarca) {	
			Marca m = (Marca) o;
			comboMarca.addItem(m.getDescricao());
		}
	}


	public ProdutoUI() {
		popularTipoMaquina();
		popularTipoProduto();
		popularMarca();
	}

	@Override
	public void monteInterface(String s) {	
		super.monteInterface("MaqControl | Cadastro de produtos");		

		panel = new JPanel();
		panel_1 = new JPanel();
		jlDescricao = new JLabel("Descrição  ");
		jlTroca = new JLabel("Troca (h)  ");
		jlMarca = new JLabel("Marca ");
		jlTipoProduto = new JLabel("Tipo de produto  ");
		jlTipoMaquina = new JLabel("Tipo de máquina  ");
		tfDescricao = new JTextField();
		tfTroca = new JTextField();
		comboTipoMaquina = new JComboBox<>();
		comboMarca = new JComboBox<>();
		comboTipoProduto = new JComboBox<>();

		panel.setBounds(0, 0, 530, 40);		
		panel.setLayout(new GridLayout(0, 3, 5, 0));

		panel_1.setBounds(0, 41, 530, 40);		
		panel_1.setLayout(new GridLayout(0, 2, 5, 0));

		panel.add(jlDescricao);		
		panel.add(jlTroca);		
		panel.add(jlTipoProduto);		
		panel.add(tfDescricao);		
		panel.add(tfTroca);		
		panel.add(comboTipoProduto);

		panel_1.add(jlTipoMaquina);		
		panel_1.add(jlMarca);		
		panel_1.add(comboTipoMaquina);		
		panel_1.add(comboMarca);

		P2.setBounds(0, 110, 531, 119);
		pBotoes.setBounds(0, 84, 531, 23);

		StatusBar.setForeground(new Color(153, 153, 153));
		StatusBar.setText("***MaqControl*** Cadastro de produtos");

		getContentPane().setLayout(null);
		getContentPane().add(panel);
		getContentPane().add(panel_1);

		setSize(540,260);		
		setResizable(false);		
	}

	@Override
	public void habilitarCampos() {
		tfDescricao.setEnabled(true);
		comboTipoProduto.setEnabled(true);
		tfTroca.setEnabled(true);
		comboTipoMaquina.setEnabled(true);
		comboMarca.setEnabled(true);

		comboTipoProduto.setSelectedItem(-1);
		comboTipoMaquina.setSelectedItem(-1);
		comboMarca.setSelectedItem(-1);
	}

	@Override
	public void desabiltarCampos() {
		tfDescricao.setEnabled(false);
		comboTipoProduto.setEnabled(false);
		tfTroca.setEnabled(false);
		comboTipoMaquina.setEnabled(false);
		comboMarca.setEnabled(false);
	}

	@Override
	public void definirFoco() {
		tfDescricao.requestFocus();
	}

	@Override
	public void executarCommandExcluir() {
		new ExcluirCommand().execute(produto);                		
	}

	@Override
	public void atualizarGrade() {
		atualizarGrade("Produto", Produto.class);
	}

	@Override
	public void carregarValoresAlterar() {
		String tipoMaquina = (String) comboTipoMaquina.getSelectedItem();
		String tipoProduto = (String) comboTipoProduto.getSelectedItem();

		getProduto().setDescricao(tfDescricao.getText());
		getProduto().setTipoProduto(tipoProduto);
		getProduto().setTroca((Integer.parseInt(tfTroca.getText())));
		getProduto().setTipoMaquina(tipoMaquina);
		getProduto().setMarca(m);
	}

	@Override
	public void carregarValoresSalvar() {
		String tipoMaquina = (String) comboTipoMaquina.getSelectedItem();
		String tipoProduto = (String) comboTipoProduto.getSelectedItem();

		setProduto(new Produto(tfDescricao.getText(), tipoProduto, (Integer.parseInt(tfTroca.getText())), tipoMaquina, m));
	}

	@Override
	public void carregarValoresObjetos() {
		m = listaMarca.get(comboMarca.getSelectedIndex());
	}

	@Override
	public void executarCommandSalvar() {
		new SalvarCommand().execute(produto);                                
	}

	@Override
	public void carregarValores() {
		setProduto((Produto) lista.get(grade.getSelectedRow()));

		tfDescricao.setText(getProduto().getDescricao());
		comboTipoProduto.setSelectedItem(getProduto().getTipoProduto());
		tfTroca.setText((Integer.toString(getProduto().getTroca())));
		comboTipoMaquina.setSelectedItem(getProduto().getTipoMaquina());
		comboMarca.setSelectedItem(getProduto().getMarca().getDescricao());	
	}

	@Override
	public void adicionaListenerCampoVazio() {
		tfDescricao.addKeyListener(createKeyListenerCampoVazio(tfDescricao));
		tfTroca.addKeyListener(createKeyListenerCampoVazio(tfTroca));
	}

	@Override
	public boolean validarFlutuantes() {
		return true;
	}

	@Override
	public boolean validarCombos() {
		if (!validaEsteCombo(comboMarca, "MARCA"))
			return false;

		if (!validaEsteCombo(comboTipoMaquina, "TIPO DE MÁQUINA"))
			return false;

		if (!validaEsteCombo(comboTipoProduto, "TIPO DE PRODUTO"))
			return false;

		return true;
	}

	@Override
	public void addDocument() {
		tfTroca.setDocument(new IntegerDocument());
	}
}
