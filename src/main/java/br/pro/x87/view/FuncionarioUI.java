package br.pro.x87.view;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.text.ParseException;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.MaskFormatter;

import br.pro.x87.dao.BairroDAO;
import br.pro.x87.dao.EnderecoDAO;
import br.pro.x87.dao.FuncionarioDAO;
import br.pro.x87.enums.TEstado;
import br.pro.x87.model.Bairro;
import br.pro.x87.model.Cidade;
import br.pro.x87.model.Endereco;
import br.pro.x87.model.Funcionario;
import br.pro.x87.view.command.ExcluirCommand;
import br.pro.x87.view.command.SalvarCommand;

public class FuncionarioUI extends AncestorUI {

	private static final long serialVersionUID = 1L;

	private Funcionario funcionario;
	private TEstado estado;
	private Cidade cidade;
	private Bairro bairro;
	private Endereco endereco;
	private BairroDAO bairroDAO = new BairroDAO();

	private List<Cidade> listCidade;
	private List<Bairro> listBairro;
	private List<Object> listEndereco;

	private JTextField tfNome;
	private JTextField tfRG;
	private JTextField tfPIS;
	private JTextField tfCTPS;
	private JTextField tfFone;
	private JTextField tfCPF;
	private JComboBox<String> comboEstado;
	private JComboBox<Object> comboCidade;
	private JComboBox<Object> comboBairro;
	private JComboBox<Object> comboEndereco;	

	public TEstado getEstado() {
		return estado;
	}

	public void setEstado(TEstado estado) {
		this.estado = estado;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public Bairro getBairro() {
		return bairro;
	}

	public void setBairro(Bairro bairro) {
		this.bairro = bairro;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Object object) {
		this.endereco = (Endereco) object;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}	

	public void popularEstado(){
		for (TEstado o : TEstado.values()) {
			comboEstado.addItem(o.getNome());
		}
	}

	public void popularCidade(){
		if (comboEstado.getSelectedIndex() == -1){
			return;
		}
		comboCidade.removeAllItems();
		listCidade = getDao().filter("Cidade", "Estado", (String) comboEstado.getSelectedItem());

		for (Object o : listCidade) {	
			Cidade c = (Cidade) o;
			comboCidade.addItem(c.getNomeCidade());
		}
	}

	public void popularBairro(){
		if (comboCidade.getSelectedIndex() == -1){
			return;
		}
		comboBairro.removeAllItems();
		listBairro = bairroDAO.listBairros( listCidade.get(comboCidade.getSelectedIndex()).getNomeCidade());

		for (Object o : listBairro) {	
			Bairro b = (Bairro) o;
			comboBairro.addItem(b.getDescricao());
		}
	}

	public void popularEndereco(){
		if (comboBairro.getSelectedIndex() == -1){
			return;
		}
		comboEndereco.removeAllItems();
		listEndereco = new EnderecoDAO<>().listEndereco(listBairro.get(comboBairro.getSelectedIndex()).getDescricao());

		for (Object o : listEndereco) {
			Endereco e = (Endereco) o;
			comboEndereco.addItem(e.getNomeNumero());
		}
	}


	@Override
	public void monteInterface(String s) {
		super.monteInterface("MaqControl | Cadastro de funcionário");

		JPanel panel = new JPanel();
		JPanel pDadosPessoais = new JPanel();
		JLabel lblNome = new JLabel("Nome");
		JLabel lblNewLabel_1 = new JLabel("Fone");
		JLabel lblRg = new JLabel("RG");
		JLabel lblCpf = new JLabel("CPF");
		JLabel lblNewLabel = new JLabel("CTPS");
		JLabel lblPis = new JLabel("PIS");
		tfNome = new JTextField();
		tfFone = new JTextField();
		tfRG = new JTextField();
		tfCPF = new JTextField();
		tfCTPS = new JTextField();
		tfPIS = new JTextField();
		
		MaskFormatter fone, rg, cpf, ctps, pis;
		
		try {
			fone = new MaskFormatter("(##) ####-####");
			rg = new MaskFormatter("#.###.###-#");
			cpf = new MaskFormatter("###.###.###-##");
			ctps = new MaskFormatter("##.###.#####/#");
			pis = new MaskFormatter("###.#####.##-#");
			
			tfFone = new JFormattedTextField(fone);
			tfRG = new JFormattedTextField(rg);
			tfCPF = new JFormattedTextField(cpf);
			tfCTPS = new JFormattedTextField(ctps);
			tfPIS = new JFormattedTextField(pis);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		

		tfNome.setColumns(10);
		tfFone.setColumns(10);		
		tfRG.setColumns(10);	
		tfCTPS.setColumns(10);	
		tfPIS.setColumns(10);

		pDadosPessoais.setBounds(0, 0, 545, 110);
		pDadosPessoais.setLayout(new GridLayout(4, 3, 10, 0));
		pDadosPessoais.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder(" Dados Pessoais "),
				BorderFactory.createEmptyBorder(5,5,5,5)));			

		pDadosPessoais.add(lblNome);		
		pDadosPessoais.add(lblNewLabel_1);		
		pDadosPessoais.add(lblRg);		
		pDadosPessoais.add(tfNome);
		pDadosPessoais.add(tfFone);				
		pDadosPessoais.add(tfRG);
		pDadosPessoais.add(lblCpf);		
		pDadosPessoais.add(lblNewLabel);		
		pDadosPessoais.add(lblPis);		
		pDadosPessoais.add(tfCPF);
		pDadosPessoais.add(tfCTPS);
		pDadosPessoais.add(tfPIS);		

		JPanel pEndereco = new JPanel();
		JLabel lblEstado = new JLabel("Estado");
		JLabel lblCidade = new JLabel("Cidade");
		JLabel lblEndereo = new JLabel("Endereço");
		JLabel lblBairro = new JLabel("Bairro");
		comboCidade = new JComboBox<>();
		comboEstado = new JComboBox<>();
		comboBairro = new JComboBox<>();
		comboEndereco = new JComboBox<>();

		pEndereco.setBounds(0, 110, 545, 110);
		pEndereco.setLayout(new GridLayout(4, 2, 10, 0));
		pEndereco.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder(" Endereço "),
				BorderFactory.createEmptyBorder(5,5,5,5)));

		pEndereco.add(lblEstado);		
		pEndereco.add(lblCidade);	
		pEndereco.add(comboEstado);	
		pEndereco.add(comboCidade);	
		pEndereco.add(lblBairro);		
		pEndereco.add(lblEndereo);		
		pEndereco.add(comboBairro);		
		pEndereco.add(comboEndereco);


		panel.setBounds(0, 0, 545, 220);
		panel.setLayout(null);
		panel.add(pDadosPessoais);
		panel.add(pEndereco);

		pBotoes.setSize(545, 23);
		pBotoes.setLocation(0, 220);
		P2.setBounds(0, 250, 545, 150);

		getContentPane().setLayout(null);		
		getContentPane().add(panel);

		setSize(550, 430);				
		setResizable(false);
	}


	public FuncionarioUI() {
		bListagem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				List lista = new FuncionarioDAO<>().imprimirFuncionarios();
				templateImprimir(lista, "funcionario.jrxml");
			}
		});
		comboEstado.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				popularCidade();
			}
		});

		comboCidade.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				popularBairro();;
			}
		});

		comboBairro.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				popularEndereco();
			}
		});

		popularEstado();
	}

	@Override
	public void executarCommandExcluir() {
		new ExcluirCommand().execute(funcionario);		
	}

	@Override
	public void executarCommandSalvar() {
		new SalvarCommand().execute(funcionario);
	}

	@Override
	public void atualizarGrade() {
		atualizarGrade("Funcionario", Funcionario.class);
	}

	@Override
	public void carregarValores() {
		setFuncionario((Funcionario) lista.get(grade.getSelectedRow()));

		tfNome.setText(getFuncionario().getNome());
		tfFone.setText(getFuncionario().getFone());
		tfRG.setText(getFuncionario().getRg());
		tfCPF.setText(getFuncionario().getCpf());
		tfPIS.setText(getFuncionario().getPis());
		tfCTPS.setText(getFuncionario().getCtps());

		comboEstado.setSelectedItem(getFuncionario().getEndereco().getBairro().getCidade().getNomeEstado());		
		comboCidade.setSelectedItem(getFuncionario().getEndereco().getBairro().getCidade().getNomeCidade());		
		comboBairro.setSelectedItem(getFuncionario().getEndereco().getBairro().getDescricao());
		comboEndereco.setSelectedItem(getFuncionario().getEndereco().getNomeNumero());
	}

	@Override
	public void definirFoco() {
		tfNome.requestFocus();
	}

	@Override
	public void habilitarCampos() {
		tfNome.setEnabled(true);
		tfFone.setEnabled(true);
		tfRG.setEnabled(true);
		tfCPF.setEnabled(true);
		tfPIS.setEnabled(true);
		tfCTPS.setEnabled(true);		
	}

	@Override
	public void desabiltarCampos() {
		tfNome.setEnabled(false);
		tfFone.setEnabled(false);
		tfRG.setEnabled(false);
		tfCPF.setEnabled(false);
		tfPIS.setEnabled(false);
		tfCTPS.setEnabled(false);
	}

	@Override
	public void carregarValoresAlterar() {
		getFuncionario().setEndereco(endereco);
		getFuncionario().setNome(tfNome.getText());
		getFuncionario().setRg(tfRG.getText());
		getFuncionario().setCpf(tfCPF.getText());
		getFuncionario().setPis(tfPIS.getText());
		getFuncionario().setCtps(tfCTPS.getText());
		getFuncionario().setFone(tfFone.getText());		
	}

	@Override
	public void carregarValoresSalvar() {
		setFuncionario(new Funcionario(getEndereco(), tfNome.getText(), tfRG.getText(), 
				tfCPF.getText(), tfPIS.getText(), tfCTPS.getText(), tfFone.getText()));
	}

	@Override
	public void carregarValoresObjetos() {
		setCidade(listCidade.get(comboCidade.getSelectedIndex()));		
		setBairro(listBairro.get(comboBairro.getSelectedIndex()));
		setEndereco(listEndereco.get(comboEndereco.getSelectedIndex()));
	}

	@Override
	public void adicionaListenerCampoVazio() {
		tfNome.addKeyListener(createKeyListenerCampoVazio(tfNome));
		tfFone.addKeyListener(createKeyListenerCampoVazio(tfFone));
		tfRG.addKeyListener(createKeyListenerCampoVazio(tfRG));
		tfCPF.addKeyListener(createKeyListenerCampoVazio(tfCPF));
		tfCTPS.addKeyListener(createKeyListenerCampoVazio(tfCTPS));
		tfPIS.addKeyListener(createKeyListenerCampoVazio(tfPIS));		
	}

	@Override
	public boolean validarFlutuantes() {
		return true;
	}

	@Override
	public boolean validarCombos() {
		if (!validaEsteCombo(comboBairro, "BAIRRO"))
			return false;

		if (!validaEsteCombo(comboCidade, "CIDADE"))
			return false;

		if (!validaEsteCombo(comboEndereco, "ENDEREÇO"))
			return false;

		if (!validaEsteCombo(comboEstado, "ESTADO"))
			return false;

		return true;
	}


	@Override
	public void addDocument() {}
}
