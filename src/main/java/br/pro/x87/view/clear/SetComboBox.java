package br.pro.x87.view.clear;

import java.awt.Component;
import java.awt.Container;

import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

public class SetComboBox implements Clear {

	private static SetComboBox instance;

	private SetComboBox() {
		// TODO Auto-generated constructor stub
	}

	public static SetComboBox getInstance(){
		if (instance == null){
			instance = new SetComboBox();
		}
		return instance;
	}

	@Override
	public void clear(Container c) {
		Component [] p = c.getComponents();
		for(Component pp : p){
			if(pp instanceof JPanel)
				clearInsidePanel((JPanel) pp);
		}
	}

	@Override
	public void clearInsidePanel(JPanel p) {
		Component [] c = p.getComponents();
		for(Component cc: c){
			if(cc instanceof JComboBox){				
				((JComboBox) cc).setSelectedIndex(-1);
			} else if (cc instanceof JPanel){
				clearInsidePanel((JPanel) cc);
			} else if (cc instanceof JTabbedPane){
				clearInsideTabbedPane((JTabbedPane) cc);
			}				
		}
	}

	/* (non-Javadoc)
	 * @see br.pro.x87.view.clear.Clear#clearInsideTabbedPane(javax.swing.JTabbedPane)
	 */
	@Override
	public void clearInsideTabbedPane(JTabbedPane p) {
		Component [] c = p.getComponents();
		for(Component cc: c){
			if(cc instanceof JComboBox){				
				((JComboBox) cc).setSelectedIndex(-1);
			} else if (cc instanceof JPanel){
				clearInsidePanel((JPanel) cc);
			}			
		}

	}

}
