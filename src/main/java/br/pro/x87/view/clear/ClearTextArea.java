/**
 * 
 */
package br.pro.x87.view.clear;

import java.awt.Component;
import java.awt.Container;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;

/**
 * @author Juliano Rodrigo Lamb
 * @since 24 fev 2018
 *
 */
public class ClearTextArea implements Clear {

	private static ClearTextArea instance;

	private ClearTextArea() {

	}

	public static ClearTextArea getInstance(){
		if (instance == null){
			instance = new ClearTextArea();
		}
		return instance;
	}

	@Override
	public void clear(Container c) {
		Component [] p = c.getComponents();
		for(Component pp : p){
			if(pp instanceof JPanel)
				clearInsidePanel((JPanel) pp);
			if(pp instanceof JTabbedPane)
				clearInsideTabbedPane((JTabbedPane) pp);
		}		
	}

	@Override
	public void clearInsidePanel(JPanel p) {
		Component [] c = p.getComponents();
		for(Component cc: c){
			if(cc instanceof JTextArea){				
				((JTextArea) cc).setText("");
			} else if (cc instanceof JPanel){
				clearInsidePanel((JPanel) cc);
			}			
		}
	}

	@Override
	public void clearInsideTabbedPane(JTabbedPane p) {
		Component [] c = p.getComponents();
		for(Component cc: c){
			if(cc instanceof JTextArea){				
				((JTextArea) cc).setText("");
			} else if (cc instanceof JPanel){
				clearInsidePanel((JPanel) cc);
			}			
		}
	}

}
