package br.pro.x87.view.clear;

import java.awt.Container;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

public interface Clear {
	public void clear(Container c);
	public void clearInsidePanel(JPanel p);
	public void clearInsideTabbedPane(JTabbedPane p);
}
