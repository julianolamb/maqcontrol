package br.pro.x87.view.clear;

import java.awt.Component;
import java.awt.Container;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;

public class ClearTextField implements Clear {
	private static ClearTextField instance;

	private ClearTextField() {

	}

	public static ClearTextField getInstance(){
		if (instance == null){
			instance = new ClearTextField();
		}
		return instance;
	}

	@Override
	public void clear(Container c) {
		Component [] p = c.getComponents();
		for(Component pp : p){
			if(pp instanceof JPanel)
				clearInsidePanel((JPanel) pp);
			if(pp instanceof JTabbedPane)
				clearInsideTabbedPane((JTabbedPane) pp);
		}
	}

	@Override
	public void clearInsidePanel(JPanel p) {
		Component [] c = p.getComponents();
		for(Component cc: c){
			if(cc instanceof JTextField){				
				((JTextField) cc).setText("");
			} else if (cc instanceof JPanel){
				clearInsidePanel((JPanel) cc);
			}			
		}
	}
	
	@Override
	public void clearInsideTabbedPane(JTabbedPane p) {
		Component [] c = p.getComponents();
		for(Component cc: c){
			if(cc instanceof JTextField){				
				((JTextField) cc).setText("");
			} else if (cc instanceof JPanel){
				clearInsidePanel((JPanel) cc);
			}			
		}
	}

}
