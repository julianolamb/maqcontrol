/**
 * 
 */
package br.pro.x87.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.SystemColor;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ContainerAdapter;
import java.awt.event.ContainerEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;

import br.pro.x87.capacidadeDeCampo.CCO;
import br.pro.x87.capacidadeDeCampo.CCT;
import br.pro.x87.capacidadeDeCampo.CapacidadeDeCampo;
import br.pro.x87.controller.CustosVariaveisModel;
import br.pro.x87.controller.DepreciacaoModel;
import br.pro.x87.controller.EstimativaCFModel;
import br.pro.x87.dao.DAO;
import br.pro.x87.depreciacao.DeclinioEmDobro;
import br.pro.x87.depreciacao.Depreciacao;
import br.pro.x87.depreciacao.DigitosPeriodicos;
import br.pro.x87.depreciacao.PorcentagensConstantes;
import br.pro.x87.depreciacao.Proporcional;
import br.pro.x87.depreciacao.QuotasConstantes;
import br.pro.x87.despesas.CampoDeCusto;
import br.pro.x87.enums.TProduto;
import br.pro.x87.model.Area;
import br.pro.x87.model.EstimativaCustosFixos;
import br.pro.x87.model.Maquina;
import br.pro.x87.model.Operacao;
import br.pro.x87.model.Produto;
import br.pro.x87.model.TipoOperacao;
import br.pro.x87.reports.RelatorioEstimativa;

/**
 * @author jrlamb
 *
 */
public class EstimativaUI extends JFrame {

	private static final long serialVersionUID = 1L;

	private DAO dao = new DAO();	

	private CapacidadeDeCampo capacidadeDeCampo;
	private CustosVariaveisModel cvm;
	private Depreciacao depre;
	private Maquina maquina;	

	private DecimalFormat df = new DecimalFormat("0.00");		

	private EstimativaCFModel ecfModel;

	private List<EstimativaCustosFixos> arrayConjuntoCalculado = new ArrayList<>();
	private List<Produto> listProduto;
	private List<TipoOperacao> listTipoOperacao = new ArrayList<>();

	private List<Maquina> arrayMaquinaDisponivel;
	private DefaultListModel<String> modelMaquinaDisponivel = new DefaultListModel<>();

	private List<Maquina> arrayConjuntoMecanizado = new ArrayList<>();
	private DefaultListModel<String> modelConjuntoMecanizado = new DefaultListModel<>();

	private JLabel lbPotencia;
	private JPanel pCustosFixos, pCustosVariaveis;

	private JComboBox<Object> comboOperacao;
	private JComboBox<Object> comboTipoProduto;
	private JComboBox<Object> comboProduto;
	private JTextField tfTamanhoArea;
	private JTextField tfDuracaoOperacao;	
	private JTextField tfTotalCustoOperacional;	
	private JTextField tfTotalCustoOperacionalHectares;
	private JTextField tfLarguraOperacional;
	private JTextField tfVelocidade;
	private JTextField tfEficiencia;
	private JTextField tfCco;
	private JTextField tfTroca;
	private JTextField tfValorUnitario;
	private JTextField tfProdutosTotal;
	private JTextField tfPrecoLitro;
	private JTextField tfCombustivelTotal;	
	private JTextField tfManutencaoTotal;
	private JTextField tfTaxaManutencao;	
	private JTextField tfQuantidadeGraxa;
	private JTextField tfPrecoGraxa;
	private JTextField tfTempoLubrificacao;
	private JTextField tfGraxaTotal;
	private JTextField tfTotalSalario;
	private JTextField tfTotalCustosVariaveis;
	private JTextField tfTotalCustosFixos;
	private JTextField tfTotalCustoHorario;
	private JTextField tfConsumoEstimado;
	private JTextField tfPotenciaCv;	
	private JTextField tfSalario;
	private JTextField tfQuantidade;
	private JTextField tfPotenciaKw;
	private JTextField tfConsumo;

	private JRadioButton rbtnQuotas;	
	private JRadioButton rbtnProporcional; 
	private JRadioButton rbtnPorcentagensConstantes;	
	private JRadioButton rbtnDigitosPeriodicos;	
	private JRadioButton rbtnDeclinioEmDobro;	
	private JRadioButton rbtnConsumoLitros;
	private JRadioButton rbtnConsumoKilowats;

	private JButton bAdicionarMaquina;
	private JButton bRemoverMaquina;
	private JButton btnAdicionarCombustivel;
	private JButton btnAdicionarProduto;
	private JButton btnAdicionarManutencao;
	private JButton btnAdicionarGraxa;
	private JButton btnAdicionarSalario;

	private JList<String> listMaquinaDisponivel;
	private JList<String> listConjuntoMecanizado;
	private JScrollPane spMaquinaDisponivel;	
	private JScrollPane spConjuntoMecanizado;

	private JTable gradeCustosFixos;	

	private JCheckBox chInformarManualmente;	
	private JTable gradeConsumo;	

	public CapacidadeDeCampo getCapacidadeDeCampo() {
		return capacidadeDeCampo;
	}

	public void setCapacidadeDeCampo(CapacidadeDeCampo custoOperacional) {
		this.capacidadeDeCampo = custoOperacional;
	}

	public CustosVariaveisModel getCvm() {
		return cvm;
	}

	public void setCvm(CustosVariaveisModel cvm) {
		this.cvm = cvm;
	}

	public Maquina getMaquina() {
		return maquina;
	}

	public void setMaquina(Maquina maquina) {
		this.maquina = maquina;
	}

	public DecimalFormat getDf() {
		return df;
	}

	public void setDf(DecimalFormat df) {
		this.df = df;
	}

	public List<Maquina> getArrayMaquinaDisponivel() {
		return arrayMaquinaDisponivel;
	}

	public void setArrayMaquinaDisponivel(List<Maquina> arrayMaquinaDisponivel) {
		this.arrayMaquinaDisponivel = arrayMaquinaDisponivel;
	}

	public EstimativaCFModel getEcfModel() {
		return ecfModel;
	}

	public void setEcfModel(EstimativaCFModel ecfModel) {
		this.ecfModel = ecfModel;
	}

	public List<EstimativaCustosFixos> getArrayConjuntoCalculado() {
		return arrayConjuntoCalculado;
	}

	public void setArrayConjuntoCalculado(List<EstimativaCustosFixos> l) {
		this.arrayConjuntoCalculado = l;
	}

	public List<Maquina> getArrayConjuntoMecanizado() {
		return arrayConjuntoMecanizado;
	}

	public void setArrayConjuntoMecanizado(List<Maquina> arrayConjuntoMecanizado) {
		this.arrayConjuntoMecanizado = arrayConjuntoMecanizado;
	}

	public Depreciacao getDepre() {
		return depre;
	}

	public void setDepre(Depreciacao depre) {
		this.depre = depre;
	}

	public DefaultListModel<String> getModelMaquinaDisponivel() {
		return modelMaquinaDisponivel;
	}

	public void setModelMaquinaDisponivel(DefaultListModel<String> modelMaquinaDisponivel) {
		this.modelMaquinaDisponivel = modelMaquinaDisponivel;
	}

	public DefaultListModel<String> getModelConjuntoMecanizado() {
		return modelConjuntoMecanizado;
	}

	public void setModelConjuntoMecanizado(DefaultListModel<String> modelConjuntoMecanizado) {
		this.modelConjuntoMecanizado = modelConjuntoMecanizado;
	}

	public DAO getDao() {
		return dao;
	}

	public void setDao(DAO dao) {
		this.dao = dao;
	}

	public EstimativaUI() {
		setTitle("MaqControl | Estimativa do custo operacional");
		JPanel panel1 = new JPanel();
		JLabel lblOperacao = new JLabel("Operação");
		JLabel lblTamanhoArea = new JLabel("Tamanho área (ha)");
		JLabel lblDuracao = new JLabel("Duração (h)");
		JLabel lblTotalCustooperacionalr = new JLabel("Custo-operacional (R$)");
		comboOperacao = new JComboBox<>();
		tfTamanhoArea = new JTextField();

		tfDuracaoOperacao = new JTextField();
		tfTotalCustoOperacional = new JTextField();

		tfTotalCustoOperacional.setEditable(false);
		tfTotalCustoOperacional.setToolTipText("Este valor só é calculado quando o custo-horário for determinado");
		tfTotalCustoOperacional.setBackground(SystemColor.info);

		panel1.setLayout(new GridLayout(2, 4, 5, 0));
		panel1.setBounds(0, 0, 874, 40);

		panel1.add(lblOperacao);		
		panel1.add(lblTamanhoArea);				
		panel1.add(lblDuracao);		
		panel1.add(lblTotalCustooperacionalr);		
		panel1.add(comboOperacao);		
		panel1.add(tfTamanhoArea);
		panel1.add(tfDuracaoOperacao);
		panel1.add(tfTotalCustoOperacional);		

		JPanel panel2 = new JPanel();
		JLabel lblLarguraOperacional = new JLabel("Largura operacional (m)");
		JLabel lblVelocidade = new JLabel("Velocidade (km/h)");		
		JLabel lblEficiencia = new JLabel("Eficiência (%)");		
		JLabel lblCco = new JLabel("Cap. campo operacional (CCO)");
		tfLarguraOperacional = new JTextField();

		TfHandlerParametrosOperacao handlerParametros = new TfHandlerParametrosOperacao();


		tfVelocidade = new JTextField();
		tfLarguraOperacional.getDocument().addDocumentListener(handlerParametros);
		tfVelocidade.getDocument().addDocumentListener(handlerParametros);
		tfTamanhoArea.getDocument().addDocumentListener(handlerParametros);

		tfEficiencia = new JTextField();
		tfCco = new JTextField();

		panel2.setBounds(0, 45, 419, 100);		
		panel2.setLayout(new GridLayout(0, 2, 5, 0));

		panel2.add(lblLarguraOperacional);
		panel2.add(lblVelocidade);
		panel2.add(tfLarguraOperacional);		
		panel2.add(tfVelocidade);
		panel2.add(lblEficiencia);
		panel2.add(lblCco);		
		panel2.add(tfEficiencia);
		panel2.add(tfCco);

		JPanel panel = new JPanel();
		JPanel pBotoesConjuntoMecanizado = new JPanel();
		JLabel lblMquinaimplementoDisponvel = new JLabel("Máquina/Impl. disponível");		
		JLabel lblNewLabel = new JLabel("Conjunto mecanizado");		

		listMaquinaDisponivel = new JList<>(getModelMaquinaDisponivel());
		listConjuntoMecanizado = new JList<>(getModelConjuntoMecanizado());
		spMaquinaDisponivel = new JScrollPane(listMaquinaDisponivel);		
		spConjuntoMecanizado = new JScrollPane(listConjuntoMecanizado);	

		bRemoverMaquina = new JButton("<- Remover");
		bAdicionarMaquina = new JButton("Adicionar ->");

		panel.setBounds(429, 45, 445, 100);
		panel.setLayout(null);

		lblMquinaimplementoDisponvel.setBounds(0, 0, 160, 14);
		lblNewLabel.setBounds(286, 0, 159, 14);

		listMaquinaDisponivel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listMaquinaDisponivel.setBounds(0, 2, 0, 139);

		listConjuntoMecanizado.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listConjuntoMecanizado.setBounds(355, 2, 0, 139);

		spConjuntoMecanizado.setBounds(285, 20, 160, 80);
		spMaquinaDisponivel.setBounds(0, 20, 160, 80);

		pBotoesConjuntoMecanizado.setLayout(new GridLayout(2, 1, 10, 10));
		pBotoesConjuntoMecanizado.setBounds(160, 20, 124, 80);

		pBotoesConjuntoMecanizado.add(bAdicionarMaquina);
		pBotoesConjuntoMecanizado.add(bRemoverMaquina);

		panel.add(lblMquinaimplementoDisponvel);
		panel.add(spMaquinaDisponivel);
		panel.add(pBotoesConjuntoMecanizado);
		panel.add(lblNewLabel);
		panel.add(spConjuntoMecanizado);

		rbtnQuotas = new JRadioButton("Quotas constantes");		
		rbtnProporcional = new JRadioButton("Proporcional às horas trabalhadas");
		rbtnPorcentagensConstantes = new JRadioButton("Porcentagens constantes (Matheson)");
		rbtnDigitosPeriodicos = new JRadioButton("Soma dos dígitos periódicos (Cole)");
		rbtnDeclinioEmDobro = new JRadioButton("Declínio em dobro");

		rbtnQuotas.setSelected(true);

		ButtonGroup groupDepreciacao = new ButtonGroup();
		groupDepreciacao.add(rbtnDeclinioEmDobro);
		groupDepreciacao.add(rbtnDigitosPeriodicos);
		groupDepreciacao.add(rbtnPorcentagensConstantes);
		groupDepreciacao.add(rbtnProporcional);
		groupDepreciacao.add(rbtnQuotas);

		RadioButtonHandler handler = new RadioButtonHandler();
		rbtnDeclinioEmDobro.addItemListener(handler);
		rbtnDigitosPeriodicos.addItemListener(handler);
		rbtnPorcentagensConstantes.addItemListener(handler);
		rbtnProporcional.addItemListener(handler);
		rbtnQuotas.addItemListener(handler);

		pCustosFixos = new JPanel();
		JPanel pDepreciacao = new JPanel();
		chInformarManualmente = new JCheckBox("Informar manualmente");

		pCustosFixos.setLocation(0, 160);
		pCustosFixos.setSize(874, 134);
		pCustosFixos.setLayout(null);
		pCustosFixos.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder("  Custos fixos  "),
				BorderFactory.createEmptyBorder(5,5,5,5)));

		pCustosFixos.add(pDepreciacao);

		pDepreciacao.setBounds(5, 20, 260, 110);
		pDepreciacao.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder(" Selecione a forma de depreciação "),
				BorderFactory.createEmptyBorder(5,5,5,5)));		
		pDepreciacao.setLayout(new GridLayout(5, 1, 1, 1));

		pDepreciacao.add(rbtnQuotas);		
		pDepreciacao.add(rbtnProporcional);		
		pDepreciacao.add(rbtnPorcentagensConstantes);		
		pDepreciacao.add(rbtnDigitosPeriodicos);		
		pDepreciacao.add(rbtnDeclinioEmDobro);		

		JPanel panelCustosVariaveis = buildPanelCustosVariaveis();
		JPanel pGraxa = construirPainelGraxa();
		JPanel pCustoTotal = construirPainelTotal();
		JScrollPane scrollPane = new JScrollPane();
		gradeCustosFixos = new JTable();

		JLabel lblOsValores = new JLabel("Os valores da largura, eficiência e CCO são automaticamente preenchidos, desde que \"Informar manualmente\" não esteja marcada e uma máquina esteja selecionada.");		
		lblOsValores.setFont(lblOsValores.getFont().deriveFont(lblOsValores.getFont().getStyle() & ~Font.BOLD, 11f));
		lblOsValores.setBounds(0, 145, 874, 14);
		lblOsValores.setHorizontalAlignment(SwingConstants.CENTER);
		lblOsValores.setOpaque(true);
		lblOsValores.setBackground(SystemColor.info);		

		chInformarManualmente.setSelected(true);
		panel2.add(chInformarManualmente);		

		scrollPane.setViewportView(gradeCustosFixos);
		scrollPane.setBounds(270, 28, 594, 99);

		pCustosFixos.add(scrollPane);
		panelCustosVariaveis.add(pGraxa);

		setResizable(false);
		setBounds(100, 100, 880, 697);
		getContentPane().setLayout(null);
		getContentPane().add(panel1);
		getContentPane().add(panel2);		
		getContentPane().add(panel);
		getContentPane().add(pCustosFixos);
		getContentPane().add(panelCustosVariaveis);
		getContentPane().add(pCustoTotal);		
		getContentPane().add(lblOsValores);	

		popularMaquina();
		popularTipoOperacao();
		df.setRoundingMode(RoundingMode.UP);		


		listMaquinaDisponivel.addContainerListener(new ContainerAdapter() {
			@Override
			public void componentAdded(ContainerEvent arg0) {				
				bRemoverMaquina.setEnabled(true);
			}
			@Override
			public void componentRemoved(ContainerEvent e) {				
				if (listMaquinaDisponivel.getModel().getSize() == 0)
					bRemoverMaquina.setEnabled(false);
			}
		});

		bRemoverMaquina.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (listConjuntoMecanizado.getSelectedIndex() == -1){
					JOptionPane.showMessageDialog(null, "Selecione uma máquina para remover do conjunto mecanizado!", 
							"Erro ao remover", JOptionPane.ERROR_MESSAGE);
					listConjuntoMecanizado.requestFocus();
					return;
				}				

				removerDoConjuntoMecanizado();				

				if (getArrayConjuntoMecanizado().size()>0){
					obterValoresCombustivel();
					calcularTotalManutencao();	
				} else {				

					tfQuantidade.setText("");
					tfManutencaoTotal.setText("");

					rbtnConsumoLitros.setEnabled(true);
					rbtnConsumoLitros.setSelected(false);
					rbtnConsumoKilowats.setSelected(false);					
					tfPrecoLitro.setText("");
					tfPotenciaCv.setText("");
					tfPotenciaKw.setText("");
					tfConsumo.setText("");
					tfCombustivelTotal.setText("");
				}
			}
		});

		bAdicionarMaquina.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				adicionarMaquina();
			}
		});

		chInformarManualmente.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (chInformarManualmente.isSelected() == true){
					tfEficiencia.setEnabled(true);
					tfLarguraOperacional.setEnabled(true);
					tfCco.setEnabled(true);

					tfEficiencia.setBackground(SystemColor.WHITE);
					tfLarguraOperacional.setBackground(SystemColor.WHITE);
					tfCco.setBackground(SystemColor.WHITE);
				} else {
					tfEficiencia.setEnabled(false);
					tfLarguraOperacional.setEnabled(false);
					tfCco.setEnabled(false);

					tfEficiencia.setBackground(SystemColor.info);
					tfLarguraOperacional.setBackground(SystemColor.info);
					tfCco.setBackground(SystemColor.info);

					if (getArrayConjuntoMecanizado().size() > 0)
						carregarParametrosOperacao();
				}
			}
		});
	}	

	public JPanel construirPainelGraxa(){
		JPanel p = new JPanel();
		p.setBounds(5, 229, 520, 70);
		pCustosVariaveis.add(p);
		p.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder("  Graxa  "),
				BorderFactory.createEmptyBorder(5,5,5,5)));
		p.setLayout(new GridLayout(0, 5, 10, 0));

		JLabel lblPreorkg = new JLabel("Preço (R$/Kg)");
		JLabel lblTempoEntreLubrif = new JLabel("Tempo entre lubrif. (h)");
		JLabel lblQuantidadekg = new JLabel("Quantidade (Kg)");
		
		TfHandlerGraxa handlerGraxa = new TfHandlerGraxa();
		
		tfQuantidadeGraxa = new JTextField();
		tfQuantidadeGraxa.setColumns(10);
		tfQuantidadeGraxa.getDocument().addDocumentListener(handlerGraxa);

		tfTempoLubrificacao = new JTextField();
		tfTempoLubrificacao.setColumns(10);		
		tfTempoLubrificacao.getDocument().addDocumentListener(handlerGraxa);
	
		tfPrecoGraxa = new JTextField();		
		tfPrecoGraxa.setColumns(10);
		tfPrecoGraxa.getDocument().addDocumentListener(handlerGraxa);

		p.add(lblPreorkg);
		p.add(lblTempoEntreLubrif);		
		p.add(lblQuantidadekg);
		JLabel lblTotalrh_3 = new JLabel("Total (R$)/h)");
		p.add(lblTotalrh_3);		
		JLabel lblNewLabel_6 = new JLabel("");
		p.add(lblNewLabel_6);		
		p.add(tfPrecoGraxa);		
		p.add(tfTempoLubrificacao);		
		p.add(tfQuantidadeGraxa);		

		tfGraxaTotal = new JTextField();
		tfGraxaTotal.setColumns(10);
		tfGraxaTotal.setEditable(false);
		tfGraxaTotal.setBackground(SystemColor.info);
		tfGraxaTotal.setText("");
		p.add(tfGraxaTotal);

		btnAdicionarGraxa = new JButton("Adicionar");
		btnAdicionarGraxa.setEnabled(false);
		p.add(btnAdicionarGraxa);

		btnAdicionarGraxa.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				adicionarAoGrid("Graxa", tfGraxaTotal.getText());
			}
		});		

		return p;
	}



	public JPanel construirPainelTotal(){
		JPanel p = new JPanel();
		p.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder(" Total custo-horário e operacional "),
				BorderFactory.createEmptyBorder(5,5,5,5)));		
		p.setBounds(0, 600, 874, 70);
		getContentPane().add(p);
		p.setLayout(new GridLayout(0, 5, 10, 0));

		JLabel lblNewLabel_12 = new JLabel("T. custos fixos (R$/h)");
		JLabel lblNewLabel_13 = new JLabel("T. custos variáveis (R$/h)");
		JLabel lblTotalCustoHorrio = new JLabel("T. custo-horário (R$/h)");
		JLabel lblNewLabel_14 = new JLabel("T. custo operacional (R$/ha)");
		JLabel lblNewLabel_11 = new JLabel("");

		JButton btnNewButton = new JButton("Rel. custo-horário");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				gerarRelatorioEstimativa();
			}
		});

		TfHandlerChTotal tfhCF = new TfHandlerChTotal();
		TfHandlerChTotal tfhCV = new TfHandlerChTotal();	

		tfTotalCustosFixos = new JTextField();		
		tfTotalCustosFixos.setEditable(false);
		tfTotalCustosFixos.setBackground(SystemColor.info);
		tfTotalCustosFixos.getDocument().addDocumentListener(tfhCF);

		tfTotalCustosVariaveis = new JTextField();
		tfTotalCustosVariaveis.setEditable(false);
		tfTotalCustosVariaveis.setBackground(SystemColor.info);
		tfTotalCustosVariaveis.getDocument().addDocumentListener(tfhCV);

		tfTotalCustoHorario = new JTextField();
		tfTotalCustoHorario.setEditable(false);
		tfTotalCustoHorario.setBackground(SystemColor.info);
		tfTotalCustoOperacionalHectares = new JTextField();		
		tfTotalCustoOperacionalHectares.setEditable(false);
		tfTotalCustoOperacionalHectares.setBackground(SystemColor.info);

		p.add(lblNewLabel_12);		
		p.add(lblNewLabel_13);		
		p.add(lblTotalCustoHorrio);	
		p.add(lblNewLabel_14);		
		p.add(lblNewLabel_11);		

		tfTotalCustosFixos.setColumns(10);
		tfTotalCustosVariaveis.setColumns(10);		
		tfTotalCustoHorario.setColumns(10);
		tfTotalCustoOperacionalHectares.setColumns(10);

		p.add(tfTotalCustosFixos);
		p.add(tfTotalCustosVariaveis);
		p.add(tfTotalCustoHorario);
		p.add(tfTotalCustoOperacionalHectares);

		p.add(btnNewButton);

		tfTotalCustoHorario.getDocument().addDocumentListener(new DocumentListener() {			
			@Override
			public void removeUpdate(DocumentEvent e) {
				calcularTotalOperacional();				
			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				calcularTotalOperacional();				
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				calcularTotalOperacional();				
			}
		});

		return p;
	}

	public void centraliza(){
		Dimension T = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension J = getSize();
		if (J.height > T.height) setSize(J.width,T.height);
		if (J.width > T.width) setSize(T.width,J.height);
		setLocation((T.width - J.width)/2,(T.height - J.height)/2);		
	}

	public void definirFoco() {
		// TODO Auto-generated method stub

	}

	public void habilitarCampos() {
		// TODO Auto-generated method stub

	}

	public void desabiltarCampos() {
		// TODO Auto-generated method stub

	}	

	public void popularMaquina(){
		arrayConjuntoMecanizado.clear();
		arrayMaquinaDisponivel = getDao().get("Maquina");		

		for (Object o : arrayMaquinaDisponivel) {	
			Maquina m = (Maquina) o;
			modelMaquinaDisponivel.addElement(m.getDescricao());
		}		
	}

	private void popularTipoOperacao(){
		comboOperacao.removeAllItems();
		listTipoOperacao = getDao().get("TipoOperacao");

		for (Object o : listTipoOperacao) {
			TipoOperacao t = (TipoOperacao) o;
			comboOperacao.addItem(t.getDescricao());
		}
	}

	public void adicionarMaquina(){
		if (listMaquinaDisponivel.getSelectedIndex() == -1){
			JOptionPane.showMessageDialog(null, "Selecione uma máquina para adicionar ao conjunto mecanizado!", "Erro ao adicionar", JOptionPane.ERROR_MESSAGE);
			listMaquinaDisponivel.requestFocus();
			return;
		}

		adicionarNoConjuntoMecanizado();

		if (getArrayConjuntoMecanizado().size()>0){			
			obterValoresCombustivel();
			calcularTotalManutencao();	
			carregarParametrosOperacao();

			if (comboProduto.getSelectedIndex() != -1){
				tfQuantidade.setText(df.format(Maquina.verificaQuantidade(comboTipoProduto.getSelectedItem().toString(), 
						getArrayConjuntoMecanizado())));
			}
		}
	}


	public void adicionarNoConjuntoMecanizado(){		
		int index = listMaquinaDisponivel.getSelectedIndex();
		Maquina aux = getArrayMaquinaDisponivel().get(index);

		if (!aux.isFill() == true){
			JOptionPane.showMessageDialog(null, "ATENÇÃO: A máquina selecionada não possui todos os campos necessários aos cálculos "
					+ "\ndevidamente preenchidos. Para que o cálculo da estimativa seja realizado, por favor"
					+ "\nacesse o cadastro desta máquina e preencha as informações faltantes!", "Aviso", JOptionPane.WARNING_MESSAGE);
			return;
		}

		EstimativaCustosFixos temp = calcularDepreciacaoMaquinaAdicionada(aux);
		getArrayConjuntoCalculado().add(temp);

		getArrayConjuntoMecanizado().add(aux);		
		getModelConjuntoMecanizado().addElement(aux.getDescricao());

		getArrayMaquinaDisponivel().remove(aux);
		getModelMaquinaDisponivel().remove(index);

		controlarBotoesAdicaoRemocao();
		atualizarGradeCustosFixos();
	}

	public void removerDoConjuntoMecanizado(){
		int index = listConjuntoMecanizado.getSelectedIndex();

		Maquina aux = getArrayConjuntoMecanizado().get(index);

		getArrayMaquinaDisponivel().add(aux);
		getModelMaquinaDisponivel().addElement(aux.getDescricao());		

		getArrayConjuntoCalculado().remove(index);

		getArrayConjuntoMecanizado().remove(aux);
		getModelConjuntoMecanizado().remove(index);

		controlarBotoesAdicaoRemocao();
		atualizarGradeCustosFixos();
	}

	public void controlarBotoesAdicaoRemocao(){
		if (getModelConjuntoMecanizado().getSize() <= 0){
			bRemoverMaquina.setEnabled(false);
		} else {
			bRemoverMaquina.setEnabled(true);
		}

		if (getModelMaquinaDisponivel().getSize() <= 0){
			bAdicionarMaquina.setEnabled(false);
		} else {
			bAdicionarMaquina.setEnabled(true);
		}		
	}

	public void atualizarGradeCustosFixos(){
		setEcfModel(new EstimativaCFModel(getArrayConjuntoCalculado()));
		gradeCustosFixos.setModel(getEcfModel());
		gradeCustosFixos.repaint();

		tfTotalCustosFixos.setText(getDf().format(getEcfModel().calcularTotalCustosFixos()));
	}

	public void identificarRadioMarcado(){
		if (rbtnDeclinioEmDobro.isSelected() == true){
			rbtnDeclinioEmDobro.setSelected(false);
			rbtnDeclinioEmDobro.setSelected(true);

		}

		if (rbtnDigitosPeriodicos.isSelected() == true){
			rbtnDigitosPeriodicos.setSelected(false);
			rbtnDigitosPeriodicos.setSelected(true);

		}

		if (rbtnPorcentagensConstantes.isSelected() == true){
			rbtnPorcentagensConstantes.setSelected(false);
			rbtnPorcentagensConstantes.setSelected(true);
		}

		if (rbtnProporcional.isSelected() == true){
			rbtnProporcional.setSelected(false);
			rbtnProporcional.setSelected(true);
		}

		if (rbtnQuotas.isSelected() == true){
			rbtnQuotas.setSelected(false);
			rbtnQuotas.setSelected(true);				
		}
	}

	public EstimativaCustosFixos calcularDepreciacaoMaquinaAdicionada(Maquina m){
		EstimativaCustosFixos ecfAux = null;
		if (m.isFill() == true){

			DepreciacaoModel model;		
			float depreciacaoHora = 0;

			if (rbtnDeclinioEmDobro.isSelected()){ 
				setDepre(new DeclinioEmDobro(m));
			}	
			if (rbtnDigitosPeriodicos.isSelected()){ 
				setDepre(new DigitosPeriodicos(m));
			}
			if (rbtnPorcentagensConstantes.isSelected()){ 
				setDepre(new PorcentagensConstantes(m));
			}
			if (rbtnProporcional.isSelected()) {
				setDepre(new Proporcional(m));
			}
			if (rbtnQuotas.isSelected()){
				setDepre(new QuotasConstantes(m));
			}

			model = new DepreciacaoModel(getDepre());		  		
			depreciacaoHora = getDepre().depreciacaoHoraria();

			ecfAux = new EstimativaCustosFixos(m.getDescricao(), m.getValorJuros(), 
					m.getValorSeguros(), m.getValorAlojamento(), depreciacaoHora);				
		}

		return ecfAux;
	}

	public void popularTipoProduto(){		
		for (TProduto o : TProduto.values()) {
			comboTipoProduto.addItem(o.getNome());
		}		
	}

	public void popularProduto(){
		comboProduto.removeAllItems();
		listProduto = getDao().filter("Produto", "tipoProduto", comboTipoProduto.getSelectedItem().toString());
		for (Object o : listProduto) {	
			Produto p = (Produto) o;
			comboProduto.addItem(p.getDescricao());
		}
	}

	public void calcularTotalProduto(){

		if (getArrayConjuntoMecanizado().size() == 0){
			tfProdutosTotal.setText("0.00");
			btnAdicionarProduto.setEnabled(false);
			return;
		}

		float valorUnitario;
		float troca;
		float quantidade;
		try {
			valorUnitario = converte(tfValorUnitario.getText());
			troca = converte(tfTroca.getText());
			quantidade = converte(tfQuantidade.getText());			
			tfProdutosTotal.setText(getDf().format((valorUnitario * quantidade) / troca));			
			btnAdicionarProduto.setEnabled(true);						
		} catch (Exception e) {
			tfProdutosTotal.setText("0.00");
			btnAdicionarProduto.setEnabled(false);
		}
	}

	public void adicionarAoGrid(String item, String valor){
		/*
		 * Adiciona cada item de CV ao gridConsumo
		 * Atualiza ambas as grades
		 * Calcula o total do CV
		 */

		Float total = 0f;
		try {
			total = converte(valor);		
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}				

		cvm.add(new CampoDeCusto(item, total));
		gradeConsumo.updateUI();

		tfTotalCustosVariaveis.setText(getDf().format(cvm.getTotal()));

		//tfTotalCustosVariaveis.setText(Float.toString(cvm.getTotal()));
		//calcularTotalCustoHorario();
	}


	private void calcularTotalCustoHorario(){		
		float cf = 0, cv = 0;		

		/*nenhum campo preenchido*/
		if ((tfTotalCustosFixos.getText().equals("")) && (tfTotalCustosVariaveis.getText().equals(""))){
			return;
		}

		/*apenas cv preenchido*/
		if (tfTotalCustosFixos.getText().equals("")){
			try {				
				cv = converte(tfTotalCustosVariaveis.getText())/converte(tfDuracaoOperacao.getText());
				tfTotalCustoHorario.setText(getDf().format(cv));								
				return;
			} catch (Exception e) {
				
			}	
		}

		/*apenas cf preenchido*/
		if (tfTotalCustosVariaveis.getText().equals("")){
			try {				
				cf = converte(tfTotalCustosFixos.getText())/converte(tfDuracaoOperacao.getText());
				tfTotalCustoHorario.setText(getDf().format(cf));
				return;
			} catch (Exception e) {
				
			}	
		}

		/*se chegou até aqui, faz a soma de cf e cv*/
		try {
			cf = converte(tfTotalCustosFixos.getText());
			cv = converte(tfTotalCustosVariaveis.getText());
			tfTotalCustoHorario.setText(getDf().format(((cf + cv)/converte(tfDuracaoOperacao.getText()))));
		} catch (Exception e) {
			
		}			
	}

	public void obterValoresCombustivel(){
		float consumoLH = 0;
		float consumoKW = 0.2135f;			
		float potenciaCV = 0;
		float potenciaKW = 0;		

		Maquina aux = Maquina.maiorConsumo(getArrayConjuntoMecanizado());		

		if (aux.getTipoPotencia().equalsIgnoreCase("cv")){			
			rbtnConsumoLitros.setEnabled(true);
			rbtnConsumoKilowats.setSelected(false);
			potenciaCV = aux.getPotencia();
			tfPotenciaCv.setText(df.format(potenciaCV));

			potenciaKW = (potenciaCV*0.735499f);
			tfPotenciaKw.setText(df.format(potenciaKW));		

			consumoLH = aux.getConsumoEspecifico();
			tfConsumo.setText(df.format(consumoLH));
		} else {
			rbtnConsumoLitros.setEnabled(false);
			rbtnConsumoKilowats.setSelected(true);
			potenciaKW = aux.getPotencia();
			tfPotenciaKw.setText(df.format(potenciaKW));
			tfPotenciaCv.setText(df.format(potenciaKW/0.735499));
			consumoKW = aux.getConsumoEspecifico();
			tfConsumoEstimado.setText(df.format(consumoKW));
			tfConsumo.setText("");
		}		
	}

	public void calcularTotalCombustivel(){		
		float consumoLH = 0;
		float consumoKW = 0;;

		try {
			consumoKW = converte("0,2135");
		} catch (ParseException e) {

		}
		
		float precoLitro = 0;		
		float potenciaKW = 0;		

		Maquina aux = Maquina.maiorConsumo(getArrayConjuntoMecanizado());

		try {			
			if (aux.getTipoPotencia().equalsIgnoreCase("cv"))
				consumoLH = converte(tfConsumo.getText());

			potenciaKW = converte(tfPotenciaKw.getText());
//			consumoKW = converte(tfConsumoEstimado.getText());
			precoLitro = converte(tfPrecoLitro.getText());
		} catch (ParseException e1) {
			return;
		} catch (NumberFormatException nfe){		
			tfCombustivelTotal.setText("0.00");
			btnAdicionarCombustivel.setEnabled(false);
			return;
		}

		if ((aux.getTipoPotencia().equalsIgnoreCase("cv") && (rbtnConsumoLitros.isSelected() == true))){			
			tfCombustivelTotal.setText(df.format(consumoLH * precoLitro));			
		}

		if ((aux.getTipoPotencia().equalsIgnoreCase("cv") && (rbtnConsumoKilowats.isSelected() == true))){			
			tfCombustivelTotal.setText(df.format(precoLitro * consumoKW * potenciaKW));			
		}

		if ((aux.getTipoPotencia().equalsIgnoreCase("kw") && (rbtnConsumoKilowats.isSelected() == true))){			
			tfCombustivelTotal.setText(df.format(precoLitro * consumoKW * potenciaKW));			
		}

		btnAdicionarCombustivel.setEnabled(true);
	}

	public void calcularTotalManutencao(){
		try {
			float taxaManutencao = 0;
			float valorFinal = 0;
			taxaManutencao = converte(tfTaxaManutencao.getText());

			valorFinal = Maquina.calcularManutencao(getArrayConjuntoMecanizado(), taxaManutencao);

			tfManutencaoTotal.setText(getDf().format(valorFinal));
			btnAdicionarManutencao.setEnabled(true);
		} catch (Exception e) {
			tfManutencaoTotal.setText("0.00");
			btnAdicionarManutencao.setEnabled(false);
		}
	}

	public void calcularTotalGraxa(){
		float total = 0;
		try {			
			total = (converte(tfQuantidadeGraxa.getText()) * converte(tfPrecoGraxa.getText())) / converte(tfTempoLubrificacao.getText()); 
			tfGraxaTotal.setText(getDf().format(total));
			btnAdicionarGraxa.setEnabled(true);
		} catch (Exception e) {		
			
			tfGraxaTotal.setText("0.00");
			btnAdicionarGraxa.setEnabled(false);			
		}
	}

	public void calcularTotalSalario(){
		Maquina m = Maquina.maiorTempoUso(getArrayConjuntoMecanizado());
		float salarioMensal = 0, totalSalario = 0;
		try {
			salarioMensal = converte(tfSalario.getText());
			totalSalario = (salarioMensal * 13.33f)/m.getTempoUso();
			tfTotalSalario.setText(df.format(totalSalario));
			btnAdicionarSalario.setEnabled(true);
		}
		catch (Exception e) {
			tfTotalSalario.setText("0.00");
			btnAdicionarSalario.setEnabled(false);
		}

	}

	public static float converte(String arg) throws ParseException{
		/*obtem um NumberFormat para o Locale default (BR)*/
		NumberFormat nf = NumberFormat.getNumberInstance(new Locale("pt", "BR"));

		/*converte um número com vírgulas ex: 2,56 para float*/
		float number = nf.parse(arg).floatValue();
		return number;
	}	

	private JPanel buildPanelCustosVariaveis() {
		pCustosVariaveis = new JPanel();
		JPanel pConsumo = new JPanel();
		JPanel pLeft = new JPanel();
		JPanel pRight = new JPanel();

		JPanel pCombustivel = construirPainelCombustivel();		
		JPanel pSalario = construirPainelSalario();
		JPanel pManutencao = construirPainelManutencao();
		JPanel pProduto = construirPainelProduto();
		//JPanel pGraxa = construirPainelGraxa();

		pCustosVariaveis.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder("  Custos variáveis  "),
				BorderFactory.createEmptyBorder(5,5,5,5)));

		gradeConsumo = new JTable();
		JScrollPane scrollPaneConsumo = new JScrollPane(gradeConsumo);		 

		JButton btnRemoverItem = new JButton("Remover item selecionado");

		pCustosVariaveis.setSize(874, 303);
		pCustosVariaveis.setLocation(0, 295);
		pCustosVariaveis.setLayout(null);

		pConsumo.setLocation(530, 15);
		pConsumo.setSize(343, 120);
		pConsumo.setLayout(new BorderLayout());
		JLabel label = new JLabel("Itens consumidos", JLabel.CENTER);
		label.setBackground(SystemColor.activeCaptionBorder);
		pConsumo.add(label,BorderLayout.NORTH);
		pConsumo.add(scrollPaneConsumo,BorderLayout.CENTER);

		setCvm(new CustosVariaveisModel());

		gradeConsumo.setFocusable(false);
		gradeConsumo.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		gradeConsumo.setCursor(new Cursor(Cursor.HAND_CURSOR));
		gradeConsumo.setToolTipText("Clique aqui para carregar as informaçoes");
		gradeConsumo.setModel(new DefaultTableModel());
		gradeConsumo.setModel(getCvm());

		btnRemoverItem.setBounds(530, 135, 343, 23);
		pLeft.setBounds(5, 15, 520, 214);
		pLeft.setLayout(new GridLayout(2, 1, 0, 0));
		pLeft.add(pCombustivel);
		pLeft.add(pProduto);

		pRight.setBounds(530, 159, 343, 140);
		pRight.setLayout(new GridLayout(2, 1, 0, 0));
		pRight.add(pSalario);		
		pRight.add(pManutencao);	

		pCustosVariaveis.add(pConsumo);		
		pCustosVariaveis.add(btnRemoverItem);		
		pCustosVariaveis.add(pLeft);				
		pCustosVariaveis.add(pRight);

		popularTipoProduto();		

		btnRemoverItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cvm.remove(gradeConsumo.getSelectedRow());

				tfTotalCustosVariaveis.setText(getDf().format(cvm.getTotal()));

				gradeConsumo.updateUI();
			}
		});

		return pCustosVariaveis;
	}

	public JPanel construirPainelCombustivel(){
		JPanel p = new JPanel();
		p.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder("  Combustível  "),
				BorderFactory.createEmptyBorder(5,5,5,5)));
		p.setLayout(new GridLayout(4, 4, 10, 0));

		JLabel lblPreorl = new JLabel("Preço (R$/L)");
		lbPotencia = new JLabel("Pot. efetiva (cv)");
		JLabel lblPotEfetivakw = new JLabel("Pot. efetiva (kW)");
		JLabel lblTotalrh_1 = new JLabel("Total (R$/h)");
		JLabel lblNewLabel_1 = new JLabel("Consumo (L/h)");
		JLabel lblConsumolh = new JLabel("Cons. est. (L/kW)");
		JLabel lblNewLabel_3 = new JLabel("");

		tfPrecoLitro = new JTextField();
		tfPotenciaCv = new JTextField();
		tfPotenciaKw = new JTextField();
		tfCombustivelTotal = new JTextField();
		tfConsumo = new JTextField();
		tfConsumoEstimado = new JTextField();

		btnAdicionarCombustivel = new JButton("Adicionar");
		rbtnConsumoLitros = new JRadioButton("Cons. (L/h)");
		rbtnConsumoKilowats = new JRadioButton("Cons. (L/kW)");
		ButtonGroup groupPotencia = new ButtonGroup();

		tfPrecoLitro.setColumns(10);

		tfPotenciaCv.setEditable(false);
		tfPotenciaCv.setBackground(SystemColor.info);		
		tfPotenciaCv.setColumns(10);

		tfPotenciaKw.setEditable(false);
		tfPotenciaKw.setBackground(SystemColor.info);		
		tfPotenciaKw.setColumns(10);

		tfCombustivelTotal.setEditable(false);
		tfCombustivelTotal.setBackground(SystemColor.info);		
		tfCombustivelTotal.setColumns(10);

		tfConsumo.setEditable(false);
		tfConsumo.setBackground(SystemColor.info);		
		tfConsumo.setColumns(10);

		tfConsumoEstimado.setText("0,2135");		
		tfConsumoEstimado.setColumns(10);

		rbtnConsumoLitros.setSelected(true);
		groupPotencia.add(rbtnConsumoLitros);
		groupPotencia.add(rbtnConsumoKilowats);

		btnAdicionarCombustivel.setEnabled(false);

		p.add(lblPreorl);
		p.add(lbPotencia);		
		p.add(lblPotEfetivakw);		
		p.add(lblTotalrh_1);
		p.add(tfPrecoLitro);
		p.add(tfPotenciaCv);		
		p.add(tfPotenciaKw);
		p.add(tfCombustivelTotal);		
		p.add(rbtnConsumoLitros);	
		p.add(lblNewLabel_1);	
		p.add(lblConsumolh);		
		p.add(lblNewLabel_3);		
		p.add(rbtnConsumoKilowats);
		p.add(tfConsumo);
		p.add(tfConsumoEstimado);		
		p.add(btnAdicionarCombustivel);

		tfPrecoLitro.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				calcularTotalCombustivel();
			}
		});

		rbtnConsumoLitros.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				try {
					calcularTotalCombustivel();	
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		});

		rbtnConsumoKilowats.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				try {
					calcularTotalCombustivel();	
				} catch (Exception ex) {
					// TODO: handle exception
				}
			}
		});

		tfConsumoEstimado.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				calcularTotalCombustivel();
			}
		});

		btnAdicionarCombustivel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				adicionarAoGrid("Combustível", tfCombustivelTotal.getText());
			}
		});

		return p;		
	}

	public JPanel construirPainelProduto(){
		JPanel p = new JPanel();
		p.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder("  Produtos  "),
				BorderFactory.createEmptyBorder(5,5,5,5)));

		GridBagLayout gbl_pProduto = new GridBagLayout();
		gbl_pProduto.columnWidths = new int[] {110, 110, 110, 110};
		gbl_pProduto.rowHeights = new int[] {10, 5, 10, 5};
		gbl_pProduto.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0};
		gbl_pProduto.rowWeights = new double[]{5.0, 5.0, 5.0, 5.0};
		p.setLayout(gbl_pProduto);

		comboTipoProduto = new JComboBox();
		comboProduto = new JComboBox();

		tfProdutosTotal = new JTextField();
		tfValorUnitario = new JTextField();
		tfTroca = new JTextField();
		tfQuantidade = new JTextField();

		btnAdicionarProduto = new JButton("Adicionar");

		JLabel lblTipoDeProduto = new JLabel("Tipo de produto");
		JLabel lblProduto = new JLabel("Produto");
		JLabel lblTotalrh = new JLabel("Total (R$/h)");
		JLabel lblValorUnitrior = new JLabel("Valor unitário (R$)");	
		JLabel lblTrocah = new JLabel("Troca (h)");
		JLabel lblQuantidade = new JLabel("Quantidade");

		GridBagConstraints gbc_lblTipoDeProduto = new GridBagConstraints();
		gbc_lblTipoDeProduto.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblTipoDeProduto.insets = new Insets(0, 0, 5, 5);
		gbc_lblTipoDeProduto.gridx = 0;
		gbc_lblTipoDeProduto.gridy = 0;		

		GridBagConstraints gbc_lblProduto = new GridBagConstraints();
		gbc_lblProduto.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblProduto.insets = new Insets(0, 0, 5, 5);
		gbc_lblProduto.gridx = 1;
		gbc_lblProduto.gridy = 0;

		GridBagConstraints gbc_lblTotalrh = new GridBagConstraints();
		gbc_lblTotalrh.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblTotalrh.insets = new Insets(0, 0, 5, 0);
		gbc_lblTotalrh.gridx = 3;
		gbc_lblTotalrh.gridy = 0;

		GridBagConstraints gbc_comboTipoProduto = new GridBagConstraints();
		gbc_comboTipoProduto.fill = GridBagConstraints.BOTH;
		gbc_comboTipoProduto.insets = new Insets(0, 0, 5, 5);
		gbc_comboTipoProduto.gridx = 0;
		gbc_comboTipoProduto.gridy = 1;

		GridBagConstraints gbc_comboProduto = new GridBagConstraints();
		gbc_comboProduto.gridwidth = 2;
		gbc_comboProduto.fill = GridBagConstraints.BOTH;
		gbc_comboProduto.insets = new Insets(0, 0, 5, 5);
		gbc_comboProduto.gridx = 1;
		gbc_comboProduto.gridy = 1;		

		GridBagConstraints gbc_tfProdutosTotal = new GridBagConstraints();
		gbc_tfProdutosTotal.insets = new Insets(0, 0, 5, 0);
		gbc_tfProdutosTotal.fill = GridBagConstraints.BOTH;
		gbc_tfProdutosTotal.gridx = 3;
		gbc_tfProdutosTotal.gridy = 1;

		GridBagConstraints gbc_lblValorUnitrior = new GridBagConstraints();
		gbc_lblValorUnitrior.fill = GridBagConstraints.BOTH;
		gbc_lblValorUnitrior.insets = new Insets(0, 0, 5, 5);
		gbc_lblValorUnitrior.gridx = 0;
		gbc_lblValorUnitrior.gridy = 2;

		GridBagConstraints gbc_lblTrocah = new GridBagConstraints();
		gbc_lblTrocah.fill = GridBagConstraints.BOTH;
		gbc_lblTrocah.insets = new Insets(0, 0, 5, 5);
		gbc_lblTrocah.gridx = 1;
		gbc_lblTrocah.gridy = 2;

		GridBagConstraints gbc_lblQuantidade = new GridBagConstraints();
		gbc_lblQuantidade.fill = GridBagConstraints.BOTH;
		gbc_lblQuantidade.insets = new Insets(0, 0, 5, 5);
		gbc_lblQuantidade.gridx = 2;
		gbc_lblQuantidade.gridy = 2;

		GridBagConstraints gbc_tfValorUnitario = new GridBagConstraints();
		gbc_tfValorUnitario.fill = GridBagConstraints.BOTH;
		gbc_tfValorUnitario.insets = new Insets(0, 0, 0, 5);
		gbc_tfValorUnitario.gridx = 0;
		gbc_tfValorUnitario.gridy = 3;

		GridBagConstraints gbc_tfTroca = new GridBagConstraints();
		gbc_tfTroca.fill = GridBagConstraints.BOTH;
		gbc_tfTroca.insets = new Insets(0, 0, 0, 5);
		gbc_tfTroca.gridx = 1;
		gbc_tfTroca.gridy = 3;

		GridBagConstraints gbc_tfQuantidade = new GridBagConstraints();
		gbc_tfQuantidade.insets = new Insets(0, 0, 0, 5);
		gbc_tfQuantidade.fill = GridBagConstraints.BOTH;
		gbc_tfQuantidade.gridx = 2;
		gbc_tfQuantidade.gridy = 3;

		GridBagConstraints gbc_btnAdicionarProduto = new GridBagConstraints();
		gbc_btnAdicionarProduto.fill = GridBagConstraints.BOTH;
		gbc_btnAdicionarProduto.gridx = 3;
		gbc_btnAdicionarProduto.gridy = 3;

		tfProdutosTotal.setColumns(10);
		tfProdutosTotal.setEditable(false);
		tfProdutosTotal.setBackground(SystemColor.info);

		tfValorUnitario.setColumns(10);

		tfTroca.setEditable(false);
		tfTroca.setBackground(SystemColor.info);
		tfTroca.setColumns(10);

		tfQuantidade.setEditable(false);
		tfQuantidade.setBackground(SystemColor.info);
		tfQuantidade.setColumns(10);

		btnAdicionarProduto.setEnabled(false);

		p.add(lblTipoDeProduto, gbc_lblTipoDeProduto);
		p.add(lblProduto, gbc_lblProduto);		
		p.add(lblTotalrh, gbc_lblTotalrh);		
		p.add(comboTipoProduto, gbc_comboTipoProduto);		
		p.add(comboProduto, gbc_comboProduto);
		p.add(tfProdutosTotal, gbc_tfProdutosTotal);		
		p.add(lblValorUnitrior, gbc_lblValorUnitrior);		
		p.add(lblTrocah, gbc_lblTrocah);	
		p.add(lblQuantidade, gbc_lblQuantidade);		
		p.add(tfValorUnitario, gbc_tfValorUnitario);	
		p.add(tfTroca, gbc_tfTroca);	
		p.add(tfQuantidade, gbc_tfQuantidade);	
		p.add(btnAdicionarProduto, gbc_btnAdicionarProduto);

		comboTipoProduto.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				popularProduto();
			}
		});

		comboProduto.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (comboProduto.getSelectedIndex() != -1){
					tfTroca.setText(df.format(listProduto.get(comboProduto.getSelectedIndex()).getTroca()));
					tfQuantidade.setText(df.format(Maquina.verificaQuantidade(comboTipoProduto.getSelectedItem().toString(), 
							getArrayConjuntoMecanizado())));
				}
				else {
					tfTroca.setText("");
				}
			}
		});

		tfValorUnitario.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				calcularTotalProduto();
			}
		});

		btnAdicionarProduto.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				adicionarAoGrid(comboProduto.getSelectedItem().toString(), tfProdutosTotal.getText());				
			}
		});

		tfTroca.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				calcularTotalProduto();
			}
		});

		return p;
	}

	public JPanel construirPainelSalario(){
		JPanel p = new JPanel();

		p.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder("  Salário  "),
				BorderFactory.createEmptyBorder(5,5,5,5)));
		p.setLayout(new GridLayout(2, 2, 10, 0));

		JLabel lblMensalr = new JLabel("Valor mensal (R$)");
		JLabel lblTotalrh_2 = new JLabel("Total (R$/h)");
		JLabel label = new JLabel("");

		tfSalario = new JTextField();
		tfSalario.setColumns(10);

		tfTotalSalario = new JTextField();
		tfTotalSalario.setEditable(false);
		tfTotalSalario.setBackground(SystemColor.info);		
		tfTotalSalario.setColumns(10);		

		btnAdicionarSalario = new JButton("Adicionar");
		btnAdicionarSalario.setEnabled(false);

		p.add(lblMensalr);		
		p.add(lblTotalrh_2);		
		p.add(label);			
		p.add(tfSalario);	
		p.add(tfTotalSalario);	
		p.add(btnAdicionarSalario);

		tfSalario.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if (arrayConjuntoMecanizado.size() == 0){
					JOptionPane.showMessageDialog(null, "Não é possível efetuar o cálculo do valor do salário enquanto não houver(em) máquina(s) selecionada(s)!", 
							"Erro ao calcular", JOptionPane.ERROR_MESSAGE);
					tfSalario.setText("");
					return;
				}
				calcularTotalSalario();
			}
		});	

		btnAdicionarSalario.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				adicionarAoGrid("Salário", tfTotalSalario.getText());
			}
		});

		return p;
	}

	public JPanel construirPainelManutencao(){
		JPanel p = new JPanel();
		p.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder("  Manutenção  "),
				BorderFactory.createEmptyBorder(5,5,5,5)));
		p.setLayout(new GridLayout(0, 3, 10, 0));

		JLabel lblManutenorh = new JLabel("Valor (R$/h)");
		JLabel lblNewLabel_2 = new JLabel("Taxa anual (%)");
		JLabel lblNewLabel_9 = new JLabel("");

		tfManutencaoTotal = new JTextField();
		tfManutencaoTotal.setBackground(SystemColor.info);
		tfManutencaoTotal.setEditable(false);
		tfManutencaoTotal.setColumns(10);

		tfTaxaManutencao = new JTextField();		
		tfTaxaManutencao.setToolTipText("Taxa anual reparo/manut. (%)");
		tfTaxaManutencao.setBackground(Color.WHITE);
		tfTaxaManutencao.setText("10,00");
		tfTaxaManutencao.setColumns(10);	

		btnAdicionarManutencao = new JButton("Adicionar");
		btnAdicionarManutencao.setEnabled(false);

		p.add(lblManutenorh);		
		p.add(lblNewLabel_2);
		p.add(lblNewLabel_9);	
		p.add(tfManutencaoTotal);
		p.add(tfTaxaManutencao);
		p.add(btnAdicionarManutencao);

		btnAdicionarManutencao.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				adicionarAoGrid("Manutenção", tfManutencaoTotal.getText());
			}
		});

		tfTaxaManutencao.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				calcularTotalManutencao();
			}
		});

		return p;		
	}

	public float calcularCCO(){		
		float larguraOperacional = Maquina.maiorLarguraOperacional(getArrayConjuntoMecanizado());
		float duracao = 0;
		float resultado = 0;
		float tamanho = 0;
		setCapacidadeDeCampo(new CCO());

		try {
			duracao = converte(tfDuracaoOperacao.getText());
			tamanho = converte(tfTamanhoArea.getText());

			resultado = capacidadeDeCampo.calcular(larguraOperacional, 0f, duracao, tamanho);
		} catch (Exception e) {

		}
		return resultado;
	}

	public float calcularCCT(){
		float larguraOperacional = Maquina.maiorLarguraOperacional(getArrayConjuntoMecanizado());
		float resultado = 0;
		float velocidade = 0;
		float tamanho = 0;
		setCapacidadeDeCampo(new CCT());

		try {
			velocidade = converte(tfVelocidade.getText());
			tamanho = converte(tfTamanhoArea.getText());
			resultado = capacidadeDeCampo.calcular(larguraOperacional, velocidade, 0f, tamanho);
		} catch (Exception e) {

		}
		return resultado;
	}

	/*
	 * Aviso: esse método é independente do cálculo realizado em Operação. Lá, usa-se a OperacaoDAO
	 */
	public float calcularEficiencia(){
		float cco = calcularCCO();
		float cct = calcularCCT();
		return (cco / cct)*100;
	}

	public void carregarParametrosOperacao(){
		if (!chInformarManualmente.isSelected()){
			tfLarguraOperacional.setText(getDf().format(Maquina.maiorLarguraOperacional(getArrayConjuntoMecanizado())));
			tfCco.setText(getDf().format(calcularCCO()));
			tfEficiencia.setText(getDf().format(calcularEficiencia()));

		}
	}

//	public void calcularCustoHorarioTotal(){
//
//		float custoFixo = 0;
//		float custoVariavel = 0;
//		float total = 0;
//		try {
//
//			custoFixo = converte(tfTotalCustosFixos.getText()); 
//			custoVariavel = converte(tfTotalCustosVariaveis.getText());
//			total = custoFixo + custoVariavel;
//			tfTotalCustoHorario.setText(getDf().format(total));			
//
//		} catch (Exception e) {
//			tfTotalCustoHorario.setText("0,00");			
//		}
//
//	}

	public void calcularTotalOperacional(){		
		float duracao = 0;
		float custoHorario = 0;
		float CCO = 0;

		try {
			duracao = converte(tfDuracaoOperacao.getText());
			custoHorario = converte(tfTotalCustoHorario.getText());
			CCO = converte(tfCco.getText());

			tfTotalCustoOperacional.setText(getDf().format(custoHorario * duracao));
			tfTotalCustoOperacionalHectares.setText(getDf().format(custoHorario/CCO));
		} catch (Exception e) {
			tfTotalCustoOperacional.setText("0,00");
			tfTotalCustoOperacionalHectares.setText("0,00");
		}
	}


	private class TfHandlerChTotal implements DocumentListener{

		@Override
		public void insertUpdate(DocumentEvent e) {
			carregarParametrosOperacao();		
		}

		@Override
		public void removeUpdate(DocumentEvent e) {
			carregarParametrosOperacao();					
		}

		@Override
		public void changedUpdate(DocumentEvent e) {
			carregarParametrosOperacao();		
		}			
	}

	private class TfHandlerParametrosOperacao implements DocumentListener{

		@Override
		public void insertUpdate(DocumentEvent e) {
			//calcularCustoHorarioTotal();
			calcularTotalCustoHorario();
		}

		@Override
		public void removeUpdate(DocumentEvent e) {
			//calcularCustoHorarioTotal();			
			calcularTotalCustoHorario();
		}

		@Override
		public void changedUpdate(DocumentEvent e) {
			//calcularCustoHorarioTotal();
			calcularTotalCustoHorario();
		}			
	}

	private class TfHandlerGraxa implements DocumentListener{

		@Override
		public void insertUpdate(DocumentEvent e) {
			calcularTotalGraxa();		
		}

		@Override
		public void removeUpdate(DocumentEvent e) {
			calcularTotalGraxa();			
		}

		@Override
		public void changedUpdate(DocumentEvent e) {
			calcularTotalGraxa();
		}			
	}

	

	private class RadioButtonHandler implements ItemListener{	
		private DepreciacaoModel model;		
		private float depreciacaoHora = 0;
		private int index = 0;

		@Override
		public void itemStateChanged(ItemEvent event) {
			index = 0;
			for (Maquina maquina : getArrayConjuntoMecanizado()) {
				if (maquina.isFill() == true){
					if (rbtnDeclinioEmDobro.isSelected()){ 
						setDepre(new DeclinioEmDobro(maquina));
					}	
					if (rbtnDigitosPeriodicos.isSelected()){ 
						setDepre(new DigitosPeriodicos(maquina));
					}
					if (rbtnPorcentagensConstantes.isSelected()){ 
						setDepre(new PorcentagensConstantes(maquina));
					}
					if (rbtnProporcional.isSelected()) {
						setDepre(new Proporcional(maquina));
					}
					if (rbtnQuotas.isSelected()){
						setDepre(new QuotasConstantes(maquina));
					}
					model = new DepreciacaoModel(getDepre());		  		
					depreciacaoHora = getDepre().depreciacaoHoraria();

					arrayConjuntoCalculado.get(index).setDepreciacaoHoraria(depreciacaoHora);				
					index++;
				}
			}		
			atualizarGradeCustosFixos();
		}			
	}

	public void gerarRelatorioEstimativa(){
		Area a = new Area();
		Operacao o =  new Operacao();
		float totalCf = 0;
		float totalCv = 0;
		float totalCh = 0;
		float totalCo = 0;
		float totalCoHa = 0;

		try {
			a.setTamanho(converte(tfTamanhoArea.getText()));
			o.setDuracao(converte(tfDuracaoOperacao.getText()));
			o.setLargura(converte(tfLarguraOperacional.getText()));
			o.setEficiencia(converte(tfEficiencia.getText()));
			o.setVelocidade(converte(tfVelocidade.getText()));
			o.setCco(converte(tfCco.getText()));

			totalCf = converte(tfTotalCustosFixos.getText());
			totalCv = converte(tfTotalCustosVariaveis.getText());
			totalCh = converte(tfTotalCustoHorario.getText());
			totalCo = converte(tfTotalCustoOperacional.getText());
			totalCoHa = converte(tfTotalCustoOperacionalHectares.getText());

		} catch (ParseException e) {

		}	

		o.setArea(a);		
		RelatorioEstimativa re = new RelatorioEstimativa(o, getCvm(), ecfModel, totalCf, totalCv, totalCh, totalCo, totalCoHa, 
				getArrayConjuntoMecanizado());
		re.imprimir(re);
	}
}
