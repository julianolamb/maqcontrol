package br.pro.x87.view;

import java.awt.GridLayout;
import java.text.ParseException;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.AbstractDocument;
import javax.swing.text.DocumentFilter;

import br.pro.x87.dao.AreaDAO;
import br.pro.x87.model.Area;
import br.pro.x87.model.Empresa;
import br.pro.x87.validators.FloatFilter;
import br.pro.x87.view.command.ExcluirCommand;
import br.pro.x87.view.command.SalvarCommand;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AreaUI extends AncestorUI {
	
	private static final long serialVersionUID = 1L;
	
	private Area area;
	private Empresa empresa;
	private List<Empresa> listEmpresa; 	
	
	private JTextField tfDescricao;
	private JTextField tfTamanho;
	private JComboBox<Object> comboEmpresa;		

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	public void popularEmpresa(){
		comboEmpresa.removeAllItems();

		listEmpresa = getDao().get("Empresa");

		for (Object o : listEmpresa) {	
			Empresa e = (Empresa) o;
			comboEmpresa.addItem(e.getNomeFantasia());
		}
	}

	@Override
	public void monteInterface(String s) {
		super.monteInterface("MaqControl | Cadastro de áreas");
		setBounds(100, 100, 500, 294);

		JPanel pEmpresa = new JPanel();
		JPanel pDescricao = new JPanel();

		JLabel lblEmpresa = new JLabel("Empresa");		
		JLabel lblDescrio = new JLabel("Descrição");		
		comboEmpresa = new JComboBox<>();
		JLabel lblTamanhoha = new JLabel("Tamanho (ha)");
		tfDescricao = new JTextField();
		tfTamanho = new JTextField();		
		
		pEmpresa.setBounds(0, 41, 544, 40);
		pEmpresa.setLayout(new GridLayout(0, 1, 0, 0));

		pEmpresa.add(lblEmpresa);		
		pEmpresa.add(comboEmpresa);				

		pDescricao.setBounds(0, 0, 544, 40);		
		pDescricao.setLayout(new GridLayout(2, 2, 10, 0));			

		pDescricao.add(lblDescrio);		
		pDescricao.add(lblTamanhoha);		
		pDescricao.add(tfDescricao);
		pDescricao.add(tfTamanho);

		tfDescricao.setColumns(10);		
		tfTamanho.setColumns(10);
				
		P2.setBounds(0, 110, 544, 150);
		pBotoes.setBounds(0, 85, 544, 23);	

		setResizable(false);
		setSize(550,289);

		getContentPane().setLayout(null);		
		getContentPane().add(pEmpresa);
		getContentPane().add(pDescricao);
	}


	public AreaUI() {
		bListagem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {				
				AreaDAO areaDAO = new AreaDAO<>();				
				List lista = areaDAO.imprimirAreas();			
			
				templateImprimir(lista, "area.jrxml");
			}
		});		
		popularEmpresa();
	}

	@Override
	public void executarCommandExcluir() {
		new ExcluirCommand().execute(area);	
	}

	@Override
	public void executarCommandSalvar() {
		new SalvarCommand().execute(area);	
	}

	@Override
	public void atualizarGrade() {
		atualizarGrade("Area", Area.class);
	}

	@Override
	public void carregarValores() {
		setArea((Area) lista.get(grade.getSelectedRow()));
		setEmpresa(getArea().getEmpresa());

		tfDescricao.setText(getArea().getDescricao());
		tfTamanho.setText(String.valueOf(getArea().getTamanhoFormatado()));
		comboEmpresa.setSelectedItem(getEmpresa().getNomeFantasia());
	}

	@Override
	public void definirFoco() {
		tfDescricao.requestFocus();
	}

	@Override
	public void habilitarCampos() {
		tfDescricao.setEnabled(true);
		tfTamanho.setEnabled(true);
		comboEmpresa.setEnabled(true);
		comboEmpresa.setSelectedIndex(-1);
	}

	@Override
	public void desabiltarCampos() {
		tfDescricao.setEnabled(false);
		tfTamanho.setEnabled(false);
		comboEmpresa.setEditable(false);
	}

	@Override
	public void carregarValoresAlterar() {	
		getArea().setDescricao(tfDescricao.getText());
		getArea().setEmpresa(getEmpresa());	

		try {
			getArea().setTamanho(converte(tfTamanho.getText()));
		} catch (ParseException e) {
			mensagemErro("Formato incorreto. Verifique o separador de decimal");
		}		
	}

	@Override
	public void carregarValoresSalvar() {	
		try {
			setArea(new Area(tfDescricao.getText(), converte(tfTamanho.getText()), getEmpresa()));
		} catch (ParseException e) {
			mensagemErro("Formato incorreto. Verifique o separador de decimal");
		}		
	}

	@Override
	public void carregarValoresObjetos() {
		setEmpresa(listEmpresa.get(comboEmpresa.getSelectedIndex()));
	}

	@Override
	public void adicionaListenerCampoVazio() {
		tfTamanho.addKeyListener(createKeyListenerCampoVazio(tfTamanho));
		tfDescricao.addKeyListener(createKeyListenerCampoVazio(tfDescricao));		
	}

	@Override
	public boolean validarFlutuantes() {
		if (!validaEsteFloat(tfTamanho, "TAMANHO"))
			return false;

		return true;		
	}
	
	@Override
	public boolean validarCombos() {
		if (!validaEsteCombo(comboEmpresa, "EMPRESA"))
			return false;

		return true;
	}
	
	@Override
	public void addDocument() {
		tfTamanho.setDocument(new FloatFilter(FloatFilter.FLOAT));	
	}	
}
