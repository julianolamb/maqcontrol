/**
 * 
 */
package br.pro.x87.view;

import java.awt.Color;
import java.awt.Component;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

/**
 * @author jrlamb
 *
 */
public class HeaderRenderer implements TableCellRenderer  {
	DefaultTableCellRenderer renderer;

	public HeaderRenderer(JTable table) {		
		renderer = (DefaultTableCellRenderer) table.getTableHeader().getDefaultRenderer();
	}

	@Override
	public Component getTableCellRendererComponent(
			JTable table, Object value, boolean isSelected,
			boolean hasFocus, int row, int col) {

		JLabel lbl = (JLabel) renderer.getTableCellRendererComponent(table,	value, isSelected, hasFocus, row, col);

		lbl.setBorder(BorderFactory.createEtchedBorder(0));
		lbl.setHorizontalAlignment(SwingConstants.CENTER);

		return lbl;
	}
}
