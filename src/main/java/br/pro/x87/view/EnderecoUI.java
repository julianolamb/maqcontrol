package br.pro.x87.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import br.pro.x87.dao.BairroDAO;
import br.pro.x87.dao.EnderecoDAO;
import br.pro.x87.enums.TEstado;
import br.pro.x87.model.Bairro;
import br.pro.x87.model.Cidade;
import br.pro.x87.model.Endereco;
import br.pro.x87.view.command.ExcluirCommand;
import br.pro.x87.view.command.SalvarCommand;

public class EnderecoUI extends AncestorUI {
	
	private static final long serialVersionUID = 1L;
	
	private Endereco endereco;
	private Bairro bairro;
	private BairroDAO bairroDAO = new BairroDAO();
	
	private List<Cidade> listCidade;
	private List<Bairro> listBairro;
	
	private CidadeUI cidadeUI;
	private BairroUI bairroUI;
	
	private JLabel jlLogradouro;
	private JLabel jlCidade;
	private JLabel jlNumero;
	private JLabel jlBairro;
	private JTextField tfNumero;
	private JTextField tfLogradouro;	
	private JComboBox<String> comboEstado;	
	private JComboBox<Object> comboCidade;
	private JComboBox<Object> comboBairro;
	
	private boolean formCidade = false;
	private boolean formBairro = false; 

	public Bairro getBairro() {
		return bairro;
	}

	public void setBairro(Bairro bairro) {
		this.bairro = bairro;
	}

	public boolean isFormCidade() {
		return formCidade;
	}

	public void setFormCidade(boolean formCidade) {
		this.formCidade = formCidade;
	}

	public boolean isFormBairro() {
		return formBairro;
	}

	public void setFormBairro(boolean formBairro) {
		this.formBairro = formBairro;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}


	public EnderecoUI() {
		bListagem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				List<Endereco> lista = new EnderecoDAO<>().imprimirEnderecos();
				templateImprimir(lista, "endereco.jrxml");
			}
		});
		comboEstado.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				popularCidade();
			}
		});

		comboCidade.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				popularBairro();
			}
		});
		

		addWindowFocusListener(new WindowFocusListener() {
			public void windowGainedFocus(WindowEvent arg0) {
				try {
					if (!cidadeUI.isVisible() && isFormCidade()){
						popularCidade();
						setFormCidade(false);
					}		
				} catch (Exception e) {
					// TODO: handle exception
				}

				try {
					if (!bairroUI.isVisible() && isFormBairro()){
						popularBairro();
						setFormBairro(false);
					}		
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
			public void windowLostFocus(WindowEvent arg0) {
			}
		});
		
		popularEstado();
	}


	@Override
	public void monteInterface(String s) {
		super.monteInterface("MaqControl | Cadastro de endereço");	

		JPanel pDescricao = new JPanel();
		jlLogradouro = new JLabel("  Logradouro  ");
		jlNumero = new JLabel("  Numero  ");
		tfLogradouro = new JTextField();
		tfNumero = new JTextField();

		pDescricao.setBounds(0, 0, 542, 40);		
		pDescricao.setLayout(new GridLayout(0, 2, 10, 0));		

		pDescricao.add(jlLogradouro);		
		pDescricao.add(jlNumero);		
		pDescricao.add(tfLogradouro);		
		pDescricao.add(tfNumero);		

		JPanel pLocalizacao = new JPanel();
		jlCidade = new JLabel("Cidade");
		jlBairro = new JLabel("Bairro");
		comboEstado = new JComboBox<>();
		comboCidade = new JComboBox<>();
		comboBairro = new JComboBox<>();

		pLocalizacao.setBounds(0, 42, 542, 48);
		pLocalizacao.setLayout(new GridLayout(2, 3, 10, 0));
		
		JLabel lblEstado = new JLabel("Estado");
		pLocalizacao.add(lblEstado);
		pLocalizacao.add(jlCidade);
		pLocalizacao.add(jlBairro);
		pLocalizacao.add(comboEstado);
		pLocalizacao.add(comboCidade);
		pLocalizacao.add(comboBairro);

		pBotoes.setSize(542, 23);
		pBotoes.setLocation(0, 100);			

		P2.setBounds(0, 135, 542, 150);
		P2.add(new JLabel("Registros já cadastrados", JLabel.CENTER),BorderLayout.NORTH);
		scrollPane.setBounds(2, 0, 540, 170);		

		StatusBar.setForeground(new Color(153, 153, 153));
		StatusBar.setText("***MaqControl*** Cadastro de endereço");
		StatusBar.setBounds(0, 168, 542, 18);
		
		getContentPane().setLayout(null);
		getContentPane().add(pDescricao);
		getContentPane().add(pLocalizacao);
		
		setSize(550, 315);
		setResizable(false);		
	}

	public void popularEstado(){
		for (TEstado o : TEstado.values()) {
			comboEstado.addItem(o.getNome());
		}
	}

	public void popularCidade(){
		if (comboEstado.getSelectedIndex() == -1){
			return;
		}

		comboCidade.removeAllItems();
		listCidade = getDao().filter("Cidade", "Estado", (String) comboEstado.getSelectedItem());

		for (Object o : listCidade) {	
			Cidade c = (Cidade) o;
			comboCidade.addItem(c.getNomeCidade());
		}
	}

	public void popularBairro(){
		if (comboCidade.getSelectedIndex() == -1){
			return;
		}

		comboBairro.removeAllItems();
		listBairro = bairroDAO.listBairros( listCidade.get(comboCidade.getSelectedIndex()).getNomeCidade());

		for (Object o : listBairro) {	
			Bairro b = (Bairro) o;
			comboBairro.addItem(b.getDescricao());
		}
	}

	@Override
	public void executarCommandExcluir() {
		new ExcluirCommand().execute(endereco);
	}

	@Override
	public void executarCommandSalvar() {
		new SalvarCommand().execute(endereco);
	}

	@Override
	public void atualizarGrade() {
		atualizarGrade("Endereco", Endereco.class);
	}

	@Override
	public void carregarValores() {
		setEndereco((Endereco) lista.get(grade.getSelectedRow()));
		tfLogradouro.setText(getEndereco().getDescricao());
		tfNumero.setText(Integer.toString(getEndereco().getNumero()));
		comboEstado.setSelectedItem(getEndereco().getNomeEstado());
		comboCidade.setSelectedItem(getEndereco().getNomeCidade());
		comboBairro.setSelectedItem(getEndereco().getDescricaoBairro());
	}

	@Override
	public void definirFoco() {
		tfLogradouro.requestFocus();
	}

	@Override
	public void habilitarCampos() {
		tfLogradouro.setEnabled(true);
		tfNumero.setEnabled(true);
		comboEstado.setEnabled(true);
		comboEstado.setSelectedIndex(-1);
		comboCidade.setEnabled(true);
		comboCidade.setSelectedIndex(-1);
	}

	@Override
	public void desabiltarCampos() {
		tfLogradouro.setEnabled(false);
		tfNumero.setEnabled(false);
		comboEstado.setEnabled(false);
		comboCidade.setEnabled(false);
	}

	@Override
	public void carregarValoresAlterar() {
		getEndereco().setBairro(getBairro());
		getEndereco().setDescricao(tfLogradouro.getText());
		getEndereco().setNumero(Integer.parseInt(tfNumero.getText()));
	}

	@Override
	public void carregarValoresSalvar() {
		setEndereco(new Endereco(getBairro(), tfLogradouro.getText(), Integer.parseInt(tfNumero.getText())));;
	}

	@Override
	public void carregarValoresObjetos() {
		setBairro(listBairro.get(comboBairro.getSelectedIndex()));
	}
	
	@Override
	public void adicionaListenerCampoVazio() {
		tfLogradouro.addKeyListener(createKeyListenerCampoVazio(tfLogradouro));
		tfNumero.addKeyListener(createKeyListenerCampoVazio(tfNumero));
	}
	
	@Override
	public boolean validarFlutuantes() {	
		return true;
	}
	
	@Override
	public boolean validarCombos() {
		if (!validaEsteCombo(comboBairro, "BAIRRO"))
			return false;

		if (!validaEsteCombo(comboCidade, "CIDADE"))
			return false;

		if (!validaEsteCombo(comboEstado, "ESTADO"))
			return false;	
		
		return true;
	}

	@Override
	public void addDocument() {	}
}
