/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pro.x87.view;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import br.pro.x87.dao.CidadeDAO;
import br.pro.x87.enums.TEstado;
import br.pro.x87.model.Cidade;
import br.pro.x87.view.command.ExcluirCommand;
import br.pro.x87.view.command.SalvarCommand;

/**
 *
 * @author JulianoRodrigo
 */
public class CidadeUI extends AncestorUI {
	
	private static final long serialVersionUID = 1L;
	
	private Cidade cidade;
	private TEstado estado;
	private boolean update;

	private JComboBox<String> comboEstados;
	private JTextField tfDescricao;
	private JLabel jlNome;
	private JLabel jlEstado;
	private JPanel panel;

	public TEstado getEstado() {
		return estado;
	}

	public void setEstado(TEstado estado) {
		this.estado = estado;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public boolean isUpdate() {
		return update;
	}

	public void setUpdate(boolean update) {
		this.update = update;
	}

	public CidadeUI() {
		bListagem.addActionListener(new ActionListener() {			
			public void actionPerformed(ActionEvent e) {
				CidadeDAO cidadeDAO = new CidadeDAO<>();
				List<Cidade> lista = cidadeDAO.imprimirCidades();
				
				templateImprimir(lista, "cidade.jrxml");
			}
		});
		popularCombo();
	}

	@Override
	public void monteInterface(String s) {    
		super.monteInterface("MaqControl | Cadastro de cidades");   			

		panel = new JPanel();
		jlNome = new JLabel(" Nome ");
		jlEstado = new JLabel(" Estado ");
		tfDescricao = new JTextField();
		comboEstados = new JComboBox<String>();	
		
		panel.setBounds(0, 0, 544, 40);
		panel.setLayout(new GridLayout(2, 2, 10, 0));		
		panel.add(jlNome);		
		panel.add(jlEstado);		
		
		panel.add(tfDescricao);	
		panel.add(comboEstados);		
		
		P2.setLocation(0, 70);
		P2.setSize(544, 150);		
		
		pBotoes.setBounds(0, 42, 544, 23);
		
		StatusBar.setForeground(new Color(153, 153, 153));
		StatusBar.setText("***MaqControl*** Cadastro de cidades");
		
		getContentPane().setLayout(null);
		getContentPane().add(panel);
		
		setSize(550,250);
		setResizable(false);
		
	}

	private void popularCombo() {
		for (TEstado o : TEstado.values()) {
			comboEstados.addItem(o.getNome());
		}
	}

	@Override
	public void habilitarCampos() {
		tfDescricao.setEnabled(true);
		comboEstados.setEnabled(true);
		comboEstados.setSelectedIndex(-1);
	}

	@Override
	public void desabiltarCampos() {
		tfDescricao.setEnabled(false);
		comboEstados.setEnabled(false);
	}

	@Override
	public void definirFoco() {
		tfDescricao.requestFocus();		
	}

	@Override
	public void executarCommandExcluir() {
		new ExcluirCommand().execute(cidade);		
	}

	@Override
	public void atualizarGrade() {
		atualizarGrade("Cidade", Cidade.class);		
	}

	@Override
	public void carregarValoresAlterar() {		
		getCidade().setNome(tfDescricao.getText());
		getCidade().setEstado((String) comboEstados.getSelectedItem());		
	}

	@Override
	public void carregarValoresSalvar() {		
		setCidade(new Cidade(tfDescricao.getText(), (String) comboEstados.getSelectedItem()));	}

	@Override
	public void carregarValoresObjetos() {
		//nothing to do here				
	}

	@Override
	public void executarCommandSalvar() {
		new SalvarCommand().execute(cidade);		
	}

	@Override
	public void carregarValores() {
		setCidade((Cidade) lista.get(grade.getSelectedRow()));
		tfDescricao.setText(getCidade().getNomeCidade());
		comboEstados.setSelectedItem(getCidade().getNomeEstado());		
	}

	@Override
	public void adicionaListenerCampoVazio() {
		tfDescricao.addKeyListener(createKeyListenerCampoVazio(tfDescricao));				
	}
	
	@Override
	public boolean validarFlutuantes() {
		return true;
	}
	
	@Override
	public boolean validarCombos() {	
		if (!validaEsteCombo(comboEstados, "ESTADO"))
			return false;
		
		return true;
	}
	
	@Override
	public void addDocument() {}
}
