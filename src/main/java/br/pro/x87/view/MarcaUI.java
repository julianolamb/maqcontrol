package br.pro.x87.view;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import br.pro.x87.dao.MarcaDAO;
import br.pro.x87.model.Marca;
import br.pro.x87.view.command.ExcluirCommand;
import br.pro.x87.view.command.SalvarCommand;

class MarcaUI extends AncestorUI {

	private static final long serialVersionUID = 1L;

	private JTextField tfMarca;
	private Marca marca;

	public Marca getMarca() {
		return marca;
	}

	public void setMarca(Marca marca) {
		this.marca = marca;
	}

	public MarcaUI() {
		bListagem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MarcaDAO marcaDAO = new MarcaDAO<>();
				List<Marca> lista = marcaDAO.imprimirMarcas();

				templateImprimir(lista, "marca.jrxml");		
			}
		});
	}

	public void monteInterface(String s){
		super.monteInterface("MaqControl | Cadastro de marcas");

		JPanel panel = new JPanel();
		JLabel label = new JLabel(" Marca ");
		tfMarca = new JTextField();

		panel.setBounds(0, 0, 545, 40);		
		panel.setLayout(new GridLayout(0, 1, 0, 0));		
		panel.add(label);		
		panel.add(tfMarca);

		P2.setBounds(0, 75, 545, 150);
		pBotoes.setBounds(0, 45, 545, 23);

		StatusBar.setForeground(new Color(153, 153, 153));
		StatusBar.setText("***MaqControl*** Cadastro de marcas");

		getContentPane().setLayout(null);
		getContentPane().add(panel);

		setResizable(false);
		setSize(550,256);		
	}

	@Override
	public void habilitarCampos() {
		tfMarca.setEnabled(true);		
	}

	@Override
	public void desabiltarCampos() {
		tfMarca.setEnabled(false);		
	}

	@Override
	public void definirFoco() {
		tfMarca.requestFocus();		
	}

	@Override
	public void executarCommandExcluir() {
		new ExcluirCommand().execute(marca);		
	}

	@Override
	public void atualizarGrade() {
		atualizarGrade("Marca", Marca.class);
	}

	@Override
	public void carregarValoresAlterar() {		
		getMarca().setDescricao(tfMarca.getText());
	}

	@Override
	public void carregarValoresSalvar() {
		setMarca(new Marca(tfMarca.getText()));
	}

	@Override
	public void carregarValoresObjetos() {
		//nothing to do here
	}

	@Override
	public void executarCommandSalvar() {
		new SalvarCommand().execute(marca);
	}

	@Override
	public void carregarValores() {
		setMarca((Marca) lista.get(grade.getSelectedRow()));
		tfMarca.setText(getMarca().getDescricao());		
	}

	@Override
	public void adicionaListenerCampoVazio() {
		tfMarca.addKeyListener(createKeyListenerCampoVazio(tfMarca));
	}

	@Override
	public boolean validarFlutuantes() {
		return true;
	}

	@Override
	public boolean validarCombos() {
		return true;
	}

	@Override
	public void addDocument() {}


}
