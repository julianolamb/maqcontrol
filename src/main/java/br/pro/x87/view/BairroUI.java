package br.pro.x87.view;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import br.pro.x87.dao.BairroDAO;
import br.pro.x87.enums.TEstado;
import br.pro.x87.model.Bairro;
import br.pro.x87.model.Cidade;
import br.pro.x87.view.command.ExcluirCommand;
import br.pro.x87.view.command.SalvarCommand;
import java.awt.GridLayout;

public class BairroUI extends AncestorUI {

	private static final long serialVersionUID = 1L;

	private Bairro bairro;
	private Cidade cidade;
	protected CidadeUI cidadeUI;	
	private List<Cidade> listCidade;	

	private JComboBox<String> comboEstados;
	private JComboBox<Object> comboCidades;
	private JTextField tfDescricao;
	private JTextField tfCEP;
	private JLabel jlNome;
	private JLabel jlEstado;
	private JLabel jlCidade;
	private JLabel jlCEP;
	private JPanel panel;

	private boolean formCidade = false;	

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public boolean isFormCidade() {
		return formCidade;
	}

	public void setFormCidade(boolean formCidade) {
		this.formCidade = formCidade;
	}

	public BairroUI() {
		bListagem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				BairroDAO bairroDao = new BairroDAO();
				List lista = bairroDao.imprimirBairros();			
			
				templateImprimir(lista, "bairro.jrxml");
			}
		});		
		comboEstados.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				popularCidade();
			}
		});		

		addWindowFocusListener(new WindowFocusListener() {
			public void windowGainedFocus(WindowEvent arg0) {
				try {
					if (!cidadeUI.isVisible() && isFormCidade()){
						popularCidade();
						setFormCidade(false);
					}		
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
			public void windowLostFocus(WindowEvent arg0) {
			}
		});

		popularEstado();

	}

	public void popularEstado(){
		for (TEstado o : TEstado.values()) {
			comboEstados.addItem(o.getNome());
		}
	}

	public void popularCidade(){
		if (comboEstados.getSelectedIndex() == -1)
			return;

		comboCidades.removeAllItems();

		listCidade = getDao().filter("Cidade", "Estado", (String) comboEstados.getSelectedItem());

		for (Object o : listCidade) {	
			Cidade c = (Cidade) o;
			comboCidades.addItem(c.getNomeCidade());
		}
	}

	@Override
	public void monteInterface(String s) {
		super.monteInterface("MaqControl | Cadastro de bairros");		

		StatusBar.setForeground(new Color(153, 153, 153));
		StatusBar.setText("MaqControl | Cadastro de bairros");		
		getContentPane().setLayout(null);

		panel = new JPanel();
		jlNome = new JLabel(" Nome ");
		jlCEP = new JLabel(" CEP ");
		comboCidades = new JComboBox<>();

		panel.setBounds(0, 5, 545, 105);

		getContentPane().add(panel);
		panel.setLayout(new GridLayout(4, 4, 10, 5));

		panel.add(jlNome);
		panel.add(jlCEP);
		tfDescricao = new JTextField();
		panel.add(tfDescricao);
		tfCEP = new JTextField();
		panel.add(tfCEP);
		jlEstado = new JLabel(" Estado ");
		panel.add(jlEstado);
		jlCidade = new JLabel(" Cidade ");
		panel.add(jlCidade);
		comboEstados = new JComboBox<String>();
		panel.add(comboEstados);
		panel.add(comboCidades);

		pBotoes.setSize(544, 23);
		pBotoes.setLocation(0, 115);

		P2.setBounds(0, 145, 544, 150);

		setResizable(false);		
		setSize(550,325);
	}

	public Bairro getBairro() {
		return bairro;
	}

	public void setBairro(Bairro bairro) {
		this.bairro = bairro;
	}

	@Override
	public void habilitarCampos() {
		tfDescricao.setEnabled(true);
		tfCEP.setEnabled(true);
		comboEstados.setEnabled(true);
		comboCidades.setEnabled(true);
		comboEstados.setSelectedIndex(-1);
		comboCidades.setSelectedIndex(-1);		
	}

	@Override
	public void desabiltarCampos() {
		tfDescricao.setEnabled(false);
		tfCEP.setEnabled(false);
		comboEstados.setEnabled(false);
		comboCidades.setEnabled(false);		
	}

	@Override
	public void definirFoco() {
		tfDescricao.requestFocus();
	}

	@Override
	public void executarCommandExcluir() {
		new ExcluirCommand().execute(bairro);		
	}

	@Override
	public void atualizarGrade() {
		atualizarGrade("Bairro", Bairro.class);		
	}

	@Override
	public void carregarValoresAlterar() {
		getBairro().setCidade(cidade);
		getBairro().setCep(tfCEP.getText());
		getBairro().setDescricao(tfDescricao.getText());		
	}

	@Override
	public void carregarValoresSalvar() {
		setBairro(new Bairro(cidade, tfDescricao.getText(), tfCEP.getText()));	
	}

	@Override
	public void carregarValoresObjetos() {
		setCidade(listCidade.get(comboCidades.getSelectedIndex()));	
	}

	@Override
	public void executarCommandSalvar() {
		new SalvarCommand().execute(bairro);		
	}

	@Override
	public void carregarValores() {
		setBairro((Bairro) lista.get(grade.getSelectedRow()));

		tfDescricao.setText(getBairro().getDescricao());
		tfCEP.setText(getBairro().getCep());
		comboEstados.setSelectedItem(getBairro().getNomeEstado());
		comboCidades.setSelectedItem(getBairro().getNomeCidade());		
	}

	@Override
	public void adicionaListenerCampoVazio() {		
		tfDescricao.addKeyListener(createKeyListenerCampoVazio(tfDescricao));		
		tfCEP.addKeyListener(createKeyListenerCampoVazio(tfCEP));
	}

	@Override
	public boolean validarFlutuantes() {
		return true;
	}

	@Override
	public boolean validarCombos() {
		if (!validaEsteCombo(comboCidades, "CIDADE"))		
			return false;

		if (!validaEsteCombo(comboEstados, "ESTADO"))		
			return false;

		return true;
	}

	@Override
	public void addDocument() {}
}
