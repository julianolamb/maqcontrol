/**
 * 
 */
package br.pro.x87.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.AbstractDocument;

import br.pro.x87.controller.CustosFixosModel;
import br.pro.x87.controller.CustosVariaveisModel;
import br.pro.x87.controller.DepreciacaoModel;
import br.pro.x87.dao.MaquinaDAO;
import br.pro.x87.depreciacao.DeclinioEmDobro;
import br.pro.x87.depreciacao.Depreciacao;
import br.pro.x87.depreciacao.DigitosPeriodicos;
import br.pro.x87.depreciacao.PorcentagensConstantes;
import br.pro.x87.depreciacao.Proporcional;
import br.pro.x87.depreciacao.QuotasConstantes;
import br.pro.x87.despesas.CampoDeCusto;
import br.pro.x87.enums.TDespesas;
import br.pro.x87.enums.TMaquina;
import br.pro.x87.enums.TProduto;
import br.pro.x87.model.Maquina;
import br.pro.x87.model.Marca;
import br.pro.x87.model.Produto;
import br.pro.x87.reports.RelatorioMaquina;
import br.pro.x87.validators.FloatFilter;
import br.pro.x87.validators.IntegerFilter;
import br.pro.x87.view.chart.ComponentesCustoHorarioChart;
import br.pro.x87.view.chart.FormasDepreciacaoChart;
import br.pro.x87.view.command.ExcluirCommand;
import br.pro.x87.view.command.SalvarCommand;
import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.UtilDateModel;

/**
 * @author jrlamb
 *
 */
public class MaquinaUI extends AncestorUI {

	private static final long serialVersionUID = 1L;	

	private JTextField tfModelo;
	private JTextField tfDescricao;
	private JTextField tfNumeroSerie;
	private JTextField tfNumeroPatrimonio;

	private UtilDateModel model;
	private JDatePanelImpl datePanel;
	private JTextField tfAno;
	private JTextField tfEficiencia;
	private JTextField tfPeso;
	private JTextField tfAnosUtilizacao;
	private JTextField tfVelocidadeMaxima;
	private JTextField tfHorimetro;
	private JTextField tfLargura;
	private JTextField tfTempoUso;
	private JTextField tfConsumoHorario;
	private JTextField tfPotencia;
	private JDatePickerImpl dpAquisicao;
	private JRadioButton rbCV;
	private JRadioButton rbKW;
	private JTabbedPane tabbedPane;

	private JComboBox<Object> comboMarca;
	private JComboBox<Object> comboTipo;
	private JComboBox<Object> comboTipoProduto;
	private JComboBox<Object> comboProduto;
	private List<Marca> listMarca;
	private List<Produto> listProduto;

	private Maquina maquina;	
	private Marca marca;
	private String tipoMaquina;	
	private JTextField tfJurosTotal;
	private JTextField tfSeguroTotal;
	private JTextField tfAlojamentoTotal;
	private JTextField tfValorAquisicao;
	private JTextField tfValorResidual;
	private JTable gradeDepreciacao;
	private JRadioButton rbtnQuotas;	
	private JRadioButton rbtnProporcional; 
	private JRadioButton rbtnPorcentagensConstantes;	
	private JRadioButton rbtnDigitosPeriodicos;	
	private JRadioButton rbtnDeclinioEmDobro;

	private Depreciacao depre;
	private JTextField tfDepreciacaoAno;
	private JTextField tfDepreciacaoHora;
	private JTextField tfTroca;
	private JTextField tfValorUnitario;
	private JTextField tfProdutosTotal;
	private JTextField tfPrecoLitro;
	private JTextField tfCombustivelTotal;
	private JTextField tfSalarioMensal;
	private JTextField tfTotalSalario;
	private JTextField tfQuantidadeGraxa;
	private JTextField tfPrecoGraxa;
	private JTextField tfTempoLubrificacao;
	private JTextField tfGraxaTotal;
	private JTextField tfManutencaoTotal;
	private JTextField tfTaxaManutencao;

	private JLabel lblTaxaJuros, lblTaxaSeguro, lblTaxaAlojamento, lblConsumoHorario;
	private JCheckBox chckbxCalcularValores;
	private JTextField tfFiltrosDiesel;
	private JTextField tfFiltrosLubrificante;
	private JTextField tfFiltrosHidraulico;
	private JTextField tfFiltrosAr;
	private JTextField tfFiltrosSeguranca;
	private JTextField tfOleoCarter;
	private JTextField tfOleoTransmissao;
	private JTextField tfOleoHidraulico;
	private JTextField tfNumPneusDianteiro;
	private JTextField tfNumPneusTraseiro;


	private JButton btnAdicionarProduto;
	private JButton btnAdicionarCombustivel;
	private JButton btnAdicionarManutencao;
	private JButton btnAdicionarGraxa;
	private JButton btnAdicionarSalario;

	private CustosVariaveisModel cvm;
	private CustosFixosModel cfm;
	private JTable gradeConsumo;
	private JTable gradeCustosFixos;
	private JTable gradeCustosVariaveis;


	private String resposta = "";	
	private JTextField tfTotalCustosFixos;
	private JTextField tfTotalCustosVariaveis;
	private JTextField tfTotalCustoHorario;	

	private ComponentesCustoHorarioChart depreciacaoPizza;
	private FormasDepreciacaoChart grafico;

	public CustosFixosModel getCfm() {
		return cfm;
	}

	public void setCfm(CustosFixosModel cfm) {
		this.cfm = cfm;
	}

	public CustosVariaveisModel getCvm() {
		return cvm;
	}

	public void setCvm(CustosVariaveisModel cvm) {
		this.cvm = cvm;
	}


	/**
	 * Create the frame.
	 */
	public MaquinaUI() {
		bListagem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				List<Maquina> lista = new MaquinaDAO<>().imprimirMaquinas();
				templateImprimir(lista, "maquina.jrxml");
			}
		});
		bSalvar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowActivated(WindowEvent arg0) {
			}
		});
		bFechar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		popularMarca();
		popularTipoMaquina();

		grade.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				templateClicarGrid();
				tabbedPane.setEnabled(true);
				carregarAbas();
			}
		});

		bNovo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {				
				templateNovo();
				tabbedPane.setEnabled(false);
			}
		});

		bExcluir.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				templateExcluir();
				tabbedPane.setEnabled(false);
			}
		});

		rbKW.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (rbKW.isSelected()){
					lblConsumoHorario.setText("Consumo horário (L/kW)");
				}
			}
		});

		rbCV.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (rbCV.isSelected()){
					lblConsumoHorario.setText("Consumo horário (L/h)");
				}
			}
		});
	}

	public void setResposta(String s) {
		this.resposta.concat(s);
	}

	public void inicializarResposta(){
		this.resposta = "";
		addMensagem("Atenção: não foi possível calcular o(s) seguinte(s) item(s): \n");
	}

	public void addMensagem(String s){
		this.resposta = resposta.concat(s).concat("\n");	
	}	

	public void exibirMensagem(){
		JOptionPane.showMessageDialog(null, getResposta()
				+ "\n\nVerifique se TODOS os campos relativos ao cadastro foram devidamente informados!", "Erro", JOptionPane.ERROR_MESSAGE);
	}

	public String getResposta() {
		return resposta;
	}	

	public Depreciacao getDepre() {
		return depre;
	}

	public void setDepre(Depreciacao depre) {
		this.depre = depre;
	}

	public Marca getMarca() {
		return marca;
	}

	public void setMarca(Marca marca) {
		this.marca = marca;
	}

	public String getTipoMaquina() {
		return tipoMaquina;
	}

	public void setTipoMaquina(String tipoMaquina) {
		this.tipoMaquina = tipoMaquina;
	}

	public Maquina getMaquina() {
		return maquina;
	}

	public void setMaquina(Maquina maquina) {
		this.maquina = maquina;
	}

	@Override
	public void executarCommandExcluir() {
		new ExcluirCommand().execute(maquina);
	}

	@Override
	public void executarCommandSalvar() {
		new SalvarCommand().execute(getMaquina());
	}

	@Override
	public void atualizarGrade() {
		atualizarGrade("Maquina", Maquina.class);
	}

	public void recuperaTfFloat(JTextField tf, Float f){
		NumberFormat nf = NumberFormat.getNumberInstance(new Locale("pt", "BR"));
		((DecimalFormat)nf).applyPattern("###,###,##0.00");		

		if (f != 0) tf.setText(getDf().format(f)); 
		else tf.setText("");
	}

	public void recuperaTfInteger(JTextField tf, Integer i){
		if (i != 0) tf.setText(Integer.toString(i)); 
		else tf.setText("");
	}

	@Override
	public void carregarValores() {
		setMaquina((Maquina) lista.get(grade.getSelectedRow()));

		tfDescricao.setText(getMaquina().getDescricao());
		tfModelo.setText(getMaquina().getModelo());
		comboMarca.setSelectedItem(getMaquina().getMarca().getDescricao());
		comboTipo.setSelectedItem(getMaquina().getTipoMaquina());
		tfNumeroSerie.setText(getMaquina().getNumeroSerie());
		tfNumeroPatrimonio.setText(Long.toString(getMaquina().getNumPatrimonio()));

		/*if (getMaquina().getHorimetro() != 0)*/ tfHorimetro.setText(getDf().format(getMaquina().getHorimetro()));
		/*else tfHorimetro.setText("");*/

		if (getMaquina().getDataAquisicao() != null){
			carregarDataDateTimePicker(getMaquina().getDataAquisicao(), model);			
		} else {
			dpAquisicao.getJFormattedTextField().setText("");
		}		

		recuperaTfFloat(tfValorResidual, getMaquina().getValorResidual());
		recuperaTfFloat(tfValorAquisicao, getMaquina().getValorAquisicao());		

		recuperaTfFloat(tfPeso, getMaquina().getPeso());
		recuperaTfFloat(tfEficiencia, getMaquina().getEficiencia());
		recuperaTfFloat(tfVelocidadeMaxima, getMaquina().getVelocidadeMaxima());
		recuperaTfFloat(tfLargura, getMaquina().getLargura());
		recuperaTfFloat(tfTempoUso, getMaquina().getTempoUso());
		recuperaTfFloat(tfConsumoHorario, getMaquina().getConsumoEspecifico());
		recuperaTfFloat(tfPotencia, getMaquina().getPotencia());
		recuperaTfFloat(tfOleoCarter, getMaquina().getCapacidadeCarter());
		recuperaTfFloat(tfOleoHidraulico, getMaquina().getCapacidadeHidraulico());
		recuperaTfFloat(tfOleoTransmissao, getMaquina().getCapacidadeTransmissao());

		recuperaTfInteger(tfAno, getMaquina().getAno());
		recuperaTfInteger(tfAnosUtilizacao, getMaquina().getVidaUtil());
		recuperaTfInteger(tfNumPneusDianteiro, getMaquina().getNrPneusDianteiro());
		recuperaTfInteger(tfNumPneusTraseiro, getMaquina().getNrPneusTraseiro());
		recuperaTfInteger(tfFiltrosAr, getMaquina().getNrFiltrosArFiltrante());
		recuperaTfInteger(tfFiltrosSeguranca, getMaquina().getNrFiltrosArSeguranca());
		recuperaTfInteger(tfFiltrosDiesel, getMaquina().getNrFiltrosDiesel());
		recuperaTfInteger(tfFiltrosHidraulico, getMaquina().getNrFiltrosHidraulico());
		recuperaTfInteger(tfFiltrosLubrificante, getMaquina().getNrFiltrosLubrificante());

		if (getMaquina().getTaxaJuros() != 0) lblTaxaJuros.setText(getDf().format(getMaquina().getTaxaJuros())); 
		else lblTaxaJuros.setText(getDf().format(TDespesas.JUROS.getValue()));		

		if (getMaquina().getTaxaAlojamento() != 0) lblTaxaAlojamento.setText(getDf().format(getMaquina().getTaxaAlojamento())); 
		else lblTaxaAlojamento.setText(getDf().format(TDespesas.ALOJAMENTO.getValue()));

		if (getMaquina().getTaxaSeguros() != 0) lblTaxaSeguro.setText(getDf().format(getMaquina().getTaxaSeguros())); 
		else lblTaxaSeguro.setText(getDf().format(TDespesas.SEGURO.getValue()));

		recuperaTfFloat(tfJurosTotal, getMaquina().getValorJuros());
		recuperaTfFloat(tfAlojamentoTotal, getMaquina().getValorAlojamento());
		recuperaTfFloat(tfSeguroTotal, getMaquina().getValorSeguros());

		if (getMaquina().getTipoPotencia() == null){} 
		else if (getMaquina().getTipoPotencia().equalsIgnoreCase("cv")) rbCV.setSelected(true);
		else rbKW.setSelected(true);

		recuperarRadioSelecionado();
	}
	
	public void recuperarRadioSelecionado(){
		if (getMaquina().getTipoDepreciacao() == null){
			rbtnQuotas.setSelected(true);
			return;
		}
		if (getMaquina().getTipoDepreciacao().equals("Declinio"))
			rbtnDeclinioEmDobro.setSelected(true);
		if (getMaquina().getTipoDepreciacao().equals("Digitos"))
			rbtnDigitosPeriodicos.setSelected(true);
		if (getMaquina().getTipoDepreciacao().equals("Porcentagens")){
			rbtnDeclinioEmDobro.setSelected(false);
			rbtnDigitosPeriodicos.setSelected(false);
			rbtnProporcional.setSelected(false);
			rbtnQuotas.setSelected(false);
			rbtnPorcentagensConstantes.setSelected(true);			
		}
		if (getMaquina().getTipoDepreciacao().equals("Proporcional"))
			rbtnProporcional.setSelected(true);
		if (getMaquina().getTipoDepreciacao().equals("Quotas"))
			rbtnQuotas.setSelected(true);
	}

	@Override
	public void definirFoco() {
		tfDescricao.requestFocus();
	}

	@Override
	public void habilitarCampos() {
		enableTextField();
		enableComboBox();
		dpAquisicao.setEnabled(true);		
	}

	@Override
	public void desabiltarCampos() {
		disableTextField();
		disableComboBox();
		dpAquisicao.setEnabled(false);
	}

	@Override
	public void carregarValoresAlterar() {
		getMaquina().setDescricao(tfDescricao.getText());
		getMaquina().setModelo(tfModelo.getText());
		getMaquina().setMarca(listMarca.get(comboMarca.getSelectedIndex()));
		getMaquina().setTipoMaquina((String) comboTipo.getSelectedItem());

		if (rbCV.isSelected()){
			getMaquina().setTipoPotencia("cv");
		} else {
			getMaquina().setTipoPotencia("kw");
		}

		if (dpAquisicao.getModel().getValue() != null)  
			getMaquina().setDataAquisicao((Date) dpAquisicao.getModel().getValue());		
		if (!tfNumeroSerie.getText().equals(""))
			getMaquina().setNumeroSerie(tfNumeroSerie.getText());
		if (!tfNumeroPatrimonio.getText().equals("")) 
			getMaquina().setNumPatrimonio(Long.parseLong(tfNumeroPatrimonio.getText()));
		if (!tfAno.getText().equals("")) 
			getMaquina().setAno(Integer.parseInt(tfAno.getText()));
		if (!tfAnosUtilizacao.getText().equals("")) 
			getMaquina().setVidaUtil(Integer.parseInt(tfAnosUtilizacao.getText()));		
		if (!tfNumPneusDianteiro.getText().equals(""))		
			getMaquina().setNrPneusDianteiro(Integer.parseInt(tfNumPneusDianteiro.getText()));
		if (!tfNumPneusTraseiro.getText().equals(""))
			getMaquina().setNrPneusTraseiro(Integer.parseInt(tfNumPneusTraseiro.getText()));
		if (!tfFiltrosAr.getText().equals(""))
			getMaquina().setNrFiltrosArFiltrante(Integer.parseInt(tfFiltrosAr.getText()));
		if (!tfFiltrosDiesel.getText().equals(""))
			getMaquina().setNrFiltrosDiesel(Integer.parseInt(tfFiltrosDiesel.getText()));
		if (!tfFiltrosHidraulico.getText().equals(""))
			getMaquina().setNrFiltrosHidraulico(Integer.parseInt(tfFiltrosHidraulico.getText()));
		if (!tfFiltrosLubrificante.getText().equals(""))
			getMaquina().setNrFiltrosLubrificante(Integer.parseInt(tfFiltrosLubrificante.getText()));
		if (!tfFiltrosSeguranca.getText().equals(""))
			getMaquina().setNrFiltrosArSeguranca(Integer.parseInt(tfFiltrosSeguranca.getText()));

		try {
			if (!tfValorAquisicao.getText().equals(""))
				getMaquina().setValorAquisicao(converte(tfValorAquisicao.getText()));
			if (!tfValorResidual.getText().equals(""))
				getMaquina().setValorResidual(converte(tfValorResidual.getText()));		
			if (!tfEficiencia.getText().equals("")) 
				getMaquina().setEficiencia(converte(tfEficiencia.getText()));
			if (!tfPeso.getText().equals("")) 
				getMaquina().setPeso(converte(tfPeso.getText()));		
			if (!tfVelocidadeMaxima.getText().equals("")) 
				getMaquina().setVelocidadeMaxima(converte(tfVelocidadeMaxima.getText()));
			if (!tfHorimetro.getText().equals("")) 
				getMaquina().setHorimetro(converte(tfHorimetro.getText()));
			if (!tfLargura.getText().equals("")) 
				getMaquina().setLargura(converte(tfLargura.getText()));
			if (!tfTempoUso.getText().equals("")) 
				getMaquina().setTempoUso(converte(tfTempoUso.getText()));
			if (!tfConsumoHorario.getText().equals("")) 
				getMaquina().setConsumoEspecifico(converte(tfConsumoHorario.getText()));
			if (!tfPotencia.getText().equals("")) 
				getMaquina().setPotencia(converte(tfPotencia.getText()));	
			if (!tfOleoCarter.getText().equals(""))
				getMaquina().setCapacidadeCarter(converte(tfOleoCarter.getText()));
			if (!tfOleoHidraulico.getText().equals(""))
				getMaquina().setCapacidadeHidraulico(converte(tfOleoHidraulico.getText()));
			if (!tfOleoTransmissao.getText().equals(""))
				getMaquina().setCapacidadeTransmissao(converte(tfOleoTransmissao.getText()));
			if (!lblTaxaJuros.getText().equals(""))			
				getMaquina().setTaxaJuros(converte(lblTaxaJuros.getText()));
			if (!tfJurosTotal.getText().equals(""))				
				getMaquina().setValorJuros(converte(tfJurosTotal.getText()));
			if (!lblTaxaSeguro.getText().equals(""))			
				getMaquina().setTaxaSeguros(converte(lblTaxaSeguro.getText()));
			if (!tfSeguroTotal.getText().equals(""))			
				getMaquina().setValorSeguros(converte(tfSeguroTotal.getText()));
			if (!lblTaxaAlojamento.getText().equals(""))
				getMaquina().setTaxaAlojamento(converte(lblTaxaAlojamento.getText()));
			if (!tfAlojamentoTotal.getText().equals(""))			
				getMaquina().setValorAlojamento(converte(tfAlojamentoTotal.getText()));
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return;
		}
	}

	@Override
	public void carregarValoresSalvar() {
		setMaquina(new Maquina(tfDescricao.getText(), getMarca(), tfModelo.getText(), getTipoMaquina()));
	}

	@Override
	public void carregarValoresObjetos() {
		setMarca(listMarca.get(comboMarca.getSelectedIndex()));
		setTipoMaquina((String) comboTipo.getSelectedItem());
	}

	/** Returns an ImageIcon, or null if the path was invalid. */
	protected ImageIcon createImageIcon(String path,
			String description) {
		java.net.URL imgURL = getClass().getResource(path);
		if (imgURL != null) {
			return new ImageIcon(imgURL, description);
		} else {
			System.err.println("Couldn't find file: " + path);
			return null;
		}
	}

	@Override
	public void monteInterface(String s) {		
		super.monteInterface("MaqControl | Cadastro de máquinas");

		JPanel pInformacoesGerais = new JPanel();
		JLabel lblDescrio = new JLabel("Descrição");
		JLabel lblTipo = new JLabel("Tipo");
//		JPanel pTopo = new JPanel();

		pInformacoesGerais.setBounds(0, 0, 854, 112);	
		pInformacoesGerais.setLayout(new GridLayout(0, 2, 10, 0));
		pInformacoesGerais.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder(" Informações gerais "),
				BorderFactory.createEmptyBorder(5,5,5,5)));
		
		lblTipo.setHorizontalAlignment(SwingConstants.LEFT);
		
		pInformacoesGerais.add(lblDescrio);
		pInformacoesGerais.add(lblTipo);

		tfDescricao = new JTextField();
		comboTipo = new JComboBox();
		JLabel lblModelo = new JLabel("Modelo");
		JLabel lblMarca = new JLabel("Marca");
		
		pInformacoesGerais.add(tfDescricao);
		
		tfDescricao.setColumns(10);


		pInformacoesGerais.add(comboTipo);

		
		pInformacoesGerais.add(lblModelo);

		
		lblMarca.setHorizontalAlignment(SwingConstants.LEFT);
		pInformacoesGerais.add(lblMarca);

		tfModelo = new JTextField();
		pInformacoesGerais.add(tfModelo);
		tfModelo.setColumns(10);

		comboMarca = new JComboBox();
		pInformacoesGerais.add(comboMarca);		

		getContentPane().add(pInformacoesGerais);

		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setEnabled(false);
		tabbedPane.setBounds(0, 307, 854, 265);
		getContentPane().add(tabbedPane);

		/*
		 * creation for tabGeral
		 */		
		JPanel pGeral = buildPanelGeral();
		tabbedPane.add("Geral", pGeral);
		JLabel lblAbaGeral = new JLabel("Informações");		
		ImageIcon iconAbaGeral = new ImageIcon(PrincipalUI.class.getResource("/br/pro/x87/images/note.png"));
		lblAbaGeral.setIcon(iconAbaGeral);
		lblAbaGeral.setIconTextGap(5);
		lblAbaGeral.setHorizontalTextPosition(SwingConstants.RIGHT);
		tabbedPane.setTabComponentAt(0, lblAbaGeral);

		/*
		 * creation for tabCustosFixos
		 */		
		JPanel pCustosFixos = buildPanelCustosFixos();		
		tabbedPane.add("Custos fixos", pCustosFixos);
		JLabel lblAbaCustosFixos = new JLabel("Custos fixos");		
		ImageIcon iconAbaCustosFixos = new ImageIcon(PrincipalUI.class.getResource("/br/pro/x87/images/vallet.png"));
		lblAbaCustosFixos.setIcon(iconAbaCustosFixos);
		lblAbaCustosFixos.setIconTextGap(5);
		lblAbaCustosFixos.setHorizontalTextPosition(SwingConstants.RIGHT);
		tabbedPane.setTabComponentAt(1, lblAbaCustosFixos);

		/*
		 * creation for tabCustosVariaveis
		 */		
		JPanel pCustosVariaveis = buildPanelCustosVariaveis();
		tabbedPane.add("Custos Variáveis", pCustosVariaveis);
		JLabel lblAbaCustosVariaveis = new JLabel("Custos variáveis");		
		ImageIcon iconAbaCustosVariaveis = new ImageIcon(PrincipalUI.class.getResource("/br/pro/x87/images/params.png"));
		lblAbaCustosVariaveis.setIcon(iconAbaCustosVariaveis);
		lblAbaCustosVariaveis.setIconTextGap(5);
		lblAbaCustosVariaveis.setHorizontalTextPosition(SwingConstants.RIGHT);
		tabbedPane.setTabComponentAt(2, lblAbaCustosVariaveis);

		/*
		 * creation for tabRelatorio
		 */

		JPanel pRelatorio = buildPanelRelatorio();
		tabbedPane.add("Imprimir", pRelatorio);
		JLabel lblAbaRelatorio = new JLabel("Relatório");		
		ImageIcon iconAbaRelatorio = new ImageIcon(PrincipalUI.class.getResource("/br/pro/x87/images/search.png"));
		lblAbaRelatorio.setIcon(iconAbaRelatorio);
		lblAbaRelatorio.setIconTextGap(5);
		lblAbaRelatorio.setHorizontalTextPosition(SwingConstants.RIGHT);
		tabbedPane.setTabComponentAt(3, lblAbaRelatorio);


		setResizable(false);
		P2.setBounds(0, 142, 854, 128);
		pBotoes.setBounds(0, 116, 854, 23);
		setBounds(100, 100, 860, 604);

		getContentPane().setLayout(null);

		JPanel panel = new JPanel();
		panel.setBackground(new Color(255, 204, 153));
		panel.setBounds(0, 274, 854, 30);
		getContentPane().add(panel);
		panel.setLayout(new GridLayout(0, 1, 0, 0));

		JLabel lblAjuda1 = new JLabel("Preencha os campos acima e salve a operação. Para completar o cadastro, selecione o registro na grade acima.");
		panel.add(lblAjuda1);
		lblAjuda1.setForeground(Color.BLACK);
		lblAjuda1.setHorizontalAlignment(SwingConstants.CENTER);
		lblAjuda1.setBackground(Color.ORANGE);
		lblAjuda1.setBackground(Color.yellow);

		JLabel lblAjuda2 = new JLabel("Você pode preencher o formuário e salvar a operação a qualquer momento.");
		panel.add(lblAjuda2);
		lblAjuda2.setForeground(Color.BLACK);
		lblAjuda2.setBackground(Color.ORANGE);
		lblAjuda2.setHorizontalAlignment(SwingConstants.CENTER);
	}

	/**
	 * @return
	 */


	private JPanel buildPanelRelatorio() {
		JPanel pRelatorio = new JPanel();

		JPanel pCustosFixos = new JPanel();
		pCustosFixos.setBounds(424, 75, 425, 160);
		pCustosFixos.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder(" Custos fixos "),
				BorderFactory.createEmptyBorder(5,5,5,5)));		

		pCustosFixos.setLayout(new BorderLayout());

		JScrollPane scrollPaneCf = new JScrollPane();
		gradeCustosFixos = new JTable();
		gradeCustosFixos.setFocusable(false);
		gradeCustosFixos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		gradeCustosFixos.setCursor(new Cursor(Cursor.HAND_CURSOR));
		gradeCustosFixos.setToolTipText("Estes itens foram obtidos a partir da aba \"Custos Fixos\"");
		scrollPaneCf.setViewportView(gradeCustosFixos);
		pCustosFixos.add(scrollPaneCf);	

		JPanel pCustosVariaveis = new JPanel();
		pCustosVariaveis.setBounds(0, 75, 425, 160);		
		pCustosVariaveis.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder(" Custos variáveis "),
				BorderFactory.createEmptyBorder(5,5,5,5)));
		pCustosVariaveis.setLayout(new BorderLayout());

		JScrollPane scrollPaneCv = new JScrollPane();			
		gradeCustosVariaveis = new JTable();
		gradeCustosVariaveis.setFocusable(false);
		gradeCustosVariaveis.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		gradeCustosVariaveis.setCursor(new Cursor(Cursor.HAND_CURSOR));
		gradeCustosVariaveis.setToolTipText("Estes itens foram obtidos a partir da aba \"Custos Variáveis\"");
		scrollPaneCv.setViewportView(gradeCustosVariaveis);
		pCustosVariaveis.add(scrollPaneCv);
		gradeCustosVariaveis.setModel(new DefaultTableModel());

		pRelatorio.setLayout(null);

		JPanel pTotal = new JPanel();
		pTotal.setBounds(0, 0, 849, 75);
		pTotal.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder(" Total "),
				BorderFactory.createEmptyBorder(5,5,5,5)));

		JLabel lblCustosFixos = new JLabel("Custos fixos (R$/h)");		
		tfTotalCustosFixos = new JTextField();
		//tfTotalCustosFixos.setBounds(0, 0, 86, 20);
		tfTotalCustosFixos.setColumns(6);			

		tfTotalCustosVariaveis = new JTextField();		
		tfTotalCustosVariaveis.setColumns(6);		
		tfTotalCustoHorario = new JTextField();		
		tfTotalCustoHorario.setColumns(6);		
		JButton btnImprimirRelatorio = new JButton("Imprimir ");
		btnImprimirRelatorio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if ((getCvm().equals(null)) || (getCfm().equals(null))){
					mensagemErro("Não foi possível exibir o relatório solicitado!");
					return;
				}

				float totalCf = 0, totalCv = 0, totalCh = 0;
				try {
					totalCf = converte(tfTotalCustosFixos.getText());
					totalCh = converte(tfTotalCustoHorario.getText());
					totalCv = converte(tfTotalCustosVariaveis.getText());					
				} catch (ParseException e1) {
					// TODO Auto-generated catch block

				}


				RelatorioMaquina rm = new RelatorioMaquina(getMaquina(), getCvm(), getCfm(), totalCf, totalCv, totalCh);
				rm.imprimir(rm);

			}
		});
		pTotal.setLayout(new GridLayout(0, 5, 10, 0));


		pTotal.add(lblCustosFixos);

		JLabel lblCustosVariaveis = new JLabel("Custos variáveis (R$/h)");		
		pTotal.add(lblCustosVariaveis);
		JLabel lblCustoHorario = new JLabel("Custo-horário (R$/h)");		
		pTotal.add(lblCustoHorario);

		JLabel label = new JLabel("");
		pTotal.add(label);

		JLabel label_1 = new JLabel("");
		pTotal.add(label_1);
		pTotal.add(tfTotalCustosFixos);
		pTotal.add(tfTotalCustosVariaveis);
		pTotal.add(tfTotalCustoHorario);
		pTotal.add(btnImprimirRelatorio);		

		pRelatorio.add(pTotal);

		JButton btnNewButton = new JButton("Gráfico");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				depreciacaoPizza = new ComponentesCustoHorarioChart(getCfm(), getCvm());
				depreciacaoPizza.setVisible(true);
				depreciacaoPizza.centraliza();				
			}
		});
		pTotal.add(btnNewButton);
		pRelatorio.add(pCustosVariaveis);
		pRelatorio.add(pCustosFixos);

		return pRelatorio;
	}

	public JPanel buildPanelGeral(){
		JPanel pGeral = new JPanel();
		pGeral.setLayout(null);

		model = new UtilDateModel();
		JDatePanelImpl datePanel = new JDatePanelImpl(model);

		JPanel pCaracteristicas = new JPanel();

		pCaracteristicas.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder(" Características "),
				BorderFactory.createEmptyBorder(5,5,5,5)));		

		pCaracteristicas.setLocation(2, 11);
		pCaracteristicas.setSize(847, 235);
		pCaracteristicas.setLayout(null);

		ButtonGroup group = new ButtonGroup();

		pGeral.add(pCaracteristicas);

		JPanel panel = new JPanel();
		panel.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder(" Número de filtros (unidades) "),
				BorderFactory.createEmptyBorder(5,5,5,5)));		

		panel.setBounds(312, 155, 530, 74);
		pCaracteristicas.add(panel);
		panel.setLayout(new GridLayout(2, 5, 10, 0));

		JLabel label = new JLabel("Diesel");
		panel.add(label);

		JLabel label_1 = new JLabel("Lubrificante");
		panel.add(label_1);

		JLabel label_2 = new JLabel("Hidráulico");
		panel.add(label_2);

		JLabel label_3 = new JLabel("Ar");
		panel.add(label_3);

		JLabel label_4 = new JLabel("Segurança");
		panel.add(label_4);

		tfFiltrosDiesel = new JTextField();
		tfFiltrosDiesel.setColumns(10);
		panel.add(tfFiltrosDiesel);

		tfFiltrosLubrificante = new JTextField();
		tfFiltrosLubrificante.setColumns(10);
		panel.add(tfFiltrosLubrificante);

		tfFiltrosHidraulico = new JTextField();
		tfFiltrosHidraulico.setColumns(10);
		panel.add(tfFiltrosHidraulico);

		tfFiltrosAr = new JTextField();
		tfFiltrosAr.setColumns(10);
		panel.add(tfFiltrosAr);

		tfFiltrosSeguranca = new JTextField();
		tfFiltrosSeguranca.setColumns(10);
		panel.add(tfFiltrosSeguranca);

		JPanel panel_1 = new JPanel();
		panel_1.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder(" Capacidade de óleo (L) "),
				BorderFactory.createEmptyBorder(5,5,5,5)));		
		panel_1.setBounds(5, 155, 309, 74);
		pCaracteristicas.add(panel_1);
		panel_1.setLayout(new GridLayout(0, 3, 10, 0));

		JLabel label_5 = new JLabel("Cárter");
		panel_1.add(label_5);

		JLabel label_6 = new JLabel("Transmissão");
		panel_1.add(label_6);

		JLabel label_7 = new JLabel("Hidráulico");
		panel_1.add(label_7);

		tfOleoCarter = new JTextField();
		tfOleoCarter.setColumns(10);
		panel_1.add(tfOleoCarter);

		tfOleoTransmissao = new JTextField();
		tfOleoTransmissao.setColumns(10);
		panel_1.add(tfOleoTransmissao);

		tfOleoHidraulico = new JTextField();
		tfOleoHidraulico.setColumns(10);
		panel_1.add(tfOleoHidraulico);

		JPanel panel_3 = new JPanel();
		panel_3.setBounds(10, 20, 830, 40);
		pCaracteristicas.add(panel_3);
		panel_3.setLayout(new GridLayout(0, 8, 10, 0));

		JLabel lblNmeroDoPatrimnio = new JLabel("Patrimônio");
		panel_3.add(lblNmeroDoPatrimnio);

		JLabel lblNmeroDeAnos = new JLabel("Utilização (anos)");
		panel_3.add(lblNmeroDeAnos);

		JLabel lblEficincia = new JLabel("Eficiência");
		panel_3.add(lblEficincia);

		JLabel lblNmeroDeSrie = new JLabel("Número de série");
		panel_3.add(lblNmeroDeSrie);

		JLabel lblHormetroh = new JLabel("Horímetro (h)");
		panel_3.add(lblHormetroh);

		JLabel lblNewLabel = new JLabel("Ano");
		panel_3.add(lblNewLabel);

		JLabel lblPotncia = new JLabel("Potência");
		panel_3.add(lblPotncia);

		rbKW = new JRadioButton("kW");

		panel_3.add(rbKW);
		group.add(rbKW);

		tfNumeroPatrimonio = new JTextField();
		panel_3.add(tfNumeroPatrimonio);

		tfAnosUtilizacao = new JTextField();
		panel_3.add(tfAnosUtilizacao);
		tfAnosUtilizacao.setColumns(10);

		tfEficiencia = new JTextField();
		panel_3.add(tfEficiencia);
		tfEficiencia.setColumns(10);

		tfNumeroSerie = new JTextField();
		panel_3.add(tfNumeroSerie);

		tfHorimetro = new JTextField();
		panel_3.add(tfHorimetro);
		tfHorimetro.setColumns(10);

		tfAno = new JTextField();
		panel_3.add(tfAno);
		tfAno.setColumns(10);

		tfPotencia = new JTextField();
		panel_3.add(tfPotencia);
		tfPotencia.setColumns(10);

		rbCV = new JRadioButton("cv");
		rbCV.setSelected(true);
		panel_3.add(rbCV);
		group.add(rbCV);

		JPanel panel_4 = new JPanel();
		panel_4.setBounds(10, 65, 830, 82);
		pCaracteristicas.add(panel_4);
		panel_4.setLayout(new GridLayout(0, 5, 10, 0));

		JLabel lblDataDeAquisio = new JLabel("Data de aquisição");
		panel_4.add(lblDataDeAquisio);

		JLabel lblValorDeAquisio = new JLabel("Valor de aquisição (R$)");
		panel_4.add(lblValorDeAquisio);

		JLabel lblValorDoEquip = new JLabel("Valor equip. usado (R$)");
		panel_4.add(lblValorDoEquip);

		JLabel lblTempoDeUso = new JLabel("Tempo de uso (h/ano)");
		panel_4.add(lblTempoDeUso);

		lblConsumoHorario = new JLabel("Consumo horário (L/h)");
		panel_4.add(lblConsumoHorario);
		dpAquisicao = new JDatePickerImpl(datePanel);
		panel_4.add(dpAquisicao);
		dpAquisicao.setMaximumSize(new Dimension(32813, 20));
		dpAquisicao.setMinimumSize(new Dimension(52, 20));
		dpAquisicao.getJFormattedTextField().setPreferredSize(new Dimension(178, 20));
		dpAquisicao.setPreferredSize(new Dimension(202, 20));
		dpAquisicao.getJFormattedTextField().setBackground(Color.WHITE);

		dpAquisicao.setBorder(new LineBorder(new Color(0, 0, 0)));
		dpAquisicao.setShowYearButtons(true);
		dpAquisicao.getJFormattedTextField().setToolTipText("Clique para selecionar uma data");	

		tfValorAquisicao = new JTextField();
		panel_4.add(tfValorAquisicao);
		tfValorAquisicao.setColumns(15);

		tfValorResidual = new JTextField();
		panel_4.add(tfValorResidual);
		tfValorResidual.setColumns(10);

		tfTempoUso = new JTextField();
		panel_4.add(tfTempoUso);
		tfTempoUso.setColumns(10);

		tfConsumoHorario = new JTextField();
		panel_4.add(tfConsumoHorario);
		tfConsumoHorario.setColumns(10);

		JLabel lblVelocidadeMximakmh = new JLabel("Velocidade máx (km/h)");
		panel_4.add(lblVelocidadeMximakmh);

		JLabel lblLarguraOperacionalm = new JLabel("Largura op (m)");
		panel_4.add(lblLarguraOperacionalm);

		JLabel lblPesokg = new JLabel("Peso (kg)");
		panel_4.add(lblPesokg);

		JLabel lblPneusDiantun = new JLabel("Pneus diant. (un)");
		panel_4.add(lblPneusDiantun);

		JLabel lblPneusTrasun = new JLabel("Pneus tras. (un)");
		panel_4.add(lblPneusTrasun);

		tfVelocidadeMaxima = new JTextField();
		panel_4.add(tfVelocidadeMaxima);
		tfVelocidadeMaxima.setColumns(10);

		tfLargura = new JTextField();
		panel_4.add(tfLargura);
		tfLargura.setColumns(10);

		tfPeso = new JTextField();
		panel_4.add(tfPeso);
		tfPeso.setColumns(10);

		tfNumPneusDianteiro = new JTextField();
		panel_4.add(tfNumPneusDianteiro);
		tfNumPneusDianteiro.setColumns(10);

		tfNumPneusTraseiro = new JTextField();
		panel_4.add(tfNumPneusTraseiro);
		tfNumPneusTraseiro.setColumns(10);

		return pGeral;
	}


	private JPanel buildPanelCustosFixos() {
		RadioButtonHandler handler = new RadioButtonHandler();
		rbtnQuotas = new JRadioButton("Quotas constantes");
		rbtnProporcional = new JRadioButton("Proporcional às horas trabalhadas");
		rbtnPorcentagensConstantes = new JRadioButton("Porcentagens constantes (Matheson)");
		rbtnDigitosPeriodicos = new JRadioButton("Soma dos dígitos periódicos (Cole)");
		rbtnDeclinioEmDobro = new JRadioButton("Declínio em dobro");

		ButtonGroup group = new ButtonGroup();
		group.add(rbtnDeclinioEmDobro);
		group.add(rbtnDigitosPeriodicos);
		group.add(rbtnPorcentagensConstantes);
		group.add(rbtnProporcional);
		group.add(rbtnQuotas);

		rbtnDeclinioEmDobro.addItemListener(handler);
		rbtnDigitosPeriodicos.addItemListener(handler);
		rbtnPorcentagensConstantes.addItemListener(handler);
		rbtnProporcional.addItemListener(handler);
		rbtnQuotas.addItemListener(handler);

		JPanel pDepreciacao = new JPanel();

		JPanel pCustosFixos = new JPanel();		
		pCustosFixos.setLayout(null);
		pCustosFixos.add(pDepreciacao);

		pDepreciacao.setBounds(5, 14, 255, 171);		
		pDepreciacao.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder(" Selecione a forma de depreciação "),
				BorderFactory.createEmptyBorder(5,5,5,5)));

		pDepreciacao.setLayout(new GridLayout(7, 1, 0, 0));
		pDepreciacao.add(rbtnQuotas);		
		pDepreciacao.add(rbtnProporcional);		
		pDepreciacao.add(rbtnPorcentagensConstantes);		
		pDepreciacao.add(rbtnDigitosPeriodicos);		
		pDepreciacao.add(rbtnDeclinioEmDobro);

		JButton btnSalvarMtodoDe = new JButton("Salvar método de depreciação");
		btnSalvarMtodoDe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String tipo = "";
				if (rbtnDeclinioEmDobro.isSelected()) tipo = "Declinio";
				if (rbtnDigitosPeriodicos.isSelected()) tipo = "Digitos";
				if (rbtnPorcentagensConstantes.isSelected()) tipo = "Porcentagens";
				if (rbtnProporcional.isSelected()) tipo = "Proporcional";
				if (rbtnQuotas.isSelected()) tipo = "Quotas";		
				
				getMaquina().setTipoDepreciacao(tipo);
				try {
					getMaquina().setDepreciacaoHoraria(converte(tfDepreciacaoHora.getText()));
				} catch (ParseException e1) {

				}
				
				executarCommandSalvar();				
				JOptionPane.showMessageDialog(null, "A forma de depreciação foi salva corretamente. Esta informação será utilizada posteriormente "
						+ "no cálculo real de uma operação!", "Sucesso", JOptionPane.INFORMATION_MESSAGE);	
			}
		});
		pDepreciacao.add(btnSalvarMtodoDe);

		JButton btnNewButton_1 = new JButton("Gráfico comparativo");
		pDepreciacao.add(btnNewButton_1);
		btnNewButton_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				grafico = new FormasDepreciacaoChart(getMaquina());
				grafico.setVisible(true);
				grafico.centraliza();
			}
		});
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});

		JPanel panel = new JPanel();
		panel.setBounds(263, 20, 404, 204);
		pCustosFixos.add(panel);
		panel.setLayout(new BorderLayout(0, 0));

		JScrollPane scrollPane = new JScrollPane();
		panel.add(scrollPane);	

		gradeDepreciacao = new JTable();
		gradeDepreciacao.setFocusable(false);
		gradeDepreciacao.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		gradeDepreciacao.setCursor(new Cursor(Cursor.HAND_CURSOR));
		gradeDepreciacao.setToolTipText("Nâo é necessário salvar essas informações. A depreciação é calculada a cada vez.");

		scrollPane.setViewportView(gradeDepreciacao);

		JPanel pDespesas = new JPanel();		
		pDespesas.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder(" Despesas "),
				BorderFactory.createEmptyBorder(5,5,5,5)));

		pDespesas.setBounds(669, 16, 180, 211);
		pCustosFixos.add(pDespesas);
		pDespesas.setLayout(null);

		JLabel lblAlojamentorh = new JLabel("Alojamento (R$/h)");
		lblAlojamentorh.setBounds(12, 133, 108, 14);
		pDespesas.add(lblAlojamentorh);

		tfAlojamentoTotal = new JTextField();
		tfAlojamentoTotal.setBackground(SystemColor.info);
		tfAlojamentoTotal.setEditable(false);
		tfAlojamentoTotal.setBounds(12, 149, 85, 20);
		pDespesas.add(tfAlojamentoTotal);
		tfAlojamentoTotal.setColumns(10);

		JLabel lblSegurorh = new JLabel("Seguro (R$/h)");
		lblSegurorh.setBounds(12, 76, 86, 14);
		pDespesas.add(lblSegurorh);

		tfSeguroTotal = new JTextField();
		tfSeguroTotal.setBackground(SystemColor.info);
		tfSeguroTotal.setEditable(false);
		tfSeguroTotal.setBounds(12, 91, 85, 20);
		pDespesas.add(tfSeguroTotal);
		tfSeguroTotal.setColumns(10);

		tfJurosTotal = new JTextField();
		tfJurosTotal.setBackground(SystemColor.info);
		tfJurosTotal.setEditable(false);
		tfJurosTotal.setBounds(12, 37, 85, 20);
		pDespesas.add(tfJurosTotal);
		tfJurosTotal.setColumns(10);

		JLabel lblJurosRh = new JLabel("Juros (R$/h)");
		lblJurosRh.setBounds(12, 21, 99, 14);
		pDespesas.add(lblJurosRh);

		JLabel lblNewLabel_3 = new JLabel("% PA anual:");
		lblNewLabel_3.setBounds(12, 110, 80, 16);
		pDespesas.add(lblNewLabel_3);

		JLabel lblAlojamentoPa = new JLabel("% PA anual:");
		lblAlojamentoPa.setBounds(12, 168, 80, 14);
		pDespesas.add(lblAlojamentoPa);

		JLabel lblNewLabel_1 = new JLabel("Taxa anual (%):");
		lblNewLabel_1.setBounds(12, 57, 108, 14);
		pDespesas.add(lblNewLabel_1);

		JButton btnTxJuros = new JButton("");
		btnTxJuros.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				String st = JOptionPane.showInputDialog(null, "Informe a nova taxa (%)", "Taxa de juros", JOptionPane.QUESTION_MESSAGE);
				if (st != null){
					try {
						converte(st);
						lblTaxaJuros.setText(getDf().format(converte(st)));
						getMaquina().setTaxaJuros(converte(st));
						executarCommandSalvar();
						calcularJuros();
					} catch (Exception e2) {
						JOptionPane.showMessageDialog(null, "O valor informado não corresponde a um valor numérico válido.", "Erro", JOptionPane.ERROR_MESSAGE);						
						lblTaxaJuros.setText(getDf().format(getMaquina().getTaxaJuros()));
					}
				}			
			}
		});
		btnTxJuros.setIcon(new ImageIcon(MaquinaUI.class.getResource("/br/pro/x87/images/btnSettings.ico")));
		btnTxJuros.setBounds(98, 35, 23, 23);
		pDespesas.add(btnTxJuros);

		JButton btnTxSeguro = new JButton("");
		btnTxSeguro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String st = JOptionPane.showInputDialog(null, "Informe a nova taxa (%)", "Taxa de seguro", JOptionPane.QUESTION_MESSAGE);
				if (st != null){
					try {
						converte(st);
						lblTaxaSeguro.setText(getDf().format(converte(st)));
						getMaquina().setTaxaSeguros(converte(st));
						executarCommandSalvar();
						calcularSeguros();
					} catch (Exception e2) {
						JOptionPane.showMessageDialog(null, "O valor informado não corresponde a um valor numérico válido.");
						lblTaxaSeguro.setText(getDf().format(getMaquina().getTaxaSeguros()));
					}
				}			
			}
		});
		btnTxSeguro.setIcon(new ImageIcon(MaquinaUI.class.getResource("/br/pro/x87/images/btnSettings.ico")));
		btnTxSeguro.setBounds(98, 90, 23, 23);
		pDespesas.add(btnTxSeguro);

		JButton btnTxAlojamento = new JButton("");
		btnTxAlojamento.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String st = JOptionPane.showInputDialog(null, "Informe a nova taxa (%)", "Taxa de alojamento", JOptionPane.QUESTION_MESSAGE);
				if (st != null){
					try {
						converte(st);
						lblTaxaAlojamento.setText(getDf().format(converte(st)));
						getMaquina().setTaxaAlojamento(converte(st));
						executarCommandSalvar();
						calcularAlojamento();
					} catch (Exception e2) {
						JOptionPane.showMessageDialog(null, "O valor informado não corresponde a um valor numérico válido.");
						lblTaxaAlojamento.setText(getDf().format(getMaquina().getTaxaAlojamento()));
					}
				}			
			}
		});
		btnTxAlojamento.setIcon(new ImageIcon(MaquinaUI.class.getResource("/br/pro/x87/images/btnSettings.ico")));
		btnTxAlojamento.setBounds(98, 147, 23, 23);
		pDespesas.add(btnTxAlojamento);

		lblTaxaJuros = new JLabel((Double.toString(TDespesas.JUROS.getValue())));
		lblTaxaJuros.setBounds(101, 57, 85, 16);
		pDespesas.add(lblTaxaJuros);

		lblTaxaSeguro = new JLabel((Double.toString(TDespesas.SEGURO.getValue())));
		lblTaxaSeguro.setBounds(81, 110, 85, 16);
		pDespesas.add(lblTaxaSeguro);

		lblTaxaAlojamento = new JLabel((Double.toString(TDespesas.ALOJAMENTO.getValue())));
		lblTaxaAlojamento.setBounds(81, 167, 85, 16);
		pDespesas.add(lblTaxaAlojamento);
		pCustosFixos.add(pDespesas);

		JPanel panel_1 = new JPanel();
		panel_1.setBounds(5, 183, 255, 40);
		pCustosFixos.add(panel_1);
		panel_1.setLayout(new GridLayout(2, 2, 10, 0));

		JLabel lblDeprte = new JLabel("Depreciação (R$/ano)");
		panel_1.add(lblDeprte);

		JLabel lblDepreciaorh = new JLabel("Depreciação (R$/h)");
		panel_1.add(lblDepreciaorh);

		tfDepreciacaoAno = new JTextField();
		panel_1.add(tfDepreciacaoAno);
		tfDepreciacaoAno.setBackground(SystemColor.info);
		tfDepreciacaoAno.setEditable(false);
		tfDepreciacaoAno.setColumns(10);

		tfDepreciacaoHora = new JTextField();
		panel_1.add(tfDepreciacaoHora);
		tfDepreciacaoHora.setBackground(SystemColor.info);
		tfDepreciacaoHora.setEditable(false);
		tfDepreciacaoHora.setColumns(10);

		return pCustosFixos;
	}

	private void fillPanelCustosFixos(){

	}

	public boolean calcularAlojamento(){
		float alojamento = getMaquina().calcularAlojamento();

		if (alojamento == -1){
			addMensagem("- Alojamento");
			addMensagem("- Juros");
			addMensagem("- Seguro");
			tfAlojamentoTotal.setText("0.00");
			return false;
		} else {
			tfAlojamentoTotal.setText(getDf().format(alojamento));
			getCfm().update("Alojamento", alojamento);
			gradeCustosFixos.updateUI();
			return true;
		}
	}


	public boolean calcularJuros(){
		float juro = getMaquina().calcularJuro();

		if (juro == -1){	
			addMensagem("- Juros");
			addMensagem("- Seguro");
			tfJurosTotal.setText("0.00");
			return false;
		} else {
			tfJurosTotal.setText(getDf().format((juro)));
			getCfm().update("Juros", juro);
			gradeCustosFixos.updateUI();
			return true;
		}
	}

	public boolean calcularSeguros(){
		float seguro = getMaquina().calcularSeguro();

		if (seguro == -1){
			addMensagem("- Seguro");
			tfSeguroTotal.setText("0.00");
			return false;
		} else {						
			tfSeguroTotal.setText((getDf().format(seguro)));
			getCfm().update("Seguro", seguro);
			gradeCustosFixos.updateUI();
			return true;
		}
	}

	public void calcularCustosFixos(){
		inicializarResposta();
		try {			
//			rbtnQuotas.setSelected(false);
//			rbtnQuotas.setSelected(true);
//			setDepre(new QuotasConstantes(getMaquina()));
			recuperarRadioSelecionado();
			
			DepreciacaoModel model = new DepreciacaoModel(getDepre());
			gradeDepreciacao.setModel(model);
			gradeDepreciacao.repaint();	
		} catch (Exception e) {
			addMensagem("- Depreciação");
			gradeDepreciacao.setModel(new DefaultTableModel());	
			gradeDepreciacao.repaint();	
			return;
		}

		float aux = 0;
		aux = getMaquina().getTaxaAlojamento();
		if (aux == 0){
			lblTaxaAlojamento.setText((Double.toString(TDespesas.ALOJAMENTO.getValue())));						
		} else {
			lblTaxaAlojamento.setText(getDf().format(getMaquina().getTaxaAlojamento()));
		}

		aux = 0;
		aux = getMaquina().getTaxaJuros();
		if (aux == 0){
			lblTaxaJuros.setText((Double.toString(TDespesas.JUROS.getValue())));
		} else {
			lblTaxaJuros.setText(getDf().format(getMaquina().getTaxaJuros()));
		}

		aux = 0;
		aux = getMaquina().getTaxaSeguros();
		if (aux == 0){
			lblTaxaSeguro.setText((Double.toString(TDespesas.SEGURO.getValue())));
		} else {
			lblTaxaSeguro.setText(getDf().format(getMaquina().getTaxaSeguros()));
		}

		lblTaxaSeguro.setText(getDf().format(getMaquina().getTaxaSeguros()));

		/*Não alterar essa ordem, visto que as mensagens são condicionadas a qual condição retornar falha primeiro*/
		if (!calcularAlojamento() || (!calcularJuros()) || (!calcularSeguros())){
			exibirMensagem();
		}
	}

	private void preencherGradeCustosFixosETotal(){
		float vAlojamento = 0, vSeguro = 0, vJuro = 0, vDepreciacao = 0;

		try {
			vAlojamento = converte(tfAlojamentoTotal.getText());
			vDepreciacao = converte(tfDepreciacaoHora.getText());
			vJuro = converte(tfJurosTotal.getText());
			vSeguro = converte(tfSeguroTotal.getText());
		} catch (ParseException e) {
			JOptionPane.showMessageDialog(null, "Nao foi possível preencher a grade com os custos fixos. "
					+ "\nVerifique se todos os valores foram calculados corretamente.\n\n", "Erro", JOptionPane.ERROR_MESSAGE);

			return;
		}

		getCfm().add(new CampoDeCusto("Alojamento", vAlojamento));
		getCfm().add(new CampoDeCusto("Depreciação", vDepreciacao));
		getCfm().add(new CampoDeCusto("Juros", vJuro));
		getCfm().add(new CampoDeCusto("Seguro", vSeguro));

		tfTotalCustosFixos.setText(getDf().format(vDepreciacao + vAlojamento + vJuro + vSeguro));
		calcularTotalCustoHorario();

		gradeCustosFixos.setModel(cfm);
		gradeCustosFixos.updateUI();
	}

	private void calcularTotalCustoHorario(){		
		float cf = 0, cv = 0;		

		/*nenhum campo preenchido*/
		if ((tfTotalCustosFixos.getText().equals("")) && (tfTotalCustosVariaveis.getText().equals(""))){
			return;
		}

		/*apenas cv preenchido*/
		if (tfTotalCustosFixos.getText().equals("")){
			try {				
				cv = converte(tfTotalCustosVariaveis.getText());
				tfTotalCustoHorario.setText(getDf().format(cv));				
				return;
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, "Ocorreu um erro ao efetuar o cálculo do custo horário final"
						+ "\nVerifique se todos os campos estão preenchidos e no formato adequado", "Erro", JOptionPane.ERROR_MESSAGE);
			}	
		}

		/*apenas cf preenchido*/
		if (tfTotalCustosVariaveis.getText().equals("")){
			try {				
				cf = converte(tfTotalCustosFixos.getText());
				tfTotalCustoHorario.setText(getDf().format(cf));
				return;
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, "Ocorreu um erro ao efetuar o cálculo do custo horário final"
						+ "\nVerifique se todos os campos estão preenchidos e no formato adequado", "Erro", JOptionPane.ERROR_MESSAGE);
			}	
		}

		/*se chegou até aqui, faz a soma de cf e cv*/
		try {
			cf = converte(tfTotalCustosFixos.getText());
			cv = converte(tfTotalCustosVariaveis.getText());
			tfTotalCustoHorario.setText(getDf().format(cf + cv));
		} catch (Exception e) {
			e.printStackTrace();
		}			
	}

	private JPanel buildPanelCustosVariaveis() {
		JPanel pCustosVariaveis = new JPanel();
		pCustosVariaveis.setLayout(null);

		JPanel pProduto = new JPanel();	

		pProduto.setBounds(7, 4, 365, 100);	
		pProduto.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder(" Produtos "),
				BorderFactory.createEmptyBorder(5,5,5,5)));

		pProduto.setLayout(null);
		pCustosVariaveis.add(pProduto);

		comboTipoProduto = new JComboBox();
		comboTipoProduto.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				popularProduto();
			}
		});
		comboTipoProduto.setBounds(9, 33, 110, 20);
		pProduto.add(comboTipoProduto);

		comboProduto = new JComboBox();
		comboProduto.setBounds(128, 33, 110, 20);
		pProduto.add(comboProduto);

		tfTroca = new JTextField();
		tfTroca.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				calcularTotalProduto();
			}
		});
		tfTroca.setBounds(9, 73, 110, 20);
		pProduto.add(tfTroca);
		tfTroca.setColumns(10);

		tfValorUnitario = new JTextField();
		tfValorUnitario.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				calcularTotalProduto();
			}
		});
		tfValorUnitario.setBounds(128, 73, 110, 20);
		pProduto.add(tfValorUnitario);
		tfValorUnitario.setColumns(10);

		tfProdutosTotal = new JTextField();
		tfProdutosTotal.setEditable(false);
		tfProdutosTotal.setBackground(SystemColor.info);
		tfProdutosTotal.setBounds(247, 33, 110, 20);
		pProduto.add(tfProdutosTotal);
		tfProdutosTotal.setColumns(10);

		JLabel lblTipoDeProduto = new JLabel("Tipo de produto");
		lblTipoDeProduto.setBounds(9, 17, 102, 14);
		pProduto.add(lblTipoDeProduto);

		JLabel lblProduto = new JLabel("Produto");
		lblProduto.setBounds(128, 17, 46, 14);
		pProduto.add(lblProduto);

		JLabel lblTrocah = new JLabel("Troca (h)");
		lblTrocah.setBounds(9, 59, 86, 14);
		pProduto.add(lblTrocah);

		JLabel lblValorUnitrior = new JLabel("Valor unitário (R$)");
		lblValorUnitrior.setBounds(128, 59, 102, 14);
		pProduto.add(lblValorUnitrior);

		JLabel lblTotalrh = new JLabel("Total (R$/h)");
		lblTotalrh.setBounds(247, 17, 86, 14);
		pProduto.add(lblTotalrh);

		btnAdicionarProduto = new JButton("Adicionar");
		btnAdicionarProduto.setEnabled(false);
		btnAdicionarProduto.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				adicionarAoGrid(comboProduto.getSelectedItem().toString(), tfProdutosTotal.getText());				
			}
		});
		btnAdicionarProduto.setIcon(new ImageIcon(MaquinaUI.class.getResource("/br/pro/x87/images/btnAdd.ico")));
		btnAdicionarProduto.setBounds(247, 71, 110, 23);
		pProduto.add(btnAdicionarProduto);

		JPanel pCombustivel = new JPanel();	

		pCombustivel.setBounds(7, 108, 365, 60);		
		pCombustivel.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder(" Combustível "),
				BorderFactory.createEmptyBorder(5,5,5,5)));

		pCombustivel.setLayout(null);
		pCustosVariaveis.add(pCombustivel);

		tfPrecoLitro = new JTextField();
		tfPrecoLitro.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				calcularTotalCombustivel();
			}
		});
		tfPrecoLitro.setBounds(9, 33, 110, 20);
		pCombustivel.add(tfPrecoLitro);
		tfPrecoLitro.setColumns(10);

		tfCombustivelTotal = new JTextField();
		tfCombustivelTotal.setEditable(false);
		tfCombustivelTotal.setBackground(SystemColor.info);
		tfCombustivelTotal.setBounds(128, 33, 110, 20);
		pCombustivel.add(tfCombustivelTotal);
		tfCombustivelTotal.setColumns(10);

		JLabel lblPreorl = new JLabel("Preço (R$/L)");
		lblPreorl.setBounds(9, 17, 86, 14);
		pCombustivel.add(lblPreorl);

		JLabel lblTotalrh_1 = new JLabel("Total (R$/h)");
		lblTotalrh_1.setBounds(128, 17, 86, 14);
		pCombustivel.add(lblTotalrh_1);

		btnAdicionarCombustivel = new JButton("Adicionar");
		btnAdicionarCombustivel.setEnabled(false);
		btnAdicionarCombustivel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				adicionarAoGrid("Combustível", tfCombustivelTotal.getText());
			}
		});
		btnAdicionarCombustivel.setIcon(new ImageIcon(MaquinaUI.class.getResource("/br/pro/x87/images/btnAdd.ico")));
		btnAdicionarCombustivel.setBounds(247, 32, 110, 22);
		pCombustivel.add(btnAdicionarCombustivel);

		JPanel pManutencao = new JPanel();	

		pManutencao.setBounds(7, 172, 365, 60);		
		pManutencao.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder(" Manutenção "),
				BorderFactory.createEmptyBorder(5,5,5,5)));
		pManutencao.setLayout(null);

		pCustosVariaveis.add(pManutencao);

		JLabel lblManutenorh = new JLabel("Valor (R$/h)");
		lblManutenorh.setBounds(9, 17, 110, 14);
		pManutencao.add(lblManutenorh);

		tfManutencaoTotal = new JTextField();
		tfManutencaoTotal.setBackground(SystemColor.info);
		tfManutencaoTotal.setEditable(false);
		tfManutencaoTotal.setColumns(10);
		tfManutencaoTotal.setBounds(9, 33, 85, 20);
		pManutencao.add(tfManutencaoTotal);

		tfTaxaManutencao = new JTextField();
		tfTaxaManutencao.setBackground(Color.WHITE);
		tfTaxaManutencao.setText("10,00");
		tfTaxaManutencao.setEditable(false);
		tfTaxaManutencao.setBounds(101, 33, 100, 20);
		pManutencao.add(tfTaxaManutencao);
		tfTaxaManutencao.setColumns(10);

		JLabel lblNewLabel_2 = new JLabel("Tx anual rep/manut (%)");
		lblNewLabel_2.setBounds(101, 17, 137, 14);
		pManutencao.add(lblNewLabel_2);

		btnAdicionarManutencao = new JButton("Adicionar");
		btnAdicionarManutencao.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnAdicionarManutencao.setEnabled(false);
		btnAdicionarManutencao.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				adicionarAoGrid("Manutenção", tfManutencaoTotal.getText());
			}
		});
		btnAdicionarManutencao.setIcon(new ImageIcon(MaquinaUI.class.getResource("/br/pro/x87/images/btnAdd.ico")));
		btnAdicionarManutencao.setBounds(247, 32, 110, 22);
		pManutencao.add(btnAdicionarManutencao);

		JButton button_4 = new JButton("");
		button_4.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				String st = JOptionPane.showInputDialog(null, "Informe a nova taxa (%)", "Taxa de manutenção", JOptionPane.QUESTION_MESSAGE);
				if (st != null){
					try {
						converte(st);
						tfTaxaManutencao.setText(getDf().format(converte(st)));	
						calcularTotalManutencao();
					} catch (Exception e2) {
						JOptionPane.showMessageDialog(null, "O valor informado não corresponde a um valor numérico válido.");
						tfTaxaManutencao.setText("10.00");
					}
				}

			}
		});
		button_4.setIcon(new ImageIcon(MaquinaUI.class.getResource("/br/pro/x87/images/btnSettings.ico")));
		button_4.setBounds(200, 33, 38, 19);
		pManutencao.add(button_4);

		JPanel pGraxa = new JPanel();	

		pGraxa.setBounds(371, 0, 188, 136);		
		pGraxa.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder(" Graxa "),
				BorderFactory.createEmptyBorder(5,5,5,5)));		
		pGraxa.setLayout(null);

		pCustosVariaveis.add(pGraxa);

		tfQuantidadeGraxa = new JTextField();
		tfQuantidadeGraxa.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				calcularTotalGraxa();
			}
		});
		tfQuantidadeGraxa.setBounds(9, 74, 110, 20);
		pGraxa.add(tfQuantidadeGraxa);
		tfQuantidadeGraxa.setColumns(10);

		tfPrecoGraxa = new JTextField();
		tfPrecoGraxa.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				calcularTotalGraxa();
			}
		});
		tfPrecoGraxa.setText("");
		tfPrecoGraxa.setBounds(9, 112, 85, 20);
		pGraxa.add(tfPrecoGraxa);
		tfPrecoGraxa.setColumns(10);

		tfTempoLubrificacao = new JTextField();
		tfTempoLubrificacao.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				calcularTotalGraxa();
			}
		});
		tfTempoLubrificacao.setBounds(9, 38, 110, 20);
		pGraxa.add(tfTempoLubrificacao);
		tfTempoLubrificacao.setColumns(10);

		tfGraxaTotal = new JTextField();
		tfGraxaTotal.setEditable(false);
		tfGraxaTotal.setBackground(SystemColor.info);
		tfGraxaTotal.setText("");
		tfGraxaTotal.setBounds(96, 112, 85, 20);
		pGraxa.add(tfGraxaTotal);
		tfGraxaTotal.setColumns(10);

		JLabel lblPreorkg = new JLabel("Preço (R$/Kg)");
		lblPreorkg.setBounds(9, 98, 85, 14);
		pGraxa.add(lblPreorkg);

		JLabel lblQuantidadekg = new JLabel("Quantidade (Kg)");
		lblQuantidadekg.setBounds(9, 60, 110, 14);
		pGraxa.add(lblQuantidadekg);

		JLabel lblTempoEntreLubrif = new JLabel("Tempo entre lubrif. (h)");
		lblTempoEntreLubrif.setBounds(10, 24, 139, 14);
		pGraxa.add(lblTempoEntreLubrif);

		JLabel lblTotalrh_3 = new JLabel("Total (R$)/h)");
		lblTotalrh_3.setBounds(96, 98, 85, 14);
		pGraxa.add(lblTotalrh_3);

		btnAdicionarGraxa = new JButton("");
		btnAdicionarGraxa.setEnabled(false);
		btnAdicionarGraxa.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				adicionarAoGrid("Graxa", tfGraxaTotal.getText());
			}
		});
		btnAdicionarGraxa.setIcon(new ImageIcon(MaquinaUI.class.getResource("/br/pro/x87/images/btnAdd.ico")));
		btnAdicionarGraxa.setBounds(140, 55, 38, 25);
		pGraxa.add(btnAdicionarGraxa);

		gradeConsumo = new JTable();
		JScrollPane scrollPaneConsumo = new JScrollPane(gradeConsumo);		 
		JPanel pConsumo = new JPanel();
		pConsumo.setLocation(562, 7);
		pConsumo.setSize(287, 209);

		gradeConsumo.setFocusable(false);
		gradeConsumo.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		gradeConsumo.setCursor(new Cursor(Cursor.HAND_CURSOR));
		gradeConsumo.setToolTipText("Clique aqui para carregar as informaçoes");

		gradeConsumo.setModel(new DefaultTableModel());

		pConsumo.setLayout(new BorderLayout());
		pConsumo.add(new JLabel("Itens consumidos", JLabel.CENTER),BorderLayout.NORTH);
		pConsumo.add(scrollPaneConsumo,BorderLayout.CENTER);

		pCustosVariaveis.add(pConsumo);

		JPanel pSalario = new JPanel();
		pSalario.setBounds(371, 136, 188, 99);		
		pSalario.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder(" Salário "),
				BorderFactory.createEmptyBorder(5,5,5,5)));

		pSalario.setLayout(null);
		pCustosVariaveis.add(pSalario);


		tfSalarioMensal = new JTextField();
		tfSalarioMensal.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				calcularTotalSalario();
			}
		});
		tfSalarioMensal.setBounds(9, 33, 86, 20);
		pSalario.add(tfSalarioMensal);
		tfSalarioMensal.setColumns(10);

		tfTotalSalario = new JTextField();
		tfTotalSalario.setEditable(false);
		tfTotalSalario.setBackground(SystemColor.info);
		tfTotalSalario.setBounds(9, 71, 86, 20);
		pSalario.add(tfTotalSalario);
		tfTotalSalario.setColumns(10);

		JLabel lblMensalr = new JLabel("Mensal (R$)");
		lblMensalr.setBounds(9, 17, 82, 14);
		pSalario.add(lblMensalr);

		JLabel lblTotalrh_2 = new JLabel("Total (R$/h)");
		lblTotalrh_2.setBounds(9, 57, 86, 14);
		pSalario.add(lblTotalrh_2);

		btnAdicionarSalario = new JButton("");
		btnAdicionarSalario.setEnabled(false);
		btnAdicionarSalario.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				adicionarAoGrid("Salário", tfTotalSalario.getText());
			}
		});
		btnAdicionarSalario.setIcon(new ImageIcon(MaquinaUI.class.getResource("/br/pro/x87/images/btnAdd.ico")));
		btnAdicionarSalario.setBounds(140, 37, 38, 25);
		pSalario.add(btnAdicionarSalario);

		JButton btnRemoverItem = new JButton("Remover item selecionado");
		btnRemoverItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cvm.remove(gradeConsumo.getSelectedRow());
				gradeConsumo.updateUI();
			}
		});
		btnRemoverItem.setBounds(562, 215, 287, 23);
		pCustosVariaveis.add(btnRemoverItem);

		return pCustosVariaveis;
	}

	public void popularMarca(){
		listMarca = getDao().get("Marca");

		for (Object o : listMarca) {	
			Marca m = (Marca) o;
			comboMarca.addItem(m.getDescricao());
		}
	}

	@SuppressWarnings("unchecked")
	public void popularProduto(){
		comboProduto.removeAllItems();
		listProduto = getDao().filter("Produto", "tipoProduto", comboTipoProduto.getSelectedItem().toString());
		for (Object o : listProduto) {	
			Produto p = (Produto) o;
			comboProduto.addItem(p.getDescricao());
		}
	}

	private void popularTipoMaquina() {
		for (TMaquina o : TMaquina.values()) {
			comboTipo.addItem(o.getNome());
		}
	}


	public void popularTipoProduto(){		
		for (TProduto o : TProduto.values()) {
			comboTipoProduto.addItem(o.getNome());
		}		
	}


	public void calcularTotalProduto(){
		float valorUnitario;
		float troca;
		try {
			valorUnitario = converte(tfValorUnitario.getText());
			troca = converte(tfTroca.getText());
			tfProdutosTotal.setText(getDf().format(valorUnitario * troca));
			btnAdicionarProduto.setEnabled(true);						
		} catch (Exception e) {
			tfProdutosTotal.setText("0.00");
			btnAdicionarProduto.setEnabled(false);
		}
	}


	public void calcularTotalCombustivel(){
		try {
			if (getMaquina().getTipoPotencia().equalsIgnoreCase("cv")){
				tfCombustivelTotal.setText(getDf().format((converte(tfPrecoLitro.getText()) * getMaquina().getConsumoEspecifico())));
			} else {
				tfCombustivelTotal.setText(getDf().format(((getMaquina().getPotencia()*0.163f)*converte(tfPrecoLitro.getText()))*0.736f)); 
			}
			btnAdicionarCombustivel.setEnabled(true);
		} catch (Exception e) {
			tfCombustivelTotal.setText("0.00");
			btnAdicionarCombustivel.setEnabled(false);
		}
	}

	public void calcularTotalManutencao(){
		try {
			float taxaManutencao = 0;			
			taxaManutencao = converte(tfTaxaManutencao.getText());

			tfManutencaoTotal.setText(getDf().format((getMaquina().getValorAquisicao() * (taxaManutencao/100))/getMaquina().getTempoUso()));
			btnAdicionarManutencao.setEnabled(true);
		} catch (Exception e) {
			btnAdicionarManutencao.setEnabled(false);
		}
	}

	public void calcularTotalSalario(){
		try {
			float salario = 0;
			salario = converte(tfSalarioMensal.getText());

			tfTotalSalario.setText(getDf().format( (salario*13.33)/getMaquina().getTempoUso() ));
			btnAdicionarSalario.setEnabled(true);
		} catch (Exception e) {
			tfTotalSalario.setText("0.00");
			btnAdicionarSalario.setEnabled(false);
		}
	}

	public void calcularTotalGraxa(){
		try {
			tfGraxaTotal.setText(getDf().format((converte(tfPrecoGraxa.getText())*converte(tfQuantidadeGraxa.getText()))
					/converte(tfTempoLubrificacao.getText())));
			btnAdicionarGraxa.setEnabled(true);
		} catch (Exception e) {
			btnAdicionarGraxa.setEnabled(false);
			tfGraxaTotal.setText("0.00");
		}
	}

	public void adicionarAoGrid(String item, String valor){
		/*
		 * Adiciona cada item de CV ao gridConsumo
		 * Atualiza ambas as grades
		 * Calcula o total do CV
		 */

		Float total = 0f;
		try {
			total = converte(valor);		
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}				

		cvm.add(new CampoDeCusto(item, total));			

		gradeCustosVariaveis.updateUI();
		gradeConsumo.updateUI();

		tfTotalCustosVariaveis.setText(getDf().format(cvm.getTotal()));
		calcularTotalCustoHorario();
	}



	public void carregarAbas(){
		/*
		 * Aba 1: Carregar informações do banco
		 * Já é invocado no super
		 */

		/*
		 * Aba 2: Verificar se tem os campos necessários preenchidos para  
		 * Efetuar calculos para o custo fixo
		 */

		if (getMaquina().isFill()){
			tabbedPane.setEnabledAt(1, true);
			tabbedPane.setEnabledAt(2, true);			
			tabbedPane.setEnabledAt(3, true);

		}else{
			JOptionPane.showMessageDialog(null, "Atenção: esse registro não está completamente preenchido. \nNão será possível efetuar o cálculo "
					+ "dos custos fixos e variáveis!", "Aviso", JOptionPane.WARNING_MESSAGE);
			tabbedPane.setSelectedIndex(0);
			tabbedPane.setEnabledAt(1, false);
			tabbedPane.setEnabledAt(2, false);			
			tabbedPane.setEnabledAt(3, false);
			return;
		}


		setCfm(new CustosFixosModel());
		calcularCustosFixos();

		/*
		 * Aba 3: Efetuar calculos para o custo variável
		 */
		setCvm(new CustosVariaveisModel());

		gradeConsumo.setModel(getCvm());

		popularTipoProduto();
		calcularTotalManutencao();

		/*
		 * Aba 4: Carregar campos para compor relatório
		 */		
		gradeCustosVariaveis.setModel(getCvm());		
		preencherGradeCustosFixosETotal();

	}

	private class RadioButtonHandler implements ItemListener{
		@Override
		public void itemStateChanged(ItemEvent event) {
			if (rbtnDeclinioEmDobro.isSelected()) setDepre(new DeclinioEmDobro(getMaquina()));
			if (rbtnDigitosPeriodicos.isSelected()) setDepre(new DigitosPeriodicos(getMaquina()));
			if (rbtnPorcentagensConstantes.isSelected()) setDepre(new PorcentagensConstantes(getMaquina()));
			if (rbtnProporcional.isSelected()) setDepre(new Proporcional(getMaquina()));
			if (rbtnQuotas.isSelected()) setDepre(new QuotasConstantes(getMaquina()));

			DepreciacaoModel model = new DepreciacaoModel(getDepre());

			gradeDepreciacao.setModel(model);
			gradeDepreciacao.repaint();

			float depreciacaoAno = getDepre().depreciacaoAnual();
			float depreciacaoHora = getDepre().depreciacaoHoraria();		

			tfDepreciacaoAno.setText(getDf().format(depreciacaoAno));		
			tfDepreciacaoHora.setText(getDf().format(depreciacaoHora));

			try {
				getCfm().update("Depreciação", depreciacaoHora);	
			} catch (Exception e) {

			}
			
			gradeCustosFixos.updateUI();
		}
	}

	@Override
	public void adicionaListenerCampoVazio() {
		// TODO Auto-generated method stub		
	}

	@Override
	public boolean validarFlutuantes() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean validarCombos() {
		// TODO Auto-generated method stub
		return true;
	}
	
	@Override
	public void addDocument() {
		tfHorimetro.setDocument(new FloatFilter(FloatFilter.FLOAT));	
		tfPotencia.setDocument(new FloatFilter(FloatFilter.FLOAT));	
		tfPeso.setDocument(new FloatFilter(FloatFilter.FLOAT));
		tfEficiencia.setDocument(new FloatFilter(FloatFilter.FLOAT));	
		tfVelocidadeMaxima.setDocument(new FloatFilter(FloatFilter.FLOAT));
		tfTempoUso.setDocument(new FloatFilter(FloatFilter.FLOAT));
		tfConsumoHorario.setDocument(new FloatFilter(FloatFilter.FLOAT));
		tfLargura.setDocument(new FloatFilter(FloatFilter.FLOAT));
		tfValorAquisicao.setDocument(new FloatFilter(FloatFilter.FLOAT));
		tfValorResidual.setDocument(new FloatFilter(FloatFilter.FLOAT));
		tfOleoCarter.setDocument(new FloatFilter(FloatFilter.FLOAT));
		tfOleoTransmissao.setDocument(new FloatFilter(FloatFilter.FLOAT));
		tfOleoHidraulico.setDocument(new FloatFilter(FloatFilter.FLOAT));	

		((AbstractDocument) tfAno.getDocument()).setDocumentFilter(new IntegerFilter());
	}
}
