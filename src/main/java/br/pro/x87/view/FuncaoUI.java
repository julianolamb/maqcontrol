package br.pro.x87.view;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import br.pro.x87.dao.FuncaoDAO;
import br.pro.x87.model.Funcao;
import br.pro.x87.view.command.ExcluirCommand;
import br.pro.x87.view.command.SalvarCommand;

public class FuncaoUI extends AncestorUI {

	private static final long serialVersionUID = 1L;

	private JTextField tfFuncao;
	private Funcao funcao;

	public Funcao getFuncao() {
		return funcao;
	}

	public void setFuncao(Funcao funcao) {
		this.funcao = funcao;
	}

	public FuncaoUI() {
		bListagem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FuncaoDAO funcaoDAO = new FuncaoDAO<>();
				List<Funcao> lista = funcaoDAO.imprimirFuncoes();
				
				templateImprimir(lista, "funcao.jrxml");			
			}
		});
		tfFuncao.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if (tfFuncao.getText().isEmpty() || tfFuncao.getText().equals("")) {
					tfFuncao.setBackground(Color.WHITE);
				}
			}
		});
	}

	public void monteInterface(String s){
		super.monteInterface("MaqControl | Cadastro de função");		

		JPanel panel = new JPanel();
		JLabel label = new JLabel(" Função ");
		tfFuncao = new JTextField();

		panel.setBounds(0, 0, 545, 40);		
		panel.setLayout(new GridLayout(0, 1, 0, 0));

		panel.add(label);
		panel.add(tfFuncao);		

		P2.setBounds(0, 75, 545, 143);
		pBotoes.setBounds(0, 45, 545, 23);

		StatusBar.setForeground(new Color(153, 153, 153));
		StatusBar.setText("***MaqControl*** Cadastro de funções");

		getContentPane().setLayout(null);
		getContentPane().add(panel);

		setSize(550, 246);		
		setResizable(false);			
	}

	@Override
	public void habilitarCampos() {
		tfFuncao.setEnabled(true);		
	}

	@Override
	public void desabiltarCampos() {
		tfFuncao.setEnabled(false);		
	}

	@Override
	public void definirFoco() {
		tfFuncao.requestFocus();
	}

	@Override
	public void executarCommandExcluir() {
		new ExcluirCommand().execute(funcao);					
	}

	@Override
	public void atualizarGrade() {
		atualizarGrade("Funcao", Funcao.class);
	}

	@Override
	public void carregarValoresAlterar() {
		getFuncao().setDescricao(tfFuncao.getText());
	}

	@Override
	public void carregarValoresSalvar() {
		setFuncao(new Funcao(tfFuncao.getText()));	
	}

	@Override
	public void carregarValoresObjetos() {
		//nothing to do here
	}

	@Override
	public void executarCommandSalvar() {
		new SalvarCommand().execute(funcao);
	}

	@Override
	public void carregarValores() {		
		setFuncao((Funcao) lista.get(grade.getSelectedRow()));

		tfFuncao.setText(getFuncao().getDescricao());								
	}

	@Override
	public void adicionaListenerCampoVazio() {
		tfFuncao.addKeyListener(createKeyListenerCampoVazio(tfFuncao));		
	}

	@Override
	public boolean validarFlutuantes() {	
		return true;
	}

	@Override
	public boolean validarCombos() {
		return true;
	}

	@Override
	public void addDocument() {	}
}
