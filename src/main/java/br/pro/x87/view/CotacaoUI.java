package br.pro.x87.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import br.pro.x87.dao.CotacaoDAO;
import br.pro.x87.model.CotacaoProduto;
import br.pro.x87.model.Produto;
import br.pro.x87.validators.FloatFilter;
import br.pro.x87.view.command.ExcluirCommand;
import br.pro.x87.view.command.SalvarCommand;
import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.UtilDateModel;

public class CotacaoUI extends AncestorUI {

	private static final long serialVersionUID = 1L;

	private CotacaoProduto cotacao;
	private Produto produto;

	private List<Produto> listProduto;
	
	private JTextField tfValor;
	private UtilDateModel model;
	private JDatePanelImpl datePanel;
	private JDatePickerImpl dpData;
	private JComboBox<Object> comboProduto;

	public CotacaoProduto getCotacao() {
		return cotacao;
	}

	public void setCotacao(CotacaoProduto cotacao) {
		this.cotacao = cotacao;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public void popularProduto(){		
		comboProduto.removeAllItems();
		listProduto= getDao().get("Produto");

		for (Object o : listProduto) {	
			Produto p = (Produto) o;
			comboProduto.addItem(p.getDescricao());
		}
	}

	public CotacaoUI() {
		bListagem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {				
				List<CotacaoProduto> lista = new CotacaoDAO().imprimirCotacoes();								
				templateImprimir(lista, "cotacao.jrxml");
			}
		});		
		dpData.getModel().addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				try {
					Date data = (Date) dpData.getModel().getValue();
					dpData.getJFormattedTextField().setBackground(Color.WHITE);					
				} catch (Exception e) {

				}			
			}
		});

		popularProduto();
	}

	@Override
	public void monteInterface(String s) {
		super.monteInterface("MaqControl | Cadastro do valor de determinado produto");

		model = new UtilDateModel();
		datePanel = new JDatePanelImpl(model);
		dpData = new JDatePickerImpl(datePanel);

		JPanel panel = new JPanel();
		JLabel lblProduto = new JLabel("Produto");
		JLabel lblValor = new JLabel("Valor");
		comboProduto = new JComboBox<Object>();
		tfValor = new JTextField();
		JLabel lblData = new JLabel("Data");

		panel.setBounds(0, 0, 544, 40);
		panel.setLayout(new GridLayout(2, 3, 10, 0));

		tfValor.setColumns(10);

		dpData.setMaximumSize(new Dimension(32811, 18));
		dpData.setMinimumSize(new Dimension(48, 18));
		dpData.setPreferredSize(new Dimension(202, 18));
		dpData.getJFormattedTextField().setPreferredSize(new Dimension(175, 18));
		dpData.getJFormattedTextField().setMinimumSize(new Dimension(4, 18));		
		dpData.getJFormattedTextField().setBackground(Color.WHITE);
		dpData.setBorder(new LineBorder(new Color(0, 0, 0)));
		dpData.setShowYearButtons(true);
		dpData.getJFormattedTextField().setToolTipText("Clique para selecionar uma data");		

		panel.add(lblProduto);		
		panel.add(lblValor);		
		panel.add(lblData);		
		panel.add(comboProduto);		
		panel.add(tfValor);
		panel.add(dpData);		

		P2.setSize(544, 150);
		P2.setLocation(0, 75);

		pBotoes.setBounds(0, 46, 544, 23);
		
		getContentPane().setLayout(null);		
		getContentPane().add(panel);
		
		setResizable(false);		
		setSize(550,253);
	}

	@Override
	public void executarCommandExcluir() {
		new ExcluirCommand().execute(cotacao);
	}

	@Override
	public void executarCommandSalvar() {
		new SalvarCommand().execute(cotacao);
	}

	@Override
	public void atualizarGrade() {
		atualizarGrade("CotacaoProduto", CotacaoProduto.class);
	}

	@Override
	public void carregarValores() {
		setCotacao((CotacaoProduto) lista.get(grade.getSelectedRow()));

		tfValor.setText(getCotacao().getValorFormatado());
		comboProduto.setSelectedItem(getCotacao().getProduto().getDescricao());		
		carregarDataDateTimePicker(getCotacao().getData(), model);
	}

	@Override
	public void definirFoco() {
		comboProduto.requestFocus();
	}

	@Override
	public void habilitarCampos() {
		comboProduto.setSelectedIndex(-1);
		
		comboProduto.setEnabled(true);		
		tfValor.setEnabled(true);
		dpData.setEnabled(true);
	}

	@Override
	public void desabiltarCampos() {
		comboProduto.setEnabled(false);
		tfValor.setEnabled(false);
		dpData.setEnabled(false);
	}

	@Override
	public void carregarValoresAlterar() {
		getCotacao().setData((Date) dpData.getModel().getValue());
		getCotacao().setProduto(produto);
		
		try {
			getCotacao().setValor(converte(tfValor.getText()));
		} catch (ParseException e) {
			mensagemErro("Formato incorreto. Verifique o separador de decimal");
		}		
	}

	@Override
	public void carregarValoresSalvar() {
		Date data = (Date) dpData.getModel().getValue();
		try {
			setCotacao(new CotacaoProduto(data, getProduto(), converte(tfValor.getText())));
		} catch (ParseException e) {
			mensagemErro("Formato incorreto. Verifique o separador de decimal");
		}
	}

	@Override
	public void carregarValoresObjetos() {
		setProduto(listProduto.get(comboProduto.getSelectedIndex()));
	}

	@Override
	public void adicionaListenerCampoVazio() {
		tfValor.addKeyListener(createKeyListenerCampoVazio(tfValor));		
		dpData.addKeyListener(createKeyListenerCampoVazio(dpData));		
	}

	@Override
	public boolean validarFlutuantes() {
		if (!validaEsteFloat(tfValor, "VALOR"))
			return false;

		return true;
	}

	@Override
	public boolean validarCombos() {
		if (!validaEsteCombo(comboProduto, "PRODUTO"))
			return false;

		return true;
	}
	
	@Override
	public void addDocument() {
		tfValor.setDocument(new FloatFilter(FloatFilter.FLOAT));	
	}
}
