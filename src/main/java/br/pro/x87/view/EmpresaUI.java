package br.pro.x87.view;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import br.pro.x87.dao.BairroDAO;
import br.pro.x87.dao.CotacaoDAO;
import br.pro.x87.dao.EmpresaDAO;
import br.pro.x87.dao.EnderecoDAO;
import br.pro.x87.enums.TEstado;
import br.pro.x87.model.Bairro;
import br.pro.x87.model.Cidade;
import br.pro.x87.model.CotacaoProduto;
import br.pro.x87.model.Empresa;
import br.pro.x87.model.Endereco;
import br.pro.x87.view.command.ExcluirCommand;
import br.pro.x87.view.command.SalvarCommand;

public class EmpresaUI extends AncestorUI {

	private static final long serialVersionUID = 1L;

	/**
	 * Atributos de interface
	 */
	private JTextField tfRazaoSocial;
	private JTextField tfNomeFantasia;
	private JComboBox<String> comboEstado;
	private JComboBox<Object> comboCidade;
	private JComboBox<Object> comboBairro;
	private JComboBox<Object> comboEndereco;
	private JButton btnAdicionar;

	/**
	 * Listas
	 */
	private List<Cidade> listCidade;
	private List<Bairro> listBairro;
	private List<Object> listEndereco;

	/**
	 * Objetos
	 */
	private Empresa empresa;
	private TEstado estado;
	private Cidade cidade;
	private Bairro bairro;
	private Endereco endereco;
	private EnderecoUI enderecoUI;	
	private BairroDAO bairroDAO = new BairroDAO();

	/**
	 * Flag para verificação de formulário 
	 */
	private boolean formEndereco = false;
	private JPanel panel1;
	private JPanel panel2;


	/**
	 * Métodos GET/SET
	 */
	public boolean isFormEndereco() {
		return formEndereco;
	}

	public void setFormEndereco(boolean formEndereco) {
		this.formEndereco = formEndereco;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}	

	public TEstado getEstado() {
		return estado;
	}

	public void setEstado(TEstado estado) {
		this.estado = estado;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public Bairro getBairro() {
		return bairro;
	}

	public void setBairro(Bairro bairro) {
		this.bairro = bairro;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Object object) {
		this.endereco = (Endereco) object;
	}

	/**
	 * Construtor:
	 * apenas listeners
	 */
	public EmpresaUI() {
		bListagem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				List<Empresa> lista = new EmpresaDAO<>().imprimirEmpresas();								
				templateImprimir(lista, "empresa.jrxml");
			}
		});		
		comboEstado.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				popularCidade();
			}
		});

		comboCidade.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				popularBairro();;
			}
		});

		comboBairro.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				popularEndereco();
			}
		});

		btnAdicionar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				enderecoUI = new EnderecoUI();        
				enderecoUI.setVisible(true);
				enderecoUI.centraliza();
				setFormEndereco(true);
			}
		});	

		addWindowFocusListener(new WindowFocusListener() {
			public void windowGainedFocus(WindowEvent arg0) {
				try {
					if (!enderecoUI.isVisible() && isFormEndereco()){
						popularEndereco();
						setFormEndereco(false);
					}		
				} catch (Exception e) {
					// TODO: handle exception
				}

			}
			public void windowLostFocus(WindowEvent arg0) {
			}
		});

		popularEstado();
	}


	/**
	 * Montagem da interface:
	 * inicialização e localização dos componentes
	 */
	public void monteInterface(String s){
		super.monteInterface("MaqControl | Cadastro de empresas");

		panel1 = new JPanel();
		panel2 = new JPanel();
		JLabel lblNewLabel = new JLabel("Razão social");
		JLabel lblNewLabel_1 = new JLabel("Nome fantasia");
		JLabel lblEstado = new JLabel("Estado");
		JLabel lblNewLabel_2 = new JLabel("Cidade");
		JLabel lblBairro = new JLabel("Bairro");
		JLabel lblNewLabel_3 = new JLabel("Endereço");		
		tfRazaoSocial = new JTextField();
		tfNomeFantasia = new JTextField();		
		comboEstado = new JComboBox<>();
		comboCidade = new JComboBox<>();
		comboBairro = new JComboBox<>();
		comboEndereco = new JComboBox<>();		
		btnAdicionar = new JButton("Novo");

		tfNomeFantasia.setColumns(10);
		tfRazaoSocial.setColumns(10);

		panel1.setBounds(0, 0, 540, 40);
		panel1.setLayout(new GridLayout(0, 2, 10, 0));

		panel2.setBounds(1, 42, 541, 100);

		panel1.add(lblNewLabel);
		panel1.add(lblNewLabel_1);		
		panel1.add(tfRazaoSocial);	
		panel1.add(tfNomeFantasia);			

		GridBagLayout gbl_panel2 = new GridBagLayout();
		gbl_panel2.columnWidths = new int[]{60, 60, 60, 60, 60, 60, 60, 60, 60, 0};
		gbl_panel2.rowHeights = new int[]{15, 18, 15, 18, 0};
		gbl_panel2.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel2.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};

		panel2.setLayout(gbl_panel2);

		GridBagConstraints gbc_lblEstado = new GridBagConstraints();
		gbc_lblEstado.anchor = GridBagConstraints.SOUTH;
		gbc_lblEstado.gridwidth = 3;
		gbc_lblEstado.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblEstado.insets = new Insets(0, 0, 5, 5);
		gbc_lblEstado.gridx = 0;
		gbc_lblEstado.gridy = 0;

		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.anchor = GridBagConstraints.SOUTH;
		gbc_lblNewLabel_2.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblNewLabel_2.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_2.gridx = 3;
		gbc_lblNewLabel_2.gridy = 0;

		GridBagConstraints gbc_lblBairro = new GridBagConstraints();
		gbc_lblBairro.anchor = GridBagConstraints.SOUTH;
		gbc_lblBairro.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblBairro.insets = new Insets(0, 0, 5, 5);
		gbc_lblBairro.gridx = 6;
		gbc_lblBairro.gridy = 0;

		GridBagConstraints gbc_comboEstado = new GridBagConstraints();
		gbc_comboEstado.gridwidth = 3;
		gbc_comboEstado.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboEstado.insets = new Insets(0, 0, 5, 5);
		gbc_comboEstado.gridx = 0;
		gbc_comboEstado.gridy = 1;

		GridBagConstraints gbc_comboCidade = new GridBagConstraints();
		gbc_comboCidade.gridwidth = 3;
		gbc_comboCidade.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboCidade.insets = new Insets(0, 0, 5, 5);
		gbc_comboCidade.gridx = 3;
		gbc_comboCidade.gridy = 1;

		GridBagConstraints gbc_comboBairro = new GridBagConstraints();
		gbc_comboBairro.gridwidth = 3;
		gbc_comboBairro.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBairro.insets = new Insets(0, 0, 5, 0);
		gbc_comboBairro.gridx = 6;
		gbc_comboBairro.gridy = 1;

		GridBagConstraints gbc_lblNewLabel_3 = new GridBagConstraints();
		gbc_lblNewLabel_3.anchor = GridBagConstraints.SOUTH;
		gbc_lblNewLabel_3.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblNewLabel_3.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_3.gridx = 0;
		gbc_lblNewLabel_3.gridy = 2;

		GridBagConstraints gbc_comboEndereco = new GridBagConstraints();
		gbc_comboEndereco.gridwidth = 8;
		gbc_comboEndereco.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboEndereco.insets = new Insets(0, 0, 0, 5);
		gbc_comboEndereco.gridx = 0;
		gbc_comboEndereco.gridy = 3;


		GridBagConstraints gbc_btnAdicionar = new GridBagConstraints();
		gbc_btnAdicionar.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnAdicionar.gridx = 8;
		gbc_btnAdicionar.gridy = 3;

		panel2.add(lblEstado, gbc_lblEstado);		
		panel2.add(lblNewLabel_2, gbc_lblNewLabel_2);		
		panel2.add(lblBairro, gbc_lblBairro);		
		panel2.add(comboEstado, gbc_comboEstado);		
		panel2.add(comboCidade, gbc_comboCidade);		
		panel2.add(comboBairro, gbc_comboBairro);		
		panel2.add(lblNewLabel_3, gbc_lblNewLabel_3);		
		panel2.add(comboEndereco, gbc_comboEndereco);		
		panel2.add(btnAdicionar, gbc_btnAdicionar);		

		P2.setBounds(0, 179, 541, 150);
		pBotoes.setBounds(0, 150, 550, 23);

		getContentPane().add(panel1);
		getContentPane().add(panel2);

		setResizable(false);
		setBounds(100, 100, 546, 357);		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		getContentPane().setLayout(null);		
	}

	public void popularEstado(){
		for (TEstado o : TEstado.values()) {
			comboEstado.addItem(o.getNome());
		}
	}

	public void popularCidade(){
		if (comboEstado.getSelectedIndex() == -1){
			return;
		}
		comboCidade.removeAllItems();
		listCidade = getDao().filter("Cidade", "Estado", (String) comboEstado.getSelectedItem());

		for (Object o : listCidade) {	
			Cidade c = (Cidade) o;
			comboCidade.addItem(c.getNomeCidade());
		}
	}

	public void popularBairro(){
		if (comboCidade.getSelectedIndex() == -1){
			return;
		}
		comboBairro.removeAllItems();
		listBairro = bairroDAO.listBairros( listCidade.get(comboCidade.getSelectedIndex()).getNomeCidade());

		for (Object o : listBairro) {	
			Bairro b = (Bairro) o;
			comboBairro.addItem(b.getDescricao());
		}
	}

	public void popularEndereco(){
		if (comboBairro.getSelectedIndex() == -1){
			return;
		}
		comboEndereco.removeAllItems();
		listEndereco = new EnderecoDAO<>().listEndereco(listBairro.get(comboBairro.getSelectedIndex()).getDescricao());

		for (Object o : listEndereco) {
			Endereco e = (Endereco) o;
			comboEndereco.addItem(e.getNomeNumero());
		}
	}

	@Override
	public void executarCommandExcluir() {
		new ExcluirCommand().execute(empresa);
	}

	@Override
	public void executarCommandSalvar() {
		new SalvarCommand().execute(empresa);		
	}

	@Override
	public void atualizarGrade() {
		atualizarGrade("Empresa", Empresa.class);		
	}

	@Override
	public void carregarValores() {
		setEmpresa((Empresa) lista.get(grade.getSelectedRow()));	

		tfNomeFantasia.setText(getEmpresa().getNomeFantasia());
		tfRazaoSocial.setText(getEmpresa().getRazaoSocial());

		comboEstado.setSelectedItem(getEmpresa().getEndereco().getNomeEstado());
		comboCidade.setSelectedItem(getEmpresa().getEndereco().getNomeCidade());
		comboBairro.setSelectedItem(getEmpresa().getEndereco().getBairro().getDescricao());
		comboEndereco.setSelectedItem(getEmpresa().getEndereco().getNomeNumero());
	}

	@Override
	public void definirFoco() {
		tfRazaoSocial.requestFocus();
	}

	@Override
	public void habilitarCampos() {			
		tfRazaoSocial.setEnabled(true);
		tfNomeFantasia.setEnabled(true);
		comboEstado.setEnabled(true);
		comboCidade.setEnabled(true);
		comboBairro.setEnabled(true);
		comboEndereco.setEnabled(true);
		comboEstado.setSelectedIndex(-1);
		comboCidade.setSelectedIndex(-1);
		comboBairro.setSelectedIndex(-1);
		comboEndereco.setSelectedIndex(-1);
	}

	@Override
	public void desabiltarCampos() {
		tfRazaoSocial.setEnabled(false);
		tfNomeFantasia.setEnabled(false);
		comboEstado.setEnabled(false);
		comboCidade.setEnabled(false);
		comboBairro.setEnabled(false);
		comboEndereco.setEnabled(false);
	}

	@Override
	public void carregarValoresAlterar() {
		getEmpresa().setEndereco(endereco);
		getEmpresa().setNomeFantasia(tfNomeFantasia.getText());
		getEmpresa().setRazaoSocial(tfRazaoSocial.getText());		
	}

	@Override
	public void carregarValoresSalvar() {
		setEmpresa(new Empresa(endereco, tfNomeFantasia.getText(), tfRazaoSocial.getText()));
	}

	@Override
	public void carregarValoresObjetos() {
		setCidade(listCidade.get(comboCidade.getSelectedIndex()));
		setBairro(listBairro.get(comboBairro.getSelectedIndex()));
		setEndereco(listEndereco.get(comboEndereco.getSelectedIndex()));
	}

	@Override
	public void adicionaListenerCampoVazio() {
		tfNomeFantasia.addKeyListener(createKeyListenerCampoVazio(tfNomeFantasia));
		tfRazaoSocial.addKeyListener(createKeyListenerCampoVazio(tfRazaoSocial));
	}

	@Override
	public boolean validarFlutuantes() {
		return true;
	}

	@Override
	public boolean validarCombos() {

		if (!validaEsteCombo(comboBairro, "BAIRRO"))
			return false;

		if (!validaEsteCombo(comboCidade, "CIDADE"))
			return false;

		if (!validaEsteCombo(comboEndereco, "ENDEREÇO"))
			return false;

		if (!validaEsteCombo(comboEstado, "ESTADO"))
			return false;

		return true;
	}

	@Override
	public void addDocument() {	}

}
