package br.pro.x87.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import br.pro.x87.controller.GenericTableModel;
import br.pro.x87.model.Funcao;
import br.pro.x87.model.VinculoFuncao;
import br.pro.x87.model.Funcionario;
import br.pro.x87.model.Vinculo;
import br.pro.x87.validators.FloatFilter;
import br.pro.x87.view.command.ExcluirCommand;
import br.pro.x87.view.command.SalvarCommand;
import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.UtilDateModel;

public class VinculoFuncaoUI extends AncestorUI {

	private static final long serialVersionUID = 1L;

	private Vinculo vinculo;
	private Funcao funcao;
	private VinculoFuncao funcaoFuncionario;

	private List<Funcionario> listFuncionario;
	private List<Vinculo> listVinculo;
	private List<Funcao> listFuncao;	

	private JScrollPane scrollPane2;	
	private UtilDateModel modelInicio;
	private JDatePanelImpl datePanelInicio;
	private JDatePickerImpl dpDataInicio;
	private UtilDateModel modelTermino;
	private JDatePanelImpl datePanelTermino;
	private JDatePickerImpl dpDataTermino;
	private JTextField tfHoras;
	private JTextField tfSalario;
	private JTable gradeVinculo;
	private JComboBox<Object> comboFuncionario;
	private JComboBox<Object> comboFuncao;	
	private JPanel panel1;	
	private JPanel panel2;

	public VinculoFuncao getFuncaoFuncionario() {
		return funcaoFuncionario;
	}

	public void setFuncaoFuncionario(VinculoFuncao funcaoFuncionario) {
		this.funcaoFuncionario = funcaoFuncionario;
	}

	public Vinculo getVinculo() {
		return vinculo;
	}

	public void setVinculo(Vinculo vinculo) {
		this.vinculo = vinculo;
	}

	public Funcao getFuncao() {
		return funcao;
	}

	public void setFuncao(Funcao funcao) {
		this.funcao = funcao;
	}

	@Override
	public void monteInterface(String s) {	
		super.monteInterface("MaqControl | Cadastro de funções para um funcionário");

		panel1 = new JPanel();
		JPanel pVinculo = new JPanel();
		modelInicio = new UtilDateModel();
		modelTermino = new UtilDateModel();
		datePanelInicio = new JDatePanelImpl(modelInicio);		
		datePanelTermino = new JDatePanelImpl(modelTermino);
		JLabel lblFuncionrio = new JLabel("Funcionário");
		comboFuncionario = new JComboBox<>();

		GridBagLayout gbl_panel1 = new GridBagLayout();
		gbl_panel1.columnWidths = new int[]{545, 0};
		gbl_panel1.rowHeights = new int[]{0, 0, 100, 0};
		gbl_panel1.columnWeights = new double[]{0.0, Double.MIN_VALUE};
		gbl_panel1.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};		

		GridBagConstraints gbc_lblFuncionrio = new GridBagConstraints();
		gbc_lblFuncionrio.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblFuncionrio.insets = new Insets(0, 0, 5, 0);
		gbc_lblFuncionrio.gridx = 0;
		gbc_lblFuncionrio.gridy = 0;		

		GridBagConstraints gbc_comboFuncionario = new GridBagConstraints();
		gbc_comboFuncionario.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboFuncionario.insets = new Insets(0, 0, 5, 0);
		gbc_comboFuncionario.gridx = 0;
		gbc_comboFuncionario.gridy = 1;

		GridBagConstraints gbc_pVinculo = new GridBagConstraints();
		gbc_pVinculo.fill = GridBagConstraints.BOTH;
		gbc_pVinculo.gridx = 0;
		gbc_pVinculo.gridy = 2;

		panel1.setBounds(0, 0, 545, 150);		
		panel1.setLayout(gbl_panel1);
		panel1.add(lblFuncionrio, gbc_lblFuncionrio);
		panel1.add(comboFuncionario, gbc_comboFuncionario);
		panel1.add(pVinculo, gbc_pVinculo);

		pVinculo.setLayout(new BorderLayout(0, 0));
		pVinculo.setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder(" Selecione um vínculo para atribuir função "),
				BorderFactory.createEmptyBorder(5,5,5,5)));

		gradeVinculo = new JTable();
		gradeVinculo.setFocusable(false);
		gradeVinculo.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		gradeVinculo.setCursor(new Cursor(Cursor.HAND_CURSOR));
		gradeVinculo.setToolTipText("Clique aqui para carregar as informaçoes");

		scrollPane2 = new JScrollPane(gradeVinculo);
		pVinculo.add(scrollPane2, BorderLayout.CENTER);

		panel2 = new JPanel();
		JLabel lblFuno = new JLabel("Função");
		JLabel lblQuantidadeDeHoras = new JLabel("Carga horária");
		comboFuncao = new JComboBox<>();
		tfHoras = new JTextField();
		JLabel lblInicio = new JLabel("Inicio");
		JLabel lblTermino = new JLabel("Termino");
		JLabel lblSalrio = new JLabel("Salário");
		tfSalario = new JTextField();

		panel2.setBounds(0, 150, 545, 93);

		tfHoras.setColumns(10);
		tfHoras.addKeyListener(createKeyListenerCampoVazio(tfHoras));

		tfSalario.setColumns(10);

		GridBagLayout gbl_panel2 = new GridBagLayout();
		gbl_panel2.columnWidths = new int[]{215, 215, 110, 0};
		gbl_panel2.rowHeights = new int[]{0, 0, 0, 20, 0};
		gbl_panel2.columnWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel2.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};		

		GridBagConstraints gbc_lblFuno = new GridBagConstraints();
		gbc_lblFuno.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblFuno.insets = new Insets(0, 0, 5, 5);
		gbc_lblFuno.anchor = GridBagConstraints.NORTH;
		gbc_lblFuno.gridx = 0;
		gbc_lblFuno.gridy = 0;

		GridBagConstraints gbc_lblQuantidadeDeHoras = new GridBagConstraints();
		gbc_lblQuantidadeDeHoras.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblQuantidadeDeHoras.insets = new Insets(0, 0, 5, 0);
		gbc_lblQuantidadeDeHoras.gridx = 2;
		gbc_lblQuantidadeDeHoras.gridy = 0;		

		GridBagConstraints gbc_comboFuncao = new GridBagConstraints();
		gbc_comboFuncao.gridwidth = 2;
		gbc_comboFuncao.insets = new Insets(0, 0, 5, 5);
		gbc_comboFuncao.anchor = GridBagConstraints.NORTH;
		gbc_comboFuncao.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboFuncao.gridx = 0;
		gbc_comboFuncao.gridy = 1;		

		GridBagConstraints gbc_tfHoras = new GridBagConstraints();
		gbc_tfHoras.fill = GridBagConstraints.HORIZONTAL;
		gbc_tfHoras.insets = new Insets(0, 0, 5, 0);
		gbc_tfHoras.gridx = 2;
		gbc_tfHoras.gridy = 1;		

		GridBagConstraints gbc_lblInicio = new GridBagConstraints();
		gbc_lblInicio.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblInicio.insets = new Insets(0, 0, 5, 5);
		gbc_lblInicio.gridx = 0;
		gbc_lblInicio.gridy = 2;

		GridBagConstraints gbc_lblTermino = new GridBagConstraints();
		gbc_lblTermino.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblTermino.insets = new Insets(0, 0, 5, 5);
		gbc_lblTermino.gridx = 1;
		gbc_lblTermino.gridy = 2;		

		GridBagConstraints gbc_lblSalrio = new GridBagConstraints();
		gbc_lblSalrio.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblSalrio.insets = new Insets(0, 0, 5, 0);
		gbc_lblSalrio.gridx = 2;
		gbc_lblSalrio.gridy = 2;		

		dpDataInicio = new JDatePickerImpl(datePanelInicio);
		dpDataInicio.setShowYearButtons(true);
		dpDataInicio.getJFormattedTextField().setMinimumSize(new Dimension(4, 19));
		dpDataInicio.setPreferredSize(new Dimension(202, 20));
		dpDataInicio.getJFormattedTextField().setPreferredSize(new Dimension(178, 20));
		dpDataInicio.getJFormattedTextField().setBackground(Color.WHITE);
		GridBagConstraints gbc_dpDataInicio = new GridBagConstraints();
		gbc_dpDataInicio.fill = GridBagConstraints.HORIZONTAL;
		gbc_dpDataInicio.insets = new Insets(0, 0, 0, 5);
		gbc_dpDataInicio.gridx = 0;
		gbc_dpDataInicio.gridy = 3;		

		dpDataTermino = new JDatePickerImpl(datePanelTermino);
		dpDataTermino.setPreferredSize(new Dimension(202, 20));
		dpDataTermino.getJFormattedTextField().setPreferredSize(new Dimension(178, 20));
		dpDataTermino.getJFormattedTextField().setBackground(Color.WHITE);
		GridBagConstraints gbc_dpDataTermino = new GridBagConstraints();
		gbc_dpDataTermino.fill = GridBagConstraints.HORIZONTAL;
		gbc_dpDataTermino.insets = new Insets(0, 0, 0, 5);
		gbc_dpDataTermino.gridx = 1;
		gbc_dpDataTermino.gridy = 3;			

		GridBagConstraints gbc_tfSalario = new GridBagConstraints();
		gbc_tfSalario.fill = GridBagConstraints.HORIZONTAL;
		gbc_tfSalario.gridx = 2;
		gbc_tfSalario.gridy = 3;		

		panel2.setLayout(gbl_panel2);
		panel2.add(lblFuno, gbc_lblFuno);
		panel2.add(lblQuantidadeDeHoras, gbc_lblQuantidadeDeHoras);
		panel2.add(comboFuncao, gbc_comboFuncao);
		panel2.add(tfHoras, gbc_tfHoras);
		panel2.add(lblInicio, gbc_lblInicio);
		panel2.add(lblTermino, gbc_lblTermino);
		panel2.add(lblSalrio, gbc_lblSalrio);
		panel2.add(dpDataInicio, gbc_dpDataInicio);
		panel2.add(dpDataTermino, gbc_dpDataTermino);
		panel2.add(tfSalario, gbc_tfSalario);		

		P2.setLocation(0, 280);
		P2.setSize(545, 150);
		pBotoes.setSize(545, 23);
		pBotoes.setLocation(0, 250);		

		setResizable(false);
		setBounds(100, 100, 550, 459);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		getContentPane().setLayout(null);		
		getContentPane().add(panel1);
		getContentPane().add(panel2);
	}

	public void popularFuncao(){		
		comboFuncao.removeAllItems();
		listFuncao = getDao().get("Funcao");

		for (Object o : listFuncao) {	
			Funcao f = (Funcao) o;
			comboFuncao.addItem(f.getDescricao());
		}
	}

	public VinculoFuncaoUI() {
		comboFuncionario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (comboFuncionario.getSelectedIndex() != -1){
					gradeVinculo.setEnabled(true);
					Funcionario f = listFuncionario.get(comboFuncionario.getSelectedIndex());
					listVinculo = getDao().filter("Vinculo", "funcionario.nome", f.getNome());				
					setTable(new GenericTableModel(listVinculo, Vinculo.class));
					gradeVinculo.setModel(getTable());
					gradeVinculo.repaint();				
				} else {
					listVinculo = getDao().filter("Vinculo", "funcionario.nome", "Astolfo");				
					setTable(new GenericTableModel(listVinculo, Vinculo.class));
					gradeVinculo.setModel(getTable());
					gradeVinculo.repaint();
				}
			}
		});		

		dpDataInicio.getModel().addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				try {
					Date data = (Date) dpDataInicio.getModel().getValue();
					dpDataInicio.getJFormattedTextField().setBackground(Color.WHITE);					
				} catch (Exception e) {

				}		
			}
		});

		dpDataTermino.getModel().addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				try {
					Date data = (Date) dpDataTermino.getModel().getValue();
					dpDataTermino.getJFormattedTextField().setBackground(Color.WHITE);					
				} catch (Exception e) {

				}		
			}
		});

		popularFuncionario();
		popularFuncao();
	}

	public void popularFuncionario(){		
		comboFuncionario.removeAllItems();
		listFuncionario = getDao().get("Funcionario");

		for (Object o : listFuncionario) {	
			Funcionario f = (Funcionario) o;
			comboFuncionario.addItem(f.getNome());
		}
	}

	@Override
	public void executarCommandExcluir() {
		new ExcluirCommand().execute(funcaoFuncionario);
	}

	@Override
	public void executarCommandSalvar() {		
		new SalvarCommand().execute(funcaoFuncionario);
	}

	@Override
	public void atualizarGrade() {
		atualizarGrade("VinculoFuncao", VinculoFuncao.class);
	}

	@Override
	public void carregarValores() {
		setFuncaoFuncionario((VinculoFuncao) lista.get(grade.getSelectedRow()));

		comboFuncionario.setSelectedItem(getFuncaoFuncionario().getVinculo().getFuncionario().getNome());
		comboFuncao.setSelectedItem(getFuncaoFuncionario().getFuncao().getDescricao());
		

		carregarDataDateTimePicker(getFuncaoFuncionario().getInicio(), modelInicio);
		if (getFuncaoFuncionario().getFim() != null)
			carregarDataDateTimePicker(getFuncaoFuncionario().getFim(), modelTermino);
		
		tfHoras.setText(getFuncaoFuncionario().getQuantidadeHorasFormatado());		
		tfSalario.setText(getDf().format(getFuncaoFuncionario().getSalario()));
	}

	@Override
	public void definirFoco() {
		comboFuncionario.requestFocus();
	}

	@Override
	public void habilitarCampos() {
		comboFuncionario.setEnabled(true);
		gradeVinculo.setEnabled(true);
		comboFuncao.setEnabled(true);
		tfSalario.setEnabled(true);
		dpDataInicio.setEnabled(true);
		dpDataInicio.getJFormattedTextField().setEnabled(true);
		dpDataTermino.setEnabled(true);
		dpDataTermino.getJFormattedTextField().setEnabled(true);
		tfHoras.setEnabled(true);
		gradeVinculo.setEnabled(true);
	}

	@Override
	public void desabiltarCampos() {
		comboFuncionario.setEnabled(false);
		gradeVinculo.setEnabled(false);
		comboFuncao.setEnabled(false);
		tfSalario.setEnabled(false);
		dpDataInicio.setEnabled(false);
		dpDataTermino.setEnabled(false);
		tfHoras.setEnabled(false);
		gradeVinculo.setEnabled(false);
	}

	@Override
	public void carregarValoresAlterar() {
		getFuncaoFuncionario().setFim((Date) dpDataTermino.getModel().getValue());
		getFuncaoFuncionario().setInicio((Date) dpDataInicio.getModel().getValue());
		getFuncaoFuncionario().setVinculo(getVinculo());
		getFuncaoFuncionario().setFuncao(getFuncao());

		try {
			getFuncaoFuncionario().setQuantidadeHoras(converte(tfHoras.getText()));
			getFuncaoFuncionario().setSalario(converte(tfSalario.getText()));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	@Override
	public void carregarValoresSalvar() {		
		try {
			setFuncaoFuncionario(new VinculoFuncao(getFuncao(), getVinculo(), (Date) dpDataInicio.getModel().getValue(), (Date) dpDataTermino.getModel().getValue(), 
					converte(tfHoras.getText()), converte(tfSalario.getText())));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	@Override
	public void carregarValoresObjetos() {
		setFuncao(listFuncao.get(comboFuncao.getSelectedIndex()));
		setVinculo((Vinculo) listVinculo.get(gradeVinculo.getSelectedRow()) );
	}

	@Override
	public boolean templateValidar() {	
		if (gradeVinculo.getSelectedRow() == -1){
			JOptionPane.showMessageDialog(null, "Atenção: você deve selecionar um vínculo na grade", "Erro", JOptionPane.ERROR_MESSAGE);
			return false;
		}

		if (!validaEsteCombo(comboFuncionario, "FUNCIONÁRIO"))
			return false;

		if (!validaEsteCombo(comboFuncao, "FUNÇÃO"))
			return false;

		if (!validaEsteFloat(tfHoras, "NÚMERO DE HORAS"))
			return false;

		if (!validaEsteFloat(tfSalario, "SALÁRIO"))
			return false;

		Date dataAdmissao = null;		
		try {
			dataAdmissao = (Date) dpDataInicio.getModel().getValue();								
		} catch (Exception e) {
			return false;
		}			

		if (dataAdmissao == null)
			return false;	

		List<VinculoFuncao> listFuncaoFuncionario;
		listFuncaoFuncionario = getDao().get("VinculoFuncao");

		SimpleDateFormat formatoData = new SimpleDateFormat("yyyy-MM-dd");

		for (VinculoFuncao ff : listFuncaoFuncionario) {
			if (ff.getFuncao().getDescricao().equals(listFuncao.get(comboFuncao.getSelectedIndex()).getDescricao())				
					&& (ff.getVinculo().getFuncionario().getNome().equals(listFuncionario.get(comboFuncionario.getSelectedIndex()).getNome()))
					&& (ff.getVinculo().getNomeFantasia().equals(listVinculo.get(gradeVinculo.getSelectedRow()).getNomeFantasia()))
					&& (formatoData.format(ff.getInicio()).equals(formatoData.format((Date) dpDataInicio.getModel().getValue())))
					&& (formatoData.format(ff.getFim()).equals(formatoData.format((Date) dpDataTermino.getModel().getValue())))){
				JOptionPane.showMessageDialog(null, "Atenção: já existe um registro para esse funcionário, empresa, função e período ", "Erro", JOptionPane.ERROR_MESSAGE);										
				return false;
			}							
		}

		Date inicio = (Date) dpDataInicio.getModel().getValue();
		Date termino = (Date) dpDataTermino.getModel().getValue();

		if (termino == null){
			if (JOptionPane.showConfirmDialog(null,
					"Não foi informado o término da função. Esta é a função atual do funcionário?",
					"Confirmar período",
					JOptionPane.YES_NO_OPTION) == 1){
				return false;
			} else
				return true;
		}

		if (termino.before(inicio)){			
			JOptionPane.showMessageDialog(null, "Atenção: a data de término da função é inferior a data de início ", "Erro", JOptionPane.ERROR_MESSAGE);
			dpDataInicio.requestFocus();					
			return false;
		}		

		return true;
	}

	@Override
	public void adicionaListenerCampoVazio() {
		tfSalario.addKeyListener(createKeyListenerCampoVazio(tfSalario));
		tfHoras.addKeyListener(createKeyListenerCampoVazio(tfHoras));
	}

	@Override
	public boolean validarFlutuantes() {
		if (!validaEsteFloat(tfSalario, "SALÁRIO"))
			return false;

		if (!validaEsteFloat(tfHoras, "HORAS"))
			return false;

		return true;
	}

	@Override
	public boolean validarCombos() {		
		if (!validaEsteCombo(comboFuncao, "FUNÇÃO"))
			return false;

		if (!validaEsteCombo(comboFuncionario, "FUNCIONÁRIO"))
			return false;	

		return true;
	}

	@Override
	public void addDocument() {
		tfSalario.setDocument(new FloatFilter(FloatFilter.FLOAT));
		tfHoras.setDocument(new FloatFilter(FloatFilter.FLOAT));	
	}
}
