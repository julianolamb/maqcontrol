/**
 * 
 */
package br.pro.x87.view;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import br.pro.x87.model.TipoOperacao;
import br.pro.x87.view.command.ExcluirCommand;
import br.pro.x87.view.command.SalvarCommand;

/**
 * @author JULIANO
 *
 */
public class TipoOperacaoUI extends AncestorUI {

	private static final long serialVersionUID = 1L;

	private JTextField tfTipoOperacao;
	private TipoOperacao tipoOperacao;	

	public TipoOperacao getTipoOperacao() {
		return tipoOperacao;
	}

	public void setTipoOperacao(TipoOperacao tipoOperacao) {
		this.tipoOperacao = tipoOperacao;
	}

	public TipoOperacaoUI() {
		
	}

	public void monteInterface(String s){
		super.monteInterface("MaqControl | Cadastro do tipo de operação");

		JPanel panel = new JPanel();
		JLabel label = new JLabel(" Descrição ");
		tfTipoOperacao = new JTextField();

		panel.setBounds(0, 0, 545, 40);		
		panel.setLayout(new GridLayout(2, 1, 0, 0));

		panel.add(label);		
		panel.add(tfTipoOperacao);

		P2.setBounds(0, 75, 545, 150);
		pBotoes.setBounds(0, 45, 545, 23);

		StatusBar.setForeground(new Color(153, 153, 153));
		StatusBar.setText("***MaqControl*** Cadastro do tipo de operação");

		getContentPane().setLayout(null);
		getContentPane().add(panel);

		setResizable(false);	
		setSize(550, 254);
	}

	@Override
	public boolean templateValidar() {		
		if (!super.templateValidar()){
			return false;
		}
		return true;	
	}

	@Override
	public void executarCommandExcluir() {
		new ExcluirCommand().execute(tipoOperacao);
	}

	@Override
	public void executarCommandSalvar() {
		new SalvarCommand().execute(tipoOperacao);
	}

	@Override
	public void atualizarGrade() {
		atualizarGrade("TipoOperacao", TipoOperacao.class);
	}

	@Override
	public void carregarValores() {
		setTipoOperacao((TipoOperacao) lista.get(grade.getSelectedRow()) );
		tfTipoOperacao.setText(getTipoOperacao().getDescricao());			
	}

	@Override
	public void definirFoco() {
		tfTipoOperacao.requestFocus();
	}

	@Override
	public void habilitarCampos() {
		tfTipoOperacao.setEnabled(true);
	}

	@Override
	public void desabiltarCampos() {
		tfTipoOperacao.setEnabled(false);		
	}

	@Override
	public void carregarValoresAlterar() {
		getTipoOperacao().setDescricao(tfTipoOperacao.getText());
	}

	@Override
	public void carregarValoresSalvar() {
		setTipoOperacao(new TipoOperacao(tfTipoOperacao.getText()));
	}

	@Override
	public void carregarValoresObjetos() {
		//nothing to do here
	}

	@Override
	public void adicionaListenerCampoVazio() {
		tfTipoOperacao.addKeyListener(createKeyListenerCampoVazio(tfTipoOperacao));
	}
	
	@Override
	public boolean validarFlutuantes() {
		return true;
	}
	
	@Override
	public boolean validarCombos() { 
		return true;
	}

	@Override
	public void addDocument() {}
}
