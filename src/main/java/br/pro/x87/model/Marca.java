package br.pro.x87.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * *********************************************************************
 * Module: Marca.java Author: Juliano Purpose: Defines the Class Marca
 * *********************************************************************
 */
import br.pro.x87.controller.Titulo;

@Entity
@Table(name = "marca", catalog = "maqcontrol")
public class Marca implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	//@GeneratedValue(strategy = IDENTITY)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@Column(name = "descricao", length = 50)
	private String descricao;

	@Titulo(nome = "Código", numeroDaColuna = 0)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {        
		this.id = id;
	}

	public Marca() {

	}

	public Marca(String descricao) {
		this.descricao = descricao;
	}

	@Titulo(nome = "Descrição", numeroDaColuna = 1)
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Marca other = (Marca) obj;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}


}
