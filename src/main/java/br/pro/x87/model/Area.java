/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pro.x87.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.pro.x87.controller.Titulo;

/**
 *
 * @author Juliano Rodrifo Lamb
 */
@Entity
@Table(name = "area", catalog = "maqcontrol")
public class Area implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "id_empresa", foreignKey = @ForeignKey(name = "FK_AREA_EMPRESA"))
	public Empresa empresa;

	@Column(name = "descricao", length = 60)
	private String descricao;

	@Column(name = "tamanho")
	private float tamanho;

	public Area() {
	}

	
	
	public Area(String descricao, float tamanho, Empresa e) {
		this.descricao = descricao;
		this.tamanho = tamanho;
		this.empresa = e;
	}

	@Titulo(nome = "Código", numeroDaColuna = 0)
	public Long getId() {
		return id;
	}
		
	public void setId(Long id) {
		this.id = id;
	}	

	@Titulo(nome = "Empresa", numeroDaColuna = 3)
	public String getNomeFantasia() {
		return empresa.getNomeFantasia();
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	@Titulo(nome = "Área", numeroDaColuna = 1)
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public float getTamanho() {
		return tamanho;
	}
	
	@Titulo(nome = "Tamanho", numeroDaColuna = 2)
	public String getTamanhoFormatado(){
		NumberFormat nf = NumberFormat.getNumberInstance(new Locale("pt", "BR"));
		((DecimalFormat)nf).applyPattern("###,###,##0.00");
		return nf.format(getTamanho());
	}

	public void setTamanho(float tamanho) {
		this.tamanho = tamanho;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result + ((empresa == null) ? 0 : empresa.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + Float.floatToIntBits(tamanho);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Area other = (Area) obj;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (empresa == null) {
			if (other.empresa != null)
				return false;
		} else if (!empresa.equals(other.empresa))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (Float.floatToIntBits(tamanho) != Float.floatToIntBits(other.tamanho))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Area [id=" + id + ", empresa=" + empresa + ", descricao=" + descricao + ", tamanho=" + tamanho + "]";
	}
	
	

}
