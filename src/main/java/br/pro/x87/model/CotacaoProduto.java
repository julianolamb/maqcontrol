package br.pro.x87.model;

/**
 * *********************************************************************
 * Module: CotacaoProduto.java Author: Juliano Purpose: Defines the Class
 * CotacaoProduto
 * *********************************************************************
 */
import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import br.pro.x87.controller.Titulo;

@Entity
@Table(name = "CotacaoProduto", catalog = "maqcontrol")
public class CotacaoProduto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "data", length = 50)
	@Type(type = "date")
	private Date data;

	@ManyToOne
	@JoinColumn(name = "id_produto", foreignKey = @ForeignKey(name = "FK_COTACAO_PRODUTO"))
	private Produto produto;

	@Column(name = "valor", length = 50)
	private Float valor;	

	@Titulo(nome = "Data", numeroDaColuna = 1)
	public String getDataFormatada(){
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		return formato.format(getData());
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}


	@Titulo(nome = "Produto", numeroDaColuna = 0)
	public String getDescricao() {
		return produto.getDescricao();
	}

	public Float getValor() {
		return valor;
	}

	@Titulo(nome = "Valor", numeroDaColuna = 2)
	public String getValorFormatado() {
		NumberFormat nf = NumberFormat.getNumberInstance(new Locale("pt", "BR"));
		((DecimalFormat)nf).applyPattern("###,###,##0.00");
		return nf.format(getValor());
	}


	public void setValor(Float valor) {
		this.valor = valor;
	}

	public CotacaoProduto() {
		// TODO: implement
	}

	public CotacaoProduto(Date data, Produto produto, Float valor) {
		this.data = data;
		this.produto = produto;
		this.valor = valor;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		result = prime * result + ((produto == null) ? 0 : produto.hashCode());
		result = prime * result + ((valor == null) ? 0 : valor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CotacaoProduto other = (CotacaoProduto) obj;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		if (produto == null) {
			if (other.produto != null)
				return false;
		} else if (!produto.equals(other.produto))
			return false;
		if (valor == null) {
			if (other.valor != null)
				return false;
		} else if (!valor.equals(other.valor))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CotacaoProduto [data=" + data + ", produto=" + produto + ", valor=" + valor + "]";
	}


}
