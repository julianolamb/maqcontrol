/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pro.x87.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import br.pro.x87.controller.Titulo;

/**
 *
 * @author jrlamb
 */
@Entity
@Table(name = "vinculo", catalog = "maqcontrol")
public class Vinculo implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;
	
	@JoinColumn(name = "id_funcionario", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_VINCULO_FUNCIONARIO"))
	@ManyToOne(optional = false, targetEntity = Funcionario.class)
    private Funcionario funcionario;
    
    @JoinColumn(name = "id_empresa", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_VINCULO_EMPRESA"))
    @ManyToOne(optional = false, targetEntity = Empresa.class)
    private Empresa empresa;
   
//    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
//    private List<FuncaoFuncionario> FuncaoFuncionario = new ArrayList<>();
    
    //fetch lazy
    @OneToMany(mappedBy = "vinculo", cascade = CascadeType.ALL, orphanRemoval = true, targetEntity = VinculoFuncao.class)
    private List<Vinculo> vinculo = new ArrayList<>();
    

    @Type(type = "date")
    @Column(name = "admissao")
    private Date admissao;

    @Type(type = "date")
    @Column(name = "demissao")
    private Date demissao;

    @Column(name = "status", length = 12)
    private String status;

    public Vinculo() {
		
	}    
    
    public Vinculo(Funcionario funcionario, Empresa empresa, Date admissao, String status) {		
		this.funcionario = funcionario;
		this.empresa = empresa;		
		this.admissao = admissao;		
		this.status = status;
	}
    
    

	public Vinculo(Funcionario funcionario, Empresa empresa, Date admissao, Date demissao,
			String status) {
		this.funcionario = funcionario;
		this.empresa = empresa;
		this.admissao = admissao;
		this.demissao = demissao;
		this.status = status;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}
	
	@Titulo(nome = "Funcionário", numeroDaColuna = 0)
	public String getNome() {
		return funcionario.getNome();
	}

	@Titulo(nome = "Empresa", numeroDaColuna = 1)
	public String getNomeFantasia() {
		return empresa.getNomeFantasia();
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public Date getAdmissao() {
		return admissao;
	}
	
	@Titulo(nome = "Admissão", numeroDaColuna = 2)
	public String getAdmissaoFormatado(){
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		return formato.format(getAdmissao());
	}

	public void setAdmissao(Date admissao) {
		this.admissao = admissao;
	}
	
	public Date getDemissao() {
		return demissao;
	}
	
	@Titulo(nome = "Demissão", numeroDaColuna = 3)
	public String getDemissaoFormatado(){
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		return formato.format(getDemissao());
	}

	public void setDemissao(Date demissao) {
		this.demissao = demissao;
	}
	
	@Titulo(nome = "Status", numeroDaColuna = 4)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((admissao == null) ? 0 : admissao.hashCode());
		result = prime * result + ((demissao == null) ? 0 : demissao.hashCode());
		result = prime * result + ((empresa == null) ? 0 : empresa.hashCode());
		result = prime * result + ((funcionario == null) ? 0 : funcionario.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vinculo other = (Vinculo) obj;
		if (admissao == null) {
			if (other.admissao != null)
				return false;
		} else if (!admissao.equals(other.admissao))
			return false;
		if (demissao == null) {
			if (other.demissao != null)
				return false;
		} else if (!demissao.equals(other.demissao))
			return false;
		if (empresa == null) {
			if (other.empresa != null)
				return false;
		} else if (!empresa.equals(other.empresa))
			return false;
		if (funcionario == null) {
			if (other.funcionario != null)
				return false;
		} else if (!funcionario.equals(other.funcionario))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Vinculo [funcionario=" + funcionario + ", empresa=" + empresa + ", admissao=" + admissao + ", demissao="
				+ demissao + ", status=" + status + "]";
	}
    
    
    
    
}
