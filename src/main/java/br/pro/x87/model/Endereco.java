/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pro.x87.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.pro.x87.controller.Titulo;

/**
 *
 * @author JulianoRodrigo
 */
@Entity
@Table(name = "endereco", catalog = "maqcontrol")
public class Endereco implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "id_bairro", foreignKey = @ForeignKey(name = "FK_ENDERECO_BAIRRO"))
	private Bairro bairro;

	@Column(name = "descricao", length = 80)
	private String descricao;

	@Column(name = "numero")
	private int numero;

	public Endereco(Bairro bairro, String descricao, int numero) {
		this.bairro = bairro;
		this.descricao = descricao;
		this.numero = numero;
	}

	public Endereco(Long id, Bairro bairro, String descricao, int numero) {
		this.id = id;
		this.bairro = bairro;
		this.descricao = descricao;
		this.numero = numero;
	}

	public Endereco() {

	}

	@Titulo(nome = "Código", numeroDaColuna = 0)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Bairro getBairro() {
		return bairro;
	}

	public void setBairro(Bairro bairro) {
		this.bairro = bairro;
	}

	@Titulo(nome = "Logradouro", numeroDaColuna = 1)
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Titulo(nome = "Numero", numeroDaColuna = 2)
	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	@Titulo(nome = "Cidade", numeroDaColuna = 3)
	public String getNomeCidade() {
		return bairro.getNomeCidade();
	}

	@Titulo(nome = "Bairro", numeroDaColuna = 4)
	public String getDescricaoBairro(){
		return bairro.getDescricao();		
	}

	public String getNomeEstado() {
		return bairro.getNomeEstado();
	}

	public String getNomeNumero() {
		return descricao + ", " + numero;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bairro == null) ? 0 : bairro.hashCode());
		result = prime * result + ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + numero;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Endereco other = (Endereco) obj;
		if (bairro == null) {
			if (other.bairro != null)
				return false;
		} else if (!bairro.equals(other.bairro))
			return false;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (numero != other.numero)
			return false;
		return true;
	}		

	@Override
	public String toString() {
		return descricao + ", " + numero;
	}
}
