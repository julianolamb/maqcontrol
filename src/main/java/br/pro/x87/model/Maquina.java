package br.pro.x87.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import br.pro.x87.controller.Titulo;
import br.pro.x87.despesas.Alojamento;
import br.pro.x87.despesas.CalcularDespesas;
import br.pro.x87.despesas.Juro;
import br.pro.x87.despesas.Seguro;

@Entity
@Table(name = "maquina", catalog = "maqcontrol")
public class Maquina implements Serializable{


	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@Column(name = "descricao", length = 50)
	private String descricao;

	@ManyToOne
	@JoinColumn(name = "id_marca", foreignKey = @ForeignKey(name = "FK_MARCA_MAQUINA"))
	public Marca marca;

	@Column(name = "ano")
	private int ano;

	@Column(name = "modelo", length=60)
	private String modelo;

	@Column(name = "numeroSerie", length=60)
	private String numeroSerie;

	@Type(type = "date")
	@Column(name = "dataAquisicao")    
	private Date dataAquisicao;

	@Column(name = "numPatrimonio")
	private long numPatrimonio;

	@Column(name = "horimetro")
	private float horimetro;

	@Column(name = "potencia")
	private float potencia;

	@Column(name = "tipoPotencia", length=2)
	private String tipoPotencia;

	@Column(name = "peso")
	private float peso;

	@Column(name = "eficiencia")
	private float eficiencia;

	@Column(name = "velocidadeMaxima")
	private float velocidadeMaxima;

	@Column(name = "vidaUtil")
	private int vidaUtil;

	@Column(name = "tempoUso")
	private float tempoUso;

	@Column(name = "consumoEspecifico")
	private float consumoEspecifico;

	@Column(name = "largura")
	private float largura;

	@Column(name = "valorAquisicao")
	private float valorAquisicao;

	@Column(name = "valorResidual")
	private float valorResidual;

	@Column(name = "valorSucata")
	private float valorSucata;

	@Column(name = "taxaJuros")
	private float taxaJuros;

	@Column(name = "valorJuros")
	private float valorJuros;

	@Column(name = "taxaAlojamento")
	private float taxaAlojamento;

	@Column(name = "valorAlojamento")
	private float valorAlojamento;

	@Column(name = "taxaSeguros")
	private float taxaSeguros;

	@Column(name = "valorSeguros")
	private float valorSeguros;	

	@Column(name = "nrPneusDianteiro")
	private int nrPneusDianteiro;

	@Column(name = "nrPneusTraseiro")
	private int nrPneusTraseiro;

	@Column(name = "capacidadeCarter")
	private float capacidadeCarter;

	@Column(name = "capacidadeTransmissao")
	private float capacidadeTransmissao;

	@Column(name = "capacidadeHidraulico")
	private float capacidadeHidraulico;

	@Column(name = "nrFiltrosDiesel")
	private int nrFiltrosDiesel;

	@Column(name = "nrFiltrosLubrificante")
	private int nrFiltrosLubrificante;

	@Column(name = "nrFiltrosArSeguranca")
	private int nrFiltrosArSeguranca;

	@Column(name = "nrFiltrosArFiltrante")
	private int nrFiltrosArFiltrante;

	@Column(name = "nrFiltrosHidraulico")
	private int nrFiltrosHidraulico;

	@Column(name = "tipoDepreciacao")
	private String tipoDepreciacao = "Quotas";
	
	@Column(name = "depreciacaoHoraria")
	private float depreciacaoHoraria;
	
	@Column(name = "tipoMaquina", length = 60)

	@ManyToMany(mappedBy = "listaMaquina", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
	private List<Operacao> listaOperacao = new ArrayList<>();

	private String tipoMaquina;	

	public Maquina(String descricao, Marca marca, String modelo, String tipoMaquina) {
		this.descricao = descricao;
		this.marca = marca;
		this.modelo = modelo;
		this.tipoMaquina = tipoMaquina;
	}

	public Maquina() {
	
	}	
	
	public float getDepreciacaoHoraria() {
		return depreciacaoHoraria;
	}

	public void setDepreciacaoHoraria(float depreciacaoHoraria) {
		this.depreciacaoHoraria = depreciacaoHoraria;
	}

	public String getTipoDepreciacao() {
		return tipoDepreciacao;
	}

	public void setTipoDepreciacao(String tipoDepreciacao) {
		this.tipoDepreciacao = tipoDepreciacao;
	}

	@Titulo(nome = "Código", numeroDaColuna = 0)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Titulo(nome = "Descrição", numeroDaColuna = 1)
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Marca getMarca() {
		return marca;
	}

	public void setMarca(Marca marca) {
		this.marca = marca;
	}

	public int getAno() {
		return ano;
	}

	public void setAno(int ano) {
		this.ano = ano;
	}

	@Titulo(nome = "Modelo", numeroDaColuna = 2)
	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getNumeroSerie() {
		return numeroSerie;
	}

	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}

	public Date getDataAquisicao() {
		return dataAquisicao;
	}

	public void setDataAquisicao(Date dataAquisicao) {
		this.dataAquisicao = dataAquisicao;
	}

	public long getNumPatrimonio() {
		return numPatrimonio;
	}

	public void setNumPatrimonio(long numPatrimonio) {
		this.numPatrimonio = numPatrimonio;
	}

	public float getHorimetro() {
		return horimetro;
	}
	
	public String getHorimetroFormatado(){
		NumberFormat nf = NumberFormat.getNumberInstance(new Locale("pt", "BR"));
		((DecimalFormat)nf).applyPattern("###,###,##0.00");
		return nf.format(getHorimetro());
	}

	public void setHorimetro(float horimetro) {
		this.horimetro = horimetro;
	}

	public float getPotencia() {
		return potencia;
	}
	
	public String getPotenciaFormatado(){
		NumberFormat nf = NumberFormat.getNumberInstance(new Locale("pt", "BR"));
		((DecimalFormat)nf).applyPattern("###,###,##0.00");
		return nf.format(getPotencia());
	}

	public void setPotencia(float potencia) {
		this.potencia = potencia;
	}

	public String getTipoPotencia() {
		return tipoPotencia;
	}

	public void setTipoPotencia(String tipoPotencia) {
		this.tipoPotencia = tipoPotencia;
	}

	public float getPeso() {
		return peso;
	}

	public String getPesoFormatado(){
		NumberFormat nf = NumberFormat.getNumberInstance(new Locale("pt", "BR"));
		((DecimalFormat)nf).applyPattern("###,###,##0.00");
		return nf.format(getPeso());
	}
	
	public void setPeso(float peso) {
		this.peso = peso;
	}

	public float getEficiencia() {
		return eficiencia;
	}
	
	public String getEficienciaFormatado(){
		NumberFormat nf = NumberFormat.getNumberInstance(new Locale("pt", "BR"));
		((DecimalFormat)nf).applyPattern("###,###,##0.00");
		return nf.format(getEficiencia());
	}

	public void setEficiencia(float eficiencia) {
		this.eficiencia = eficiencia;
	}

	public float getVelocidadeMaxima() {
		return velocidadeMaxima;
	}
	
	public String getVelocidadeFormatado(){
		NumberFormat nf = NumberFormat.getNumberInstance(new Locale("pt", "BR"));
		((DecimalFormat)nf).applyPattern("###,###,##0.00");
		return nf.format(getVelocidadeMaxima());
	}

	public void setVelocidadeMaxima(float velocidadeMaxima) {
		this.velocidadeMaxima = velocidadeMaxima;
	}

	public int getVidaUtil() {
		return vidaUtil;
	}

	public void setVidaUtil(int vidaUtil) {
		this.vidaUtil = vidaUtil;
	}

	public float getTempoUso() {
		return tempoUso;
	}
	
	public String getTempoUsoFormatado(){
		NumberFormat nf = NumberFormat.getNumberInstance(new Locale("pt", "BR"));
		((DecimalFormat)nf).applyPattern("###,###,##0.00");
		return nf.format(getTempoUso());
	}

	public void setTempoUso(float tempoUso) {
		this.tempoUso = tempoUso;
	}

	public float getConsumoEspecifico() {
		return consumoEspecifico;
	}
	
	public String getConsumoEspFormatado(){
		NumberFormat nf = NumberFormat.getNumberInstance(new Locale("pt", "BR"));
		((DecimalFormat)nf).applyPattern("###,###,##0.00");
		return nf.format(getConsumoEspecifico());
	}

	public void setConsumoEspecifico(float consumoEspecifico) {
		this.consumoEspecifico = consumoEspecifico;
	}

	public float getLargura() {
		return largura;
	}
	
	public String getLarguraFormatado(){
		NumberFormat nf = NumberFormat.getNumberInstance(new Locale("pt", "BR"));
		((DecimalFormat)nf).applyPattern("###,###,##0.00");
		return nf.format(getLargura());
	}

	public void setLargura(float largura) {
		this.largura = largura;
	}

	public float getValorAquisicao() {
		return valorAquisicao;
	}

	public String getValorAquisicaoFormatado(){
		NumberFormat nf = NumberFormat.getNumberInstance(new Locale("pt", "BR"));
		((DecimalFormat)nf).applyPattern("###,###,##0.00");
		return nf.format(getValorAquisicao());
	}
	
	public void setValorAquisicao(float valorAquisicao) {
		this.valorAquisicao = valorAquisicao;
	}

	public float getValorResidual() {
		return valorResidual;
	}

	public String getValorResidualFormatado(){
		NumberFormat nf = NumberFormat.getNumberInstance(new Locale("pt", "BR"));
		((DecimalFormat)nf).applyPattern("###,###,##0.00");
		return nf.format(getValorResidual());
	}
	
	public void setValorResidual(float valorResidual) {
		this.valorResidual = valorResidual;
	}

	public float getValorSucata() {
		return valorSucata;
	}
	
	public String getValorSucataFormatado(){
		NumberFormat nf = NumberFormat.getNumberInstance(new Locale("pt", "BR"));
		((DecimalFormat)nf).applyPattern("###,###,##0.00");
		return nf.format(getValorSucata());
	}

	public void setValorSucata(float valorSucata) {
		this.valorSucata = valorSucata;
	}

	public float getTaxaJuros() {
		return taxaJuros;
	}

	public String getTaxaJurosFormatado(){
		NumberFormat nf = NumberFormat.getNumberInstance(new Locale("pt", "BR"));
		((DecimalFormat)nf).applyPattern("###,###,##0.00");
		return nf.format(getTaxaJuros());
	}
	
	public void setTaxaJuros(float taxaJuros) {
		this.taxaJuros = taxaJuros;
	}

	public float getValorJuros() {
		return valorJuros;
	}
	
	public String getValorJurosFormatado(){
		NumberFormat nf = NumberFormat.getNumberInstance(new Locale("pt", "BR"));
		((DecimalFormat)nf).applyPattern("###,###,##0.00");
		return nf.format(getValorJuros());
	}

	public void setValorJuros(float valorJuros) {
		this.valorJuros = valorJuros;
	}

	public float getTaxaAlojamento() {
		return taxaAlojamento;
	}
	
	public String getTaxaAlojamentoFormatado(){
		NumberFormat nf = NumberFormat.getNumberInstance(new Locale("pt", "BR"));
		((DecimalFormat)nf).applyPattern("###,###,##0.00");
		return nf.format(getTaxaAlojamento());
	}

	public void setTaxaAlojamento(float taxaAlojamento) {
		this.taxaAlojamento = taxaAlojamento;
	}

	public float getValorAlojamento() {
		return valorAlojamento;
	}
	
	public String getValorAlojamentoFormatado(){
		NumberFormat nf = NumberFormat.getNumberInstance(new Locale("pt", "BR"));
		((DecimalFormat)nf).applyPattern("###,###,##0.00");
		return nf.format(getValorAlojamento());
	}

	public void setValorAlojamento(float valorAlojamento) {
		this.valorAlojamento = valorAlojamento;
	}

	public float getTaxaSeguros() {
		return taxaSeguros;
	}
	
	public String getTaxaSegurosFormatado(){
		NumberFormat nf = NumberFormat.getNumberInstance(new Locale("pt", "BR"));
		((DecimalFormat)nf).applyPattern("###,###,##0.00");
		return nf.format(getTaxaSeguros());
	}

	public void setTaxaSeguros(float taxaSeguros) {
		this.taxaSeguros = taxaSeguros;
	}

	public float getValorSeguros() {
		return valorSeguros;
	}

	public String getValorSegurosFormatado(){
		NumberFormat nf = NumberFormat.getNumberInstance(new Locale("pt", "BR"));
		((DecimalFormat)nf).applyPattern("###,###,##0.00");
		return nf.format(getValorSeguros());
	}
	
	public void setValorSeguros(float valorSeguros) {
		this.valorSeguros = valorSeguros;
	}

	public int getNrPneusDianteiro() {
		return nrPneusDianteiro;
	}

	public void setNrPneusDianteiro(int nrPneusDianteiro) {
		this.nrPneusDianteiro = nrPneusDianteiro;
	}

	public int getNrPneusTraseiro() {
		return nrPneusTraseiro;
	}

	public void setNrPneusTraseiro(int nrPneusTraseiro) {
		this.nrPneusTraseiro = nrPneusTraseiro;
	}

	public float getCapacidadeCarter() {
		return capacidadeCarter;
	}

	public String getCapacidadeCarterFormatado(Double value){
		NumberFormat nf = NumberFormat.getNumberInstance(new Locale("pt", "BR"));
		((DecimalFormat)nf).applyPattern("###,###,##0.00");
		return nf.format(getCapacidadeCarter());
	}
	
	public void setCapacidadeCarter(float capacidadeCarter) {
		this.capacidadeCarter = capacidadeCarter;
	}

	public float getCapacidadeTransmissao() {
		return capacidadeTransmissao;
	}
	
	public String getCapacidadeTransmissaoFormatado(Double value){
		NumberFormat nf = NumberFormat.getNumberInstance(new Locale("pt", "BR"));
		((DecimalFormat)nf).applyPattern("###,###,##0.00");
		return nf.format(getCapacidadeTransmissao());
	}

	public void setCapacidadeTransmissao(float capacidadeTransmissao) {
		this.capacidadeTransmissao = capacidadeTransmissao;
	}

	public float getCapacidadeHidraulico() {
		return capacidadeHidraulico;
	}
	
	public String getCapacidadeHidraulicoFormatado(Double value){
		NumberFormat nf = NumberFormat.getNumberInstance(new Locale("pt", "BR"));
		((DecimalFormat)nf).applyPattern("###,###,##0.00");
		return nf.format(getCapacidadeHidraulico());
	}
	
	public void setCapacidadeHidraulico(float capacidadeHidraulico) {
		this.capacidadeHidraulico = capacidadeHidraulico;
	}

	public int getNrFiltrosDiesel() {
		return nrFiltrosDiesel;
	}

	public void setNrFiltrosDiesel(int nrFiltrosDiesel) {
		this.nrFiltrosDiesel = nrFiltrosDiesel;
	}

	public int getNrFiltrosLubrificante() {
		return nrFiltrosLubrificante;
	}

	public void setNrFiltrosLubrificante(int nrFiltrosLubrificante) {
		this.nrFiltrosLubrificante = nrFiltrosLubrificante;
	}

	public int getNrFiltrosArSeguranca() {
		return nrFiltrosArSeguranca;
	}

	public void setNrFiltrosArSeguranca(int nrFiltrosArSeguranca) {
		this.nrFiltrosArSeguranca = nrFiltrosArSeguranca;
	}

	public int getNrFiltrosArFiltrante() {
		return nrFiltrosArFiltrante;
	}

	public void setNrFiltrosArFiltrante(int nrFiltrosArFiltrante) {
		this.nrFiltrosArFiltrante = nrFiltrosArFiltrante;
	}

	public int getNrFiltrosHidraulico() {
		return nrFiltrosHidraulico;
	}

	public void setNrFiltrosHidraulico(int nrFiltrosHidraulico) {
		this.nrFiltrosHidraulico = nrFiltrosHidraulico;
	}

	public String getTipoMaquina() {
		return tipoMaquina;
	}

	public void setTipoMaquina(String tipoMaquina) {
		this.tipoMaquina = tipoMaquina;
	}	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ano;
		result = prime * result + Float.floatToIntBits(capacidadeCarter);
		result = prime * result + Float.floatToIntBits(capacidadeHidraulico);
		result = prime * result + Float.floatToIntBits(capacidadeTransmissao);
		result = prime * result + Float.floatToIntBits(consumoEspecifico);
		result = prime * result + ((dataAquisicao == null) ? 0 : dataAquisicao.hashCode());
		result = prime * result + ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result + Float.floatToIntBits(eficiencia);
		result = prime * result + Float.floatToIntBits(horimetro);
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + Float.floatToIntBits(largura);
		result = prime * result + ((listaOperacao == null) ? 0 : listaOperacao.hashCode());
		result = prime * result + ((marca == null) ? 0 : marca.hashCode());
		result = prime * result + ((modelo == null) ? 0 : modelo.hashCode());
		result = prime * result + nrFiltrosArFiltrante;
		result = prime * result + nrFiltrosArSeguranca;
		result = prime * result + nrFiltrosDiesel;
		result = prime * result + nrFiltrosHidraulico;
		result = prime * result + nrFiltrosLubrificante;
		result = prime * result + nrPneusDianteiro;
		result = prime * result + nrPneusTraseiro;
		result = prime * result + (int) (numPatrimonio ^ (numPatrimonio >>> 32));
		result = prime * result + ((numeroSerie == null) ? 0 : numeroSerie.hashCode());
		result = prime * result + Float.floatToIntBits(peso);
		result = prime * result + Float.floatToIntBits(potencia);
		result = prime * result + Float.floatToIntBits(taxaAlojamento);
		result = prime * result + Float.floatToIntBits(taxaJuros);
		result = prime * result + Float.floatToIntBits(taxaSeguros);
		result = prime * result + Float.floatToIntBits(tempoUso);
		result = prime * result + ((tipoMaquina == null) ? 0 : tipoMaquina.hashCode());
		result = prime * result + ((tipoPotencia == null) ? 0 : tipoPotencia.hashCode());
		result = prime * result + Float.floatToIntBits(valorAlojamento);
		result = prime * result + Float.floatToIntBits(valorAquisicao);
		result = prime * result + Float.floatToIntBits(valorJuros);
		result = prime * result + Float.floatToIntBits(valorResidual);
		result = prime * result + Float.floatToIntBits(valorSeguros);
		result = prime * result + Float.floatToIntBits(valorSucata);
		result = prime * result + Float.floatToIntBits(velocidadeMaxima);
		result = prime * result + vidaUtil;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Maquina other = (Maquina) obj;
		if (ano != other.ano)
			return false;
		if (Float.floatToIntBits(capacidadeCarter) != Float.floatToIntBits(other.capacidadeCarter))
			return false;
		if (Float.floatToIntBits(capacidadeHidraulico) != Float.floatToIntBits(other.capacidadeHidraulico))
			return false;
		if (Float.floatToIntBits(capacidadeTransmissao) != Float.floatToIntBits(other.capacidadeTransmissao))
			return false;
		if (Float.floatToIntBits(consumoEspecifico) != Float.floatToIntBits(other.consumoEspecifico))
			return false;
		if (dataAquisicao == null) {
			if (other.dataAquisicao != null)
				return false;
		} else if (!dataAquisicao.equals(other.dataAquisicao))
			return false;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (Float.floatToIntBits(eficiencia) != Float.floatToIntBits(other.eficiencia))
			return false;
		if (Float.floatToIntBits(horimetro) != Float.floatToIntBits(other.horimetro))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (Float.floatToIntBits(largura) != Float.floatToIntBits(other.largura))
			return false;
		if (listaOperacao == null) {
			if (other.listaOperacao != null)
				return false;
		} else if (!listaOperacao.equals(other.listaOperacao))
			return false;
		if (marca == null) {
			if (other.marca != null)
				return false;
		} else if (!marca.equals(other.marca))
			return false;
		if (modelo == null) {
			if (other.modelo != null)
				return false;
		} else if (!modelo.equals(other.modelo))
			return false;
		if (nrFiltrosArFiltrante != other.nrFiltrosArFiltrante)
			return false;
		if (nrFiltrosArSeguranca != other.nrFiltrosArSeguranca)
			return false;
		if (nrFiltrosDiesel != other.nrFiltrosDiesel)
			return false;
		if (nrFiltrosHidraulico != other.nrFiltrosHidraulico)
			return false;
		if (nrFiltrosLubrificante != other.nrFiltrosLubrificante)
			return false;
		if (nrPneusDianteiro != other.nrPneusDianteiro)
			return false;
		if (nrPneusTraseiro != other.nrPneusTraseiro)
			return false;
		if (numPatrimonio != other.numPatrimonio)
			return false;
		if (numeroSerie == null) {
			if (other.numeroSerie != null)
				return false;
		} else if (!numeroSerie.equals(other.numeroSerie))
			return false;
		if (Float.floatToIntBits(peso) != Float.floatToIntBits(other.peso))
			return false;
		if (Float.floatToIntBits(potencia) != Float.floatToIntBits(other.potencia))
			return false;
		if (Float.floatToIntBits(taxaAlojamento) != Float.floatToIntBits(other.taxaAlojamento))
			return false;
		if (Float.floatToIntBits(taxaJuros) != Float.floatToIntBits(other.taxaJuros))
			return false;
		if (Float.floatToIntBits(taxaSeguros) != Float.floatToIntBits(other.taxaSeguros))
			return false;
		if (Float.floatToIntBits(tempoUso) != Float.floatToIntBits(other.tempoUso))
			return false;
		if (tipoMaquina == null) {
			if (other.tipoMaquina != null)
				return false;
		} else if (!tipoMaquina.equals(other.tipoMaquina))
			return false;
		if (tipoPotencia == null) {
			if (other.tipoPotencia != null)
				return false;
		} else if (!tipoPotencia.equals(other.tipoPotencia))
			return false;
		if (Float.floatToIntBits(valorAlojamento) != Float.floatToIntBits(other.valorAlojamento))
			return false;
		if (Float.floatToIntBits(valorAquisicao) != Float.floatToIntBits(other.valorAquisicao))
			return false;
		if (Float.floatToIntBits(valorJuros) != Float.floatToIntBits(other.valorJuros))
			return false;
		if (Float.floatToIntBits(valorResidual) != Float.floatToIntBits(other.valorResidual))
			return false;
		if (Float.floatToIntBits(valorSeguros) != Float.floatToIntBits(other.valorSeguros))
			return false;
		if (Float.floatToIntBits(valorSucata) != Float.floatToIntBits(other.valorSucata))
			return false;
		if (Float.floatToIntBits(velocidadeMaxima) != Float.floatToIntBits(other.velocidadeMaxima))
			return false;
		if (vidaUtil != other.vidaUtil)
			return false;
		return true;
	}

	public float calcularJuro(){
		CalcularDespesas c = new Juro();
		return c.calcular(this);
	}

	public float calcularSeguro(){
		CalcularDespesas c = new Seguro();
		return c.calcular(this);
	}

	public float calcularAlojamento(){
		CalcularDespesas c = new Alojamento();
		return c.calcular(this);
	}

	public boolean isFill(){
		if ((getValorAquisicao() == 0)
				|| (getTempoUso() == 0)
				|| (getValorResidual() == 0)
				|| (getVidaUtil() == 0)){
			return false;
		}
		return true;
	}

	public static Maquina maiorTempoUso(List<Maquina> m){
		float tempoUso = 0;
		Maquina aux = null;
		for (Maquina maquina : m) {
			if (maquina.getTempoUso() > tempoUso){
				tempoUso = maquina.getTempoUso();
				aux = maquina;
			}
		}
		return aux;
	}

	public static Maquina maiorConsumo(List<Maquina> m){
		/*
		 * O método retorna a maior maquina, pois é necessário identificar
		 * se a potencia é em CV ou KW, bem como necessário obter a potencia
		 */
		float consumo = 0;
		Maquina aux = null;
		for (Maquina maquina : m) {
			if (maquina.getConsumoEspecifico() > consumo){
				consumo = maquina.getConsumoEspecifico();
				aux = maquina;
			}
		}
		return aux;
	}

	public static float calcularManutencao(List<Maquina> m, float trm){
		float soma = 0;		
		float eme = 0;		
		float taxa = trm/100;

		for (Maquina maquina : m) {
			eme = (trm * maquina.getValorAquisicao()) / maquina.getTempoUso();
			soma += eme;
		}

		return soma/100;
	}

	public static float verificaQuantidade(String item, List<Maquina> m){		
		/* 
		 * Por alguma razão na primeira versão do software, quando o produto é 
		 * Pneu Traseiro, ele divide por 2. Nesta função é mantido o retorno da quantidade
		 * real. Se tem 2 pneus traseiros, retorna 2.
		 */

		float quantidade = 0;

		for (Maquina maquina : m) {
			switch (item) {
			case "Filtro de ar (Filtrante)":
				quantidade += maquina.getNrFiltrosArFiltrante();
				break;
			case "Filtro de ar (Segurança)":
				quantidade += maquina.getNrFiltrosArSeguranca();
				break;
			case "Filtro de diesel":
				quantidade += maquina.getNrFiltrosDiesel();
				break;
			case "Filtro do lubrificante do motor":
				quantidade += maquina.getNrFiltrosLubrificante();
				break;
			case "Filtro do sistema hidráulico":
				quantidade += maquina.getNrFiltrosHidraulico();
				break;
			case "Óleo da transmissão":
				quantidade += maquina.getCapacidadeTransmissao();
				break;
			case "Óleo hidráulico":
				quantidade += maquina.getCapacidadeHidraulico();
				break;
			case "Óleo do motor":
				quantidade += maquina.getCapacidadeCarter();
				break;
			case "Pneu dianteiro":
				quantidade += maquina.getNrPneusDianteiro();
				break;
			case "Pneu traseiro":
				quantidade += maquina.getNrPneusTraseiro();
				break;			
			}
		}
		return quantidade;
	}

	public static float maiorLarguraOperacional(List<Maquina> l){
		float maior = 0;		
		
		for (Maquina m : l) {
			if (m.getLargura() > maior){
				maior = m.getLargura();				
			}
		}		
		return maior;	
	}

	@Override
	public String toString() {
		return "Maquina [id=" + id + ", descricao=" + descricao +"]";
	}
	
	
}
