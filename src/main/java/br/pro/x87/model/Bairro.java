/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pro.x87.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.pro.x87.controller.Titulo;

/**
 *
 * @author JulianoRodrigo
 */
@Entity
@Table(name = "bairro", catalog = "maqcontrol")
public class Bairro implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "id_cidade", foreignKey = @ForeignKey(name = "FK_BAIRRO_CIDADE"))    
	public Cidade cidade;

	@Column(name = "descricao", length = 50)
	private String descricao;

	@Column(name = "cep", length = 9)
	private String cep;

	public Bairro() {
	}

	public Bairro(Cidade cidade, String descricao, String cep) {
		this.cidade = cidade;
		this.descricao = descricao;
		this.cep = cep;
	}	

	public Bairro(Long id, Cidade cidade, String descricao, String cep) {
		this.id = id;
		this.cidade = cidade;
		this.descricao = descricao;
		this.cep = cep;
	}

	@Titulo(nome = "Código", numeroDaColuna = 0)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Titulo(nome = "Cidade", numeroDaColuna = 3)
	public String getNomeCidade(){
		return cidade.getNomeCidade();
	}

	@Titulo(nome = "Estado", numeroDaColuna = 4)
	public String getNomeEstado() {
		return cidade.getNomeEstado();
	}
		
	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	@Titulo(nome = "Descrição", numeroDaColuna = 1)
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Titulo(nome = "CEP", numeroDaColuna = 2)
	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cep == null) ? 0 : cep.hashCode());
		result = prime * result + ((cidade == null) ? 0 : cidade.hashCode());
		result = prime * result + ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bairro other = (Bairro) obj;
		if (cep == null) {
			if (other.cep != null)
				return false;
		} else if (!cep.equals(other.cep))
			return false;
		if (cidade == null) {
			if (other.cidade != null)
				return false;
		} else if (!cidade.equals(other.cidade))
			return false;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Bairro [id=" + id + ", cidade=" + cidade + ", descricao=" + descricao + ", cep=" + cep + "]";
	}



}
