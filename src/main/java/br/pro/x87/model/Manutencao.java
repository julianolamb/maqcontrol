/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pro.x87.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import br.pro.x87.controller.Titulo;

/**
 *
 * @author Juliano Rodrifo Lamb
 */
@Entity
@Table(name = "manutencao", catalog = "maqcontrol")
public class Manutencao implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "idManutencao", unique = true, nullable = false)
	private int id;

	@OneToMany(mappedBy = "manutencao", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Servicos> servicoManutencao = new ArrayList<>();

	@ManyToOne
	@JoinColumn(name = "id_operacao", foreignKey = @ForeignKey(name = "FK_MANUTENCAO_OPERACAO"))
	private Operacao operacao;

	@ManyToOne
	@JoinColumn(name = "id_maquina", foreignKey = @ForeignKey(name = "FK_MANUTENCAO_MAQUINA"))
	public Maquina maquina;

	@ManyToOne
	@JoinColumn(name = "id_funcionario", foreignKey = @ForeignKey(name = "FK_MANUTENCAO_FUNCIONARIO"))
	public Funcionario funcionario;

	@Type(type = "date")    
	@Column(name = "incio")
	private Date inicio;

	@Type(type = "date")
	@Column(name = "fim")
	private Date fim;

	@Column(name = "horaMaquina")
	private float horaMaquina;

	@Column(name = "observacao", length = 100)
	private String observacao;

	/*
	 * Construtor default, requerido pelo hibernate
	 */
	public Manutencao() {

	}

	/*
	 * Construtor para manutenção de rotina
	 */
	public Manutencao(Maquina maquina, Funcionario funcionario, Date inicio, Date fim, float horaMaquina,
			String observacao) {		
		this.maquina = maquina;
		this.funcionario = funcionario;
		this.inicio = inicio;
		this.fim = fim;
		this.horaMaquina = horaMaquina;
		this.observacao = observacao;
	}

	/*
	 * Construtor para manutenção corretiva
	 */
	public Manutencao(Operacao operacao, Maquina maquina, Funcionario funcionario, Date inicio, Date fim,
			float horaMaquina, String observacao) {		
		this.operacao = operacao;
		this.maquina = maquina;
		this.funcionario = funcionario;
		this.inicio = inicio;
		this.fim = fim;
		this.horaMaquina = horaMaquina;
		this.observacao = observacao;
	}

	@Titulo(nome = "Código", numeroDaColuna = 0)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<Servicos> getServicoManutencao() {
		return servicoManutencao;
	}

	public void setServicoManutencao(List<Servicos> servicoManutencao) {
		this.servicoManutencao = servicoManutencao;
	}

	public Operacao getOperacao() {
		return operacao;
	}

	public void setOperacao(Operacao operacao) {
		this.operacao = operacao;
	}

	public Maquina getMaquina() {
		return maquina;
	}

	public void setMaquina(Maquina maquina) {
		this.maquina = maquina;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public Date getInicio() {
		return inicio;
	}

	@Titulo(nome = "Início", numeroDaColuna = 1)
	public String getInicioFormatado(){
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		return formato.format(getInicio());
	}

	public void setInicio(Date inicio) {
		this.inicio = inicio;
	}

	public Date getFim() {
		return fim;
	}

	@Titulo(nome = "Fim", numeroDaColuna = 2)
	public String getFimFormatado(){
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		return formato.format(getFim());
	}

	public void setFim(Date fim) {
		this.fim = fim;
	}

	public float getHoraMaquina() {
		return horaMaquina;
	}

	public String getHoraMaquinaFormatado() {
		NumberFormat nf = NumberFormat.getNumberInstance(new Locale("pt", "BR"));
		((DecimalFormat)nf).applyPattern("###,###,##0.00");
		return nf.format(getHoraMaquina());		
	}

	public void setHoraMaquina(float horaMaquina) {
		this.horaMaquina = horaMaquina;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}	

	@Titulo(nome = "Operação", numeroDaColuna = 5)
	public String getTipoOperacao() {
		return operacao.getTipoOperacao().getDescricao();
	}

	@Titulo(nome = "Máquina", numeroDaColuna = 3)
	public String getDescricaoMaquina() {
		return maquina.getDescricao();
	}

	@Titulo(nome = "Funcionário", numeroDaColuna = 4)
	public String getNomeFuncionario() {
		return funcionario.getNome();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fim == null) ? 0 : fim.hashCode());
		result = prime * result + ((funcionario == null) ? 0 : funcionario.hashCode());
		result = prime * result + Float.floatToIntBits(horaMaquina);
		result = prime * result + id;
		result = prime * result + ((inicio == null) ? 0 : inicio.hashCode());
		result = prime * result + ((maquina == null) ? 0 : maquina.hashCode());
		result = prime * result + ((observacao == null) ? 0 : observacao.hashCode());
		result = prime * result + ((operacao == null) ? 0 : operacao.hashCode());
		result = prime * result + ((servicoManutencao == null) ? 0 : servicoManutencao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Manutencao other = (Manutencao) obj;
		if (fim == null) {
			if (other.fim != null)
				return false;
		} else if (!fim.equals(other.fim))
			return false;
		if (funcionario == null) {
			if (other.funcionario != null)
				return false;
		} else if (!funcionario.equals(other.funcionario))
			return false;
		if (Float.floatToIntBits(horaMaquina) != Float.floatToIntBits(other.horaMaquina))
			return false;
		if (id != other.id)
			return false;
		if (inicio == null) {
			if (other.inicio != null)
				return false;
		} else if (!inicio.equals(other.inicio))
			return false;
		if (maquina == null) {
			if (other.maquina != null)
				return false;
		} else if (!maquina.equals(other.maquina))
			return false;
		if (observacao == null) {
			if (other.observacao != null)
				return false;
		} else if (!observacao.equals(other.observacao))
			return false;
		if (operacao == null) {
			if (other.operacao != null)
				return false;
		} else if (!operacao.equals(other.operacao))
			return false;
		if (servicoManutencao == null) {
			if (other.servicoManutencao != null)
				return false;
		} else if (!servicoManutencao.equals(other.servicoManutencao))
			return false;
		return true;
	}
}
