/**
 * 
 */
package br.pro.x87.model;

/**
 * @author jrlamb
 *
 */
public class EstimativaCustosFixos {
	
	private String descricao;
	private float juros;
	private float seguro;
	private float alojamento;
	private float depreciacaoHoraria;
	
	public EstimativaCustosFixos() {
		// TODO Auto-generated constructor stub
	}

	public EstimativaCustosFixos(String descricao, float juros, float seguro, float alojamento, float depreciacaoHoraria) {
		super();
		this.descricao = descricao;
		this.juros = juros;
		this.seguro = seguro;
		this.alojamento = alojamento;
		this.depreciacaoHoraria = depreciacaoHoraria;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public float getJuros() {
		return juros;
	}

	public void setJuros(float juros) {
		this.juros = juros;
	}

	public float getSeguro() {
		return seguro;
	}

	public void setSeguro(float seguro) {
		this.seguro = seguro;
	}

	public float getAlojamento() {
		return alojamento;
	}

	public void setAlojamento(float alojamento) {
		this.alojamento = alojamento;
	}

	public float getDepreciacaoHoraria() {
		return depreciacaoHoraria;
	}

	public void setDepreciacaoHoraria(float depreciacaoHoraria) {
		this.depreciacaoHoraria = depreciacaoHoraria;
	}

	@Override
	public String toString() {
		return "EstimativaCustosFixos [descricao=" + descricao + ", juros=" + juros + ", seguro=" + seguro
				+ ", alojamento=" + alojamento + ", depreciacaoHoraria=" + depreciacaoHoraria + "]";
	}

	
	
}
