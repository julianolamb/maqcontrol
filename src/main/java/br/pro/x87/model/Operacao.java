/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pro.x87.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import br.pro.x87.controller.Titulo;

/**
 *
 * @author Juliano Rodrigo Lamb
 */

@Entity
@Table(name = "operacao", catalog = "maqcontrol")
public class Operacao implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "idOperacao", unique = true, nullable = false)
	private long id;

	@ManyToOne
	@JoinColumn(name = "id_area", foreignKey = @ForeignKey(name = "FK_AREA_OPERACAO"))
	public Area area;

	@ManyToOne
	@JoinColumn(name = "id_funcao", foreignKey = @ForeignKey(name = "FK_FUNCAO_OPERACAO"))
	public VinculoFuncao vinculoFuncao;

	@ManyToOne
	@JoinColumn(name = "id_toperacao", foreignKey = @ForeignKey(name = "FK_TOPERACAO_OPERACAO"))
	public TipoOperacao tipoOperacao;

	@Column(name = "tamanhoArea")
	private float tamanhoArea;

	@Type(type = "date")
	@Column(name = "data")
	private Date data; 

	@Column(name = "observacao", length = 60)
	private String observao;

	@Column(name = "duracao")
	private float duracao;

	@Column(name = "largura")
	private float largura;

	@Column(name = "eficiencia")
	private float eficiencia;

	@Column(name = "velocidade")
	private float velocidade;

	@Column(name = "horimetro")
	private float horimetro;

	@Column(name = "combustivel")
	private float combustivel;

	@Column(name = "graxa")
	private float graxa;
	
	/*
	 * Add CCO para funcionar em relestimativa. Não foram verificados outros impactos
	 */
	
	@Column(name = "cco")
	private float cco;

	@OneToMany(mappedBy = "operacao", targetEntity = Manutencao.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Manutencao> listaManutencao = new ArrayList<>();

	@ManyToMany()
	@JoinTable(
			name = "OperacaoMaquina", 
			joinColumns = { @JoinColumn(name = "operacao_id") }, 
			inverseJoinColumns = { @JoinColumn(name = "maquina_id") }
			)    
	private List<Maquina> listaMaquina = new ArrayList<Maquina>();	

	public List<Maquina> getListaMaquina() {
		return listaMaquina;
	}

	public void setListaMaquina(List<Maquina> listaMaquina) {
		this.listaMaquina = listaMaquina;
	}	
	
	

	// --------------- construtor -----------------------
	
	
	public Operacao() {

	}	

	public List<Manutencao> getListaManutencao() {
		return listaManutencao;
	}

	public void setListaManutencao(List<Manutencao> listaManutencao) {
		this.listaManutencao = listaManutencao;
	}

	public Operacao(Area area, TipoOperacao tipoOperacao, float tamanhoArea, Date data,
			String observao, float duracao, float horimetro, float combustivel, float graxa, float velocidade, 
			VinculoFuncao v) {		
		this.area = area;		
		this.tipoOperacao = tipoOperacao;
		this.tamanhoArea = tamanhoArea;
		this.data = data;
		this.observao = observao;
		this.duracao = duracao;
		this.horimetro = horimetro;
		this.combustivel = combustivel;
		this.graxa = graxa;
		this.listaMaquina = new ArrayList<>();
		this.velocidade = velocidade;
		this.vinculoFuncao = v;
	}

	// --------------- getters and setters  -----------------------


	public int getTamanhoListaMaquina(){
		return listaMaquina.size();
	}

	public VinculoFuncao getVinculoFuncao() {
		return vinculoFuncao;
	}

	public void setVinculoFuncao(VinculoFuncao vinculoFuncao) {
		this.vinculoFuncao = vinculoFuncao;
	}

	public List<Maquina> getMaquina() {		
		return listaMaquina;
	}	

	public TipoOperacao getTipoOperacao() {
		return tipoOperacao;
	}

	public void setTipoOperacao(TipoOperacao tipoOperacao) {
		this.tipoOperacao = tipoOperacao;
	}

	public float getTamanhoArea() {
		return tamanhoArea;
	}

	public String getTamanhoAreaFormatado(){
		NumberFormat nf = NumberFormat.getNumberInstance(new Locale("pt", "BR"));
		((DecimalFormat)nf).applyPattern("###,###,##0.00");
		return nf.format(getTamanhoArea());
	}

	public void setTamanhoArea(float tamanhoArea) {
		this.tamanhoArea = tamanhoArea;
	}

	public String getObservao() {
		return observao;
	}

	public void setObservao(String observao) {
		this.observao = observao;
	}

	public float getDuracao() {
		return duracao;
	}

	public String getDuracaoFormatado(){
		NumberFormat nf = NumberFormat.getNumberInstance(new Locale("pt", "BR"));
		((DecimalFormat)nf).applyPattern("###,###,##0.00");
		return nf.format(getDuracao());
	}

	public void setDuracao(float duracao) {
		this.duracao = duracao;
	}

	public float getLargura() {
		return largura;
	}

	public String getLarguraFormatado(){
		NumberFormat nf = NumberFormat.getNumberInstance(new Locale("pt", "BR"));
		((DecimalFormat)nf).applyPattern("###,###,##0.00");
		return nf.format(getLargura());
	}

	public void setLargura(float largura) {
		this.largura = largura;
	}

	public void setEficiencia(float eficiencia) {
		this.eficiencia = eficiencia;
	}

	public float getVelocidade() {
		return velocidade;
	}

	public String getVelocidadeFormatado(){
		NumberFormat nf = NumberFormat.getNumberInstance(new Locale("pt", "BR"));
		((DecimalFormat)nf).applyPattern("###,###,##0.00");
		return nf.format(getVelocidade());
	}

	public void setVelocidade(float velocidade) {
		this.velocidade = velocidade;
	}

	public float getHorimetro() {
		return horimetro;
	}

	public String getHorimetroFormatado(){
		NumberFormat nf = NumberFormat.getNumberInstance(new Locale("pt", "BR"));
		((DecimalFormat)nf).applyPattern("###,###,##0.00");
		return nf.format(getHorimetro());
	}

	public void setHorimetro(float horimetro) {
		this.horimetro = horimetro;
	}

	public float getCombustivel() {
		return combustivel;
	}

	public String getCombustivelFormatado(){
		NumberFormat nf = NumberFormat.getNumberInstance(new Locale("pt", "BR"));
		((DecimalFormat)nf).applyPattern("###,###,##0.00");
		return nf.format(getCombustivel());
	}	

	public void setCombustivel(float combustivel) {
		this.combustivel = combustivel;
	}

	public float getGraxa() {
		return graxa;
	}

	public String getGraxaFormatado(){
		NumberFormat nf = NumberFormat.getNumberInstance(new Locale("pt", "BR"));
		((DecimalFormat)nf).applyPattern("###,###,##0.00");
		return nf.format(getGraxa());
	}

	public void setGraxa(float graxa) {
		this.graxa = graxa;
	}

	@Titulo(nome = "Código", numeroDaColuna = 0)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	@Titulo(nome = "Área", numeroDaColuna = 1)
	public String getNomeArea() {
		return area.getDescricao();
	}


	@Titulo(nome = "Funcionário", numeroDaColuna = 2)
	public String getNomeFuncionario() {
		return vinculoFuncao.getVinculo().getFuncionario().getNome();
	}
	
	public Date getData() {
		return data;
	}
	
	@Titulo(nome = "Data", numeroDaColuna = 3)
	public String getDataFormatado(){
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		return formato.format(getData());
	}

	public void setData(Date data) {
		this.data = data;
	}	

	public String getDescricaoOperacao() {
		return tipoOperacao.getDescricao();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((area == null) ? 0 : area.hashCode());
		result = prime * result + Float.floatToIntBits(cco);
		result = prime * result + Float.floatToIntBits(combustivel);
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		result = prime * result + Float.floatToIntBits(duracao);
		result = prime * result + Float.floatToIntBits(eficiencia);
		result = prime * result + Float.floatToIntBits(graxa);
		result = prime * result + Float.floatToIntBits(horimetro);
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + Float.floatToIntBits(largura);
		result = prime * result + ((listaManutencao == null) ? 0 : listaManutencao.hashCode());
		result = prime * result + ((listaMaquina == null) ? 0 : listaMaquina.hashCode());
		result = prime * result + ((observao == null) ? 0 : observao.hashCode());
		result = prime * result + Float.floatToIntBits(tamanhoArea);
		result = prime * result + ((tipoOperacao == null) ? 0 : tipoOperacao.hashCode());
		result = prime * result + Float.floatToIntBits(velocidade);
		result = prime * result + ((vinculoFuncao == null) ? 0 : vinculoFuncao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Operacao other = (Operacao) obj;
		if (area == null) {
			if (other.area != null)
				return false;
		} else if (!area.equals(other.area))
			return false;
		if (Float.floatToIntBits(cco) != Float.floatToIntBits(other.cco))
			return false;
		if (Float.floatToIntBits(combustivel) != Float.floatToIntBits(other.combustivel))
			return false;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		if (Float.floatToIntBits(duracao) != Float.floatToIntBits(other.duracao))
			return false;
		if (Float.floatToIntBits(eficiencia) != Float.floatToIntBits(other.eficiencia))
			return false;
		if (Float.floatToIntBits(graxa) != Float.floatToIntBits(other.graxa))
			return false;
		if (Float.floatToIntBits(horimetro) != Float.floatToIntBits(other.horimetro))
			return false;
		if (id != other.id)
			return false;
		if (Float.floatToIntBits(largura) != Float.floatToIntBits(other.largura))
			return false;
		if (listaManutencao == null) {
			if (other.listaManutencao != null)
				return false;
		} else if (!listaManutencao.equals(other.listaManutencao))
			return false;
		if (listaMaquina == null) {
			if (other.listaMaquina != null)
				return false;
		} else if (!listaMaquina.equals(other.listaMaquina))
			return false;
		if (observao == null) {
			if (other.observao != null)
				return false;
		} else if (!observao.equals(other.observao))
			return false;
		if (Float.floatToIntBits(tamanhoArea) != Float.floatToIntBits(other.tamanhoArea))
			return false;
		if (tipoOperacao == null) {
			if (other.tipoOperacao != null)
				return false;
		} else if (!tipoOperacao.equals(other.tipoOperacao))
			return false;
		if (Float.floatToIntBits(velocidade) != Float.floatToIntBits(other.velocidade))
			return false;
		if (vinculoFuncao == null) {
			if (other.vinculoFuncao != null)
				return false;
		} else if (!vinculoFuncao.equals(other.vinculoFuncao))
			return false;
		return true;
	}

	public float getEficiencia() {
		return eficiencia;
	}

	public String getEficienciaFormatado() {
		NumberFormat nf = NumberFormat.getNumberInstance(new Locale("pt", "BR"));
		((DecimalFormat)nf).applyPattern("###,###,##0.00");
		return nf.format(getEficiencia());
	}
	
	public float getCCO() {
		return cco;
	}
		
	public void setCco(float cco) {
		this.cco = cco;
	}

	public String getCCOFormatado() {
		NumberFormat nf = NumberFormat.getNumberInstance(new Locale("pt", "BR"));
		((DecimalFormat)nf).applyPattern("###,###,##0.00");
		return nf.format(getCCO());
	}
	
}
