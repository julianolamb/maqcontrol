/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pro.x87.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Juliano Rodrigo Lamb
 */
@Entity
@Table(name = "servicos", catalog = "maqcontrol")
public class Servicos implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "idServicos", unique = true, nullable = false)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "id_manutencao", foreignKey = @ForeignKey(name = "FK_SERVICOS_MANUTENCAO"))
	private Manutencao manutencao;

	@ManyToOne
	@JoinColumn(name = "id_tipoServico", foreignKey = @ForeignKey(name = "FK_SERVICOS_TIPOSERVICO"))
	private TipoServico tipoServico;

	@ManyToOne
	@JoinColumn(name = "id_produto", foreignKey = @ForeignKey(name = "FK_SERVICOS_PRODUTO"))
	private Produto produto;

	@Column(name = "quantidade")
	private float quantidade;

	@Column(name = "custo")    
	private float custo;

	@Column(name = "observacao", length = 100)
	private String observacao;

	
	
	public int getTroca() {
		return produto.getTroca();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Manutencao getManutencao() {
		return manutencao;
	}

	public void setManutencao(Manutencao manutencao) {
		this.manutencao = manutencao;
	}

	public TipoServico getTipoServico() {
		return tipoServico;
	}

	public void setTipoServico(TipoServico tipoServico) {
		this.tipoServico = tipoServico;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public float getQuantidade() {
		return quantidade;
	}
	
	public String getQuantidadeFormatado(){
		NumberFormat nf = NumberFormat.getNumberInstance(new Locale("pt", "BR"));
		((DecimalFormat)nf).applyPattern("###,###,##0.00");
		return nf.format(getQuantidade());
	}

	public void setQuantidade(float quantidade) {
		this.quantidade = quantidade;
	}

	public float getCusto() {
		return custo;
	}
	
	public String getCustoFormatado(){
		NumberFormat nf = NumberFormat.getNumberInstance(new Locale("pt", "BR"));
		((DecimalFormat)nf).applyPattern("###,###,##0.00");
		return nf.format(getCusto());
	}

	public void setCusto(float custo) {
		this.custo = custo;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Servicos() {

	}
	
	public Servicos(Manutencao manutencao, TipoServico tipoServico, Produto produto, float quantidade, float custo,
			String observacao) {
		super();
		this.manutencao = manutencao;
		this.tipoServico = tipoServico;
		this.produto = produto;
		this.quantidade = quantidade;
		this.custo = custo;
		this.observacao = observacao;
	}

	@Override
	public String toString() {
		return "Servicos [id=" + id + ", manutencao=" + manutencao + ", tipoServico=" + tipoServico + ", produto="
				+ produto + ", quantidade=" + quantidade + ", custo=" + custo + ", observacao=" + observacao + "]";
	}	

	
	
}
