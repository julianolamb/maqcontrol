/**
 * 
 */
package br.pro.x87.model;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import br.pro.x87.controller.Titulo;

/**
 * @author jrlamb
 *
 */

@Entity
@Table(name = "VinculoSalario", catalog = "maqcontrol")
public class VinculoSalario implements Serializable, Comparable<VinculoSalario>{

	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
	private Long id;
	
	@ManyToOne(optional = false, targetEntity = VinculoFuncao.class)
	@JoinColumn(name = "id_vinculo", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_VINCSALARIO_VINCFUNCAO"))
	private VinculoFuncao vinculoFuncao;
	
	@Type(type = "date")
	@Column(name = "inicio")
	private Date dataAlteracao;

	@Column(name = "salario")
	private float novoSalario;
	
	
	@Titulo(nome = "Funcionário", numeroDaColuna = 0)
	public String getNomeFuncionario() {
		return vinculoFuncao.getVinculo().getFuncionario().getNome();
	}
	
	@Titulo(nome = "Empresa", numeroDaColuna = 1)
	public String getNomeEmpresa() {
		return vinculoFuncao.getVinculo().getEmpresa().getNomeFantasia();
	}
	
	@Titulo(nome = "Função", numeroDaColuna = 2)
	public String getNomeFuncao() {
		return vinculoFuncao.getFuncao().getDescricao();
	}	

	public VinculoSalario() {
		
	}	

	public VinculoSalario(VinculoFuncao vinculoFuncao, Date dataAlteracao, float novoSalario) {		
		this.vinculoFuncao = vinculoFuncao;
		this.dataAlteracao = dataAlteracao;
		this.novoSalario = novoSalario;
	}

	public VinculoFuncao getVinculoFuncao() {
		return vinculoFuncao;
	}

	public void setVinculoFuncao(VinculoFuncao vinculoFuncao) {
		this.vinculoFuncao = vinculoFuncao;
	}	
	
	public Date getDataAlteracao() {
		return dataAlteracao;
	}
	
	@Titulo(nome = "Reajuste em", numeroDaColuna = 3)
	public String getDataAlteracaoFormatado(){
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		return formato.format(getDataAlteracao());
	}

	public void setDataAlteracao(Date dataAlteracao) {
		this.dataAlteracao = dataAlteracao;
	}
	
	public float getNovoSalario() {
		return novoSalario;
	}
	
	@Titulo(nome = "Novo valor", numeroDaColuna = 4)
	public String getNovoSalarioFormatado() {
		NumberFormat nf = NumberFormat.getNumberInstance(new Locale("pt", "BR"));
		((DecimalFormat)nf).applyPattern("###,###,##0.00");
		return nf.format(getNovoSalario());
	}

	public void setNovoSalario(float novoSalario) {
		this.novoSalario = novoSalario;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dataAlteracao == null) ? 0 : dataAlteracao.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + Float.floatToIntBits(novoSalario);
		result = prime * result + ((vinculoFuncao == null) ? 0 : vinculoFuncao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VinculoSalario other = (VinculoSalario) obj;
		if (dataAlteracao == null) {
			if (other.dataAlteracao != null)
				return false;
		} else if (!dataAlteracao.equals(other.dataAlteracao))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (Float.floatToIntBits(novoSalario) != Float.floatToIntBits(other.novoSalario))
			return false;
		if (vinculoFuncao == null) {
			if (other.vinculoFuncao != null)
				return false;
		} else if (!vinculoFuncao.equals(other.vinculoFuncao))
			return false;
		return true;
	}
	
	@Override
	public int compareTo(VinculoSalario outroVinculo) {
		if (this.getDataAlteracao().before(outroVinculo.getDataAlteracao()))
			return -1;

		if (this.getDataAlteracao().after(outroVinculo.getDataAlteracao()))
			return 1;
		
		return 0;
	}	

}
