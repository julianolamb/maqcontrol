/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pro.x87.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import br.pro.x87.controller.Titulo;

/**
 *
 * @author jrlamb
 */
@Entity
@Table(name = "tipoServico", catalog = "maqcontrol")
public class TipoServico implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	//    @OneToMany(mappedBy = "servico", cascade = CascadeType.ALL, orphanRemoval = true)
	//    private List<OperacaoServico> operacaoServico = new ArrayList<>();  
	//
	//    @OneToMany(mappedBy = "servico", cascade = CascadeType.ALL, orphanRemoval = true)
	//    private List<ServicoManutencao> servicoManutencao = new ArrayList<>();

	@Column(name = "descricao", length = 50)
	private String descricao;

	@Column(name = "tipo", length = 50)
	private String tipoServico;

	public TipoServico() {    	

	}

	public TipoServico(String descricao, String tipoServico) {		
		this.descricao = descricao;
		this.tipoServico = tipoServico;
	}

	@Titulo(nome = "Código", numeroDaColuna = 0)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Titulo(nome = "Descrição", numeroDaColuna = 1)
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Titulo(nome = "Tipo", numeroDaColuna = 2)
	public String getTipoServico() {
		return tipoServico;
	}

	public void setTipoServico(String tipoServico) {
		this.tipoServico = tipoServico;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((tipoServico == null) ? 0 : tipoServico.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoServico other = (TipoServico) obj;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (tipoServico == null) {
			if (other.tipoServico != null)
				return false;
		} else if (!tipoServico.equals(other.tipoServico))
			return false;
		return true;
	}
}
