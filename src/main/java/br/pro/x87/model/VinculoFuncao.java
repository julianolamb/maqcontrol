package br.pro.x87.model;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import br.pro.x87.controller.Titulo;

@Entity
@Table(name = "VinculoFuncao", catalog = "maqcontrol")
public class VinculoFuncao implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
	private Long id;

	@ManyToOne(optional = false, targetEntity = Funcao.class)
	@JoinColumn(name = "id_funcao", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_VF_FUNCAO"))
	private Funcao funcao;

	@ManyToOne(optional = false, targetEntity = Vinculo.class)
	@JoinColumn(name = "id_vinculo", referencedColumnName = "id", foreignKey = @ForeignKey(name = "FK_VF_VINCULO"))
	private Vinculo vinculo;
	
	@OneToMany(mappedBy = "vinculoFuncao", cascade = CascadeType.ALL, orphanRemoval = true, targetEntity = VinculoSalario.class)
	private List<VinculoSalario> vinculoSalario = new ArrayList<>();
	
	@Type(type = "date")
	@Column(name = "inicio")
	private Date inicio;

	@Type(type = "date")
	@Column(name = "fim")
	private Date fim;

	@Column(name = "quantidadeHoras")
	private float quantidadeHoras;

	@Column(name = "salario")
	private float salario;

	public VinculoFuncao() {

	}

	public VinculoFuncao(Funcao funcao, Vinculo vinculo, Date inicio, Date fim, float quantidadeHoras, float salario) {
		this.vinculo = vinculo;
		this.funcao = funcao;
		this.inicio = inicio;
		this.fim = fim;
		this.quantidadeHoras = quantidadeHoras;
		this.salario = salario;
	}	

	public List<VinculoSalario> getVinculoSalario() {
		return vinculoSalario;
	}

	public void setVinculoSalario(List<VinculoSalario> vinculoSalario) {
		this.vinculoSalario = vinculoSalario;
	}

	@Titulo(nome = "Empresa", numeroDaColuna = 3)
	public String getNomeFantasia() {
		return vinculo.getNomeFantasia();
	}

	public Vinculo getVinculo() {
		return vinculo;
	}

	public void setVinculo(Vinculo vinculo) {
		this.vinculo = vinculo;
	}

	public Funcao getFuncao() {
		return funcao;
	}

	public void setFuncao(Funcao funcao) {
		this.funcao = funcao;
	}	

	@Titulo(nome = "Funcao", numeroDaColuna = 2)
	public String getDescricao() {
		return funcao.getDescricao();
	}
	
	public Date getInicio() {
		return inicio;
	}
	
	@Titulo(nome = "Inicio", numeroDaColuna = 0)
	public String getInicioFormatado(){
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		return formato.format(getInicio());
	}

	public void setInicio(Date inicio) {
		this.inicio = inicio;
	}
	
	public Date getFim() {
		return fim;
	}
	
	@Titulo(nome = "Fim", numeroDaColuna = 1)
	public String getFimFormatado(){
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		return formato.format(getFim());
	}
	
	public void setFim(Date fim) {
		this.fim = fim;
	}

	public float getQuantidadeHoras() {
		return quantidadeHoras;
	}
	
	public String getQuantidadeHorasFormatado() {
		NumberFormat nf = NumberFormat.getNumberInstance(new Locale("pt", "BR"));
		((DecimalFormat)nf).applyPattern("###,###,##0.00");
		return nf.format(getQuantidadeHoras());
	}

	public void setQuantidadeHoras(float quantidadeHoras) {
		this.quantidadeHoras = quantidadeHoras;
	}

	public float getSalario() {
		return salario;
	}
	
	public String getSalarioFormatado() {
		NumberFormat nf = NumberFormat.getNumberInstance(new Locale("pt", "BR"));
		((DecimalFormat)nf).applyPattern("###,###,##0.00");
		return nf.format(getSalario());
	}
	

	public void setSalario(float salario) {
		this.salario = salario;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fim == null) ? 0 : fim.hashCode());
		result = prime * result + ((funcao == null) ? 0 : funcao.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((inicio == null) ? 0 : inicio.hashCode());
		result = prime * result + Float.floatToIntBits(quantidadeHoras);
		result = prime * result + Float.floatToIntBits(salario);
		result = prime * result + ((vinculo == null) ? 0 : vinculo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VinculoFuncao other = (VinculoFuncao) obj;
		if (fim == null) {
			if (other.fim != null)
				return false;
		} else if (!fim.equals(other.fim))
			return false;
		if (funcao == null) {
			if (other.funcao != null)
				return false;
		} else if (!funcao.equals(other.funcao))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (inicio == null) {
			if (other.inicio != null)
				return false;
		} else if (!inicio.equals(other.inicio))
			return false;
		if (Float.floatToIntBits(quantidadeHoras) != Float.floatToIntBits(other.quantidadeHoras))
			return false;
		if (Float.floatToIntBits(salario) != Float.floatToIntBits(other.salario))
			return false;
		if (vinculo == null) {
			if (other.vinculo != null)
				return false;
		} else if (!vinculo.equals(other.vinculo))
			return false;
		return true;
	}

}
