package br.pro.x87.model;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.pro.x87.controller.Titulo;

@Entity
@Table(name = "produto", catalog = "maqcontrol")
public class Produto {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private int id;

//    @OneToMany(mappedBy = "produto", cascade = CascadeType.ALL, orphanRemoval = true)
//    private List<OperacaoServico> operacaoServico = new ArrayList<>();    
//
//    @OneToMany(mappedBy = "produto", cascade = CascadeType.ALL, orphanRemoval = true)
//    private List<ServicoManutencao> servicoManutencao = new ArrayList<>();

    @Column(name = "descricao", length = 50)
    private String descricao;

    @Column(name = "tipoProduto", length = 60)
    private String tipoProduto;

    @Column(name = "troca")
    private int troca;

    @Column(name = "aplicacao", length = 60)
    private String tipoMaquina;

    @ManyToOne
    @JoinColumn(name = "id_marca", foreignKey = @ForeignKey(name = "FK_MRCA_PRODUTO"))
    public Marca marca;

    public Produto() {
        // TODO: implement
    }

	public Produto(String descricao, String tipoProduto, int troca, String tipoMaquina, Marca marca) {		
		this.descricao = descricao;
		this.tipoProduto = tipoProduto;
		this.troca = troca;
		this.tipoMaquina = tipoMaquina;
		this.marca = marca;
	}

	@Titulo(nome = "Código", numeroDaColuna = 0)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Titulo(nome = "Descrição", numeroDaColuna = 1)
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Titulo(nome = "Tipo", numeroDaColuna = 2)
	public String getTipoProduto() {
		return tipoProduto;
	}

	public void setTipoProduto(String tipoProduto) {
		this.tipoProduto = tipoProduto;
	}

	@Titulo(nome = "Troca (h)", numeroDaColuna = 4)
	public int getTroca() {
		return troca;
	}

	public void setTroca(int troca) {
		this.troca = troca;
	}

	@Titulo(nome = "Aplicação", numeroDaColuna = 3)
	public String getTipoMaquina() {
		return tipoMaquina;
	}

	public void setTipoMaquina(String tipoMaquina) {
		this.tipoMaquina = tipoMaquina;
	}

	public Marca getMarca() {
		return marca;
	}

	public void setMarca(Marca marca) {
		this.marca = marca;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result + id;
		result = prime * result + ((marca == null) ? 0 : marca.hashCode());
		result = prime * result + ((tipoMaquina == null) ? 0 : tipoMaquina.hashCode());
		result = prime * result + ((tipoProduto == null) ? 0 : tipoProduto.hashCode());
		result = prime * result + troca;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Produto other = (Produto) obj;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (id != other.id)
			return false;
		if (marca == null) {
			if (other.marca != null)
				return false;
		} else if (!marca.equals(other.marca))
			return false;
		if (tipoMaquina == null) {
			if (other.tipoMaquina != null)
				return false;
		} else if (!tipoMaquina.equals(other.tipoMaquina))
			return false;
		if (tipoProduto == null) {
			if (other.tipoProduto != null)
				return false;
		} else if (!tipoProduto.equals(other.tipoProduto))
			return false;
		if (troca != other.troca)
			return false;
		return true;
	}
}
