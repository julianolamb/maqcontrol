/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pro.x87.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import br.pro.x87.controller.Titulo;

/**
 *
 * @author Juliano Rodrifo Lamb
 */
@Entity
@Table(name = "empresa", catalog = "maqcontrol")
public class Empresa implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @OneToMany(mappedBy = "empresa", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Vinculo> vinculo = new ArrayList<>();
    
    @ManyToOne
    @JoinColumn(name = "id_endereco", foreignKey = @ForeignKey(name = "FK_EMPRESA_ENDERECO"))
    public Endereco endereco; 
    
    @Column(name = "nomeFantasia", length = 60)
    private String nomeFantasia;

    @Column(name = "razaoSocial", length = 60)
    private String razaoSocial;

    public Empresa() {
    }

    public Empresa(Endereco endereco, String nomeFantasia, String razaoSocial) {
        this.endereco = endereco;
        this.nomeFantasia = nomeFantasia;
        this.razaoSocial = razaoSocial;
    }

    public List<Vinculo> getVinculo() {
		return vinculo;
	}

	public void setVinculo(List<Vinculo> vinculo) {
		this.vinculo = vinculo;
	}

	@Titulo(nome = "Código", numeroDaColuna = 0)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	@Titulo(nome = "Nome fantasia", numeroDaColuna = 1)
	public String getNomeFantasia() {
		return nomeFantasia;
	}

	public String getnomeFantasia() {
		return nomeFantasia;
	}
	
	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}
	
	@Titulo(nome = "Razão Social", numeroDaColuna = 2)
	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((endereco == null) ? 0 : endereco.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((nomeFantasia == null) ? 0 : nomeFantasia.hashCode());
		result = prime * result + ((razaoSocial == null) ? 0 : razaoSocial.hashCode());
		result = prime * result + ((vinculo == null) ? 0 : vinculo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Empresa other = (Empresa) obj;
		if (endereco == null) {
			if (other.endereco != null)
				return false;
		} else if (!endereco.equals(other.endereco))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (nomeFantasia == null) {
			if (other.nomeFantasia != null)
				return false;
		} else if (!nomeFantasia.equals(other.nomeFantasia))
			return false;
		if (razaoSocial == null) {
			if (other.razaoSocial != null)
				return false;
		} else if (!razaoSocial.equals(other.razaoSocial))
			return false;
		if (vinculo == null) {
			if (other.vinculo != null)
				return false;
		} else if (!vinculo.equals(other.vinculo))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Empresa [id=" + id + ", vinculo=" + vinculo + ", endereco=" + endereco + ", nomeFantasia="
				+ nomeFantasia + ", razaoSocial=" + razaoSocial + "]";
	}

	
    
    

}
