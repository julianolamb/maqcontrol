/**
 * 
 */
package br.pro.x87.validators;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;

/**
 * @author jrlamb
 *
 */
public class IntegerFilter extends DocumentFilter {

	@Override
	public void insertString(FilterBypass fb, int offset, String string, AttributeSet attr)
			throws BadLocationException {
		
		

		if (string == null)
			return;

		try {
			Integer.parseInt(string);
		} catch (Exception e) {
			return;
		} 

		super.insertString(fb, offset, string, attr);
	}

	@Override
	public void replace(FilterBypass fb, int offset, int length, String string, AttributeSet attrs)
			throws BadLocationException {

		

		if (string == null)
			return;

		try {
			Integer.parseInt(string);
		} catch (Exception e) {
			return;
		} 
		
		super.replace(fb, offset, length, string, attrs);
	}

	


}
