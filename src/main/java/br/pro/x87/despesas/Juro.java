/**
 * 
 */
package br.pro.x87.despesas;

import br.pro.x87.model.Maquina;

/**
 * @author jrlamb
 *
 */
public class Juro implements CalcularDespesas{

	@Override
	public float calcular(Maquina m) {
		float valorJuro=-1;
		float juroAnual = 0;
	
		if ((m.getValorAquisicao() == 0) && (m.getValorResidual() == 0) && (m.getTaxaJuros() == 0) && (m.getTempoUso()==0)){
			return valorJuro;
		}		

		try{
			float numerador = 0, denominador = 0;
			numerador = m.getValorAquisicao() + m.getValorResidual();
			denominador = 200*(m.getTaxaJuros()/100);
			juroAnual = numerador /denominador;			
			valorJuro = juroAnual / m.getTempoUso();	
		} catch (Exception e) {
			valorJuro=0;
		}
		
		return valorJuro;
	}

}
