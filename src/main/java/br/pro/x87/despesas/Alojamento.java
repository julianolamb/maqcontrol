/**
 * 
 */
package br.pro.x87.despesas;

import br.pro.x87.model.Maquina;

/**
 * @author jrlamb
 *
 */
public class Alojamento implements CalcularDespesas{

	@Override
	public float calcular(Maquina m) {

		float valorAlojamento = -1;

		if ((m.getTaxaAlojamento() == 0) && (m.getValorAquisicao() == 0) && (m.getTempoUso() == 0)) {
			return valorAlojamento;
		}

		try {
			valorAlojamento = ((m.getTaxaAlojamento() * m.getValorAquisicao()) / m.getTempoUso()) / 100;
		} catch (Exception e) {
			valorAlojamento = 0;
		}		

		return valorAlojamento;
	}

}
