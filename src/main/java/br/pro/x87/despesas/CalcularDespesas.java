/**
 * 
 */
package br.pro.x87.despesas;

import br.pro.x87.model.Maquina;

/**
 * @author jrlamb
 *
 */
public interface CalcularDespesas {
	
	public float calcular(Maquina m);

}
