/**
 * 
 */
package br.pro.x87.despesas;

import br.pro.x87.model.Maquina;

/**
 * @author jrlamb
 *
 */
public class Seguro implements CalcularDespesas {

	@Override
	public float calcular(Maquina m) {
		
		float valorSeguro = -1;

		if ((m.getTaxaSeguros() == 0) && (m.getValorAquisicao() == 0) && (m.getTempoUso()==0)){
			return valorSeguro;
		}
		
		try {
			valorSeguro = (m.getTaxaSeguros() * m.getValorAquisicao()) / (100 * m.getTempoUso());
		} catch (Exception e) {
			valorSeguro = 0;
		}
		
		return valorSeguro;
	}

}
