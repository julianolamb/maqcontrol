/**
 * 
 */
package br.pro.x87.despesas;

/**
 * @author jrlamb
 *
 */
public class CampoDeCusto {

	private String item;
	private String tipo;		
	private Float valor;	
		
	public CampoDeCusto(String item, Float valor) {
		super();
		this.item = item;
		this.valor = valor;
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public Float getValor() {
		return valor;
	}

	public void setValor(Float valor) {
		this.valor = valor;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}	

	
}
