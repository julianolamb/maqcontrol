package br.pro.x87.depreciacao;

import java.util.ArrayList;

import br.pro.x87.model.Maquina;

/**
 * @author jrlamb
 *
 */
public class DeclinioEmDobro extends Depreciacao {

	public DeclinioEmDobro(Maquina m) {
		prepararVetor(m);
	}

	@Override
	public void setTitle() {
		setTitle("Depreciação por declínio em dobro");
	}

	@Override
	public ArrayList<Campos> calcular() {

		float valorAquisicao = getM().getValorAquisicao();
		int vidaUtil = getM().getVidaUtil();

		for (int i = 1; i <= vidaUtil; i++) {
			(getList().get(i)).setCol1( (getList().get(i - 1).getCol3() * 2) / vidaUtil ) ;
			(getList().get(i)).setCol2( ((getList().get(i)).getCol1()*100)/valorAquisicao );
			(getList().get(i)).setCol3( (getList().get(i - 1)).getCol3() - (getList().get(i)).getCol1() );			
		}
		
		return getList();
	}
}
