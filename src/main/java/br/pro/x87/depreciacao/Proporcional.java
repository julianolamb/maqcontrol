/**
 * 
 */
package br.pro.x87.depreciacao;

import java.util.ArrayList;

import br.pro.x87.model.Maquina;

/**
 * @author jrlamb
 *
 */
public class Proporcional extends Depreciacao {

	public Proporcional(Maquina m) {
		prepararVetor(m);
	}

	@Override
	public void setTitle() {
		setTitle("Depreciação proporcional às horas trabalhadas");
	}
	
	@Override
	public ArrayList<Campos> calcular() {

		float valorAquisicao = getM().getValorAquisicao();
		float tempoUso = getM().getTempoUso();
		int vidaUtil = getM().getVidaUtil();
		
		for (int i = 1; i <= vidaUtil; i++) {
			(getList().get(i)).setCol1( (tempoUso / (vidaUtil * tempoUso)) * getList().get(0).getCol3()) ;
			(getList().get(i)).setCol2( ((getList().get(i)).getCol1()*100)/valorAquisicao );
			(getList().get(i)).setCol3( (getList().get(i - 1)).getCol3() - (getList().get(i)).getCol1() );
		}
		
		return getList();
	}



}
