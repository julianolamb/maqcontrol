/**
 * 
 */
package br.pro.x87.depreciacao;

/**
 * @author jrlamb
 *
 */
public class Campos {
	private int col0;
	private float col1;
	private float col2;
	private float col3;
	
	/**
	 * 
	 */
	public Campos() {
		// TODO Auto-generated constructor stub
	}

	public Campos(int ano, float col2, float col3, float col4) {
		super();
		this.col0 = ano;
		this.col1 = col2;
		this.col2 = col3;
		this.col3 = col4;
	}

	public int getCol0() {
		return col0;
	}

	public void setCol0(int col0) {
		this.col0 = col0;
	}

	public float getCol1() {
		return col1;
	}

	public void setCol1(float col1) {
		this.col1 = col1;
	}

	public float getCol2() {
		return col2;
	}

	public void setCol2(float col2) {
		this.col2 = col2;
	}

	public float getCol3() {
		return col3;
	}

	public void setCol3(float col3) {
		this.col3 = col3;
	}
	
	

}
