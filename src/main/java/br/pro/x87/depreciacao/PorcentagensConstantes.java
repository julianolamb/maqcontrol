package br.pro.x87.depreciacao;

import java.util.ArrayList;

import br.pro.x87.model.Maquina;

/**
 * @author jrlamb
 *
 */
public class PorcentagensConstantes extends Depreciacao {

	public PorcentagensConstantes(Maquina m) {
		prepararVetor(m);
	}
	
	@Override
	public void setTitle() {
		setTitle("Depreciação por porcentagens constantes (Matheson)");
	}	

	@Override
	public ArrayList<Campos> calcular() {
		float valorAquisicao = getM().getValorAquisicao();
		float valorResidual = getM().getValorResidual();
		float depreciacaoAnual = 0;
		int vidaUtil = getM().getVidaUtil();		

		if (vidaUtil == 4) depreciacaoAnual = 0.25f;
		if (vidaUtil == 5) depreciacaoAnual = 0.20f;
		if (vidaUtil == 10) depreciacaoAnual = 0.10f;

		for (int i = 1; i <= vidaUtil; i++) {
			(getList().get(i)).setCol1( (getList().get(i - 1)).getCol3() * depreciacaoAnual );
			(getList().get(i)).setCol2( ((getList().get(i)).getCol1() * 100)/valorAquisicao);
			(getList().get(i)).setCol3( (getList().get(i - 1)).getCol3() - (getList().get(i)).getCol1() );			
		}
		
		return getList();
	}
}
