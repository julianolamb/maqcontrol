package br.pro.x87.depreciacao;

import java.util.ArrayList;

import br.pro.x87.model.Maquina;

/**
 * @author jrlamb
 *
 */
public class QuotasConstantes extends Depreciacao {

	public QuotasConstantes(Maquina m) {		
		prepararVetor(m);
	}

	@Override
	public void setTitle() {
		setTitle("Depreciação por quotas constantes");
	}	
	
	@Override
	public void setLabels() {
		setLabel1("Ano");
		setLabel2("Quota de dep.");
		setLabel3("Fundo de dep.");
		setLabel4("Valor atual");				
	}

	@Override
	public ArrayList<Campos> calcular() throws IndexOutOfBoundsException{
		float valorAquisicao = getM().getValorAquisicao();
		float valorResidual = getM().getValorResidual();
		float depreciacaoAnual = 0;
		int vidaUtil = getM().getVidaUtil(); 

		depreciacaoAnual = ((valorAquisicao - valorResidual) / vidaUtil);

		(getList().get(1)).setCol2(depreciacaoAnual);

		for (int i = 1; i <= vidaUtil; i++) {
			(getList().get(i)).setCol1(depreciacaoAnual);
			(getList().get(i)).setCol3( (getList().get(i - 1)).getCol3() - (getList().get(i)).getCol1() );
			if (i< vidaUtil)
				(getList().get(i + 1)).setCol2( (getList().get(i)).getCol2() + (getList().get(i)).getCol1() );
		}

		return getList();
	}
}
