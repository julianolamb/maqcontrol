/**
 * 
 */
package br.pro.x87.depreciacao;

import java.util.ArrayList;

import br.pro.x87.model.Maquina;

/**
 * @author jrlamb
 *
 */
public abstract class Depreciacao{

	private String label1;
	private String label2;
	private String label3;
	private String label4;
	private String title;
	
	private ArrayList<Campos> list;
	private Maquina m;

	public abstract void setTitle();
	public abstract ArrayList<Campos> calcular();
	
	public void setLabels(){
		setLabel1("Ano");
		setLabel2("Depreciação");
		setLabel3("% Real");
		setLabel4("Valor depreciado");
	}	
		
	public Maquina getM() {
		return m;
	}
	
	public ArrayList<Campos> getList() {
		return list;
	}
	
	public void setLabel1(String label1) {
		this.label1 = label1;
	}
	
	public void setLabel2(String label2) {
		this.label2 = label2;
	}
	
	public void setLabel3(String label3) {
		this.label3 = label3;
	}
	
	public void setLabel4(String label4) {
		this.label4 = label4;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getLabel1() {
		return label1;
	}
	
	public String getLabel2() {
		return label2;
	}
	
	public String getLabel3() {
		return label3;
	}
	
	public String getLabel4() {
		return label4;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void inicializarVetor(){
		list = new ArrayList<Campos>();
		setLabels();		
	}

	public void injetarMaquina(Maquina m){
		this.m = m;
	}

	public void prepararVetor(Maquina m){		
		inicializarVetor();
		injetarMaquina(m);

		int vidaUtil = m.getVidaUtil(); 

		for (int i = 0; i <= vidaUtil; i++) {
			list.add(new Campos(i, 0, 0, 0));
		}
		
		(getList().get(0)).setCol3(getM().getValorAquisicao());
	}	
	
	public float depreciacaoAnual(){
		float soma = 0;

		for (Campos elemento : list) {			
			soma += elemento.getCol1();
		}

		return soma/getM().getVidaUtil();		
	}

	public float depreciacaoHoraria(){
		float depreciacaoAnual = depreciacaoAnual();

		float soma = 0;

		soma = depreciacaoAnual / getM().getTempoUso();
		return soma;
	}
}
