package br.pro.x87.depreciacao;

import java.util.ArrayList;

import br.pro.x87.model.Maquina;

/**
 * @author jrlamb
 *
 */
public class DigitosPeriodicos extends Depreciacao {

	public DigitosPeriodicos(Maquina m) {
		prepararVetor(m);
	}

	@Override
	public void setTitle() {
		setTitle("Depreciação por soma dos dígitos periódicos (Método Cole)");		
	}

	@Override
	public ArrayList<Campos> calcular() {
		float valorAquisicao = getM().getValorAquisicao();
		int vidaUtil = getM().getVidaUtil();

		float denominador = (vidaUtil + 1);	
		denominador = (denominador/2) * vidaUtil;	

		for (int i = 1; i <= vidaUtil; i++) {
			(getList().get(i)).setCol1( ((vidaUtil - (getList().get(i)).getCol0() + 1) / denominador)*valorAquisicao );
			(getList().get(i)).setCol2( ((getList().get(i)).getCol1()*100)/valorAquisicao );
			(getList().get(i)).setCol3( (getList().get(i - 1)).getCol3() - (getList().get(i)).getCol1() );			
		}

		return getList();
	}
}
