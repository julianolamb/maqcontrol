/**
 * 
 */
package br.pro.x87.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;

import br.pro.x87.config.HibernateConnector;
import br.pro.x87.model.Cidade;

/**
 * @author jrlamb
 * @param <T>
 *
 */
public class CidadeDAO<T> extends DAO<T> {

	public List<Cidade> imprimirCidades() {
		List<Cidade> lista = new ArrayList<Cidade>();
		try {
			session = HibernateConnector.getInstance().getSession();
			String hql = "FROM Cidade";
			Query query = session.createQuery(hql);  
			lista = query.list();			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
		return lista;
	}
}
