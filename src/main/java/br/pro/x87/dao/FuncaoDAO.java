/**
 * 
 */
package br.pro.x87.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;

import br.pro.x87.config.HibernateConnector;
import br.pro.x87.model.Funcao;

/**
 * @author jrlamb
 * @param <T>
 *
 */
public class FuncaoDAO<T> extends DAO<T> {

	public List<Funcao> imprimirFuncoes() {
		List<Funcao> lista = new ArrayList<Funcao>();
		try {
			session = HibernateConnector.getInstance().getSession();
			String hql = "FROM Funcao";
			Query query = session.createQuery(hql);  
			lista = query.list();			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
		return lista;
	}
	
}
