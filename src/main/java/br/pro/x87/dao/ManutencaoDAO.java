/**
 * 
 */
package br.pro.x87.dao;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import br.pro.x87.config.HibernateConnector;
import br.pro.x87.model.Manutencao;
import br.pro.x87.model.Operacao;
import br.pro.x87.model.Servicos;

/**
 * @author jrlamb
 * @param <T>
 *
 */
public class ManutencaoDAO<T> extends DAO<T>{
	
	public List<Servicos> getListaServicos(Operacao o) {		
		List<Servicos> listaServicos = new ArrayList<>();
		
		try {
			session = HibernateConnector.getInstance().getSession();
			Operacao operacao = (Operacao) session.load(Operacao.class, o.getId());
			for (Manutencao aux : operacao.getListaManutencao())
				for (Servicos s : aux.getServicoManutencao()) 
					listaServicos.add(s);			
		
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Erro ao recuperar as informações de manutenção da operação. Verifique se todos os "
					+ "campos estão devidamente preenchidos!", 
					"Erro ao recuperar", JOptionPane.ERROR_MESSAGE);
		} finally {
			session.close();
		}		
		return listaServicos;
	}	

	
	

}
