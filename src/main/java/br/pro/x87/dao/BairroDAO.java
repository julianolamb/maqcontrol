/**
 * 
 */
package br.pro.x87.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;

import br.pro.x87.config.HibernateConnector;
import br.pro.x87.model.Bairro;
import br.pro.x87.model.Cidade;

/**
 * @author jrlamb
 * @param <T>
 *
 */
public class BairroDAO<T> extends DAO<T> {

	public List<Object> listBairros(String cidade) {
		List lista = new ArrayList();
		try {
			session = HibernateConnector.getInstance().getSession();
			String hql = "FROM Bairro bairro JOIN FETCH bairro.cidade WHERE nome = :valor";            
			Query query = session.createQuery(hql);
			query.setParameter("valor", cidade);
			lista = query.list();            
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
		return lista;
	}
	
	public List<Bairro> imprimirBairros() {
		List<Object[]> listaObjetos;		
		List<Bairro> listaBairros = new ArrayList<Bairro>();

		try {
			session = HibernateConnector.getInstance().getSession();
			String hql = "select b.id, b.descricao, b.cep, b.cidade.nome, b.cidade.estado from Bairro b";
			Query query = session.createQuery(hql);
			listaObjetos = query.list();		

			for (Object[] objects : listaObjetos) {
				Object[] aux = objects;
				Bairro b = new Bairro();

				b.setId((Long) aux[0]);
				b.setDescricao((String) aux[1]);
				b.setCep((String) aux[2]);

				Cidade c = new Cidade();
				c.setNome((String) aux[3]);
				c.setEstado((String) aux[4]);

				b.setCidade(c);

				listaBairros.add(b);
			}			
		} catch (Exception e) {
			e.printStackTrace();			
		} finally {
			session.close();
		}
		return listaBairros;
	}
}
