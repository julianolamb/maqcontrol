/**
 * 
 */
package br.pro.x87.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;

import br.pro.x87.config.HibernateConnector;
import br.pro.x87.model.Funcionario;
import br.pro.x87.model.Maquina;

/**
 * @author jrlamb
 * @param <T>
 *
 */
public class MaquinaDAO<T> extends DAO<T> {

	public List<Maquina> imprimirMaquinas() {
		List<Object[]> listaObjetos;		
		List<Maquina> listaMaquinas = new ArrayList<Maquina>();

		try {
			session = HibernateConnector.getInstance().getSession();
			String hql = "select m.id, m.descricao from Maquina m";
			Query query = session.createQuery(hql);
			listaObjetos = query.list();		

			for (Object[] objects : listaObjetos) {
				Object[] aux = objects;
				Maquina m = new Maquina();

				m.setId((Long) aux[0]);
				m.setDescricao((String) aux[1]);				

				listaMaquinas.add(m);
			}			
		} catch (Exception e) {
			e.printStackTrace();			
		} finally {
			session.close();
		}
		return listaMaquinas;
	}
	
}
