/**
 * 
 */
package br.pro.x87.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;

import br.pro.x87.config.HibernateConnector;
import br.pro.x87.model.CotacaoProduto;
import br.pro.x87.model.Produto;

/**
 * @author jrlamb
 * @param <T>
 *
 */
public class CotacaoDAO<T> extends DAO<T> {

	public List<CotacaoProduto> imprimirCotacoes() {
		List<Object[]> listaObjetos;		
		List<CotacaoProduto> listaCotacoes = new ArrayList<CotacaoProduto>();

		try {
			session = HibernateConnector.getInstance().getSession();
			String hql = "select c.produto.descricao, c.valor, c.data from CotacaoProduto c";
			Query query = session.createQuery(hql);
			listaObjetos = query.list();		

			for (Object[] objects : listaObjetos) {
				Object[] aux = objects;
				CotacaoProduto c = new CotacaoProduto();
				Produto p = new Produto();
				
				p.setDescricao((String)aux[0]);
				
				c.setProduto(p);
				c.setValor((float) aux[1]);
				c.setData((Date) aux[2]);
				
				listaCotacoes.add(c);
			}			
			System.out.println(listaCotacoes);
		} catch (Exception e) {
			e.printStackTrace();			
		} finally {
			session.close();
		}
		return listaCotacoes;
	}
	
}
