/**
 * 
 */
package br.pro.x87.dao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.swing.JOptionPane;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;

import br.pro.x87.capacidadeDeCampo.CCO;
import br.pro.x87.capacidadeDeCampo.CCT;
import br.pro.x87.capacidadeDeCampo.CapacidadeDeCampo;
import br.pro.x87.config.HibernateConnector;
import br.pro.x87.model.Area;
import br.pro.x87.model.Manutencao;
import br.pro.x87.model.Maquina;
import br.pro.x87.model.Operacao;
import br.pro.x87.model.TipoOperacao;
import br.pro.x87.model.VinculoSalario;

/**
 * @author jrlamb
 *
 */
public class OperacaoDAO<T> {

	Session session = null;
	Transaction transaction = null;
	private CapacidadeDeCampo capacidadeDeCampo;	

	public CapacidadeDeCampo getCapacidadeDeCampo() {
		return capacidadeDeCampo;
	}

	public void setCapacidadeDeCampo(CapacidadeDeCampo custoOperacional) {
		this.capacidadeDeCampo = custoOperacional;
	}

	public void delete(Operacao op) {

		try {
			session = HibernateConnector.getInstance().getSession();
			transaction = session.beginTransaction();

			Operacao operacao = (Operacao) session.load(Operacao.class, op.getId());
			operacao.getListaMaquina().clear();
			session.saveOrUpdate(operacao);
			session = HibernateConnector.getInstance().getSession();
			transaction = session.beginTransaction();
			session.delete(op);
			session.flush();
			transaction.commit();

		} catch (ConstraintViolationException e) {
			JOptionPane.showMessageDialog(null, "Esse registro está em uso. \nNão foi possível completar a operação.", 
					"Erro ao remover", JOptionPane.ERROR_MESSAGE);
		} finally {
			session.close();
		}
	}

	public boolean adicionarConjuntoMecanizado(Maquina m, Operacao o) {
		try {
			session = HibernateConnector.getInstance().getSession();
			transaction = session.beginTransaction();

			Operacao operacao = (Operacao) session.load(Operacao.class, o.getId());

			for (Maquina aux : operacao.getListaMaquina()) {
				if (aux.getId() == m.getId()){
					return false;
				}
			}

			operacao.getListaMaquina().add(m);			

			session.saveOrUpdate(operacao);
			session.flush();

		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Erro ao adicionar uma máquina ao conjunto mecanizado. Verifique se todos os "
					+ "campos estão devidamente preenchidos!", 
					"Erro ao adicionar", JOptionPane.ERROR_MESSAGE);
		} finally {
			session.close();
		}

		return true;
	}

	public void removerConjuntoMecanizado(Maquina m, Operacao o) {
		try {
			session = HibernateConnector.getInstance().getSession();
			transaction = session.beginTransaction();

			Operacao operacao = (Operacao) session.load(Operacao.class, o.getId());

			for (Maquina aux : operacao.getListaMaquina()) {
				if (aux.getId() == m.getId()){
					operacao.getListaMaquina().remove(aux);
					break;
				}
			}

			session.saveOrUpdate(operacao);
			session.flush();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Erro ao remover uma máquina do conjunto mecanizado. Verifique se todos os "
					+ "campos estão devidamente preenchidos!", 
					"Erro ao remover", JOptionPane.ERROR_MESSAGE);
		} finally {
			session.close();
		}
	}


	public List<Maquina> getConjuntoMecanizado(Operacao o) {
		List<Maquina> lista = new ArrayList<Maquina>();
		try {
			session = HibernateConnector.getInstance().getSession();
			Operacao operacao = (Operacao) session.load(Operacao.class, o.getId());
			for (Maquina aux : operacao.getListaMaquina()) 
				lista.add(aux);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Erro ao recuperar as informações do conjunto mecanizado. Verifique se todos os "
					+ "campos estão devidamente preenchidos!", 
					"Erro ao recuperar", JOptionPane.ERROR_MESSAGE);
		} finally {
			session.close();
		}
		return lista;
	}	
	
	public float getLarguraOperacional(Operacao o){
		float maior = 0;

		try {
			session = HibernateConnector.getInstance().getSession();
			Operacao operacao = (Operacao) session.load(Operacao.class, o.getId());

			for (Maquina m : operacao.getListaMaquina()) {
				if (m.getLargura() > maior){
					maior = m.getLargura();
				}
			}			
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Erro ao obter a largura operacional do conjunto mecanizado. Verifique se todos os "
					+ "campos estão devidamente preenchidos!", 
					"Largura operacional", JOptionPane.ERROR_MESSAGE);
		} 

		return maior;
	}

	public float getCCO(Operacao o){
		setCapacidadeDeCampo(new CCO());
		float valor = 0;

		try {
			session = HibernateConnector.getInstance().getSession();
			Operacao operacao = (Operacao) session.load(Operacao.class, o.getId());
			valor = capacidadeDeCampo.calcular(getLarguraOperacional(o), 0f, operacao.getDuracao(), operacao.getTamanhoArea());
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Erro ao efetuar o cálculo da capacidade de campo operacional. Verifique se todos os "
					+ "campos estão devidamente preenchidos!", 
					"Cálculo do CCO", JOptionPane.ERROR_MESSAGE);
		}

		return valor;
	}

	public float getCCT(Operacao o){
		setCapacidadeDeCampo(new CCT());
		float valor = 0;

		try {
			session = HibernateConnector.getInstance().getSession();
			Operacao operacao = (Operacao) session.load(Operacao.class, o.getId());
			valor = capacidadeDeCampo.calcular(getLarguraOperacional(o), operacao.getVelocidade(), 0f, 0f);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Erro ao efetuar o cálculo da capacidade de campo teórica. Verifique se todos os "
					+ "campos estão devidamente preenchidos!", 
					"Cálculo da CCT", JOptionPane.ERROR_MESSAGE);
		} 

		return valor;
	}

	public float getEficiencia(Operacao o){
		float cco = getCCO(o);
		float cct = getCCT(o);
		return (cco / cct)*100;
	}

	public List<Operacao> imprimirOperacoes() {
		List<Object[]> listaObjetos;		
		List<Operacao> listaOperacoes = new ArrayList<Operacao>();

		try {
			session = HibernateConnector.getInstance().getSession();
			String hql = "select o.id, o.tipoOperacao.descricao, o.data, o.area.descricao from Operacao o";
			Query query = session.createQuery(hql);
			listaObjetos = query.list();		

			for (Object[] objects : listaObjetos) {
				Object[] aux = objects;
				Operacao o = new Operacao();
				TipoOperacao tipoOperacao = new TipoOperacao();
				Area area = new Area();

				o.setId((Long) aux[0]);
				tipoOperacao.setDescricao((String) aux[1]);
				o.setData((Date) aux[2]);
				area.setDescricao((String) aux[3]);

				o.setArea(area);
				o.setTipoOperacao(tipoOperacao);

				listaOperacoes.add(o);
			}			
		} catch (Exception e) {
			e.printStackTrace();			
		} finally {
			session.close();
		}
		return listaOperacoes;
	}

	public float getSalario(Operacao o){
		float salario = 0;
		List<VinculoSalario> listaSalario;
		Date dataOperacao;			

		try {
			session = HibernateConnector.getInstance().getSession();
			Operacao operacao = (Operacao) session.load(Operacao.class, o.getId());
			salario = operacao.getVinculoFuncao().getSalario();
			dataOperacao = operacao.getData();

			listaSalario = operacao.getVinculoFuncao().getVinculoSalario();
			Collections.sort(listaSalario);
			if (listaSalario.size() > 0)
			{
				for (VinculoSalario vs : listaSalario) {
					if (vs.getDataAlteracao().before(dataOperacao)){
						salario = vs.getNovoSalario();						
					}
				}						
			}			

		} catch (Exception e) {
			e.printStackTrace();			
		} finally {
			session.close();
		}

		return salario;
	}

}
