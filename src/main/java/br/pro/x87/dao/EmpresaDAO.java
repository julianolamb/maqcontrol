/**
 * 
 */
package br.pro.x87.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;

import br.pro.x87.config.HibernateConnector;
import br.pro.x87.model.Bairro;
import br.pro.x87.model.Cidade;
import br.pro.x87.model.Empresa;
import br.pro.x87.model.Endereco;

/**
 * @author jrlamb
 * @param <T>
 *
 */
public class EmpresaDAO<T> extends DAO<T> {

	public List<Empresa> imprimirEmpresas() {
		List<Object[]> listaObjetos;		
		List<Empresa> listaEmpresas = new ArrayList<Empresa>();

		try {
			session = HibernateConnector.getInstance().getSession();
			String hql = "select e.id, e.nomeFantasia, e.razaoSocial, e.endereco.bairro.cidade.nome from Empresa e";
			Query query = session.createQuery(hql);
			listaObjetos = query.list();		

			for (Object[] objects : listaObjetos) {
				Object[] aux = objects;
				Empresa e = new Empresa();


				e.setId((Long) aux[0]);
				e.setNomeFantasia((String) aux[1]);
				e.setRazaoSocial((String) aux[2]);

				Cidade c = new Cidade();
				c.setNome((String) aux[3]);

				Bairro b = new Bairro();
				b.setCidade(c);

				Endereco en = new Endereco();
				en.setBairro(b);

				e.setEndereco(en);

				listaEmpresas.add(e);
			}			
		} catch (Exception e) {
			e.printStackTrace();			
		} finally {
			session.close();
		}
		return listaEmpresas;
	}

}
