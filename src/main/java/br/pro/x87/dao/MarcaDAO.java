/**
 * 
 */
package br.pro.x87.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;

import br.pro.x87.config.HibernateConnector;
import br.pro.x87.model.Marca;

/**
 * @author jrlamb
 * @param <T>
 *
 */
public class MarcaDAO<T> extends DAO<T> {

	public List<Marca> imprimirMarcas() {
		List<Marca> lista = new ArrayList<Marca>();
		try {
			session = HibernateConnector.getInstance().getSession();
			String hql = "FROM Marca";
			Query query = session.createQuery(hql);  
			lista = query.list();			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
		return lista;
	}


}
