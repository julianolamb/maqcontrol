/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pro.x87.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.exception.JDBCConnectionException;

import br.pro.x87.config.HibernateConnector;
import br.pro.x87.model.Manutencao;
import br.pro.x87.model.Servicos;

/**
 *
 * @author jrlamb
 */
public class DAO<T> {

	Session session = null;
	Transaction transaction = null;


	public List<Object> find(String s) {
		List lista = new ArrayList();
		try {
			session = HibernateConnector.getInstance().getSession();
			String hql = "FROM " + s;
			Query query = session.createQuery(hql);  
			lista = query.list();			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
		return lista;
	}

	public void connect(){
		try {
			session = HibernateConnector.getInstance().getSession();                
		}
		catch (Exception e) {			
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	public void disconnect(){
		StringBuilder derbyPath = new StringBuilder("jdbc:derby:maqcontrol");
        derbyPath.append(";createDatabaseIfNotExist=true&useUnicode=yes&characterEncoding=UTF-8");
        derbyPath.append(";useTimezone=true");
        derbyPath.append(";serverTimezone=UTC");
        derbyPath.append(";shutdown=true");
        System.out.println("Desconectando...");
        
		try {
			HibernateConnector.getInstance().setDerbyPath(derbyPath);			
			session = HibernateConnector.getInstance().getSession();                
		}
		catch (Exception e) {			
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	public void add(T object) {
		try {
			session = HibernateConnector.getInstance().getSession();
			transaction = session.beginTransaction();
			session.save(object);
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	public void delete(T object) {
		try {
			session = HibernateConnector.getInstance().getSession();
			transaction = session.beginTransaction();
			session.delete(object);
			session.flush();
			transaction.commit();
		} catch (ConstraintViolationException e) {
			JOptionPane.showMessageDialog(null, "Esse registro está em uso. \nNão foi possível completar a operação.", 
					"Erro ao excluir", JOptionPane.WARNING_MESSAGE);			
		} finally {
			session.close();
		}
	}

	public void update(T object) {
		try {
			session = HibernateConnector.getInstance().getSession();
			transaction = session.beginTransaction();
			session.saveOrUpdate(object);
			session.flush();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	public List<Object> get(String table) {
		List<Object> lista = new ArrayList<>();
		try {
			session = HibernateConnector.getInstance().getSession();
			String hql = "FROM " + table;
			Query query = session.createQuery(hql);            
			lista = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
		return lista;
	}	

	public List<Object> filter(String table, String field, String criterious) {
		List lista = new ArrayList();
		try {
			session = HibernateConnector.getInstance().getSession();
			String hql = "FROM " + table + " WHERE " + field + " = '" + criterious + "'";
			Query query = session.createQuery(hql);  
			lista = query.list();			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
		return lista;
	}



	public List<Servicos> getListaServicos(Manutencao m) {
		List<Servicos> lista = new ArrayList<Servicos>();
		try {
			session = HibernateConnector.getInstance().getSession();

			Manutencao manutencao = (Manutencao) session.load(Manutencao.class, m.getId());

			for (Servicos aux : manutencao.getServicoManutencao()) {
				lista.add(aux);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
		return lista;
	}
}
