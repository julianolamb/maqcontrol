/**
 * 
 */
package br.pro.x87.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;

import br.pro.x87.config.HibernateConnector;
import br.pro.x87.model.Bairro;
import br.pro.x87.model.Cidade;
import br.pro.x87.model.Empresa;
import br.pro.x87.model.Endereco;

/**
 * @author jrlamb
 * @param <T>
 *
 */
public class EnderecoDAO<T> extends DAO<T> {
	
	public List<Object> listEndereco(String bairro) {
		List lista = new ArrayList();
		try {
			session = HibernateConnector.getInstance().getSession();
			String hql = "FROM Endereco endereco  JOIN  FETCH endereco.bairro WHERE endereco.bairro.descricao = :valor";
			System.out.println("valor do bairro: " + bairro);
			Query query = session.createQuery(hql);
			query.setParameter("valor", bairro);
			lista = query.list();            
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
		return lista;
	}

	public List<Endereco> imprimirEnderecos() {
		List<Object[]> listaObjetos;		
		List<Endereco> listaEnderecos = new ArrayList<Endereco>();

		try {
			session = HibernateConnector.getInstance().getSession();
			String hql = "select e.id, e.descricao, e.numero, e.bairro.descricao, e.bairro.cidade.nome from Endereco e";
			Query query = session.createQuery(hql);
			listaObjetos = query.list();		

			for (Object[] objects : listaObjetos) {
				Object[] aux = objects;
				
				Endereco e = new Endereco();
				e.setId((Long) aux[0]);
				e.setDescricao((String) aux[1]);
				e.setNumero((int) aux[2]);

				Bairro b = new Bairro();
				b.setDescricao((String) aux[3]);
				
				Cidade c = new Cidade();
				c.setNome((String) aux[4]);
				
				b.setCidade(c);
				e.setBairro(b);

				listaEnderecos.add(e);
			}			
		} catch (Exception e) {
			e.printStackTrace();			
		} finally {
			session.close();
		}
		return listaEnderecos;
	}
	
}
