/**
 * 
 */
package br.pro.x87.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;

import br.pro.x87.config.HibernateConnector;
import br.pro.x87.model.Funcionario;

/**
 * @author jrlamb
 * @param <T>
 *
 */
public class FuncionarioDAO<T> extends DAO<T> {

	public List<Funcionario> imprimirFuncionarios() {
		List<Object[]> listaObjetos;		
		List<Funcionario> listaFuncionarios = new ArrayList<Funcionario>();

		try {
			session = HibernateConnector.getInstance().getSession();
			String hql = "select f.id, f.nome, f.cpf, f.fone from Funcionario f";
			Query query = session.createQuery(hql);
			listaObjetos = query.list();		

			for (Object[] objects : listaObjetos) {
				Object[] aux = objects;
				Funcionario f = new Funcionario();

				f.setId((Long) aux[0]);
				f.setNome((String) aux[1]);
				f.setCpf((String) aux[2]);
				f.setFone((String) aux[3]);

				listaFuncionarios.add(f);
			}			
		} catch (Exception e) {
			e.printStackTrace();			
		} finally {
			session.close();
		}
		return listaFuncionarios;
	}
}
