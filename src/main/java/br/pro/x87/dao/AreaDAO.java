/**
 * 
 */
package br.pro.x87.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;

import br.pro.x87.config.HibernateConnector;
import br.pro.x87.model.Area;
import br.pro.x87.model.Empresa;

/**
 * @author jrlamb
 * @param <T>
 *
 */
public class AreaDAO<T> extends DAO<T> {

	public List imprimirAreas() {
		List<Object[]> listaObjetos;		
		List<Area> listaArea = new ArrayList<Area>();

		try {
			session = HibernateConnector.getInstance().getSession();
			String hql = "select a.id, a.descricao, a.tamanho, a.empresa.nomeFantasia from Area a";
			Query query = session.createQuery(hql);
			listaObjetos = query.list();		

			for (Object[] objects : listaObjetos) {
				Object[] aux = objects;
				Area a = new Area();

				a.setId((Long) aux[0]);
				a.setDescricao((String) aux[1]);
				a.setTamanho((float) aux[2]);

				Empresa e = new Empresa();
				e.setNomeFantasia((String) aux[3]);

				a.setEmpresa(e);
				listaArea.add(a);
			}			
		} catch (Exception e) {
			e.printStackTrace();			
		} finally {
			session.close();
		}
		return listaArea;
	}
}
