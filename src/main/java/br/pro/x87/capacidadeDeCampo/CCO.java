/**
 * 
 */
package br.pro.x87.capacidadeDeCampo;

/**
 * @author jrlamb
 *
 */
public class CCO implements CapacidadeDeCampo{

	@Override
	public float calcular(float larguraOperacional, float velocidade, float duracao, float tamanho) {		
		return tamanho / duracao;
	}

}
