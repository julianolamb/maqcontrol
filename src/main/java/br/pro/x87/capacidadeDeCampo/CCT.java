/**
 * 
 */
package br.pro.x87.capacidadeDeCampo;

/**
 * @author jrlamb
 *
 */
public class CCT implements CapacidadeDeCampo {
	
	@Override
	public float calcular(float larguraOperacional, float velocidade, float duracao, float tamanho) {
		return (larguraOperacional *(velocidade*1000))/10000;
	}

}
