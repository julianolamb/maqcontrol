/**
 * 
 */
package br.pro.x87.capacidadeDeCampo;

/**
 * @author jrlamb
 *
 */
public interface CapacidadeDeCampo {
	public float calcular(float larguraOperacional, float velocidade, float duracao, float tamanho); 
}
