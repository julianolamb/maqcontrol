/**
 * 
 */
package br.pro.x87.controller;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import br.pro.x87.depreciacao.Campos;
import br.pro.x87.depreciacao.Depreciacao;
import br.pro.x87.depreciacao.QuotasConstantes;

/**
 * @author jrlamb
 *
 */
public class DepreciacaoModel extends AbstractTableModel {

	private Depreciacao depre;	
	private String[] columnNames;

	DecimalFormat df = new DecimalFormat("#0.00");

	private ArrayList<Campos> list;

	public DepreciacaoModel(Depreciacao d) {		
		this.depre = d;

		columnNames = new String[4];
		columnNames[0] = depre.getLabel1();
		columnNames[1] = depre.getLabel2();
		columnNames[2] = depre.getLabel3();
		columnNames[3] = depre.getLabel4();

		list = depre.calcular();
	}

	public DepreciacaoModel() {
		// TODO Auto-generated constructor stub
	}

	public String getColumnName(int col) {		 
		return columnNames[col];
	}

	@Override
	public int getColumnCount() {
		return 4;
	}

	@Override
	public int getRowCount() {
		return list.size();
	}

	@Override
	public Object getValueAt(int arg0, int arg1) {

		//arg1 = coluna
		//arg0 = linha

		df.setRoundingMode(RoundingMode.UP);

		if (arg1 == 0) return list.get(arg0).getCol0();
		if (arg1 == 1) return df.format(list.get(arg0).getCol1());
		if (arg1 == 2) {
			if (depre instanceof QuotasConstantes)
				return df.format(list.get(arg0).getCol2());
			else 
				return df.format(list.get(arg0).getCol2()) + "%";
		}
		if (arg1 == 3) return df.format(list.get(arg0).getCol3());		

		return null;
	}

}
