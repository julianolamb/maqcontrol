/**
 * 
 */
package br.pro.x87.controller;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import br.pro.x87.model.EstimativaCustosFixos;

/**
 * @author jrlamb
 *
 */
public class EstimativaCFModel extends AbstractTableModel{

	private static final long serialVersionUID = 1L;

	private List<EstimativaCustosFixos> ecf;
	private String[] columnNames;

	DecimalFormat df = new DecimalFormat("#0.00");

	public EstimativaCFModel(List<EstimativaCustosFixos> ecf) {
		this.ecf = ecf;			

		columnNames = new String[5];
		columnNames[0] = "";
		columnNames[1] = "Juros (R$/h)";
		columnNames[2] = "Seguro (R$/h)";
		columnNames[3] = "Aloj. (R$/h)";
		columnNames[4] = "Depre. (R$/h)";
		df.setRoundingMode(RoundingMode.UP);
	}
	
	public List<EstimativaCustosFixos> getList(){
		return ecf;
	}

	@Override
	public int getRowCount() {
		return ecf.size() + 1;
	}

	@Override
	public int getColumnCount() {	
		return 5;
	}

	public String getColumnName(int col) {		 
		return columnNames[col];
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {

		if (rowIndex < ecf.size()){
			if (columnIndex == 0) return ecf.get(rowIndex).getDescricao();
			if (columnIndex == 1) return df.format(ecf.get(rowIndex).getJuros());
			if (columnIndex == 2) return df.format(ecf.get(rowIndex).getSeguro());
			if (columnIndex == 3) return df.format(ecf.get(rowIndex).getAlojamento());		
			if (columnIndex == 4) return df.format(ecf.get(rowIndex).getDepreciacaoHoraria());
		} 

		if (rowIndex >= ecf.size()){
			float juros = 0;
			float seguro = 0;
			float alojamento = 0;
			float depreciacao = 0;

			for (EstimativaCustosFixos e : ecf) {
				juros += e.getJuros();
			}

			for (EstimativaCustosFixos e : ecf) {
				seguro += e.getSeguro();
			}

			for (EstimativaCustosFixos e : ecf) {
				alojamento += e.getAlojamento();
			}

			for (EstimativaCustosFixos e : ecf) {
				depreciacao += e.getDepreciacaoHoraria();
			}
			if (columnIndex == 0) return "Total (R$/h " + df.format(juros+seguro+alojamento+depreciacao) + ")";
			if (columnIndex == 1) return df.format(juros);
			if (columnIndex == 2) return df.format(seguro);
			if (columnIndex == 3) return df.format(alojamento);		
			if (columnIndex == 4) return df.format(depreciacao);
		}

		return null;
	}
	
	public float calcularTotalCustosFixos(){
		float total = 0;
		
		for (EstimativaCustosFixos estimativaCustosFixos : ecf) {
			total += estimativaCustosFixos.getJuros();
			total += estimativaCustosFixos.getSeguro();
			total += estimativaCustosFixos.getAlojamento();
			total += estimativaCustosFixos.getDepreciacaoHoraria();
		}
		
		return total;
	}
}
