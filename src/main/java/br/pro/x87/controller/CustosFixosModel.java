/**
 * 
 */
package br.pro.x87.controller;

import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import br.pro.x87.depreciacao.Campos;
import br.pro.x87.despesas.CampoDeCusto;

/**
 * @author jrlamb
 *
 */
public class CustosFixosModel extends AbstractTableModel {


	private ArrayList<CampoDeCusto> list;
	private String[] columnNames;

	DecimalFormat df = new DecimalFormat("#0.00");

	public CustosFixosModel(){
		list = new ArrayList<CampoDeCusto>();
		columnNames = new String[2];
		columnNames[0] = "Item";
		columnNames[1] = "Valor";
	}	

	public void add(CampoDeCusto cv){
		list.add(cv);
	}

	public void remove(Campos cv){
		list.remove(cv);
	}

	public void remove(int i){
		list.remove(i);
	}

	public int getColumnCount() {
		return 2;
	}

	public int getRowCount() {
		return list.size();
	}
	
	public Object getValueAt(int arg0, int arg1) {

		//arg1 = coluna
		//arg0 = linha

		if (arg1 == 0) return list.get(arg0).getItem();
		if (arg1 == 1) return df.format(list.get(arg0).getValor());				

		return null;
	}

	public void update(String s, float value){
		for (CampoDeCusto camposCV : list) {
			if (camposCV.getItem().equals(s) == true){
				camposCV.setValor(value);
			}
		}
	}

	public String getColumnName(int col) {		 
		return columnNames[col];
	}
	
	public ArrayList<CampoDeCusto> getList(){
		return list;
	}
}
