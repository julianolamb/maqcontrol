/**
 * 
 */
package br.pro.x87.controller;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import br.pro.x87.depreciacao.Campos;
import br.pro.x87.despesas.CampoDeCusto;

/**
 * @author jrlamb
 *
 */
public class CustosVariaveisModel extends AbstractTableModel  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private ArrayList<CampoDeCusto> list;
	private String[] columnNames;

	DecimalFormat df = new DecimalFormat("#0.00");

	public CustosVariaveisModel(){
		list = new ArrayList<CampoDeCusto>();
		columnNames = new String[2];
		columnNames[0] = "Item";
		columnNames[1] = "Valor";
		
		df.setRoundingMode(RoundingMode.UP);
	}	

	public void add(CampoDeCusto cv){
		list.add(cv);
	}

	public void remove(Campos cv){
		list.remove(cv);
	}
	
	public void remove(int i){
		list.remove(i);
	}

	@Override
	public int getColumnCount() {
		return 2;
	}

	@Override
	public int getRowCount() {
		return list.size();
	}

	@Override
	public Object getValueAt(int arg0, int arg1) {

		//arg1 = coluna
		//arg0 = linha

		if (arg1 == 0) return list.get(arg0).getItem();
		if (arg1 == 1) return df.format(list.get(arg0).getValor());				

		return null;
	}

	public void print(){
		for (CampoDeCusto camposCV : list) {
			System.out.println(camposCV.getItem());
			System.out.println(camposCV.getValor());
		}
	}

	public String getColumnName(int col) {		 
		return columnNames[col];
	}
	
	public float getTotal(){
		float total = 0;
		for (CampoDeCusto c : list) {
			total += c.getValor();
		}
		return total;
	}
	
	public ArrayList<CampoDeCusto> getList(){
		return list;
	}
}
