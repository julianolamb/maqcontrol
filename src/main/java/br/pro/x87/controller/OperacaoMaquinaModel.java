/**
 * 
 */
package br.pro.x87.controller;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import br.pro.x87.model.Maquina;

/**
 * @author jrlamb
 *
 */
public class OperacaoMaquinaModel extends AbstractTableModel {

	private static final long serialVersionUID = 1L;
	
	private List<Maquina> listMaquina;	
	private String[] columnNames;

	public OperacaoMaquinaModel(List<Maquina> lista) {		
		this.listMaquina = lista;

		columnNames = new String[5];
		columnNames[0] = "Código";
		columnNames[1] = "Descrição";
		columnNames[2] = "Eficiència (%)";
		columnNames[3] = "Velocidade (km/h)";
		columnNames[4] = "Larg. Operacional (m)";
	}

	public String getColumnName(int col) {		 
		return columnNames[col];
	}

	@Override
	public int getRowCount() {		
		return listMaquina.size();
	}

	@Override
	public int getColumnCount() {
		return 5;
	}
	
	public Maquina getMaquina(int posicao){
		return listMaquina.get(posicao);
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {		

		if (columnIndex == 0) return listMaquina.get(rowIndex).getId();
		if (columnIndex == 1) return listMaquina.get(rowIndex).getDescricao();
		if (columnIndex == 2) return listMaquina.get(rowIndex).getEficienciaFormatado();
		if (columnIndex == 3) return listMaquina.get(rowIndex).getVelocidadeFormatado();		
		if (columnIndex == 4) return listMaquina.get(rowIndex).getLarguraFormatado();
		
		return null;
	}

}
