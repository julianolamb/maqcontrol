/**
 * 
 */
package br.pro.x87.controller;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import br.pro.x87.model.Servicos;

/**
 * @author Juliano
 * @since 26 fev. 2018
 */
public class ServicosModel extends AbstractTableModel {

	private static final long serialVersionUID = 1L;
	
	private List<Servicos> lista;
	private String[] columnNames;
	
	public ServicosModel(List<Servicos> lista) {
		this.lista = lista;

		columnNames = new String[5];
		columnNames[0] = "Código";
		columnNames[1] = "Serviço";
		columnNames[2] = "Produto";
		columnNames[3] = "Quantidade";
		columnNames[4] = "Valor Unitário";
	}
	
	public String getColumnName(int col) {		 
		return columnNames[col];
	}

	@Override
	public int getColumnCount() {
		return 5;
	}

	@Override
	public int getRowCount() {
		return lista.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		if (columnIndex == 0) return lista.get(rowIndex).getId();
		if (columnIndex == 1) return lista.get(rowIndex).getTipoServico().getDescricao();
		if (columnIndex == 2) return lista.get(rowIndex).getProduto().getDescricao();
		if (columnIndex == 3) return lista.get(rowIndex).getQuantidadeFormatado();
		if (columnIndex == 4) return lista.get(rowIndex).getCustoFormatado();
		
		return null;
	}
}
