/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pro.x87.app;

import javax.swing.JFrame;

import br.pro.x87.view.PrincipalUI;

/**
 *
 * @author Juliano Rodrigo Lamb
 */
public class App {

	static PrincipalUI principal;
	
    public static void main(String[] args) {    	
        principal = new PrincipalUI();
        principal.setVisible(true);
        principal.setExtendedState(JFrame.MAXIMIZED_BOTH);        
    }
}
