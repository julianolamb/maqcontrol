/**
 * 
 */
package br.pro.x87.reports;

import java.awt.Desktop;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;

import br.pro.x87.dao.ManutencaoDAO;
import br.pro.x87.dao.OperacaoDAO;
import br.pro.x87.model.EstimativaCustosFixos;
import br.pro.x87.model.Manutencao;
import br.pro.x87.model.Maquina;
import br.pro.x87.model.Operacao;
import br.pro.x87.model.Servicos;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

/**
 * @author jrlamb
 *
 */
public class RelatorioCustoReal {

	private OperacaoDAO<Object> opDAO = new OperacaoDAO<>();
	private ManutencaoDAO manutencaoDAO = new ManutencaoDAO<>();

	private Operacao operacao;
	private List<Servicos> listaServicos;	
	private List<Maquina> listaConjuntoMecanizado;	
	private List<EstimativaCustosFixos> listaCustosFixos = new ArrayList<>();
	
	private float tempoLubrificacao;
	private float quantidadeGraxa;
	
	private float custoRealHora;
	private float custoRealHectare;
	private float custoTotalOperacao;	

	public RelatorioCustoReal() {
		// TODO Auto-generated constructor stub
	}	

	public RelatorioCustoReal(Operacao operacao, float tempoLubrificacao, float quantidadeGraxa) {	
		this.operacao = operacao;
		this.tempoLubrificacao = tempoLubrificacao;
		this.quantidadeGraxa = quantidadeGraxa;

		obterConjuntoMecanizado();
		gerarListaCustosFixos();
		obterListaServicos();
	}

	
	
	public List<Servicos> getListaServicos() {
		return listaServicos;
	}

	public void setListaServicos(List<Servicos> listaServicos) {
		this.listaServicos = listaServicos;
	}

	public List<EstimativaCustosFixos> getListaCustosFixos() {
		return listaCustosFixos;
	}

	public void setListaCustosFixos(List<EstimativaCustosFixos> listaCustosFixos) {
		this.listaCustosFixos = listaCustosFixos;
	}

	public void gerarListaCustosFixos(){		
		for (Maquina m : getListaConjuntoMecanizado()) {
			EstimativaCustosFixos aux = new EstimativaCustosFixos(m.getDescricao(), m.getValorJuros(), 
					m.getValorSeguros(), m.getValorAlojamento(), m.getDepreciacaoHoraria());
			getListaCustosFixos().add(aux);
		}		
	}	

	public String getNomeArea() {
		return operacao.getNomeArea();
	}

	public float getTamanhoArea() {
		return operacao.getTamanhoArea();
	}

	public float getDuracao() {
		return operacao.getDuracao();
	}	

	public String getTipoOperacao() {
		return operacao.getTipoOperacao().getDescricao();
	}

	public String getNomeFuncionario() {
		return operacao.getNomeFuncionario();
	}

	public Date getData() {
		return operacao.getData();
	}

	public Operacao getOperacao() {
		return operacao;
	}

	public void setOperacao(Operacao operacao) {
		this.operacao = operacao;
	}	

	private void obterConjuntoMecanizado(){
		listaConjuntoMecanizado = opDAO.getConjuntoMecanizado(getOperacao());
	}
	
	private void obterListaServicos(){
		listaServicos = manutencaoDAO.getListaServicos(getOperacao());		
	}

	public List<Maquina> getListaConjuntoMecanizado() {
		return listaConjuntoMecanizado;
	}

	public void setListaConjuntoMecanizado(List<Maquina> listaConjuntoMecanizado) {
		this.listaConjuntoMecanizado = listaConjuntoMecanizado;
	}	

	public float somarManutencao(){
		float soma = 0;

//		for (OperacaoServico os : getListaServicos()) {
//			soma += (os.getQuantidade() * os.getValorUnitario());
//		}

		return soma;
	}

	public float calcularTotalGraxa(){
		float total = 0;
		try {			
			total = (getOperacao().getGraxa() * this.quantidadeGraxa) / this.tempoLubrificacao;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return total;
	}	

	public float calcularTotalSalario(){
		Maquina m = Maquina.maiorTempoUso(getListaConjuntoMecanizado());
		float salarioMensal = 0, totalSalario = 0;
		salarioMensal = opDAO.getSalario(getOperacao());
		try {			
			totalSalario = (salarioMensal * 13.33f)/m.getTempoUso();			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return totalSalario;
	}

	public float calcularTotalCombustivel(){		
		float consumoLH = 0;
		float consumoKW = 0.2135f;			
		float potenciaCV = 0;
		float potenciaKW = 0;
		float totalCombustivel = 0;

		Maquina aux = Maquina.maiorConsumo(getListaConjuntoMecanizado());		

		if (aux.getTipoPotencia().equalsIgnoreCase("cv")){			
			potenciaCV = aux.getPotencia();
			potenciaKW = (potenciaCV*0.735499f);
			consumoLH = aux.getConsumoEspecifico();
			totalCombustivel = consumoLH * getOperacao().getCombustivel();
		} else {			
			potenciaKW = aux.getPotencia();			
			potenciaCV = potenciaKW/0.735499f;
			consumoKW = aux.getConsumoEspecifico();
			totalCombustivel = getOperacao().getCombustivel() * consumoKW * potenciaKW;
		}
		return totalCombustivel;
	}
	
	public void determinarCustos(){
		float soma = 0;
		
		for (EstimativaCustosFixos ecf : getListaCustosFixos()) {
			soma += ecf.getAlojamento();
			soma += ecf.getJuros();
			soma += ecf.getSeguro();
			soma += ecf.getDepreciacaoHoraria();
		}
		
		soma += calcularTotalGraxa();
		soma += calcularTotalCombustivel();
		soma += calcularTotalSalario();
		
		for (Servicos s : getListaServicos()) {
			soma += ((s.getCusto()*s.getQuantidade()) / s.getTroca()) * getOperacao().getDuracao();
		}
		
		custoTotalOperacao = soma;
		custoRealHectare = (soma / getOperacao().getTamanhoArea());
		custoRealHora = (soma / getOperacao().getDuracao());
		
	}

	public void imprimir(RelatorioCustoReal rcr){
		determinarCustos();
		System.out.println(custoRealHectare);
		System.out.println(custoRealHora);
		System.out.println(custoTotalOperacao);
		String path = System.getProperty("user.dir");
		String temp = System.getProperty("java.io.tmpdir");

		String pathToReportPackage = "\\src\\main\\java\\br\\pro\\x87\\reports\\src\\";

		try {				
			JasperReport report = JasperCompileManager.compileReport(path + pathToReportPackage + "relcustoreal.jrxml");

			Map<String,Object> params = new HashMap<String,Object>();
			params.put("RelatorioCustoReal", rcr);
			params.put("listarCustosFixos", getListaCustosFixos());		
			params.put("localizacaoCFSubReport", path + pathToReportPackage + "custosFixos.jasper");		

			params.put("listarMaquina", getListaConjuntoMecanizado());
			params.put("localizacaoCMSubReport", path + pathToReportPackage + "conjuntomecanizado.jasper");		

			params.put("localizacaoCVRSubReport", path + pathToReportPackage + "custosvariavelreal.jasper");
			params.put("graxa", calcularTotalGraxa());
			params.put("combustivel", calcularTotalCombustivel());
			params.put("salario", calcularTotalSalario());
			params.put("listarCustosManutencao", getListaServicos());
			
			params.put("localizacaoTotalSubReport", path + pathToReportPackage + "totalcustoreal.jasper");
			params.put("custoRealHora", custoRealHora);
			params.put("custoRealHectare", custoRealHectare);
			params.put("custoTotalOperacao", custoTotalOperacao);

			JasperPrint print = JasperFillManager.fillReport(report, params, new JREmptyDataSource());
			JasperExportManager.exportReportToPdfFile(print, temp + "MaqControl.pdf");					

			File f = new File(temp + "MaqControl.pdf");
			Desktop.getDesktop().open(f);	
		} catch (JRException jre) {
			mensagemErro("Ocorreu um erro inesperado, contate o administrador! "
					+ "\n\nErro ocorrido: \n" + jre.getMessage());
			jre.printStackTrace();
			return;				
		}
		catch (Exception e){
			mensagemErro("Não foi possível gerar o relatório. Possíveis razões:"
					+ "\n- o leitor de PDF está em execução; "
					+ "\n- algum relatório antigo está carregado."
					+ "\n\n Salve os trabalhos, feche o leitor de PDF e clique novamente em LISTAGEM para gerar o relatório.");
			e.printStackTrace();
			return;						
		}

		JOptionPane.showMessageDialog(null, "O relatório foi gerado com sucesso! "
				+ "Lembre de salvar ou imprimir o seu trabalho", "Relatório gerado com sucesso", 
				JOptionPane.INFORMATION_MESSAGE);
	}

	public void mensagemErro(String s){
		JOptionPane.showMessageDialog(null, s, "Erro", JOptionPane.ERROR_MESSAGE);
	}	

}
