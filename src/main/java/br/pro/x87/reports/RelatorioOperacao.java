/**
 * 
 */
package br.pro.x87.reports;

import java.awt.Desktop;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;

import br.pro.x87.dao.OperacaoDAO;
import br.pro.x87.model.Maquina;
import br.pro.x87.model.Operacao;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 * @author jrlamb
 *
 */
public class RelatorioOperacao {
	private Operacao operacao;
	
	private List<Maquina> listaMaquinas = new ArrayList<>();
	
	private OperacaoDAO opDAO = new OperacaoDAO<>();
	
	public RelatorioOperacao(Operacao o) {
		this.operacao = o;
		listaMaquinas = getMaquina();
	}

	public List<Maquina> getMaquina() {
		return opDAO.getConjuntoMecanizado(operacao);
	}

	public String getTipoOperacao() {
		return operacao.getDescricaoOperacao();
	}

	public float getTamanhoArea() {
		return operacao.getTamanhoArea();
	}

	public String getDuracaoFormatado() {
		return operacao.getDuracaoFormatado();
	}

	public String getNomeArea() {
		return operacao.getNomeArea();
	}

	public String getNomeFuncionario() {
		return operacao.getNomeFuncionario();
	}

	public String getDataFormatado() {
		return operacao.getDataFormatado();
	}	
	
	public List<Maquina> getListaMaquinas() {
		return listaMaquinas;
	}

	public void setListaMaquinas(List<Maquina> listaMaquinas) {
		this.listaMaquinas = listaMaquinas;
	}

	public void imprimir(RelatorioOperacao ro){
		String path = System.getProperty("user.dir");
		String temp = System.getProperty("java.io.tmpdir");

		String pathToReportPackage = "\\src\\main\\java\\br\\pro\\x87\\reports\\src\\";

		try {					
			JasperReport report = JasperCompileManager.compileReport(path + pathToReportPackage + "reloperacao.jrxml");						

			Map<String,Object> params = new HashMap<String,Object>();
			params.put("RelatorioOperacao", ro);		

			JasperPrint print = JasperFillManager.fillReport(report, params, new JRBeanCollectionDataSource(getListaMaquinas()));
			JasperExportManager.exportReportToPdfFile(print, temp + "MaqControl.pdf");					

			File f = new File(temp + "MaqControl.pdf");
			Desktop.getDesktop().open(f);	
		} catch (JRException jre) {
			mensagemErro("Ocorreu um erro inesperado, contate o administrador! "
					+ "\n\nErro ocorrido: \n" + jre.getMessage());			
			return;				
		}
		catch (Exception e){
			mensagemErro("Não foi possível gerar o relatório. Possíveis razões:"
					+ "\n- o leitor de PDF está em execução; "
					+ "\n- algum relatório antigo está carregado."
					+ "\n\n Salve os trabalhos, feche o leitor de PDF e clique novamente em LISTAGEM para gerar o relatório.");
			e.printStackTrace();
			return;						
		}

		JOptionPane.showMessageDialog(null, "O relatório foi gerado com sucesso! "
				+ "Lembre de salvar ou imprimir o seu trabalho", "Relatório gerado com sucesso", 
				JOptionPane.INFORMATION_MESSAGE);
	}

	public void mensagemErro(String s){
		JOptionPane.showMessageDialog(null, s, "Erro", JOptionPane.ERROR_MESSAGE);
	}	
	
}
