/**
 * 
 */
package br.pro.x87.reports;

import java.awt.Desktop;
import java.io.File;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.swing.JOptionPane;

import br.pro.x87.controller.CustosFixosModel;
import br.pro.x87.controller.CustosVariaveisModel;
import br.pro.x87.despesas.CampoDeCusto;
import br.pro.x87.model.Maquina;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 * @author jrlamb
 *
 */
public class RelatorioMaquina {

	private Maquina maquina;
	private CustosVariaveisModel cvm;
	private CustosFixosModel cfm;
	private float totalCf;
	private float totalCv;
	private float totalCh;
	private NumberFormat nf = NumberFormat.getNumberInstance(new Locale("pt", "BR"));
	

	private List<CampoDeCusto> listaCustos = new ArrayList<>();
	
	public RelatorioMaquina() {
		// TODO Auto-generated constructor stub
	}

	public RelatorioMaquina(Maquina maquina, CustosVariaveisModel cvm, CustosFixosModel cfm, float totalCf, float totalCv, 
			float totalCh) {		
		this.maquina = maquina;
		this.cvm = cvm;
		this.cfm = cfm;
		this.totalCf = totalCf;
		this.totalCv = totalCv;
		this.totalCh = totalCh;
		
		generateListaCustos();
		((DecimalFormat)nf).applyPattern("###,###,##0.00");
	}
	
	public String getTotalCf() {
		return nf.format(totalCf);
	}

	public void setTotalCf(float totalCf) {
		this.totalCf = totalCf;
	}

	public String getTotalCv() {
		return nf.format(totalCv);
	}

	public void setTotalCv(float totalCv) {
		this.totalCv = totalCv;
	}

	public String getTotalCh() {
		return nf.format(totalCh);
	}

	public void setTotalCh(float totalCh) {
		this.totalCh = totalCh;
	}

	public String getTipoPotencia() {
		return maquina.getTipoPotencia();
	}

	public String getPotenciaFormatado() {
		return maquina.getPotenciaFormatado();
	}

	public String getEficienciaFormatado() {
		return maquina.getEficienciaFormatado();
	}

	public float getVelocidadeMaxima() {
		return maquina.getVelocidadeMaxima();
	}

	public int getVidaUtil() {
		return maquina.getVidaUtil();
	}

	public String getTempoUsoFormatado() {
		return maquina.getTempoUsoFormatado();
	}

	public String getConsumoEspFormatado() {
		return maquina.getConsumoEspFormatado();
	}

	public String getLarguraFormatado() {
		return maquina.getLarguraFormatado();
	}

	public String getValorAquisicaoFormatado() {
		return maquina.getValorAquisicaoFormatado();
	}

	public String getValorResidualFormatado() {
		return maquina.getValorResidualFormatado();
	}

	public String getTaxaJurosFormatado() {
		return maquina.getTaxaJurosFormatado();
	}

	public String getTaxaAlojamentoFormatado() {
		return maquina.getTaxaAlojamentoFormatado();
	}

	public String getTaxaSegurosFormatado() {
		return maquina.getTaxaSegurosFormatado();
	}

	public List<CampoDeCusto> getListaCustos() {
		return listaCustos;
	}

	public void setListaCustos(List<CampoDeCusto> listaCustos) {
		this.listaCustos = listaCustos;
	}

	public Long getId() {
		return maquina.getId();
	}

	public String getDescricao() {
		return maquina.getDescricao();
	}

	public CustosVariaveisModel getCvm() {
		return cvm;
	}

	public void setCvm(CustosVariaveisModel cvm) {
		this.cvm = cvm;
	}

	public CustosFixosModel getCfm() {
		return cfm;
	}

	public void setCfm(CustosFixosModel cfm) {
		this.cfm = cfm;
	}

	public Maquina getMaquina() {
		return maquina;
	}

	public void setMaquina(Maquina maquina) {
		this.maquina = maquina;
	}
	
	public void generateListaCustos(){
		for (CampoDeCusto c : getCfm().getList()) {
			c.setTipo("Custo fixo");
			listaCustos.add(c);
		}

		for (CampoDeCusto c : getCvm().getList()) {
			c.setTipo("Custo variável");
			listaCustos.add(c);
		}
	}

	public void imprimir(RelatorioMaquina rm){
		String path = System.getProperty("user.dir");
		String temp = System.getProperty("java.io.tmpdir");

		String pathToReportPackage = "\\src\\main\\java\\br\\pro\\x87\\reports\\src\\";

		try {					
			JasperReport report = JasperCompileManager.compileReport(path + pathToReportPackage + "relmaquina.jrxml");						

			Map<String,Object> params = new HashMap<String,Object>();
			params.put("RelatorioMaquina", rm);		

			JasperPrint print = JasperFillManager.fillReport(report, params, new JRBeanCollectionDataSource(getListaCustos()));
			JasperExportManager.exportReportToPdfFile(print, temp + "MaqControl.pdf");					

			File f = new File(temp + "MaqControl.pdf");
			Desktop.getDesktop().open(f);	
		} catch (JRException jre) {
			mensagemErro("Ocorreu um erro inesperado, contate o administrador! "
					+ "\n\nErro ocorrido: \n" + jre.getMessage());			
			return;				
		}
		catch (Exception e){
			mensagemErro("Não foi possível gerar o relatório. Possíveis razões:"
					+ "\n- o leitor de PDF está em execução; "
					+ "\n- algum relatório antigo está carregado."
					+ "\n\n Salve os trabalhos, feche o leitor de PDF e clique novamente em LISTAGEM para gerar o relatório.");
			e.printStackTrace();
			return;						
		}

		JOptionPane.showMessageDialog(null, "O relatório foi gerado com sucesso! "
				+ "Lembre de salvar ou imprimir o seu trabalho", "Relatório gerado com sucesso", 
				JOptionPane.INFORMATION_MESSAGE);
	}

	public void mensagemErro(String s){
		JOptionPane.showMessageDialog(null, s, "Erro", JOptionPane.ERROR_MESSAGE);
	}

}
