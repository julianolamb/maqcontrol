/**
 * 
 */
package br.pro.x87.reports;

import java.awt.Desktop;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;

import br.pro.x87.controller.CustosVariaveisModel;
import br.pro.x87.controller.EstimativaCFModel;
import br.pro.x87.dao.OperacaoDAO;
import br.pro.x87.model.Maquina;
import br.pro.x87.model.Operacao;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

/**
 * @author jrlamb
 *
 */
public class RelatorioEstimativa {
	private Operacao operacao;
	private CustosVariaveisModel cvm;
	private EstimativaCFModel ecfm;	
	private List<Maquina> listaMaquina = new ArrayList<>();
	
	
	private float totalCf = 0;
	private float totalCv = 0;
	private float totalCh = 0;
	private float totalCo = 0;
	private float totalCoHa = 0;
	
	public RelatorioEstimativa() {
		// TODO Auto-generated constructor stub
	}	
		
	public RelatorioEstimativa(Operacao operacao, CustosVariaveisModel cvm, EstimativaCFModel ecfm, float totalCf,
			float totalCv, float totalCh, float totalCo, float totalCoHa, List listaMaquina) {
		super();
		this.operacao = operacao;
		this.cvm = cvm;
		this.ecfm = ecfm;
		this.totalCf = totalCf;
		this.totalCv = totalCv;
		this.totalCh = totalCh;
		this.totalCo = totalCo;
		this.totalCoHa = totalCoHa;
		
		this.listaMaquina = listaMaquina;
	}	



	public float getDuracao() {
		return operacao.getDuracao();
	}

	public float getLargura() {
		return operacao.getLargura();
	}

	public float getVelocidade() {
		return operacao.getVelocidade();
	}

	public float getEficiencia() {
		return operacao.getEficiencia();
	}

	public float getCCO() {
		return operacao.getCCO();
	}

	public float getTotalCf() {
		return totalCf;
	}

	public void setTotalCf(float totalCf) {
		this.totalCf = totalCf;
	}

	public float getTotalCv() {
		return totalCv;
	}

	public void setTotalCv(float totalCv) {
		this.totalCv = totalCv;
	}

	public float getTotalCh() {
		return totalCh;
	}

	public void setTotalCh(float totalCh) {
		this.totalCh = totalCh;
	}

	public float getTotalCo() {
		return totalCo;
	}

	public void setTotalCo(float totalCo) {
		this.totalCo = totalCo;
	}

	public float getTotalCoHa() {
		return totalCoHa;
	}

	public void setTotalCoHa(float totalCoHa) {
		this.totalCoHa = totalCoHa;
	}

	public float getTamanhoArea() {
		return operacao.getArea().getTamanho();
	}

	public String getDuracaoFormatado() {
		return operacao.getDuracaoFormatado();
	}

	public String getLarguraFormatado() {
		return operacao.getLarguraFormatado();
	}

	public String getDescricaoOperacao() {
		return operacao.getDescricaoOperacao();
	}

	public String getEficienciaFormatado() {
		return operacao.getEficienciaFormatado();
	}

	public String getCCOFormatado() {
		return operacao.getCCOFormatado();
	}
	
	public void imprimir(RelatorioEstimativa re){
		String path = System.getProperty("user.dir");
		String temp = System.getProperty("java.io.tmpdir");

		String pathToReportPackage = "\\src\\main\\java\\br\\pro\\x87\\reports\\src\\";

		try {				
			JasperReport report = JasperCompileManager.compileReport(path + pathToReportPackage + "relestimativa.jrxml");
			
			Map<String,Object> params = new HashMap<String,Object>();
			params.put("RelatorioEstimativa", re);
			params.put("listarCustosFixos", ecfm.getList());		
			params.put("localizacaoCFSubReport", path + pathToReportPackage + "custosFixos.jasper");		
			
			params.put("listarCustosVariaveis", cvm.getList());		
			params.put("localizacaoCVSubReport", path + pathToReportPackage + "custosvariaveis.jasper");
						
			params.put("listarMaquina", listaMaquina);
			params.put("localizacaoCMSubReport", path + pathToReportPackage + "conjuntomecanizado.jasper");
			

			JasperPrint print = JasperFillManager.fillReport(report, params, new JREmptyDataSource());
			JasperExportManager.exportReportToPdfFile(print, temp + "MaqControl.pdf");					

			File f = new File(temp + "MaqControl.pdf");
			Desktop.getDesktop().open(f);	
		} catch (JRException jre) {
			mensagemErro("Ocorreu um erro inesperado, contate o administrador! "
					+ "\n\nErro ocorrido: \n" + jre.getMessage());
			jre.printStackTrace();
			return;				
		}
		catch (Exception e){
			mensagemErro("Não foi possível gerar o relatório. Possíveis razões:"
					+ "\n- o leitor de PDF está em execução; "
					+ "\n- algum relatório antigo está carregado."
					+ "\n\n Salve os trabalhos, feche o leitor de PDF e clique novamente em LISTAGEM para gerar o relatório.");
			e.printStackTrace();
			return;						
		}

		JOptionPane.showMessageDialog(null, "O relatório foi gerado com sucesso! "
				+ "Lembre de salvar ou imprimir o seu trabalho", "Relatório gerado com sucesso", 
				JOptionPane.INFORMATION_MESSAGE);
	}

	public void mensagemErro(String s){
		JOptionPane.showMessageDialog(null, s, "Erro", JOptionPane.ERROR_MESSAGE);
	}	
	
	
	

}
