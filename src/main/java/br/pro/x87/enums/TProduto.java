/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pro.x87.enums;

/**
 *
 * @author Juliano Rodrifo Lamb
 */
public enum TProduto {
    COMBUSTIVEL ("Combustível"), 
    FILTRO_AR_FILTRANTE ("Filtro de ar (Filtrante)"), 
    FILTRO_AR_SEGURANCA ("Filtro de ar (Segurança)"), 
    FILTRO_DIESEL ("Filtro de diesel"), 
    FILTRO_LUBRIFICANTE_MOTOR ("Filtro do lubrificante do motor"), 
    FILTRO_SISTEMA_HIDRAULICO ("Filtro do sistema hidráulico"), 
    GRAXA ("Graxa"), 
    OLEO_TRANSMISSAO ("Óleo da transmissão"),
    OLEO_MOTOR ("Óleo do motor"), 
    OLEO_HIDRAULICO ("Óleo hidráulico"), 
    OUTRAS_PECAS ("Outras peças"), 
    PNEU_DIANTEIRO ("Pneu dianteiro"), 
    PNEU_TRASEIRO ("Pneu traseiro");
    
	private String nome;   

	private TProduto(String nome) {
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}   
    
}
