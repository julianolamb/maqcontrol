/**
 * 
 */
package br.pro.x87.enums;

/**
 * @author jrlamb
 *
 */
public enum TDespesas {
	JUROS (12.00), ALOJAMENTO (0.50), SEGURO (1.40);

	private double value;
	
	private TDespesas(double value){
		this.value = value;
	}

	public double getValue() {
		return value;
	}	
}
