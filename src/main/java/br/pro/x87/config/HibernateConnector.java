/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.pro.x87.config;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author JulianoRodrigoLamb
 */
public class HibernateConnector {

	private static HibernateConnector me;
	private Configuration cfg;
	private SessionFactory sessionFactory;    
	private StringBuilder derbyPath;     

	public void setDerbyPath(StringBuilder derbyPath) {
		this.derbyPath = derbyPath;
	}

	private HibernateConnector() throws HibernateException {

		cfg = new Configuration();

		/**
		 * Connection Information..
		 */                  

		System.setProperty("derby.system.home", System.getProperty("user.dir") + "\\db");
		System.setProperty("derby.language.sequence.preallocator", "1");

		cfg.setProperty("hibernate.connection.driver_class", "org.apache.derby.jdbc.EmbeddedDriver");
		cfg.setProperty("hibernate.dialect", "org.hibernate.dialect.DerbyTenSevenDialect");
		cfg.setProperty("hibernate.connection.username", "maqcontrol");
		cfg.setProperty("hibernate.connection.password", "123");

		derbyPath = new StringBuilder("jdbc:derby:maqcontrol");
		derbyPath.append(";createDatabaseIfNotExist=true&useUnicode=yes&characterEncoding=UTF-8");
		derbyPath.append(";useTimezone=true");
		derbyPath.append(";serverTimezone=UTC");

		cfg.setProperty("hibernate.connection.url", derbyPath.toString());         		  

	    //cfg.setProperty("hibernate.hbm2ddl.auto", "create");        
		//cfg.setProperty("hibernate.show_sql", "true");               


		/**
		 * Mapping Resources..
		 */
		cfg.addAnnotatedClass(br.pro.x87.model.Marca.class);
		cfg.addAnnotatedClass(br.pro.x87.model.Maquina.class);
		cfg.addAnnotatedClass(br.pro.x87.model.CotacaoProduto.class);
		cfg.addAnnotatedClass(br.pro.x87.model.Cidade.class);
		cfg.addAnnotatedClass(br.pro.x87.model.Bairro.class);
		cfg.addAnnotatedClass(br.pro.x87.model.Endereco.class);
		cfg.addAnnotatedClass(br.pro.x87.model.Area.class);
		cfg.addAnnotatedClass(br.pro.x87.model.Empresa.class);
		cfg.addAnnotatedClass(br.pro.x87.model.Manutencao.class);
		cfg.addAnnotatedClass(br.pro.x87.model.Operacao.class);
		cfg.addAnnotatedClass(br.pro.x87.model.TipoOperacao.class);
		cfg.addAnnotatedClass(br.pro.x87.model.Funcao.class);
		cfg.addAnnotatedClass(br.pro.x87.model.VinculoFuncao.class);
		cfg.addAnnotatedClass(br.pro.x87.model.VinculoSalario.class);
		cfg.addAnnotatedClass(br.pro.x87.model.Funcionario.class);
		cfg.addAnnotatedClass(br.pro.x87.model.TipoServico.class);
		//cfg.addAnnotatedClass(br.pro.x87.model.OperacaoServico.class);
		cfg.addAnnotatedClass(br.pro.x87.model.Servicos.class);
		cfg.addAnnotatedClass(br.pro.x87.model.Produto.class);
		cfg.addAnnotatedClass(br.pro.x87.model.Vinculo.class);        

		sessionFactory = cfg.buildSessionFactory();
	}

	public static synchronized HibernateConnector getInstance() throws HibernateException {
		if (me == null) {
			me = new HibernateConnector();
		}

		return me;
	}

	public Session getSession() throws HibernateException {
		Session session = sessionFactory.openSession();
		if (!session.isConnected()) {
			this.reconnect();
		}
		return session;
	}

	private void reconnect() throws HibernateException {
		this.sessionFactory = cfg.buildSessionFactory();
	}
}
